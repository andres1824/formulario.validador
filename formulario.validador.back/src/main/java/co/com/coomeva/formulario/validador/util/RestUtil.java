package co.com.coomeva.formulario.validador.util;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.bouncycastle.util.encoders.EncoderException;

import okhttp3.OkHttpClient;

public class RestUtil {
	
	private static final Logger log = Logger.getLogger(RestUtil.class);
	
	private RestUtil() {
		
	}
	/**
	 * 
	 * @param url
	 * @param requestBody
	 * @param codApiUser
	 * @param codApiKey
	 * @return
	 * @throws EncoderException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static String sendPostProteccion(String url, String requestBody, String codApiUser, String codApiKey) throws EncoderException, IOException, InterruptedException {
		
		
		String output = "";
	    
		log.info("url " + url);
		log.info("requestBody " + requestBody);
		OkHttpClient client = new OkHttpClient().newBuilder()					
				.connectTimeout(160, TimeUnit.SECONDS)
				.readTimeout(160, TimeUnit.SECONDS)
				.writeTimeout(160, TimeUnit.SECONDS)			
	            .build();
		
		okhttp3.MediaType mediaType = okhttp3.MediaType.parse("application/json");
        okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, requestBody);
          okhttp3.Request request = new okhttp3.Request.Builder()
            .url(url)
            .method("POST", body)
            .addHeader("CooApiUser", codApiUser)
            .addHeader("CooApiKey", codApiKey)
            .addHeader("Content-Type", "application/json")
            .build();
          okhttp3.Response response = client.newCall(request).execute();              
          output = (response.body().string());
          log.info(output);
          
          return output;
	}
}
