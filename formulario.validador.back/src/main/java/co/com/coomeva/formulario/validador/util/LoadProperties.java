package co.com.coomeva.formulario.validador.util;

import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase Gen�rica para cargar las propuedades de los archivos .properties
 */
public class LoadProperties {

	private static LoadProperties instance;

	private static final Logger log = LoggerFactory.getLogger(LoadProperties.class);

	private LoadProperties() {
	}

	/**
	 * Patron singular
	 * 
	 * @return LoadProperties Instancia de la clase.
	 */
	public static LoadProperties getInstance() {
		if (instance == null) {
			instance = new LoadProperties();
		}
		return instance;
	}

	public String getProperty(String fileName, String property) {
		Properties pro = null;
		try {
			pro = new Properties();
			pro.load(this.getClass().getResource(fileName).openStream());
			return pro.getProperty(property);
		} catch (Exception ex) {
			log.error("Error leyendo la propiedad por: " + fileName, ex);
		}
		return null;
	}
}
