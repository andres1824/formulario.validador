package co.com.coomeva.formulario.validador.dao.impl.parametrizacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import co.com.coomeva.formulario.validador.dao.parametrizacion.RespuestaDao;
import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Respuesta;

/**
* Respuesta Dao Impl <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Repository
public class RespuestaDaoImpl implements RespuestaDao {
    
    @PersistenceContext
    private EntityManager em;
    public static final String FROM = "select a from Respuesta a ";
	
	
    public EntityManager getEntityManager() {
        return em;
    }


    @Override
    public List<Respuesta> findAllActive() throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.estado = :active ");

        builder.append("order by a.valor asc ");
        
        
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("active", (EstadoEnum.ACTIVE.getId()));
        return query.getResultList();
    }
    
    @Override
    public List<Respuesta> findByRespuesta(Respuesta respuesta) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.respuestaId = :id ");

        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("id", respuesta.getRespuestaId());
        return query.getResultList();
    }

    @Override
    public List<Respuesta> findByFiltro(Respuesta respuesta) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE 1=1 ");
        
        if(respuesta.getValor() != null && !respuesta.getValor().trim().isEmpty()) {
            builder.append("AND lower(a.valor) like lower(:valor) ");
        }




        if(respuesta.getEstado() != null && !respuesta.getEstado().trim().isEmpty()) {
            builder.append("AND a.estado = :estado ");
        }

        builder.append("order by a.respuestaId desc ");
        Query query = this.getEntityManager().createQuery(builder.toString());

        if(respuesta.getValor() != null && !respuesta.getValor().trim().isEmpty()) {
            query.setParameter("valor", "%" + respuesta.getValor() + "%");
        }




        if(respuesta.getEstado() != null && !respuesta.getEstado().trim().isEmpty()) {
            query.setParameter("estado", respuesta.getEstado());
        }

        return query.getResultList();
    }
    
    @Override
    public List<Respuesta> findByValor(Respuesta respuesta) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE 1=1 ");
        
        if(respuesta.getValor() != null && !respuesta.getValor().trim().isEmpty()) {
            builder.append("AND lower(a.valor) = lower(:valor) ");
        }
        
        builder.append("order by a.respuestaId desc ");
        Query query = this.getEntityManager().createQuery(builder.toString());

        if(respuesta.getValor() != null && !respuesta.getValor().trim().isEmpty()) {
            query.setParameter("valor", respuesta.getValor());
        }

        return query.getResultList();
    }

}