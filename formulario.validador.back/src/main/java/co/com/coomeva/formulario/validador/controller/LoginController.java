package co.com.coomeva.formulario.validador.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.NodeList;

import co.com.coomeva.formulario.validador.dto.LoginDTO;
import co.com.coomeva.formulario.validador.dto.LoginResponseDTO;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.UserInfoDTO;
import co.com.coomeva.formulario.validador.model.AccesoUsuario;
import co.com.coomeva.formulario.validador.service.parametrizacion.AccesoUsuarioService;
import co.com.coomeva.formulario.validador.util.ClientWS;
import co.com.coomeva.formulario.validador.util.ManagePropertiesLogic;
import co.com.coomeva.formulario.validador.util.ResponseServiceUtil;
import co.com.coomeva.formulario.validador.util.SoapService;

/**
 * Controlador que expone los servicios rest para los roles
 * 
 * @author aflr0108
 */
@RestController
@RequestMapping(value = "/api/login", produces = "application/json; charset=UTF-8")
@CrossOrigin(origins = "http://localhost:4200", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS })
public class LoginController {

	private static final Logger log = Logger.getLogger(LoginController.class);
	ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
	public static final String GENERAL_PROPERTIES = "general.properties";
	public static final String MENU_PROPERTIES = "menu.properties";
	
	@Autowired
	AccesoUsuarioService accesoUsuarioService;

	
	Map<String, String> responseBody = new HashMap<>();

	@PostMapping(value = "/access")
	public ResponseService login(@RequestBody LoginDTO loginDTO, HttpServletRequest request) {
		ResponseService responseService = new ResponseService();
		try {
				String app = managePropertiesLogic.getProperties(GENERAL_PROPERTIES, "appProfile");
				String url = managePropertiesLogic.getProperties(GENERAL_PROPERTIES, "urlProfile");
				
				SoapService service = new SoapService();
				NodeList hash = service.validarProfile(loginDTO.getLogin(), loginDTO.getPassword(), app, url);
				List<String> authorized = new ArrayList<>();
				ClientWS.getChildValueByName(hash, "authorized", authorized);
				
				

				List<String> roleList = new ArrayList<>();
				ClientWS.getChildValueByName(hash, "name", "applications", roleList);
				LoginResponseDTO loginResponse = new LoginResponseDTO();

				if (!authorized.isEmpty()) {
					String auth = authorized.get(0);

					if (auth.equals("true") && roleList != null && !roleList.isEmpty()) {
						StringBuilder userEmail = new StringBuilder();
						ClientWS.getChildValueByName(hash, "mail", userEmail);
						StringBuilder userName = new StringBuilder();
						ClientWS.getChildValueByName(hash, "name", "return", userName);
						StringBuilder userId = new StringBuilder();
						ClientWS.getChildValueByName(hash, "userId", "return", userId);
						UserInfoDTO userInfo = new UserInfoDTO();
						userInfo.setUserId(userId.toString());
						userInfo.setEmail(userEmail.toString());
						userInfo.setName(userName.toString());
						userInfo.setCompany(loginDTO.getCompany());
						userInfo.setCompanyId(loginDTO.getCompanyId());
						loginResponse.setUserInfo(userInfo);
						AccesoUsuario accesoUsuario = new AccesoUsuario();
						accesoUsuario.setUsuario(userName.toString());
						accesoUsuario.setFechaAcceso(new Date());
						if(request.getSession() != null) {
							accesoUsuario.setToken(request.getSession().getId());
						}
						
						accesoUsuarioService.create(accesoUsuario);
						ResponseServiceUtil.buildSuccessResponse(responseService, loginResponse);
						return responseService;
					} else if(auth.equals("false")){
						ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties("general.properties", "userInvalid"));
						return responseService;
					}
				}

				ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties("general.properties", "userSinPermisos"));
		} catch (Exception e) {
			String error = "Error en el servicio /login " + e.getMessage();
			log.error(error);
			ResponseServiceUtil.buildFailResponse(responseService, e);

		}
		return responseService;
	}

}
