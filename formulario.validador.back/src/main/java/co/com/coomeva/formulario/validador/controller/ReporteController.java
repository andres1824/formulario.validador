package co.com.coomeva.formulario.validador.controller;

import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.util.ResponseServiceUtil;

@RestController
public class ReporteController {
	
	
	private static final Logger log = Logger.getLogger(ReporteController.class);  
	
	@RequestMapping(value = "/xlsReporteWolvox", method = RequestMethod.POST, 
 			produces = MediaType.APPLICATION_JSON_VALUE)
 	public ResponseService getxlsReporte(){
 		ResponseService responseService = new ResponseService();
 		
 		try{
 					
 			 		
 		}catch(Exception e){
 			log.error("/xlsReporteWolvox failed", e);
 			ResponseServiceUtil.buildFailResponse(responseService, e);
		}
 		return responseService;
 	}
}
