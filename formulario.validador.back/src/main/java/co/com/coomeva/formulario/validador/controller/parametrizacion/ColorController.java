package co.com.coomeva.formulario.validador.controller.parametrizacion;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.formulario.validador.dto.ColorDTO;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.model.Color;
import co.com.coomeva.formulario.validador.service.parametrizacion.ColorService;
import co.com.coomeva.formulario.validador.util.AppUtil;
import co.com.coomeva.formulario.validador.util.ManagePropertiesLogic;
import co.com.coomeva.formulario.validador.util.ResponseServiceUtil;


/**
* Color Controller <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/

@RestController
@RequestMapping(value = "/api/color", produces = "application/json; charset=UTF-8")
@CrossOrigin(origins = "http://localhost:4200", methods = { RequestMethod.GET, RequestMethod.POST })
public class ColorController {

    @Autowired
    ColorService colorService;
    
    private static final Logger log = LoggerFactory.getLogger(ColorController.class);
    ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
    public static final String PROPERTIES = "color.properties";

    /**
     * Crea color <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param color
     * @return
     */
    @PostMapping(value = "/create")
    public ResponseService createColor(@RequestBody ColorDTO colorDTO) {
            ResponseService responseService = new ResponseService();
        try {
            Color color = new Color();
            color = (Color) AppUtil.convertOneToAnother(colorDTO, color);
            Color colorName = new Color();
            colorName.setNombre(color.getNombre());
            List<Color> listColorName = colorService.findByName(colorName);
            
            if(listColorName != null && !listColorName.isEmpty()) {
                ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                return responseService;
            }
            
            color.setFechaCreacion(new Date());
            colorService.create(color);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroCreado"));
        } catch (Exception e) {
            log.error("Error en el servicio /color " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }

    /**
     * Modifica color <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param color
     * @return
     */
    @PostMapping(value = "/update")
    public ResponseService updateColor(@RequestBody ColorDTO colorDTO) {
       ResponseService responseService = new ResponseService();
       try {
            Color color = new Color();
            color = (Color) AppUtil.convertOneToAnother(colorDTO, color);
            Color colorName = new Color();
            colorName.setNombre(color.getNombre());
            List<Color> listColorName = colorService.findByName(colorName);
            
            if(listColorName != null && !listColorName.isEmpty()) {
                boolean existName = false;
                for (Color color1 : listColorName) {
                    if (!color1.getColorId().equals(color.getColorId())) {
                        existName = true;
                    }
                }

                if (existName) {
                    ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                    return responseService;
                }                
            }
            
            color.setFechaModificacion(new Date());
            colorService.store(color);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEditado"));
        } catch (Exception e) {
            log.error("Error en el servicio /color/update " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Elimina color <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param color
     * @return
     */
    @PostMapping(value = "/delete")
    public ResponseService deleteColor(@RequestBody ColorDTO colorDTO) {
      ResponseService responseService = new ResponseService();
      try {
            Color color = new Color();
            color = (Color) AppUtil.convertOneToAnother(colorDTO, color);
            colorService.remove(color);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEliminado"));
        } catch (Exception e) {
            log.error("Error en el servicio /color/delete " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Consulta color <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     */
    @GetMapping(value = "/get/all")
    public ResponseService list() {
        ResponseService responseService = new ResponseService();
        try {     
            List<Color> colorList = colorService.findAllActive();
            ResponseServiceUtil.buildSuccessResponse(responseService, colorList);
        } catch (Exception e) {
            log.error("Error en el servicio /color/get/all " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Consulta color <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     */
    @PostMapping(value = "/find")
    public ResponseService find(@RequestBody ColorDTO colorDTO) {
       ResponseService responseService = new ResponseService();
       try {
            Color color = new Color();
            color = (Color) AppUtil.convertOneToAnother(colorDTO, color);
            List<Color> colorList = colorService.findByFiltro(color);
            ResponseServiceUtil.buildSuccessResponse(responseService, colorList);
        } catch (Exception e) {
            log.error("Error en el servicio /color/find " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
}
