package co.com.coomeva.formulario.validador.util;

import java.util.Base64;

public class Base64Util {
	
	private Base64Util() {
		
	}
	
	public static String descifrarBase64(String a){
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] decodedByteArray = decoder.decode(a);
        
        return new String(decodedByteArray); 
    }
}
