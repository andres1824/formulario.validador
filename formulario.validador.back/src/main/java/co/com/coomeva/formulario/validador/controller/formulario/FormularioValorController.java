package co.com.coomeva.formulario.validador.controller.formulario;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import co.com.coomeva.formulario.validador.dao.impl.formulario.FormularioListaRestrictivaDaoImpl;
import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.dto.FormularioAgrCampoDTO;
import co.com.coomeva.formulario.validador.dto.FormularioAgrCampoValueDTO;
import co.com.coomeva.formulario.validador.dto.FormularioDTO;
import co.com.coomeva.formulario.validador.dto.InspektorResponseDTO;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.Status;
import co.com.coomeva.formulario.validador.model.Agrupacion;
import co.com.coomeva.formulario.validador.model.AgrupacionCampo;
import co.com.coomeva.formulario.validador.model.Campo;
import co.com.coomeva.formulario.validador.model.Empresa;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioAgrCampo;
import co.com.coomeva.formulario.validador.model.FormularioEmpleado;
import co.com.coomeva.formulario.validador.model.FormularioListaRestrictiva;
import co.com.coomeva.formulario.validador.model.FormularioValor;
import co.com.coomeva.formulario.validador.model.InspektorDTO;
import co.com.coomeva.formulario.validador.service.formulario.FormularioEmpleadoService;
import co.com.coomeva.formulario.validador.service.formulario.FormularioListaRestrictivaService;
import co.com.coomeva.formulario.validador.service.formulario.FormularioService;
import co.com.coomeva.formulario.validador.service.formulario.FormularioValorService;
import co.com.coomeva.formulario.validador.service.parametrizacion.AgrupacionService;
import co.com.coomeva.formulario.validador.util.AppUtil;
import co.com.coomeva.formulario.validador.util.FileService;
import co.com.coomeva.formulario.validador.util.ManagePropertiesLogic;
import co.com.coomeva.formulario.validador.util.ResponseServiceUtil;


/**
* Formulario Controller <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/

@RestController
@RequestMapping(value = "/api/formularioValor", produces = "application/json; charset=UTF-8")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class FormularioValorController {

    @Autowired
    FormularioValorService formularioValorService;
    
    @Autowired
    FormularioListaRestrictivaService formularioListaRestrictivaService;
    
    @Autowired
    FormularioEmpleadoService formularioAsociadoService;
    
    @Autowired
	FileService fileService;
    
    private static final Logger log = LoggerFactory.getLogger(FormularioValorController.class);
    ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
    public static final String PROPERTIES = "formulario.properties";
    private final Long maxSizeFile = Long.valueOf(managePropertiesLogic.getProperties("general.properties", "maxSizeFile"));

    /**
     * Crea formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     */
    @PostMapping(value = "/create")
    public ResponseService createFormularioValor(@RequestBody ArrayList<FormularioAgrCampoValueDTO> formularioAgrCampo) {
            ResponseService responseService = new ResponseService();
        try {
        	
        	boolean isListaRestrictiva = false;
        	String message = "";
        	for (FormularioAgrCampoValueDTO formularioAgrCampoValueDTO : formularioAgrCampo) {
        		
         		Formulario formulario=new Formulario();
            	formulario.setFormularioId(formularioAgrCampoValueDTO.getFvFormulario().getFormularioId());
        		
        		boolean esAsociado=formularioValorService.validarAsociado(formularioAgrCampoValueDTO);
        		if(esAsociado) {
        			FormularioEmpleado formularioAsociado=new FormularioEmpleado();
        			formularioAsociado.setFechaCreacion(new Date());
        			formularioAsociado.setFvFormulario(formulario);
        			formularioAsociado.setNumeroIdentificacion(formularioAgrCampoValueDTO.getFvAgrupacionCampo().getFvCampo().getValue());
        			formularioAsociado.setUsuarioCreacion(formularioAgrCampoValueDTO.getFvFormulario().getUsuarioCreacion());
        			formularioAsociadoService.create(formularioAsociado);
        		}
        		
        		InspektorResponseDTO objInspektor = formularioListaRestrictivaService.saveListasRestrictivas(formularioAgrCampoValueDTO);
        		List<InspektorDTO> arrayInspektor = objInspektor.getInspektorList();
        		
        		if(objInspektor.getMessage() != null && !objInspektor.getMessage().equals("")) {
        			isListaRestrictiva = true;
        			message = objInspektor.getMessage();
        		}
        		
        		FormularioListaRestrictiva formularioListaRestrictiva;
        		if(arrayInspektor!=null) {
        			
        			for (InspektorDTO inspektor : arrayInspektor) {
        				
        				formularioListaRestrictiva=new FormularioListaRestrictiva();
        				formularioListaRestrictiva.setFechaCreacion(new Date());
        				formularioListaRestrictiva.setFvFormulario(formulario);
        				formularioListaRestrictiva.setNombreLista(inspektor.getNombreLista());
        				formularioListaRestrictiva.setNoSecuencia(inspektor.getNo().toString());
        				formularioListaRestrictiva.setNumeroIdentificacion(inspektor.getNumDoc());
        				formularioListaRestrictiva.setPrioridad(inspektor.getPrioridad().toString());
        				formularioListaRestrictiva.setTipoIdentificacion(inspektor.getTipoDoc());
        				formularioListaRestrictiva.setUsuarioCreacion(formularioAgrCampoValueDTO.getFvFormulario().getUsuarioCreacion());
        				formularioListaRestrictivaService.create(formularioListaRestrictiva);
        				
						
					}
        		}
       
            	Campo campo=new Campo();
            	campo.setCampoId(formularioAgrCampoValueDTO.getFvAgrupacionCampo().getFvCampo().getCampoId());
            	
                FormularioValor formularioValor = new FormularioValor();
                formularioValor.setUsuarioCreacion(formularioAgrCampoValueDTO.getFvFormulario().getUsuarioCreacion());
                formularioValor.setEstado("Activo");
                formularioValor.setFechaCreacion(new Date());
                formularioValor.setFvCampo(campo);
                formularioValor.setFvFormulario(formulario);
                formularioValor.setValor(formularioAgrCampoValueDTO.getFvAgrupacionCampo().getFvCampo().getValue());
                formularioValorService.create(formularioValor);
			}
        	String mensajeExito=managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroCreado");
        	
        	if(isListaRestrictiva) {
        			mensajeExito+="<br>"+message;
        	}
        	
            ResponseServiceUtil.buildSuccessResponse(responseService, mensajeExito, formularioAgrCampo);
        } catch (Exception e) {
            log.error("Error en el servicio /formulario " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }

    /**
     * Modifica formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     */
    @PostMapping(value = "/update")
    public ResponseService updateFormulario(@RequestBody FormularioDTO formularioDTO) {
       ResponseService responseService = new ResponseService();
       try {
            FormularioValor formulario = new FormularioValor();
            /*formulario = (Formulario) AppUtil.convertOneToAnother(formularioDTO, formulario);
            Formulario formularioName = new Formulario();
            formularioName.setNombre(formulario.getNombre());
            List<Formulario> listFormularioName = formularioService.findByName(formularioName);
            
            if(listFormularioName != null && !listFormularioName.isEmpty()) {
                boolean existName = false;
                for (Formulario formulario1 : listFormularioName) {
                    if (!formulario1.getFormularioId().equals(formulario.getFormularioId())) {
                        existName = true;
                    }
                }

                if (existName) {
                    ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                    return responseService;
                }                
            }
            
            formulario.setFechaModificacion(new Date());*/
            formulario = formularioValorService.store(formulario);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEditado"), formulario);
        } catch (Exception e) {
            log.error("Error en el servicio /formulario/update " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Elimina formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     */
    @PostMapping(value = "/delete")
    public ResponseService deleteFormulario(@RequestBody FormularioDTO formularioDTO) {
      ResponseService responseService = new ResponseService();
      try {
            FormularioValor formulario = new FormularioValor();
            //formulario = (Formulario) AppUtil.convertOneToAnother(formularioDTO, formulario);
            formularioValorService.remove(formulario);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEliminado"));
        } catch (Exception e) {
            log.error("Error en el servicio /formulario/delete " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Consulta formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     */
    @GetMapping(value = "/get/all")
    public ResponseService list() {
        ResponseService responseService = new ResponseService();
        try {     
            List<FormularioValor> formularioList = formularioValorService.findAllActive();
            ResponseServiceUtil.buildSuccessResponse(responseService, formularioList);
        } catch (Exception e) {
            log.error("Error en el servicio /formulario/get/all " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }


    /**
     * Consulta formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     */
    @PostMapping(value = "/find")
    public ResponseService find(@RequestBody FormularioDTO formularioDTO) {
       ResponseService responseService = new ResponseService();
       try {
    	   FormularioValor formulario = new FormularioValor();
            //formulario = (Formulario) AppUtil.convertOneToAnother(formularioDTO, formulario);
            List<FormularioValor> formularioList = formularioValorService.findByFiltro(formulario);
            ResponseServiceUtil.buildSuccessResponse(responseService, formularioList);
        } catch (Exception e) {
            log.error("Error en el servicio /formulario/find " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
  
   
}
