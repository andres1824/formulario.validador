package co.com.coomeva.formulario.validador.controller.formulario;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.dto.FormularioAgrCampoDTO;
import co.com.coomeva.formulario.validador.dto.FormularioAgrCampoValueDTO;
import co.com.coomeva.formulario.validador.dto.FormularioDTO;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.Status;
import co.com.coomeva.formulario.validador.model.Agrupacion;
import co.com.coomeva.formulario.validador.model.AgrupacionCampo;
import co.com.coomeva.formulario.validador.model.Campo;
import co.com.coomeva.formulario.validador.model.Empresa;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioAgrCampo;
import co.com.coomeva.formulario.validador.model.FormularioEmpleado;
import co.com.coomeva.formulario.validador.model.FormularioValor;
import co.com.coomeva.formulario.validador.service.formulario.FormularioEmpleadoService;
import co.com.coomeva.formulario.validador.service.formulario.FormularioService;
import co.com.coomeva.formulario.validador.service.formulario.FormularioValorService;
import co.com.coomeva.formulario.validador.service.parametrizacion.AgrupacionService;
import co.com.coomeva.formulario.validador.util.AppUtil;
import co.com.coomeva.formulario.validador.util.FileService;
import co.com.coomeva.formulario.validador.util.ManagePropertiesLogic;
import co.com.coomeva.formulario.validador.util.ResponseServiceUtil;


/**
* Formulario Controller <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/

@RestController
@RequestMapping(value = "/api/formularioEmpleado", produces = "application/json; charset=UTF-8")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class FormularioEmpleadoController {

    @Autowired
    FormularioEmpleadoService formularioValorService;
    
    
    private static final Logger log = LoggerFactory.getLogger(FormularioEmpleadoController.class);
    ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
    public static final String PROPERTIES = "formulario.properties";
    private final Long maxSizeFile = Long.valueOf(managePropertiesLogic.getProperties("general.properties", "maxSizeFile"));

    /**
     * Crea formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     */
    @PostMapping(value = "/create")
    public ResponseService createFormularioValor() {
            ResponseService responseService = new ResponseService();
        try {
        		FormularioEmpleado formularioAsociado=new FormularioEmpleado();
       
        		formularioAsociado.setFechaCreacion(new Date());
        		Formulario formulario=new Formulario();
        		formulario.setFormularioId(1L);
        		formularioAsociado.setFvFormulario(formulario);
        		formularioAsociado.setNumeroIdentificacion("1144075");
        		formularioAsociado.setUsuarioCreacion("oli");
        	 formularioValorService.create(formularioAsociado);
           // ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroCreado"), formularioAgrCampo);
        } catch (Exception e) {
            log.error("Error en el servicio /formulario " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }


    
   
}
