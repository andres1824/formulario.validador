package co.com.coomeva.formulario.validador.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the FV_FORMULARIO database table.
 * 
 */
@Entity
@Table(name="FV_FORMULARIO_EMPLEADO")
@NamedQuery(name="FormularioAsociado.findAll", query="SELECT f FROM FormularioEmpleado f")
public class FormularioEmpleado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @Basic(optional = false)
    @GeneratedValue(generator = "fv_formulario_empleado_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "fv_formulario_empleado_seq", sequenceName = "fv_formulario_empleado_seq",allocationSize=1)    
	@Column(name="FORMULARIO_EMPLEADO_ID")
	private Long formularioEmpleadoId;


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;

	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
	
	@Column(name="NUMERO_IDENTIFICACION")
	private String numeroIdentificacion;

	//bi-directional many-to-one association to TipoReporte
	@ManyToOne
	@JoinColumn(name="FORMULARIO_ID")
	private Formulario fvFormulario;
	public Long getFormularioEmpleadoId() {
		return formularioEmpleadoId;
	}
	public void setFormularioEmpleadoId(Long formularioAsociadoId) {
		this.formularioEmpleadoId = formularioAsociadoId;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}
	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}

	public Formulario getFvFormulario() {
		return fvFormulario;
	}
	public void setFvFormulario(Formulario fvFormulario) {
		this.fvFormulario = fvFormulario;
	}
	
	

	

}