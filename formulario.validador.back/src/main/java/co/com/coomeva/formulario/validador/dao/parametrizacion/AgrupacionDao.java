package co.com.coomeva.formulario.validador.dao.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Agrupacion;
import co.com.coomeva.formulario.validador.model.AgrupacionCampo;

/**
* Agrupacion Dao <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
public interface AgrupacionDao  {
    
    /**
     * Obtiene todos los registros activos de agrupacion <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     * @throws Exception 
     */
    public List<Agrupacion> findAllActive() throws BusinessException;

    /**
     * Obtiene todos los registros de agrupacion por su id <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param agrupacion
     * @return
     * @throws Exception 
     */
    public List<Agrupacion> findByAgrupacion(Agrupacion agrupacion) throws BusinessException;   

    /**
     * Obtiene todos los registros de agrupacion por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param agrupacion
     * @return
     * @throws Exception 
     */
    public List<Agrupacion> findByFiltro(Agrupacion agrupacion) throws BusinessException; 

    /**
    * Obtiene todos los registros de agrupacion por nombre <br>
    * Info. Creación: <br>
    * fecha 1 may 2021 <br>
    * @author GTC
    * @param agrupacion
    * @return
    * @throws Exception 
    */
    public List<Agrupacion> findByName(Agrupacion agrupacion) throws BusinessException; 
    
    /**
     * Obtiene todos los registros de agrupacion campo <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param agrupacion
     * @return
     * @throws BusinessException 
     */
    public List<Agrupacion> findByAgrupacionCampo(AgrupacionCampo agrupacion) throws BusinessException;
}
