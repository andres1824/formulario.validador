package co.com.coomeva.formulario.validador.util;

import java.util.List;

import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.Status;

public class ResponseServiceUtil {
	
	private ResponseServiceUtil() {
		
	}
	/**
	 * Builds a success response message.
	 * 
	 * @param responseService the response message object.
	 * @param data the data to encapsulate into the response message.
	 */
	public static void buildSuccessResponse(ResponseService responseService, List<?> data) {
		responseService.setStatus(Status.OK);
		responseService.setData(data);
	}
	
	/**
	 * Builds a fail response message.
	 * 
	 * @param responseService the response message object.
	 * @param data the data to encapsulate into the response message.
	 */
	public static void buildFailResponse(ResponseService responseService, String message) {
		responseService.setStatus(Status.FAILURE);
		responseService.setMessage(message);
		responseService.setMessageError(message);
		responseService.setTitle("Error");
	}
	
	/**
	 * Builds a success response message.
	 * 
	 * @param responseService the response message object.
	 * @param data the data to encapsulate into the response message.
	 */
	public static void buildSuccessResponse(ResponseService responseService, Object data) {
		responseService.setStatus(Status.OK);
		responseService.setData(data);
	}
	
	/**
	 * Builds a success response message.
	 * 
	 * @param responseService the response message object.
	 * @param data the data to encapsulate into the response message.
	 */
	public static void buildSuccessResponse(ResponseService responseService, String data) {
		responseService.setStatus(Status.OK);
		responseService.setMessage(data);
	}
	
	/**
	 * Builds a success response message.
	 * 
	 * @param responseService the response message object.
	 * @param data the data to encapsulate into the response message.
	 */
	public static void buildSuccessResponse(ResponseService responseService, String message, Object data) {
		responseService.setStatus(Status.OK);
		responseService.setMessage(message);
		responseService.setData(data);
	}
	
	/**
	 * Builds a fail response message.
	 * 
	 * @param responseService the response message object.
	 * @param ex the exception object.
	 */
	public static void buildFailResponse(ResponseService responseService, Exception ex) {
		responseService.setStatus(Status.FAILURE);
		responseService.setMessage("Error al consultar el servicio. Comun&iacute;quese con el administrador para m&aacute;s informaci&oacute;n.");
		responseService.setTitle("Error");
		responseService.setMessageError("Error al consultar el servicio: " + ex.getMessage());
	}

}