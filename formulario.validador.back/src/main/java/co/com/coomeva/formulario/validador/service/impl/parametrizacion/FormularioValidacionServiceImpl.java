package co.com.coomeva.formulario.validador.service.impl.parametrizacion;

import java.util.List;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.dao.parametrizacion.CampoDao;
import co.com.coomeva.formulario.validador.dao.parametrizacion.CampoRep;
import co.com.coomeva.formulario.validador.dao.parametrizacion.FormularioValidacionDao;
import co.com.coomeva.formulario.validador.dao.parametrizacion.FormularioValidacionRep;
import co.com.coomeva.formulario.validador.dao.parametrizacion.RespuestaCampoRep;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Campo;
import co.com.coomeva.formulario.validador.model.FormularioValidacion;
import co.com.coomeva.formulario.validador.model.RespuestaCampo;
import co.com.coomeva.formulario.validador.service.parametrizacion.CampoService;
import co.com.coomeva.formulario.validador.service.parametrizacion.FormularioValidacionService;

/**
* Campo Service Impl <br>
* Info. Creación: <br>
* fecha 25 abr 2021 <br>
* @author GTC
**/
@Service("formularioValidacionService")
@Transactional
public class FormularioValidacionServiceImpl implements FormularioValidacionService {

    @Autowired
    private FormularioValidacionDao formularioValidacionDao;

    @Autowired
    private FormularioValidacionRep formularioValidacionRep;

    @Override
    public FormularioValidacion create(FormularioValidacion formularioValidacion) throws BusinessException {
        return formularioValidacionRep.save(formularioValidacion);
    }

    @Override
    public FormularioValidacion store(FormularioValidacion formularioValidacion) throws BusinessException {
       return formularioValidacionRep.save(formularioValidacion);
    }

    @Override
    public void remove(FormularioValidacion formularioValidacion) throws BusinessException {
    	formularioValidacionRep.delete(formularioValidacion);
    }

    @Override
    public List<FormularioValidacion> findAll() throws BusinessException {
        return formularioValidacionRep.findAll();
    }

    @Override
    public List<FormularioValidacion> findAllActive() throws BusinessException {
        return formularioValidacionDao.findAllActive();
    }
    @Override
    public List<FormularioValidacion> findByFormularioValidacion(FormularioValidacion formularioValidacion) throws BusinessException {
        return formularioValidacionDao.findByFormularioValidacion(formularioValidacion);
    }

    @Override
    public List<FormularioValidacion> findByFiltro(FormularioValidacion formularioValidacion) throws BusinessException {
        return formularioValidacionDao.findByFiltro(formularioValidacion);
    }

	@Override
	public List<FormularioValidacion> findByIdFormulario(FormularioValidacion formularioValidacion)
			throws BusinessException {
		// TODO Auto-generated method stub
		return formularioValidacionDao.findByIdFormulario(formularioValidacion);
	}

    @Override
	public List<FormularioValidacion> findById(FormularioValidacion formularioValidacion)
			throws BusinessException {
	     return formularioValidacionDao.findById(formularioValidacion);
	}


}