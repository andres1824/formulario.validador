package co.com.coomeva.formulario.validador.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the FV_AGRUPACION database table.
 * 
 */
@Entity
@Table(name="FV_AGRUPACION")
@NamedQuery(name="Agrupacion.findAll", query="SELECT a FROM Agrupacion a")
public class Agrupacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @Basic(optional = false)
    @GeneratedValue(generator = "fv_agrupacion_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "fv_agrupacion_seq", sequenceName = "fv_agrupacion_seq",allocationSize=1)    
	@Column(name="AGRUPACION_ID")
	private Long agrupacionId;

	private String estado;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_MODIFICACION")
	private Date fechaModificacion;

	private String nombre;
	
	private String descripcion;

	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;

	@Column(name="USUARIO_MODIFICACION")
	private String usuarioModificacion;

	//bi-directional many-to-one association to AgrupacionCampo
	@OneToMany(mappedBy="fvAgrupacion")
	private List<AgrupacionCampo> fvAgrupacionCampos;

	public Agrupacion() {
	}

	public Long getAgrupacionId() {
		return this.agrupacionId;
	}

	public void setAgrupacionId(Long agrupacionId) {
		this.agrupacionId = agrupacionId;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}
	
	@JsonIgnore
	public List<AgrupacionCampo> getFvAgrupacionCampos() {
		return this.fvAgrupacionCampos;
	}

	public void setFvAgrupacionCampos(List<AgrupacionCampo> fvAgrupacionCampos) {
		this.fvAgrupacionCampos = fvAgrupacionCampos;
	}

	public AgrupacionCampo addFvAgrupacionCampo(AgrupacionCampo fvAgrupacionCampo) {
		getFvAgrupacionCampos().add(fvAgrupacionCampo);
		fvAgrupacionCampo.setFvAgrupacion(this);

		return fvAgrupacionCampo;
	}

	public AgrupacionCampo removeFvAgrupacionCampo(AgrupacionCampo fvAgrupacionCampo) {
		getFvAgrupacionCampos().remove(fvAgrupacionCampo);
		fvAgrupacionCampo.setFvAgrupacion(null);

		return fvAgrupacionCampo;
	}

}