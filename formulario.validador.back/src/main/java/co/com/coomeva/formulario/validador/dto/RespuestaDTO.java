package co.com.coomeva.formulario.validador.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the FV_RESPUESTA database table.
 * 
 */
public class RespuestaDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long respuestaId;

	private String valor;
	
	private String estado;
	private Date fechaCreacion;
	private Date fechaModificacion;
	private String usuarioCreacion;
	private String usuarioModificacion;

	private List<RespuestaCampoDTO> fvRespuestaCampos;

	public RespuestaDTO() {
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public void setRespuestaId(Long respuestaId) {
		this.respuestaId = respuestaId;
	}

	public Long getRespuestaId() {
		return this.respuestaId;
	}

	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	@JsonIgnore
	public List<RespuestaCampoDTO> getFvRespuestaCampos() {
		return this.fvRespuestaCampos;
	}

	public void setFvRespuestaCampos(List<RespuestaCampoDTO> fvRespuestaCampos) {
		this.fvRespuestaCampos = fvRespuestaCampos;
	}

	public RespuestaCampoDTO addFvRespuestaCampo(RespuestaCampoDTO fvRespuestaCampo) {
		getFvRespuestaCampos().add(fvRespuestaCampo);
		fvRespuestaCampo.setFvRespuesta(this);

		return fvRespuestaCampo;
	}

	public RespuestaCampoDTO removeFvRespuestaCampo(RespuestaCampoDTO fvRespuestaCampo) {
		getFvRespuestaCampos().remove(fvRespuestaCampo);
		fvRespuestaCampo.setFvRespuesta(null);

		return fvRespuestaCampo;
	}

}