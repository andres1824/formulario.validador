package co.com.coomeva.formulario.validador.dao.impl.parametrizacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import co.com.coomeva.formulario.validador.dao.parametrizacion.TipografiaDao;
import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Tipografia;

/**
* Tipografia Dao Impl <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Repository
public class TipografiaDaoImpl implements TipografiaDao {
    
    @PersistenceContext
    private EntityManager em;
    public static final String FROM = "select a from Tipografia a ";
	
	
    public EntityManager getEntityManager() {
        return em;
    }


    @Override
    public List<Tipografia> findAllActive() throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.estado = :active ");

        builder.append("order by a.nombre asc ");
        
        
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("active", (EstadoEnum.ACTIVE.getId()));
        return query.getResultList();
    }
    
    @Override
    public List<Tipografia> findByTipografia(Tipografia tipografia) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.tipografiaId = :id ");

        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("id", tipografia.getTipografiaId());
        return query.getResultList();
    }

    @Override
    public List<Tipografia> findByFiltro(Tipografia tipografia) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE 1=1 ");
        
        if(tipografia.getNombre() != null && !tipografia.getNombre().trim().isEmpty()) {
            builder.append("AND lower(a.nombre) like lower(:nombre) ");
        }




        if(tipografia.getEstado() != null && !tipografia.getEstado().trim().isEmpty()) {
            builder.append("AND a.estado = :estado ");
        }
        if(tipografia.getCodigoHtml() != null && !tipografia.getCodigoHtml().trim().isEmpty()) {
            builder.append("AND lower(a.codigoHtml) like lower(:codigoHtml) ");
        }



        builder.append("order by a.nombre asc ");
        Query query = this.getEntityManager().createQuery(builder.toString());

        if(tipografia.getNombre() != null && !tipografia.getNombre().trim().isEmpty()) {
            query.setParameter("nombre", "%" + tipografia.getNombre() + "%");
        }




        if(tipografia.getEstado() != null && !tipografia.getEstado().trim().isEmpty()) {
            query.setParameter("estado", tipografia.getEstado());
        }
        if(tipografia.getCodigoHtml() != null && !tipografia.getCodigoHtml().trim().isEmpty()) {
            query.setParameter("codigoHtml", "%" + tipografia.getCodigoHtml() + "%");
        }



        return query.getResultList();
    }

    @Override
    public List<Tipografia> findByName(Tipografia tipografia) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE lower(trim(a.nombre)) = lower(trim(:nombre)) ");
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("nombre", tipografia.getNombre().trim());
        return query.getResultList();
    }
}