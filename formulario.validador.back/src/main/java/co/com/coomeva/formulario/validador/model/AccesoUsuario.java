package co.com.coomeva.formulario.validador.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the FV_EMPRESA database table.
 * 
 */
@Entity
@Table(name="FV_ACCESO_USUARIO")
public class AccesoUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @Basic(optional = false)
    @GeneratedValue(generator = "fv_acceso_usuario_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "fv_acceso_usuario_seq", sequenceName = "fv_acceso_usuario_seq",allocationSize=1)    
	@Column(name="ACCESO_USUARIO_ID")
	private Long accesoUsuarioId;

	private String usuario;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_ACCESO")
	private Date fechaAcceso;


	private String token;

	public AccesoUsuario() {
	}

	public Long getAccesoUsuarioId() {
		return accesoUsuarioId;
	}

	public void setAccesoUsuarioId(Long accesoUsuarioId) {
		this.accesoUsuarioId = accesoUsuarioId;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Date getFechaAcceso() {
		return fechaAcceso;
	}

	public void setFechaAcceso(Date fechaAcceso) {
		this.fechaAcceso = fechaAcceso;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	

}