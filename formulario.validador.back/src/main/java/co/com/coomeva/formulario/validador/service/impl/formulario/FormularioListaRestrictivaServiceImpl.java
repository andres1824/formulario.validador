package co.com.coomeva.formulario.validador.service.impl.formulario;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.dao.formulario.FormularioAgrCampoRep;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioEmpleadoDao;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioEmpleadoRep;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioListaRestrictivaDao;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioListaRestrictivaRep;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioDao;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioRep;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioValorDao;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioValorRep;
import co.com.coomeva.formulario.validador.dto.FormularioAgrCampoValueDTO;
import co.com.coomeva.formulario.validador.dto.InspektorResponseDTO;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioAgrCampo;
import co.com.coomeva.formulario.validador.model.FormularioEmpleado;
import co.com.coomeva.formulario.validador.model.FormularioListaRestrictiva;
import co.com.coomeva.formulario.validador.model.FormularioValor;
import co.com.coomeva.formulario.validador.model.InspektorDTO;
import co.com.coomeva.formulario.validador.service.formulario.FormularioEmpleadoService;
import co.com.coomeva.formulario.validador.service.formulario.FormularioListaRestrictivaService;
import co.com.coomeva.formulario.validador.service.formulario.FormularioService;
import co.com.coomeva.formulario.validador.service.formulario.FormularioValorService;

/**
* Formulario Service Impl <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
@Service("formularioListaRestrictivaService")
@Transactional
public class FormularioListaRestrictivaServiceImpl implements FormularioListaRestrictivaService {

    @Autowired
    private FormularioListaRestrictivaDao formularioDao;

    @Autowired
    private FormularioListaRestrictivaRep formularioRep;


    
    @Override
    public FormularioListaRestrictiva create(FormularioListaRestrictiva formulario) throws BusinessException {
        return formularioRep.save(formulario);
    }

    @Override
    public FormularioListaRestrictiva store(FormularioListaRestrictiva formulario) throws BusinessException {
       return formularioRep.save(formulario);
    }

    @Override
    public void remove(FormularioListaRestrictiva formulario) throws BusinessException {
        formularioRep.delete(formulario);
    }

    @Override
    public List<FormularioListaRestrictiva> findAll() throws BusinessException {
        return formularioRep.findAll();
    }

    @Override
    public List<FormularioListaRestrictiva> findAllActive() throws BusinessException {
        return formularioDao.findAllActive();
    }
    @Override
    public List<FormularioListaRestrictiva> findByFormularioListaRestrictiva(FormularioListaRestrictiva formulario) throws BusinessException {
        return formularioDao.findByFormularioListaRestrictiva(formulario);
    }

    @Override
    public List<FormularioListaRestrictiva> findByFiltro(FormularioListaRestrictiva formulario) throws BusinessException {
        return formularioDao.findByFiltro(formulario);
    }

    @Override
    public InspektorResponseDTO saveListasRestrictivas(FormularioAgrCampoValueDTO formularioAgrCampoValueDTO) throws BusinessException, IOException {
    	
         return formularioDao.saveListasRestrictivas(formularioAgrCampoValueDTO);
    }


    
}