package co.com.coomeva.formulario.validador.controller.parametrizacion;

import java.util.Date;
import java.util.List;

import javax.ws.rs.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.formulario.validador.dto.CampoDTO;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.RespuestaCampoDTO;
import co.com.coomeva.formulario.validador.model.Campo;
import co.com.coomeva.formulario.validador.model.RespuestaCampo;
import co.com.coomeva.formulario.validador.service.parametrizacion.CampoService;
import co.com.coomeva.formulario.validador.util.AppUtil;
import co.com.coomeva.formulario.validador.util.ManagePropertiesLogic;
import co.com.coomeva.formulario.validador.util.ResponseServiceUtil;


/**
* Campo Controller <br>
* Info. Creación: <br>
* fecha 25 abr 2021 <br>
* @author GTC
**/

@RestController
@RequestMapping(value = "/api/campo", produces = "application/json; charset=UTF-8")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class CampoController {

    @Autowired
    CampoService campoService;
    
    private static final Logger log = LoggerFactory.getLogger(CampoController.class);
    ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
    public static final String PROPERTIES = "campo.properties";

    /**
     * Crea campo <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @return
     */
    @PostMapping(value = "/create")
    public ResponseService createCampo(@RequestBody CampoDTO campoDTO) {
            ResponseService responseService = new ResponseService();
        try {
            Campo campo = new Campo();
            campo = (Campo) AppUtil.convertOneToAnother(campoDTO, campo);
            Campo campoName = new Campo();
            campoName.setNombre(campo.getNombre());
            List<Campo> listCampoName = campoService.findByName(campoName);
            
            if(listCampoName != null && !listCampoName.isEmpty()) {
                ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                return responseService;
            }
            
            campo.setFechaCreacion(new Date());
            campo = campoService.create(campo);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroCreado"));
        } catch (Exception e) {
            log.error("Error en el servicio /campo " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }

    /**
     * Modifica campo <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @return
     */
    @PostMapping(value = "/update")
    public ResponseService updateCampo(@RequestBody CampoDTO campoDTO) {
       ResponseService responseService = new ResponseService();
       try {
            Campo campo = new Campo();
            campo = (Campo) AppUtil.convertOneToAnother(campoDTO, campo);
            Campo campoName = new Campo();
            campoName.setNombre(campo.getNombre());
            List<Campo> listCampoName = campoService.findByName(campoName);
            
            if(listCampoName != null && !listCampoName.isEmpty()) {
                boolean existName = false;
                for (Campo campo1 : listCampoName) {
                    if (!campo1.getCampoId().equals(campo.getCampoId())) {
                        existName = true;
                    }
                }

                if (existName) {
                    ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                    return responseService;
                }                
            }
            
            campo.setFechaModificacion(new Date());
            campoService.store(campo);
            
            if(campoDTO.getFvRespuesta() != null && campoDTO.getFvRespuesta().getRespuestaId() != null) {
            	RespuestaCampo res = new RespuestaCampo();
            	res.setFvRespuesta(campoDTO.getFvRespuesta());
            	res.setFvCampo(campo);
            	campoService.store(res);
            }
            
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEditado"));
        } catch (Exception e) {
            log.error("Error en el servicio /campo/update " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Elimina campo <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @return
     */
    @PostMapping(value = "/delete")
    public ResponseService deleteCampo(@RequestBody CampoDTO campoDTO) {
      ResponseService responseService = new ResponseService();
      try {
            Campo campo = new Campo();
            campo = (Campo) AppUtil.convertOneToAnother(campoDTO, campo);
            campoService.remove(campo);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEliminado"));
        } catch (Exception e) {
            log.error("Error en el servicio /campo/delete " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Consulta campo <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @return
     */
    @GetMapping(value = "/get/all")
    public ResponseService list() {
        ResponseService responseService = new ResponseService();
        try {     
            List<Campo> campoList = campoService.findAllActive();
            ResponseServiceUtil.buildSuccessResponse(responseService, campoList);
        } catch (Exception e) {
            log.error("Error en el servicio /campo/get/all " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Consulta campo <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @return
     */
    @GetMapping(value = "/get/dependiente/{campoId}")
    public ResponseService listDependiente(@PathVariable("campoId") Long campoId) {
        ResponseService responseService = new ResponseService();
        try {     
            List<Campo> campoList = campoService.findAllDependiente(campoId);
            ResponseServiceUtil.buildSuccessResponse(responseService, campoList);
        } catch (Exception e) {
            log.error("Error en el servicio /campo/dependiente/ " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    @GetMapping(value = "/get/tipoCampo/logica/{logica}")
    public ResponseService findByTipoCampoLogica(@PathVariable("logica") String logica) {
        ResponseService responseService = new ResponseService();
        try {     
            List<Campo> campoList = campoService.findByTipoCampoLogica(logica);
            ResponseServiceUtil.buildSuccessResponse(responseService, campoList);
        } catch (Exception e) {
            log.error("Error en el servicio /campo/dependiente/ " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Consulta campo <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @return
     */
    @PostMapping(value = "/find")
    public ResponseService find(@RequestBody CampoDTO campoDTO) {
       ResponseService responseService = new ResponseService();
       try {
            Campo campo = new Campo();
            campo = (Campo) AppUtil.convertOneToAnother(campoDTO, campo);
            List<Campo> campoList = campoService.findByFiltro(campo);
            ResponseServiceUtil.buildSuccessResponse(responseService, campoList);
        } catch (Exception e) {
            log.error("Error en el servicio /campo/find " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * 
     */
    @PostMapping(value = "/createRespuesta")
    public ResponseService createRespuesta(@RequestBody CampoDTO campoDTO) {
            ResponseService responseService = new ResponseService();
        try {
            
        	Campo campo = new Campo();
        	campo.setCampoId(campoDTO.getCampoId());
        	RespuestaCampo res = new RespuestaCampo();
        	
        	if(campoDTO.getCampoDependienteId() != null) {
        		Campo campoDependiente = new Campo();
            	campoDependiente.setCampoId(campoDTO.getCampoDependienteId());
            	res.setFvCampoDependiente(campoDependiente);
        	}
        	
        	res.setFvRespuesta(campoDTO.getFvRespuesta());
        	res.setRespuestaCorrecta(campoDTO.getRespuestaCorrecta());
        	res.setFvCampo(campo);
        	
        	List<RespuestaCampo> respuestaCampoList = campoService.findByRespuestaCampo(campo, 
        			campoDTO.getFvRespuesta().getRespuestaId());
        	
        	if(respuestaCampoList != null && !respuestaCampoList.isEmpty()) {
        		ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRespuestaExiste"));
                return responseService;
        	}
        	
        	campoService.create(res);
            
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroCreado"));
        } catch (Exception e) {
            log.error("Error en el servicio /respuesta " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }
    
    @PostMapping(value = "/deleteRespuesta")
    public ResponseService deleteRespuesta(@RequestBody RespuestaCampoDTO respuestaCampoDTO) {
            ResponseService responseService = new ResponseService();
        try {
        	RespuestaCampo respuestaCampo = new RespuestaCampo();
        	respuestaCampo = (RespuestaCampo) AppUtil.convertOneToAnother(respuestaCampoDTO, respuestaCampo);
        	campoService.remove(respuestaCampo);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEliminado"));
        } catch (Exception e) {
            log.error("Error en el servicio /respuesta " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }
    
    @PostMapping(value = "/respuestaCampo/get/all")
    public ResponseService getRespuestaCampo(@RequestBody CampoDTO campoDTO) {
       ResponseService responseService = new ResponseService();
       try {
            Campo campo = new Campo();
            campo = (Campo) AppUtil.convertOneToAnother(campoDTO, campo);
            List<RespuestaCampo> campoList = campoService.findByRespuestaCampo(campo, null);
            ResponseServiceUtil.buildSuccessResponse(responseService, campoList);
        } catch (Exception e) {
            log.error("Error en el servicio /campo/respuesta " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
}
