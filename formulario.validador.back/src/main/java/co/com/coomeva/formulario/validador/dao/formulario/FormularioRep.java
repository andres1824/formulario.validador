package co.com.coomeva.formulario.validador.dao.formulario;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.model.Formulario;

/**
* Formulario Dao <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
@Repository
@Transactional
public interface FormularioRep extends JpaRepository<Formulario, Long>{


}