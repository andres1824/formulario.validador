package co.com.coomeva.formulario.validador.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the FV_AGRUPACION_CAMPO database table.
 * 
 */
public class AgrupacionCampoValueDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long agrupacionCampoId;
	private Long orden;

	private AgrupacionDTO fvAgrupacion;
	private CampoValueDTO fvCampo;
	private List<FormularioAgrCampoDTO> fvFormularioAgrCampos;

	public AgrupacionCampoValueDTO() {
	}

	public Long getOrden() {
		return orden;
	}

	public void setOrden(Long orden) {
		this.orden = orden;
	}

	public Long getAgrupacionCampoId() {
		return this.agrupacionCampoId;
	}

	public void setAgrupacionCampoId(Long agrupacionCampoId) {
		this.agrupacionCampoId = agrupacionCampoId;
	}

	public AgrupacionDTO getFvAgrupacion() {
		return this.fvAgrupacion;
	}

	public void setFvAgrupacion(AgrupacionDTO fvAgrupacion) {
		this.fvAgrupacion = fvAgrupacion;
	}

	
	public CampoValueDTO getFvCampo() {
		return fvCampo;
	}

	public void setFvCampo(CampoValueDTO fvCampo) {
		this.fvCampo = fvCampo;
	}

	@JsonIgnore
	public List<FormularioAgrCampoDTO> getFvFormularioAgrCampos() {
		return this.fvFormularioAgrCampos;
	}

	public void setFvFormularioAgrCampos(List<FormularioAgrCampoDTO> fvFormularioAgrCampos) {
		this.fvFormularioAgrCampos = fvFormularioAgrCampos;
	}



}