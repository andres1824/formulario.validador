package co.com.coomeva.formulario.validador.controller.parametrizacion;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.formulario.validador.dto.EmpresaDTO;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.model.Empresa;
import co.com.coomeva.formulario.validador.service.parametrizacion.EmpresaService;
import co.com.coomeva.formulario.validador.util.AppUtil;
import co.com.coomeva.formulario.validador.util.ManagePropertiesLogic;
import co.com.coomeva.formulario.validador.util.ResponseServiceUtil;


/**
* Empresa Controller <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/

@RestController
@RequestMapping(value = "/api/empresa", produces = "application/json; charset=UTF-8")
@CrossOrigin(origins = "http://localhost:4200", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS })
public class EmpresaController {

    @Autowired
    EmpresaService empresaService;
    
    private static final Logger log = LoggerFactory.getLogger(EmpresaController.class);
    ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
    public static final String PROPERTIES = "empresa.properties";

    /**
     * Crea empresa <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param empresa
     * @return
     */
    @PostMapping(value = "/create")
    public ResponseService createEmpresa(@RequestBody EmpresaDTO empresaDTO) {
            ResponseService responseService = new ResponseService();
        try {
            Empresa empresa = new Empresa();
            empresa = (Empresa) AppUtil.convertOneToAnother(empresaDTO, empresa);
            Empresa empresaName = new Empresa();
            empresaName.setNombre(empresa.getNombre());
            List<Empresa> listEmpresaName = empresaService.findByName(empresaName);
            
            if(listEmpresaName != null && !listEmpresaName.isEmpty()) {
                ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                return responseService;
            }
            
            empresa.setFechaCreacion(new Date());
            empresaService.create(empresa);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroCreado"));
        } catch (Exception e) {
            log.error("Error en el servicio /empresa " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }

    /**
     * Modifica empresa <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param empresa
     * @return
     */
    @PostMapping(value = "/update")
    public ResponseService updateEmpresa(@RequestBody EmpresaDTO empresaDTO) {
       ResponseService responseService = new ResponseService();
       try {
            Empresa empresa = new Empresa();
            empresa = (Empresa) AppUtil.convertOneToAnother(empresaDTO, empresa);
            Empresa empresaName = new Empresa();
            empresaName.setNombre(empresa.getNombre());
            List<Empresa> listEmpresaName = empresaService.findByName(empresaName);
            
            if(listEmpresaName != null && !listEmpresaName.isEmpty()) {
                boolean existName = false;
                for (Empresa empresa1 : listEmpresaName) {
                    if (!empresa1.getEmpresaId().equals(empresa.getEmpresaId())) {
                        existName = true;
                    }
                }

                if (existName) {
                    ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                    return responseService;
                }                
            }
            
            empresa.setFechaModificacion(new Date());
            empresaService.store(empresa);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEditado"));
        } catch (Exception e) {
            log.error("Error en el servicio /empresa/update " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Elimina empresa <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param empresa
     * @return
     */
    @PostMapping(value = "/delete")
    public ResponseService deleteEmpresa(@RequestBody EmpresaDTO empresaDTO) {
      ResponseService responseService = new ResponseService();
      try {
            Empresa empresa = new Empresa();
            empresa = (Empresa) AppUtil.convertOneToAnother(empresaDTO, empresa);
            empresaService.remove(empresa);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEliminado"));
        } catch (Exception e) {
            log.error("Error en el servicio /empresa/delete " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Consulta empresa <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     */
    @GetMapping(value = "/get/all")
    public ResponseService list() {
        ResponseService responseService = new ResponseService();
        try {     
            List<Empresa> empresaList = empresaService.findAllActive();
            ResponseServiceUtil.buildSuccessResponse(responseService, empresaList);
        } catch (Exception e) {
            log.error("Error en el servicio /empresa/get/all " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Consulta empresa <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     */
    @PostMapping(value = "/find")
    public ResponseService find(@RequestBody EmpresaDTO empresaDTO) {
       ResponseService responseService = new ResponseService();
       try {
            Empresa empresa = new Empresa();
            empresa = (Empresa) AppUtil.convertOneToAnother(empresaDTO, empresa);
            List<Empresa> empresaList = empresaService.findByFiltro(empresa);
            ResponseServiceUtil.buildSuccessResponse(responseService, empresaList);
        } catch (Exception e) {
            log.error("Error en el servicio /empresa/find " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
}
