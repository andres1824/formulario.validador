package co.com.coomeva.formulario.validador.controller.parametrizacion;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.RespuestaDTO;
import co.com.coomeva.formulario.validador.model.Respuesta;
import co.com.coomeva.formulario.validador.service.parametrizacion.RespuestaService;
import co.com.coomeva.formulario.validador.util.AppUtil;
import co.com.coomeva.formulario.validador.util.ManagePropertiesLogic;
import co.com.coomeva.formulario.validador.util.ResponseServiceUtil;


/**
* Respuesta Controller <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/

@RestController
@RequestMapping(value = "/api/respuesta", produces = "application/json; charset=UTF-8")
@CrossOrigin(origins = "http://localhost:4200", methods = { RequestMethod.GET, RequestMethod.POST })
public class RespuestaController {

    @Autowired
    RespuestaService respuestaService;
    
    private static final Logger log = LoggerFactory.getLogger(RespuestaController.class);
    ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
    public static final String PROPERTIES = "respuesta.properties";

    /**
     * Crea respuesta <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param respuesta
     * @return
     */
    @PostMapping(value = "/create")
    public ResponseService createRespuesta(@RequestBody RespuestaDTO respuestaDTO) {
            ResponseService responseService = new ResponseService();
        try {
            Respuesta respuesta = new Respuesta();
            respuesta = (Respuesta) AppUtil.convertOneToAnother(respuestaDTO, respuesta);
            Respuesta respuestaName = new Respuesta();
            respuestaName.setValor(respuesta.getValor());
            List<Respuesta> listRespuestaName = respuestaService.findByValor(respuestaName);
            
            if(listRespuestaName != null && !listRespuestaName.isEmpty()) {
                ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                return responseService;
            }
            
            respuesta.setFechaCreacion(new Date());
            respuestaService.create(respuesta);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroCreado"));
        } catch (Exception e) {
            log.error("Error en el servicio /respuesta " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }

    /**
     * Modifica respuesta <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param respuesta
     * @return
     */
    @PostMapping(value = "/update")
    public ResponseService updateRespuesta(@RequestBody RespuestaDTO respuestaDTO) {
       ResponseService responseService = new ResponseService();
       try {
            Respuesta respuesta = new Respuesta();
            respuesta = (Respuesta) AppUtil.convertOneToAnother(respuestaDTO, respuesta);
            Respuesta respuestaName = new Respuesta();
            respuestaName.setValor(respuesta.getValor());
            List<Respuesta> listRespuestaName = respuestaService.findByValor(respuestaName);
            
            if(listRespuestaName != null && !listRespuestaName.isEmpty()) {
                boolean existName = false;
                for (Respuesta respuesta1 : listRespuestaName) {
                    if (!respuesta1.getRespuestaId().equals(respuesta.getRespuestaId())) {
                        existName = true;
                    }
                }

                if (existName) {
                    ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                    return responseService;
                }                
            }
            
            respuesta.setFechaModificacion(new Date());
            respuestaService.store(respuesta);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEditado"));
        } catch (Exception e) {
            log.error("Error en el servicio /respuesta/update " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Elimina respuesta <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param respuesta
     * @return
     */
    @PostMapping(value = "/delete")
    public ResponseService deleteRespuesta(@RequestBody RespuestaDTO respuestaDTO) {
      ResponseService responseService = new ResponseService();
      try {
            Respuesta respuesta = new Respuesta();
            respuesta = (Respuesta) AppUtil.convertOneToAnother(respuestaDTO, respuesta);
            respuestaService.remove(respuesta);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEliminado"));
        } catch (Exception e) {
            log.error("Error en el servicio /respuesta/delete " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Consulta respuesta <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     */
    @GetMapping(value = "/get/all")
    public ResponseService list() {
        ResponseService responseService = new ResponseService();
        try {     
            List<Respuesta> respuestaList = respuestaService.findAllActive();
            ResponseServiceUtil.buildSuccessResponse(responseService, respuestaList);
        } catch (Exception e) {
            log.error("Error en el servicio /respuesta/get/all " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Consulta respuesta <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     */
    @PostMapping(value = "/find")
    public ResponseService find(@RequestBody RespuestaDTO respuestaDTO) {
       ResponseService responseService = new ResponseService();
       try {
            Respuesta respuesta = new Respuesta();
            respuesta = (Respuesta) AppUtil.convertOneToAnother(respuestaDTO, respuesta);
            List<Respuesta> respuestaList = respuestaService.findByFiltro(respuesta);
            ResponseServiceUtil.buildSuccessResponse(responseService, respuestaList);
        } catch (Exception e) {
            log.error("Error en el servicio /respuesta/find " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
}
