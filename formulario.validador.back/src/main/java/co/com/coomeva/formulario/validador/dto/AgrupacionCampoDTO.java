package co.com.coomeva.formulario.validador.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the FV_AGRUPACION_CAMPO database table.
 * 
 */
public class AgrupacionCampoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long agrupacionCampoId;
	private Long orden;

	private AgrupacionDTO fvAgrupacion;
	private CampoDTO fvCampo;
	private List<FormularioAgrCampoDTO> fvFormularioAgrCampos;

	public AgrupacionCampoDTO() {
	}

	public Long getOrden() {
		return orden;
	}

	public void setOrden(Long orden) {
		this.orden = orden;
	}

	public Long getAgrupacionCampoId() {
		return this.agrupacionCampoId;
	}

	public void setAgrupacionCampoId(Long agrupacionCampoId) {
		this.agrupacionCampoId = agrupacionCampoId;
	}

	public AgrupacionDTO getFvAgrupacion() {
		return this.fvAgrupacion;
	}

	public void setFvAgrupacion(AgrupacionDTO fvAgrupacion) {
		this.fvAgrupacion = fvAgrupacion;
	}

	public CampoDTO getFvCampo() {
		return this.fvCampo;
	}

	public void setFvCampo(CampoDTO fvCampo) {
		this.fvCampo = fvCampo;
	}
	
	@JsonIgnore
	public List<FormularioAgrCampoDTO> getFvFormularioAgrCampos() {
		return this.fvFormularioAgrCampos;
	}

	public void setFvFormularioAgrCampos(List<FormularioAgrCampoDTO> fvFormularioAgrCampos) {
		this.fvFormularioAgrCampos = fvFormularioAgrCampos;
	}

	public FormularioAgrCampoDTO addFvFormularioAgrCampo(FormularioAgrCampoDTO fvFormularioAgrCampo) {
		getFvFormularioAgrCampos().add(fvFormularioAgrCampo);
		fvFormularioAgrCampo.setFvAgrupacionCampo(this);

		return fvFormularioAgrCampo;
	}

	public FormularioAgrCampoDTO removeFvFormularioAgrCampo(FormularioAgrCampoDTO fvFormularioAgrCampo) {
		getFvFormularioAgrCampos().remove(fvFormularioAgrCampo);
		fvFormularioAgrCampo.setFvAgrupacionCampo(null);

		return fvFormularioAgrCampo;
	}

}