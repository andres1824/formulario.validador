package co.com.coomeva.formulario.validador.dao.impl.parametrizacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import co.com.coomeva.formulario.validador.dao.parametrizacion.ColorDao;
import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Color;

/**
* Color Dao Impl <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Repository
public class ColorDaoImpl implements ColorDao {
    
    @PersistenceContext
    private EntityManager em;
    public static final String FROM = "select a from Color a ";
	
	
    public EntityManager getEntityManager() {
        return em;
    }


    @Override
    public List<Color> findAllActive() throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.estado = :active ");

        builder.append("order by a.nombre asc ");
        
        
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("active", (EstadoEnum.ACTIVE.getId()));
        return query.getResultList();
    }
    
    @Override
    public List<Color> findByColor(Color color) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.colorId = :id ");

        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("id", color.getColorId());
        return query.getResultList();
    }

    @Override
    public List<Color> findByFiltro(Color color) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE 1=1 ");
        
        if(color.getNombre() != null && !color.getNombre().trim().isEmpty()) {
            builder.append("AND lower(a.nombre) like lower(:nombre) ");
        }




        if(color.getEstado() != null && !color.getEstado().trim().isEmpty()) {
            builder.append("AND a.estado = :estado ");
        }
        if(color.getCodigoHtml() != null && !color.getCodigoHtml().trim().isEmpty()) {
            builder.append("AND lower(a.codigoHtml) like lower(:codigoHtml) ");
        }



        builder.append("order by a.nombre asc ");
        Query query = this.getEntityManager().createQuery(builder.toString());

        if(color.getNombre() != null && !color.getNombre().trim().isEmpty()) {
            query.setParameter("nombre", "%" + color.getNombre() + "%");
        }




        if(color.getEstado() != null && !color.getEstado().trim().isEmpty()) {
            query.setParameter("estado", color.getEstado());
        }
        if(color.getCodigoHtml() != null && !color.getCodigoHtml().trim().isEmpty()) {
            query.setParameter("codigoHtml", "%" + color.getCodigoHtml() + "%");
        }



        return query.getResultList();
    }

    @Override
    public List<Color> findByName(Color color) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE lower(trim(a.nombre)) = lower(trim(:nombre)) ");
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("nombre", color.getNombre().trim());
        return query.getResultList();
    }
}