package co.com.coomeva.formulario.validador.service.impl.parametrizacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.dao.parametrizacion.AccesoUsuarioRep;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.AccesoUsuario;
import co.com.coomeva.formulario.validador.service.parametrizacion.AccesoUsuarioService;

/**
* Empresa Service Impl <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Service("accesoUsuarioService")
@Transactional
public class AccesoUsuarioServiceImpl implements AccesoUsuarioService {

    @Autowired
	@Qualifier("accesoUsuarioRep")
    private AccesoUsuarioRep accesoUsuarioRep;

    @Override
    public AccesoUsuario create(AccesoUsuario accesoUsuario) throws BusinessException {
        return accesoUsuarioRep.save(accesoUsuario);
    }

    @Override
    public AccesoUsuario store(AccesoUsuario accesoUsuario) throws BusinessException {
       return accesoUsuarioRep.save(accesoUsuario);
    }

    @Override
    public void remove(AccesoUsuario accesoUsuario) throws BusinessException {
    	accesoUsuarioRep.delete(accesoUsuario);
    }
}