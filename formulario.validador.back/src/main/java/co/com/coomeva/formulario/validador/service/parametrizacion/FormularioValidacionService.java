package co.com.coomeva.formulario.validador.service.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Campo;
import co.com.coomeva.formulario.validador.model.FormularioValidacion;
import co.com.coomeva.formulario.validador.model.RespuestaCampo;

/**
* FormularioValidacion Service <br>
* Info. Creación: <br>
* fecha 25 abr 2021 <br>
* @author GTC
**/
public interface FormularioValidacionService {
    

    /**
     * Crea formularioValidacion <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param formularioValidacion
     * @return 
     * @throws BusinessException 
     */
    public FormularioValidacion create(FormularioValidacion formularioValidacion) throws BusinessException;
    /**
     * Modifica formularioValidacion <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @return 
     * @throws BusinessException 
     */
    public FormularioValidacion store(FormularioValidacion formularioValidacion) throws BusinessException;
    /**
     * Elimina formularioValidacion<br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @throws BusinessException 
     */
    public void remove(FormularioValidacion formularioValidacion) throws BusinessException;
    /**
     * Obtiene todos los registros de formularioValidacion <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<FormularioValidacion> findAll() throws BusinessException;
    
    /**
     * Obtiene todos los registros activos de formularioValidacion <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<FormularioValidacion> findAllActive() throws BusinessException;
    /**
     * Obtiene todos los registros de formularioValidacion <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param formularioValidacion
     * @return
     * @throws BusinessException 
     */
    public List<FormularioValidacion> findByFormularioValidacion(FormularioValidacion formularioValidacion) throws BusinessException;   

    /**
     * Obtiene todos los registros de campo por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param formularioValidacion
     * @return
     * @throws BusinessException 
     */
    public List<FormularioValidacion> findByFiltro(FormularioValidacion formularioValidacion) throws BusinessException; 
    
    public List<FormularioValidacion> findByIdFormulario(FormularioValidacion formularioValidacion) throws BusinessException; 
    
    public List<FormularioValidacion> findById(FormularioValidacion formularioValidacion) throws BusinessException;


       
}