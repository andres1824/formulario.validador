package co.com.coomeva.formulario.validador.dao.parametrizacion;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.model.Parametro;

/**
* Parametro Dao <br>
* Info. Creación: <br>
* fecha 23 abr 2021 <br>
* @author GTC
**/
@Repository
@Transactional
public interface ParametroRep extends JpaRepository<Parametro, Long>{


}