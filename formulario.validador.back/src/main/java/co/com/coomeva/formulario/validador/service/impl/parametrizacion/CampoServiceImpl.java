package co.com.coomeva.formulario.validador.service.impl.parametrizacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.dao.parametrizacion.CampoDao;
import co.com.coomeva.formulario.validador.dao.parametrizacion.CampoRep;
import co.com.coomeva.formulario.validador.dao.parametrizacion.RespuestaCampoRep;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Campo;
import co.com.coomeva.formulario.validador.model.RespuestaCampo;
import co.com.coomeva.formulario.validador.model.TipoCampo;
import co.com.coomeva.formulario.validador.service.parametrizacion.CampoService;

/**
* Campo Service Impl <br>
* Info. Creación: <br>
* fecha 25 abr 2021 <br>
* @author GTC
**/
@Service("campoService")
@Transactional
public class CampoServiceImpl implements CampoService {

    @Autowired
    private CampoDao campoDao;

    @Autowired
    private CampoRep campoRep;
    
    @Autowired
    private RespuestaCampoRep respuestaCampoRep;

    @Override
    public Campo create(Campo campo) throws BusinessException {
        return campoRep.save(campo);
    }

    @Override
    public Campo store(Campo campo) throws BusinessException {
       return campoRep.save(campo);
    }

    @Override
    public void remove(Campo campo) throws BusinessException {
        campoRep.delete(campo);
    }
    
    @Override
    public RespuestaCampo create(RespuestaCampo campo) throws BusinessException {
        return respuestaCampoRep.save(campo);
    }

    @Override
    public RespuestaCampo store(RespuestaCampo campo) throws BusinessException {
       return respuestaCampoRep.save(campo);
    }

    @Override
    public void remove(RespuestaCampo campo) throws BusinessException {
    	respuestaCampoRep.delete(campo);
    }

    @Override
    public List<Campo> findAll() throws BusinessException {
        return campoRep.findAll();
    }

    @Override
    public List<Campo> findAllActive() throws BusinessException {
        return campoDao.findAllActive();
    }
    @Override
    public List<Campo> findByCampo(Campo campo) throws BusinessException {
        return campoDao.findByCampo(campo);
    }

    @Override
    public List<Campo> findByFiltro(Campo campo) throws BusinessException {
        return campoDao.findByFiltro(campo);
    }

    @Override
    public List<Campo> findByName(Campo campo) throws BusinessException {
        return campoDao.findByName(campo);
    }

	@Override
	public List<RespuestaCampo> findByRespuestaCampo(Campo campo, Long respuestaId) throws BusinessException {
		if(respuestaId != null) {
			return respuestaCampoRep.find(campo.getCampoId(), respuestaId);
		} else {
			return respuestaCampoRep.find(campo.getCampoId());
		}
	}

	@Override
	public List<Campo> findAllDependiente(Long campoId) throws BusinessException {
		return campoDao.findAllDependiente(campoId);
	}

	@Override
	public List<Campo> findByTipoCampoLogica(String logica) throws BusinessException {
		return campoRep.findByLogicaTipoCampo(logica);
	}
}