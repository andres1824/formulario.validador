package co.com.coomeva.formulario.validador.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the FV_FORMULARIO database table.
 * 
 */
@Entity
@Table(name="FV_FORMULARIO")
@NamedQuery(name="Formulario.findAll", query="SELECT f FROM Formulario f")
public class Formulario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @Basic(optional = false)
    @GeneratedValue(generator = "fv_formulario_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "fv_formulario_seq", sequenceName = "fv_formulario_seq",allocationSize=1)    
	@Column(name="FORMULARIO_ID")
	private Long formularioId;

	private String banner;

	private String codigo;
	
	@Column(name="NOMBRE_ARCHIVO_REPORTE")
	private String nombreArchivoReporte;


	private String descripcion;

	private String estado;
	
	@Column(name="NOMBRE_ADJUNTO")
	private String nombreAdjunto;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_MODIFICACION")
	private Date fechaModificacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_VIGENCIA")
	private Date fechaVigencia;

	private String frecuencia;

	@Column(name="GENERA_ASOCIADOS")
	private String generaAsociados;

	@Column(name="GENERA_NO_ASOCIADOS")
	private String generaNoAsociados;

	@Column(name="GENERA_REPORTE_AUTO")
	private String generaReporteAuto;

	private String horario;

	@Column(name="IND_CSS_EXTERNO")
	private String indCssExterno;

	private String nombre;

	@Column(name="NOMBRE_ARCHIVO")
	private String nombreArchivo;

	@Column(name="RUTA_REPORTE")
	private String rutaReporte;

	@Column(name="URL_CSS_EXTERNO")
	private String urlCssExterno;

	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;

	@Column(name="USUARIO_MODIFICACION")
	private String usuarioModificacion;

	//bi-directional many-to-one association to Color
	@ManyToOne
	@JoinColumn(name="COLOR_ID")
	private Color fvColor;

	//bi-directional many-to-one association to Empresa
	@ManyToOne
	@JoinColumn(name="EMPRESA_ID")
	private Empresa fvEmpresa;

	//bi-directional many-to-one association to Tipografia
	@ManyToOne
	@JoinColumn(name="TIPOGRAFIA_ID")
	private Tipografia fvTipografia;

	//bi-directional many-to-one association to TipoReporte
	@ManyToOne
	@JoinColumn(name="TIPO_REPORTE_ID")
	private TipoReporte fvTipoReporte;

	//bi-directional many-to-one association to FormularioAgrCampo
	@OneToMany(mappedBy="fvFormulario")
	private List<FormularioAgrCampo> fvFormularioAgrCampos;

	public Formulario() {
	}

	public Long getFormularioId() {
		return this.formularioId;
	}

	public void setFormularioId(Long formularioId) {
		this.formularioId = formularioId;
	}

	public String getNombreAdjunto() {
		return nombreAdjunto;
	}

	public void setNombreAdjunto(String nombreAdjunto) {
		this.nombreAdjunto = nombreAdjunto;
	}

	public String getBanner() {
		return this.banner;
	}

	public void setBanner(String banner) {
		this.banner = banner;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Date getFechaVigencia() {
		return this.fechaVigencia;
	}

	public void setFechaVigencia(Date fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

	public String getFrecuencia() {
		return this.frecuencia;
	}

	public void setFrecuencia(String frecuencia) {
		this.frecuencia = frecuencia;
	}

	public String getGeneraAsociados() {
		return this.generaAsociados;
	}

	public void setGeneraAsociados(String generaAsociados) {
		this.generaAsociados = generaAsociados;
	}

	public String getGeneraNoAsociados() {
		return this.generaNoAsociados;
	}

	public void setGeneraNoAsociados(String generaNoAsociados) {
		this.generaNoAsociados = generaNoAsociados;
	}

	public String getGeneraReporteAuto() {
		return this.generaReporteAuto;
	}

	public void setGeneraReporteAuto(String generaReporteAuto) {
		this.generaReporteAuto = generaReporteAuto;
	}

	public String getHorario() {
		return this.horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public String getIndCssExterno() {
		return this.indCssExterno;
	}

	public void setIndCssExterno(String indCssExterno) {
		this.indCssExterno = indCssExterno;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombreArchivo() {
		return this.nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getRutaReporte() {
		return this.rutaReporte;
	}

	public void setRutaReporte(String rutaReporte) {
		this.rutaReporte = rutaReporte;
	}

	public String getUrlCssExterno() {
		return this.urlCssExterno;
	}

	public void setUrlCssExterno(String urlCssExterno) {
		this.urlCssExterno = urlCssExterno;
	}

	public String getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public Color getFvColor() {
		return this.fvColor;
	}

	public void setFvColor(Color fvColor) {
		this.fvColor = fvColor;
	}

	public Empresa getFvEmpresa() {
		return this.fvEmpresa;
	}

	public void setFvEmpresa(Empresa fvEmpresa) {
		this.fvEmpresa = fvEmpresa;
	}

	public Tipografia getFvTipografia() {
		return this.fvTipografia;
	}

	public void setFvTipografia(Tipografia fvTipografia) {
		this.fvTipografia = fvTipografia;
	}

	public TipoReporte getFvTipoReporte() {
		return this.fvTipoReporte;
	}

	public void setFvTipoReporte(TipoReporte fvTipoReporte) {
		this.fvTipoReporte = fvTipoReporte;
	}
	
	public String getNombreArchivoReporte() {
		return nombreArchivoReporte;
	}

	public void setNombreArchivoReporte(String nombreArchivoReporte) {
		this.nombreArchivoReporte = nombreArchivoReporte;
	}

	@JsonIgnore
	public List<FormularioAgrCampo> getFvFormularioAgrCampos() {
		return this.fvFormularioAgrCampos;
	}

	public void setFvFormularioAgrCampos(List<FormularioAgrCampo> fvFormularioAgrCampos) {
		this.fvFormularioAgrCampos = fvFormularioAgrCampos;
	}

	public FormularioAgrCampo addFvFormularioAgrCampo(FormularioAgrCampo fvFormularioAgrCampo) {
		getFvFormularioAgrCampos().add(fvFormularioAgrCampo);
		fvFormularioAgrCampo.setFvFormulario(this);

		return fvFormularioAgrCampo;
	}

	public FormularioAgrCampo removeFvFormularioAgrCampo(FormularioAgrCampo fvFormularioAgrCampo) {
		getFvFormularioAgrCampos().remove(fvFormularioAgrCampo);
		fvFormularioAgrCampo.setFvFormulario(null);

		return fvFormularioAgrCampo;
	}

}