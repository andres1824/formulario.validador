package co.com.coomeva.formulario.validador.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the FV_TIPO_CAMPO database table.
 * 
 */
@Entity
@Table(name="FV_TIPO_CAMPO")
@NamedQuery(name="TipoCampo.findAll", query="SELECT t FROM TipoCampo t")
public class TipoCampo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @Basic(optional = false)
    @GeneratedValue(generator = "fv_tipo_campo_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "fv_tipo_campo_seq", sequenceName = "fv_tipo_campo_seq",allocationSize=1)    
	@Column(name="TIPO_CAMPO_ID")
	private Long tipoCampoId;

	private String logica;

	private String nombre;
	
	private String estado;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_MODIFICACION")
	private Date fechaModificacion;


	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;

	@Column(name="USUARIO_MODIFICACION")
	private String usuarioModificacion;

	//bi-directional many-to-one association to Campo
	@OneToMany(mappedBy="fvTipoCampo")
	private List<Campo> fvCampos;

	public TipoCampo() {
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public Long getTipoCampoId() {
		return this.tipoCampoId;
	}

	public void setTipoCampoId(Long tipoCampoId) {
		this.tipoCampoId = tipoCampoId;
	}

	public String getLogica() {
		return this.logica;
	}

	public void setLogica(String logica) {
		this.logica = logica;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@JsonIgnore
	public List<Campo> getFvCampos() {
		return this.fvCampos;
	}

	public void setFvCampos(List<Campo> fvCampos) {
		this.fvCampos = fvCampos;
	}

	public Campo addFvCampo(Campo fvCampo) {
		getFvCampos().add(fvCampo);
		fvCampo.setFvTipoCampo(this);

		return fvCampo;
	}

	public Campo removeFvCampo(Campo fvCampo) {
		getFvCampos().remove(fvCampo);
		fvCampo.setFvTipoCampo(null);

		return fvCampo;
	}

}