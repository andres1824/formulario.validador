package co.com.coomeva.formulario.validador.dao.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.TipoCampo;

/**
* TipoCampo Dao <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
public interface TipoCampoDao  {
    
    /**
     * Obtiene todos los registros activos de tipoCampo <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws Exception 
     */
    public List<TipoCampo> findAllActive() throws BusinessException;

    /**
     * Obtiene todos los registros de tipoCampo por su id <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoCampo
     * @return
     * @throws Exception 
     */
    public List<TipoCampo> findByTipoCampo(TipoCampo tipoCampo) throws BusinessException;   

    /**
     * Obtiene todos los registros de tipoCampo por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoCampo
     * @return
     * @throws Exception 
     */
    public List<TipoCampo> findByFiltro(TipoCampo tipoCampo) throws BusinessException; 

    /**
    * Obtiene todos los registros de tipoCampo por nombre <br>
    * Info. Creación: <br>
    * fecha 24 abr 2021 <br>
    * @author GTC
    * @param tipoCampo
    * @return
    * @throws Exception 
    */
    public List<TipoCampo> findByName(TipoCampo tipoCampo) throws BusinessException;  
}
