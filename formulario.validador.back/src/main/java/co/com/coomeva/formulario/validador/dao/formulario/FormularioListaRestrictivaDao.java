package co.com.coomeva.formulario.validador.dao.formulario;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.com.coomeva.formulario.validador.dto.FormularioAgrCampoValueDTO;
import co.com.coomeva.formulario.validador.dto.InspektorResponseDTO;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.FormularioListaRestrictiva;
import co.com.coomeva.formulario.validador.model.InspektorDTO;

/**
* Formulario Dao <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
public interface FormularioListaRestrictivaDao  {
    
    /**
     * Obtiene todos los registros activos de formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     * @throws Exception 
     */
    public List<FormularioListaRestrictiva> findAllActive() throws BusinessException;

    /**
     * Obtiene todos los registros de formulario por su id <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     * @throws Exception 
     */
    public List<FormularioListaRestrictiva> findByFormularioListaRestrictiva(FormularioListaRestrictiva formularioAsociado) throws BusinessException;   

    /**
     * Obtiene todos los registros de formulario por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     * @throws Exception 
     */
    public List<FormularioListaRestrictiva> findByFiltro(FormularioListaRestrictiva formularioAsociado) throws BusinessException; 
    public InspektorResponseDTO saveListasRestrictivas(FormularioAgrCampoValueDTO formularioAgrCampoValueDTO) throws BusinessException, IOException;
     
}
