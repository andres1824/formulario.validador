package co.com.coomeva.formulario.validador.service.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Color;

/**
* Color Service <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
public interface ColorService {
    

    /**
     * Crea color <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param color
     * @return 
     * @throws BusinessException 
     */
    public Color create(Color color) throws BusinessException;
    /**
     * Modifica color <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param color
     * @return 
     * @throws BusinessException 
     */
    public Color store(Color color) throws BusinessException;
    /**
     * Elimina color<br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param color
     * @throws BusinessException 
     */
    public void remove(Color color) throws BusinessException;
    /**
     * Obtiene todos los registros de color <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<Color> findAll() throws BusinessException;
    
    /**
     * Obtiene todos los registros activos de color <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<Color> findAllActive() throws BusinessException;
    /**
     * Obtiene todos los registros de color paginados <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param color
     * @return
     * @throws BusinessException 
     */
    public List<Color> findByColor(Color color) throws BusinessException;   

    /**
     * Obtiene todos los registros de color por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param color
     * @return
     * @throws BusinessException 
     */
    public List<Color> findByFiltro(Color color) throws BusinessException; 


    /**
    * Obtiene todos los registros de color por nombre <br>
    * Info. Creación: <br>
    * fecha 24 abr 2021 <br>
    * @author GTC
    * @param color
    * @return
    * @throws BusinessException 
    */
    public List<Color> findByName(Color color) throws BusinessException;  

    
}