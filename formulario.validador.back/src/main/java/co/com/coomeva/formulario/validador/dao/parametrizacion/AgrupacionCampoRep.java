package co.com.coomeva.formulario.validador.dao.parametrizacion;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.model.AgrupacionCampo;

/**
* Agrupacion Dao <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
@Repository
@Transactional
public interface AgrupacionCampoRep extends JpaRepository<AgrupacionCampo, Long>{

	@Query("SELECT p FROM AgrupacionCampo p WHERE p.fvAgrupacion.agrupacionId = (:agrupacionId)")
    public List<AgrupacionCampo> find(@Param("agrupacionId") Long agrupacionId);
}