package co.com.coomeva.formulario.validador.dto;

public enum EstadoEnum {
	INACTIVE("Inactivo"),
	ACTIVE("Activo")
	;
	
	private final String id;

    private EstadoEnum(String id) {
        this.id = id;
    }

	public String getId() {
		return id;
	}
    
    
}
