package co.com.coomeva.formulario.validador.dao.parametrizacion;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.model.Campo;

/**
* Campo Dao <br>
* Info. Creación: <br>
* fecha 25 abr 2021 <br>
* @author GTC
**/
@Repository
@Transactional
public interface CampoRep extends JpaRepository<Campo, Long>{

	@Query("SELECT p FROM Campo p WHERE p.fvTipoCampo.logica IN (:logica)")
    public List<Campo> findByLogicaTipoCampo(@Param("logica") String logica);
}