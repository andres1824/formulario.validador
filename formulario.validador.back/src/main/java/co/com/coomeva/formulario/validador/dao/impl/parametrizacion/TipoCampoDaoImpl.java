package co.com.coomeva.formulario.validador.dao.impl.parametrizacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import co.com.coomeva.formulario.validador.dao.parametrizacion.TipoCampoDao;
import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.TipoCampo;

/**
* TipoCampo Dao Impl <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Repository
public class TipoCampoDaoImpl implements TipoCampoDao {
    
    @PersistenceContext
    private EntityManager em;
    public static final String FROM = "select a from TipoCampo a ";
	
	
    public EntityManager getEntityManager() {
        return em;
    }


    @Override
    public List<TipoCampo> findAllActive() throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.estado = :active ");

        builder.append("order by a.nombre asc ");
        
        
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("active", (EstadoEnum.ACTIVE.getId()));
        return query.getResultList();
    }
    
    @Override
    public List<TipoCampo> findByTipoCampo(TipoCampo tipoCampo) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.tipoCampoId = :id ");

        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("id", tipoCampo.getTipoCampoId());
        return query.getResultList();
    }

    @Override
    public List<TipoCampo> findByFiltro(TipoCampo tipoCampo) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE 1=1 ");
        
        if(tipoCampo.getNombre() != null && !tipoCampo.getNombre().trim().isEmpty()) {
            builder.append("AND lower(a.nombre) like lower(:nombre) ");
        }


        if(tipoCampo.getLogica() != null && !tipoCampo.getLogica().trim().isEmpty()) {
            builder.append("AND lower(a.logica) like lower(:logica) ");
        }




        if(tipoCampo.getEstado() != null && !tipoCampo.getEstado().trim().isEmpty()) {
            builder.append("AND a.estado = :estado ");
        }

        builder.append("order by a.nombre asc ");
        Query query = this.getEntityManager().createQuery(builder.toString());

        if(tipoCampo.getNombre() != null && !tipoCampo.getNombre().trim().isEmpty()) {
            query.setParameter("nombre", "%" + tipoCampo.getNombre() + "%");
        }


        if(tipoCampo.getLogica() != null && !tipoCampo.getLogica().trim().isEmpty()) {
            query.setParameter("logica", "%" + tipoCampo.getLogica() + "%");
        }




        if(tipoCampo.getEstado() != null && !tipoCampo.getEstado().trim().isEmpty()) {
            query.setParameter("estado", tipoCampo.getEstado());
        }

        return query.getResultList();
    }

    @Override
    public List<TipoCampo> findByName(TipoCampo tipoCampo) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE lower(trim(a.nombre)) = lower(trim(:nombre)) ");
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("nombre", tipoCampo.getNombre().trim());
        return query.getResultList();
    }
}