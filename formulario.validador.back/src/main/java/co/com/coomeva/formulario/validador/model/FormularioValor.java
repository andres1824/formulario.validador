package co.com.coomeva.formulario.validador.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the FV_FORMULARIO_AGR_CAMPO database table.
 * 
 */
@Entity
@Table(name="FV_FORMULARIO_VALOR")
@NamedQuery(name="FormularioValor.findAll", query="SELECT f FROM FormularioValor f")
public class FormularioValor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @Basic(optional = false)
    @GeneratedValue(generator = "fv_formulario_valor_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "fv_formulario_valor_seq", sequenceName = "fv_formulario_valor_seq",allocationSize=1)    
	@Column(name="FORMULARIO_VALOR_ID")
	private Long formularioValorId;

	//bi-directional many-to-one association to AgrupacionCampo
	@ManyToOne
	@JoinColumn(name="CAMPO_ID")
	private Campo fvCampo;

	//bi-directional many-to-one association to Formulario
	@ManyToOne
	@JoinColumn(name="FORMULARIO_ID")
	private Formulario fvFormulario;
	
	@Column(name="VALOR")
	private String valor;
	
	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;

	@Column(name="USUARIO_MODIFICACION")
	private String usuarioModificacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_MODIFICACION")
	private Date fechaModificacion;

	@Column(name="ESTADO")
	private String estado;

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}


	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}


	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}


	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}


	public Date getFechaCreacion() {
		return fechaCreacion;
	}


	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}


	public Date getFechaModificacion() {
		return fechaModificacion;
	}


	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}


	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public FormularioValor() {
	}


	public Long getFormularioValorId() {
		return formularioValorId;
	}


	public void setFormularioValorId(Long formularioValorId) {
		this.formularioValorId = formularioValorId;
	}


	public Campo getFvCampo() {
		return fvCampo;
	}


	public void setFvCampo(Campo fvCampo) {
		this.fvCampo = fvCampo;
	}


	public String getValor() {
		return valor;
	}


	public void setValor(String valor) {
		this.valor = valor;
	}


	public Formulario getFvFormulario() {
		return this.fvFormulario;
	}

	public void setFvFormulario(Formulario fvFormulario) {
		this.fvFormulario = fvFormulario;
	}

}