package co.com.coomeva.formulario.validador.service.impl.parametrizacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.dao.parametrizacion.RespuestaDao;
import co.com.coomeva.formulario.validador.dao.parametrizacion.RespuestaRep;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Respuesta;
import co.com.coomeva.formulario.validador.service.parametrizacion.RespuestaService;

/**
* Respuesta Service Impl <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Service("respuestaService")
@Transactional
public class RespuestaServiceImpl implements RespuestaService {

    @Autowired
    private RespuestaDao respuestaDao;

    @Autowired
    private RespuestaRep respuestaRep;

    @Override
    public Respuesta create(Respuesta respuesta) throws BusinessException {
        return respuestaRep.save(respuesta);
    }

    @Override
    public Respuesta store(Respuesta respuesta) throws BusinessException {
       return respuestaRep.save(respuesta);
    }

    @Override
    public void remove(Respuesta respuesta) throws BusinessException {
        respuestaRep.delete(respuesta);
    }

    @Override
    public List<Respuesta> findAll() throws BusinessException {
        return respuestaRep.findAll();
    }

    @Override
    public List<Respuesta> findAllActive() throws BusinessException {
        return respuestaDao.findAllActive();
    }
    @Override
    public List<Respuesta> findByRespuesta(Respuesta respuesta) throws BusinessException {
        return respuestaDao.findByRespuesta(respuesta);
    }

    @Override
    public List<Respuesta> findByFiltro(Respuesta respuesta) throws BusinessException {
        return respuestaDao.findByFiltro(respuesta);
    }

	@Override
	public List<Respuesta> findByValor(Respuesta respuesta) throws BusinessException {
		return respuestaDao.findByValor(respuesta);
	}

}