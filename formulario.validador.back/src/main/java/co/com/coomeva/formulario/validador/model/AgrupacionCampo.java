package co.com.coomeva.formulario.validador.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the FV_AGRUPACION_CAMPO database table.
 * 
 */
@Entity
@Table(name="FV_AGRUPACION_CAMPO")
@NamedQuery(name="AgrupacionCampo.findAll", query="SELECT a FROM AgrupacionCampo a")
public class AgrupacionCampo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @Basic(optional = false)
    @GeneratedValue(generator = "fv_agrupacion_campo_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "fv_agrupacion_campo_seq", sequenceName = "fv_agrupacion_campo_seq",allocationSize=1)    
	@Column(name="AGRUPACION_CAMPO_ID")
	private Long agrupacionCampoId;

	//bi-directional many-to-one association to Agrupacion
	@ManyToOne
	@JoinColumn(name="AGRUPACION_ID")
	private Agrupacion fvAgrupacion;

	//bi-directional many-to-one association to Campo
	@ManyToOne
	@JoinColumn(name="CAMPO_ID")
	private Campo fvCampo;
	
	private Long orden;

	//bi-directional many-to-one association to FormularioAgrCampo
	@OneToMany(mappedBy="fvAgrupacionCampo")
	private List<FormularioAgrCampo> fvFormularioAgrCampos;

	public AgrupacionCampo() {
	}

	public Long getAgrupacionCampoId() {
		return this.agrupacionCampoId;
	}

	public void setAgrupacionCampoId(Long agrupacionCampoId) {
		this.agrupacionCampoId = agrupacionCampoId;
	}

	public Agrupacion getFvAgrupacion() {
		return this.fvAgrupacion;
	}

	public void setFvAgrupacion(Agrupacion fvAgrupacion) {
		this.fvAgrupacion = fvAgrupacion;
	}

	public Campo getFvCampo() {
		return this.fvCampo;
	}

	public void setFvCampo(Campo fvCampo) {
		this.fvCampo = fvCampo;
	}
	
	@JsonIgnore
	public List<FormularioAgrCampo> getFvFormularioAgrCampos() {
		return this.fvFormularioAgrCampos;
	}

	public void setFvFormularioAgrCampos(List<FormularioAgrCampo> fvFormularioAgrCampos) {
		this.fvFormularioAgrCampos = fvFormularioAgrCampos;
	}

	public FormularioAgrCampo addFvFormularioAgrCampo(FormularioAgrCampo fvFormularioAgrCampo) {
		getFvFormularioAgrCampos().add(fvFormularioAgrCampo);
		fvFormularioAgrCampo.setFvAgrupacionCampo(this);

		return fvFormularioAgrCampo;
	}

	public FormularioAgrCampo removeFvFormularioAgrCampo(FormularioAgrCampo fvFormularioAgrCampo) {
		getFvFormularioAgrCampos().remove(fvFormularioAgrCampo);
		fvFormularioAgrCampo.setFvAgrupacionCampo(null);

		return fvFormularioAgrCampo;
	}

	public Long getOrden() {
		return orden;
	}

	public void setOrden(Long orden) {
		this.orden = orden;
	}
	
	

}