package co.com.coomeva.formulario.validador.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the FV_TIPOGRAFIA database table.
 * 
 */
public class TipografiaDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long tipografiaId;

	private String codigoHtml;

	private String nombre;
	private String estado;
	private Date fechaCreacion;
	private Date fechaModificacion;
	private String usuarioCreacion;
	private String usuarioModificacion;

	private List<FormularioDTO> fvFormularios;

	public TipografiaDTO() {
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public Long getTipografiaId() {
		return this.tipografiaId;
	}

	public void setTipografiaId(Long tipografiaId) {
		this.tipografiaId = tipografiaId;
	}

	public String getCodigoHtml() {
		return this.codigoHtml;
	}

	public void setCodigoHtml(String codigoHtml) {
		this.codigoHtml = codigoHtml;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@JsonIgnore
	public List<FormularioDTO> getFvFormularios() {
		return this.fvFormularios;
	}

	public void setFvFormularios(List<FormularioDTO> fvFormularios) {
		this.fvFormularios = fvFormularios;
	}

	public FormularioDTO addFvFormulario(FormularioDTO fvFormulario) {
		getFvFormularios().add(fvFormulario);
		fvFormulario.setFvTipografia(this);

		return fvFormulario;
	}

	public FormularioDTO removeFvFormulario(FormularioDTO fvFormulario) {
		getFvFormularios().remove(fvFormulario);
		fvFormulario.setFvTipografia(null);

		return fvFormulario;
	}

}