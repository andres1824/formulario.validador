/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.coomeva.formulario.validador.util;

import org.w3c.dom.NodeList;

/**
 *
 * @author aflr0108
 */
public class SoapService {

	/**
	 * 
	 * @param login
	 * @param password
	 * @param app
	 * @param url
	 * @return
	 */
	public NodeList validarProfile(String login, String password, String app, String url) {
        String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ges=\"http://geside.coomeva.com.co\">\n" +
        		"   <soapenv:Header/>\n" +
        		"   <soapenv:Body>\n" +
        		"      <ges:validateUserApp>\n" +
        		"         <!--Optional:-->\n" +
        		"         <ges:directory>2</ges:directory>\n" +
        		"         <!--Optional:-->\n" +
        		"         <ges:userName>"+login+"</ges:userName>\n" +
        		"         <!--Optional:-->\n" +
        		"         <ges:password>"+password+"</ges:password>\n" +
        		"         <!--Optional:-->\n" +
        		"         <ges:validaAplicacion>2</ges:validaAplicacion>\n" +
        		"         <!--Optional:-->\n" +
        		"         <ges:app>"+app+"</ges:app>\n" +
        		"      </ges:validateUserApp>\n" +
        		"   </soapenv:Body>\n" +
        		"</soapenv:Envelope>";
        return ClientWS.sendXmlCustomNode(xml, url);
    }
}
