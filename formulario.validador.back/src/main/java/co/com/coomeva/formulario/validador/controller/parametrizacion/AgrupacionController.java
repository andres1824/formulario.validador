package co.com.coomeva.formulario.validador.controller.parametrizacion;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.formulario.validador.dto.AgrupacionCampoDTO;
import co.com.coomeva.formulario.validador.dto.AgrupacionDTO;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.model.Agrupacion;
import co.com.coomeva.formulario.validador.model.AgrupacionCampo;
import co.com.coomeva.formulario.validador.service.parametrizacion.AgrupacionService;
import co.com.coomeva.formulario.validador.util.AppUtil;
import co.com.coomeva.formulario.validador.util.ManagePropertiesLogic;
import co.com.coomeva.formulario.validador.util.ResponseServiceUtil;


/**
* Agrupacion Controller <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/

@RestController
@RequestMapping(value = "/api/agrupacion", produces = "application/json; charset=UTF-8")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class AgrupacionController {

    @Autowired
    AgrupacionService agrupacionService;
    
    private static final Logger log = LoggerFactory.getLogger(AgrupacionController.class);
    ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
    public static final String PROPERTIES = "agrupacion.properties";

    /**
     * Crea agrupacion <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param agrupacion
     * @return
     */
    @PostMapping(value = "/create")
    public ResponseService createAgrupacion(@RequestBody AgrupacionDTO agrupacionDTO) {
            ResponseService responseService = new ResponseService();
        try {
            Agrupacion agrupacion = new Agrupacion();
            agrupacion = (Agrupacion) AppUtil.convertOneToAnother(agrupacionDTO, agrupacion);
            Agrupacion agrupacionName = new Agrupacion();
            agrupacionName.setNombre(agrupacion.getNombre());
            List<Agrupacion> listAgrupacionName = agrupacionService.findByName(agrupacionName);
            
            if(listAgrupacionName != null && !listAgrupacionName.isEmpty()) {
                ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                return responseService;
            }
            
            agrupacion.setFechaCreacion(new Date());
            agrupacionService.create(agrupacion);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroCreado"));
        } catch (Exception e) {
            log.error("Error en el servicio /agrupacion " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }

    /**
     * Modifica agrupacion <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param agrupacion
     * @return
     */
    @PostMapping(value = "/update")
    public ResponseService updateAgrupacion(@RequestBody AgrupacionDTO agrupacionDTO) {
       ResponseService responseService = new ResponseService();
       try {
            Agrupacion agrupacion = new Agrupacion();
            agrupacion = (Agrupacion) AppUtil.convertOneToAnother(agrupacionDTO, agrupacion);
            Agrupacion agrupacionName = new Agrupacion();
            agrupacionName.setNombre(agrupacion.getNombre());
            List<Agrupacion> listAgrupacionName = agrupacionService.findByName(agrupacionName);
            
            if(listAgrupacionName != null && !listAgrupacionName.isEmpty()) {
                boolean existName = false;
                for (Agrupacion agrupacion1 : listAgrupacionName) {
                    if (!agrupacion1.getAgrupacionId().equals(agrupacion.getAgrupacionId())) {
                        existName = true;
                    }
                }

                if (existName) {
                    ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                    return responseService;
                }                
            }
            
            agrupacion.setFechaModificacion(new Date());
            agrupacionService.store(agrupacion);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEditado"));
        } catch (Exception e) {
            log.error("Error en el servicio /agrupacion/update " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Elimina agrupacion <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param agrupacion
     * @return
     */
    @PostMapping(value = "/delete")
    public ResponseService deleteAgrupacion(@RequestBody AgrupacionDTO agrupacionDTO) {
      ResponseService responseService = new ResponseService();
      try {
            Agrupacion agrupacion = new Agrupacion();
            agrupacion = (Agrupacion) AppUtil.convertOneToAnother(agrupacionDTO, agrupacion);
            agrupacionService.remove(agrupacion);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEliminado"));
        } catch (Exception e) {
            log.error("Error en el servicio /agrupacion/delete " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Consulta agrupacion <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     */
    @GetMapping(value = "/get/all")
    public ResponseService list() {
        ResponseService responseService = new ResponseService();
        try {     
            List<Agrupacion> agrupacionList = agrupacionService.findAllActive();
            ResponseServiceUtil.buildSuccessResponse(responseService, agrupacionList);
        } catch (Exception e) {
            log.error("Error en el servicio /agrupacion/get/all " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Consulta agrupacion <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     */
    @PostMapping(value = "/find")
    public ResponseService find(@RequestBody AgrupacionDTO agrupacionDTO) {
       ResponseService responseService = new ResponseService();
       try {
            Agrupacion agrupacion = new Agrupacion();
            agrupacion = (Agrupacion) AppUtil.convertOneToAnother(agrupacionDTO, agrupacion);
            List<Agrupacion> agrupacionList = agrupacionService.findByFiltro(agrupacion);
            ResponseServiceUtil.buildSuccessResponse(responseService, agrupacionList);
        } catch (Exception e) {
            log.error("Error en el servicio /agrupacion/find " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Consulta agrupacion <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     */
    @GetMapping(value = "/campo/get/all/{agrupacionId}")
    public ResponseService CampoList(@PathVariable("agrupacionId") Long agrupacionId) {
        ResponseService responseService = new ResponseService();
        try {     
            List<AgrupacionCampo> agrupacionList = agrupacionService.findAllAgrupacionCampo(agrupacionId);
            ResponseServiceUtil.buildSuccessResponse(responseService, agrupacionList);
        } catch (Exception e) {
            log.error("Error en el servicio /campo/get/all " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Crea agrupacion campo <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param agrupacion
     * @return
     */
    @PostMapping(value = "/createCampo")
    public ResponseService createCampo(@RequestBody AgrupacionCampoDTO agrupacionDTO) {
            ResponseService responseService = new ResponseService();
        try {
        	AgrupacionCampo agrupacion = new AgrupacionCampo();
            agrupacion = (AgrupacionCampo) AppUtil.convertOneToAnother(agrupacionDTO, agrupacion);
            
            List<Agrupacion> listAgrupacionName = agrupacionService.findByAgrupacionCampo(agrupacion);
            
            if(listAgrupacionName != null && !listAgrupacionName.isEmpty()) {
                ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgAgrupacionCampoExiste"));
                return responseService;
            }

            agrupacionService.createCampo(agrupacion);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroCreado"));
        } catch (Exception e) {
            log.error("Error en el servicio /agrupacion " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }
    
    /**
     * Elimina agrupacion <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param agrupacion
     * @return
     */
    @PostMapping(value = "/deleteCampo")
    public ResponseService deleteAgrupacionCampo(@RequestBody AgrupacionCampoDTO agrupacionDTO) {
      ResponseService responseService = new ResponseService();
      try {
    	  	AgrupacionCampo agrupacion = new AgrupacionCampo();
            agrupacion = (AgrupacionCampo) AppUtil.convertOneToAnother(agrupacionDTO, agrupacion);
            agrupacionService.removeCampo(agrupacion);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEliminado"));
        } catch (Exception e) {
            log.error("Error en el servicio /agrupacion/delete " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroNoEliminadoConDependencia"));
        }
        return responseService;
    }
}
