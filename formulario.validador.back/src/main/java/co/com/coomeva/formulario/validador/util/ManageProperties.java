package co.com.coomeva.formulario.validador.util;

public class ManageProperties {

	private String label;
	private String value;

	public ManageProperties() {
		// Construct
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
