package co.com.coomeva.formulario.validador.service.impl.formulario;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.dao.formulario.FormularioAgrCampoRep;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioDao;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioRep;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioAgrCampo;
import co.com.coomeva.formulario.validador.service.formulario.FormularioService;

/**
* Formulario Service Impl <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
@Service("formularioService")
@Transactional
public class FormularioServiceImpl implements FormularioService {

    @Autowired
    private FormularioDao formularioDao;

    @Autowired
    private FormularioRep formularioRep;

    @Autowired
    private FormularioAgrCampoRep formularioAgrCampoRep;
    
    @Override
    public Formulario create(Formulario formulario) throws BusinessException {
        return formularioRep.save(formulario);
    }

    @Override
    public Formulario store(Formulario formulario) throws BusinessException {
       return formularioRep.save(formulario);
    }

    @Override
    public void remove(Formulario formulario) throws BusinessException {
        formularioRep.delete(formulario);
    }

    @Override
    public List<Formulario> findAll() throws BusinessException {
        return formularioRep.findAll();
    }

    @Override
    public List<Formulario> findAllActive() throws BusinessException {
        return formularioDao.findAllActive();
    }
    @Override
    public List<Formulario> findByFormulario(Formulario formulario) throws BusinessException {
        return formularioDao.findByFormulario(formulario);
    }

    @Override
    public List<Formulario> findByFiltro(Formulario formulario) throws BusinessException {
        return formularioDao.findByFiltro(formulario);
    }

    @Override
    public List<Formulario> findByName(Formulario formulario) throws BusinessException {
        return formularioDao.findByName(formulario);
    }

	@Override
	public List<FormularioAgrCampo> findByFormulario(Long formularioId) throws BusinessException {
		return formularioAgrCampoRep.findByFormulario(formularioId);
	}

	@Override
	public FormularioAgrCampo createAgrupacionCampo(FormularioAgrCampo formulario) throws BusinessException {
		return formularioAgrCampoRep.save(formulario);
	}

	@Override
	public void removeAgrupacionCampo(FormularioAgrCampo formulario) throws BusinessException {
		formularioAgrCampoRep.delete(formulario);
		
	}

	@Override
	public List<Formulario> findByFormularioAgrupacionCampo(FormularioAgrCampo formularioId) throws BusinessException {
		return formularioDao.findByFormularioAgrupacionCampo(formularioId);
	}
}