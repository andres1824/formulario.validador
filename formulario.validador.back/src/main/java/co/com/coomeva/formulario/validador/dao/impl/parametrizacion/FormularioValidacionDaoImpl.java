package co.com.coomeva.formulario.validador.dao.impl.parametrizacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import co.com.coomeva.formulario.validador.dao.parametrizacion.CampoDao;
import co.com.coomeva.formulario.validador.dao.parametrizacion.FormularioValidacionDao;
import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Campo;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioValidacion;

/**
* Campo Dao Impl <br>
* Info. Creación: <br>
* fecha 25 abr 2021 <br>
* @author GTC
**/
@Repository
public class FormularioValidacionDaoImpl implements FormularioValidacionDao {
    
    @PersistenceContext
    private EntityManager em;
    public static final String FROM = "select a from FormularioValidacion a ";
	
	
    public EntityManager getEntityManager() {
        return em;
    }


    @Override
    public List<FormularioValidacion> findAllActive() throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        Query query = this.getEntityManager().createQuery(builder.toString());
        //query.setParameter("active", (EstadoEnum.ACTIVE.getId()));
        return query.getResultList();
    }
    
    @Override
    public List<FormularioValidacion> findByFormularioValidacion(FormularioValidacion formularioValidacion) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.FORMULARIO_VALIDACION_ID = :id ");

        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("id", formularioValidacion.getFormularioValidacionId());
        return query.getResultList();
    }

    @Override
    public List<FormularioValidacion> findByFiltro(FormularioValidacion formularioValidacion) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE 1=1 ");
        
        if(formularioValidacion.getConsultaPeopleNet() != null && !formularioValidacion.getConsultaPeopleNet().trim().isEmpty()) {
            builder.append("AND lower(a.consultaPeopleNet) like lower(:consultaPeopleNet) ");
        }


        if(formularioValidacion.getConsultaListaControl() != null && !formularioValidacion.getConsultaListaControl().trim().isEmpty()) {
            builder.append("AND lower(a.consultaListaControl) like lower(:consultaListaControl) ");
        }


        if(formularioValidacion.getCentralRiesgo() != null && !formularioValidacion.getCentralRiesgo().trim().isEmpty()) {
            builder.append("AND lower(a.centralRiesgo) like lower(:centralRiesgo) ");
        }


        if(formularioValidacion.getConsultaBuc() != null && !formularioValidacion.getConsultaBuc().trim().isEmpty()) {
            builder.append("AND lower(a.consultaBuc) like lower(:consultaBuc) ");
        }


        if(formularioValidacion.getConsultaCentralRiesgo() != null && !formularioValidacion.getConsultaCentralRiesgo().trim().isEmpty()) {
            builder.append("AND lower(a.consultaCentralRiesgo) like lower(:consultaCentralRiesgo) ");
        }
        
        if(formularioValidacion.getFvFormulario() != null && formularioValidacion.getFvFormulario().getFormularioId() != null) {
            builder.append("AND a.fvFormulario.formularioId = :fvFormulario ");
        }

        if(formularioValidacion.getFvCampoDocumento() != null && formularioValidacion.getFvCampoDocumento().getCampoId() != null) {
            builder.append("AND a.fvCampoDocumento.campoId = :fvCampoDocumento ");
        }


        if(formularioValidacion.getFvCampoDocumento() != null && formularioValidacion.getFvCampoDocumento().getCampoId() != null) {
            builder.append("AND a.fvCampoTpDocumento.campoId = :fvCampoTpDocumento ");
        }

     

        Query query = this.getEntityManager().createQuery(builder.toString());
        
        if(formularioValidacion.getConsultaBuc() != null && !formularioValidacion.getConsultaBuc().trim().isEmpty()) {
            query.setParameter("consultaBuc", "%" + formularioValidacion.getConsultaBuc() + "%");
        }
        
        if(formularioValidacion.getConsultaListaControl() != null && !formularioValidacion.getConsultaListaControl().trim().isEmpty()) {
            query.setParameter("consultaListaControl", "%" + formularioValidacion.getConsultaListaControl() + "%");
        }

        if(formularioValidacion.getConsultaPeopleNet() != null && !formularioValidacion.getConsultaPeopleNet().trim().isEmpty()) {
            query.setParameter("consultaPeopleNet", "%" + formularioValidacion.getConsultaPeopleNet() + "%");
        }

        
        if(formularioValidacion.getConsultaCentralRiesgo() != null && !formularioValidacion.getConsultaCentralRiesgo().trim().isEmpty()) {
            query.setParameter("consultaCentralRiesgo", "%" + formularioValidacion.getConsultaCentralRiesgo() + "%");
        }

        if(formularioValidacion.getCentralRiesgo() != null && !formularioValidacion.getCentralRiesgo().trim().isEmpty()) {
            query.setParameter("centralRiesgo", "%" + formularioValidacion.getCentralRiesgo() + "%");
        }
        
        if(formularioValidacion.getFvFormulario() != null && formularioValidacion.getFvFormulario().getFormularioId() != null) {
            query.setParameter("fvFormulario", formularioValidacion.getFvFormulario().getFormularioId());
        }
        
        if(formularioValidacion.getFvFormulario() != null && formularioValidacion.getFvFormulario().getFormularioId() != null) {
            query.setParameter("fvFormulario", formularioValidacion.getFvFormulario().getFormularioId());
        }
        
        if(formularioValidacion.getFvCampoDocumento() != null && formularioValidacion.getFvCampoDocumento().getCampoId() != null) {
            query.setParameter("fvCampoDocumento", formularioValidacion.getFvCampoDocumento().getCampoId());
        }
        
        
        if(formularioValidacion.getFvCampoTpDocumento() != null && formularioValidacion.getFvCampoTpDocumento().getCampoId() != null) {
            query.setParameter("fvCampoTpDocumento", formularioValidacion.getFvCampoTpDocumento().getCampoId());
        }
        
        
        
        return query.getResultList();
    }
    
    @Override
	public List<FormularioValidacion> findById(FormularioValidacion formularioValidacion)
			throws BusinessException {
	     StringBuilder builder = new StringBuilder();
	        builder.append(FROM);
	        builder.append("WHERE a.formularioValidacionId = :formularioValidacionId ");
	        Query query = this.getEntityManager().createQuery(builder.toString());
	        query.setParameter("formularioValidacionId", formularioValidacion.getFormularioValidacionId());
		return query.getResultList();
	}


	@Override
	public List<FormularioValidacion> findByIdFormulario(FormularioValidacion formularioValidacion)
			throws BusinessException {
	     StringBuilder builder = new StringBuilder();
	        builder.append(FROM);
	        builder.append("WHERE a.fvFormulario.formularioId = :fvFormulario ");
	        Query query = this.getEntityManager().createQuery(builder.toString());
	        query.setParameter("fvFormulario", formularioValidacion.getFvFormulario().getFormularioId());
		return query.getResultList();
	}
	


}