package co.com.coomeva.formulario.validador.service.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Campo;
import co.com.coomeva.formulario.validador.model.RespuestaCampo;
import co.com.coomeva.formulario.validador.model.TipoCampo;

/**
* Campo Service <br>
* Info. Creación: <br>
* fecha 25 abr 2021 <br>
* @author GTC
**/
public interface CampoService {
    

    /**
     * Crea campo <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @return 
     * @throws BusinessException 
     */
    public Campo create(Campo campo) throws BusinessException;
    /**
     * Modifica campo <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @return 
     * @throws BusinessException 
     */
    public Campo store(Campo campo) throws BusinessException;
    /**
     * Elimina campo<br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @throws BusinessException 
     */
    public void remove(Campo campo) throws BusinessException;
    /**
     * Obtiene todos los registros de campo <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<Campo> findAll() throws BusinessException;
    
    /**
     * Obtiene todos los registros activos de campo <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<Campo> findAllActive() throws BusinessException;
    
    /**
     * Obtiene todos los registros activos de campo <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<Campo> findAllDependiente(Long campoId) throws BusinessException;
    /**
     * Obtiene todos los registros de campo paginados <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @return
     * @throws BusinessException 
     */
    public List<Campo> findByCampo(Campo campo) throws BusinessException;   

    /**
     * Obtiene todos los registros de campo por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @return
     * @throws BusinessException 
     */
    public List<Campo> findByFiltro(Campo campo) throws BusinessException; 


    /**
    * Obtiene todos los registros de campo por nombre <br>
    * Info. Creación: <br>
    * fecha 25 abr 2021 <br>
    * @author GTC
    * @param campo
    * @return
    * @throws BusinessException 
    */
    public List<Campo> findByName(Campo campo) throws BusinessException;  
    
    /**
     * Crea campo <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @return 
     * @throws BusinessException 
     */
    public RespuestaCampo create(RespuestaCampo campo) throws BusinessException;
    /**
     * Modifica campo <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @return 
     * @throws BusinessException 
     */
    public RespuestaCampo store(RespuestaCampo campo) throws BusinessException;
    /**
     * Elimina campo<br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @throws BusinessException 
     */
    public void remove(RespuestaCampo campo) throws BusinessException;
    
    /**
     * Obtiene todos los registros de campo por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @return
     * @throws BusinessException
     */
    public List<RespuestaCampo> findByRespuestaCampo(Campo campo, Long respuestaId) throws BusinessException; 
    
    /**
     * Obtiene todos los registros de campo por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @return
     * @throws BusinessException
     */
    public List<Campo> findByTipoCampoLogica(String logica) throws BusinessException; 

    
}