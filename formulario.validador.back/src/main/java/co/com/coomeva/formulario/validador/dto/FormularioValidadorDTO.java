package co.com.coomeva.formulario.validador.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the FV_CAMPO database table.
 * 
 */
public class FormularioValidadorDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long formularioValidacionId;

	private CampoDTO fvCampoDocumento;
	
	private CampoDTO fvCampoTpDocumento;
	
	private FormularioDTO fvFormulario;
	
	private String centralRiesgo;
	
	private String consultaBuc;
	
	private String consultaCentralRiesgo;
	
	private String consultaListaControl;
	
	private String consultaPeopleNet;
	
	private Date fechaCreacion;

	private Date fechaModificacion;
	
	private String usuarioCreacion;

	private String usuarioModificacion;
	
	

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public FormularioValidadorDTO() {
	}

	public Long getFormularioValidacionId() {
		return formularioValidacionId;
	}

	public void setFormularioValidacionId(Long formularioValidacionId) {
		this.formularioValidacionId = formularioValidacionId;
	}



	public CampoDTO getFvCampoDocumento() {
		return fvCampoDocumento;
	}

	public void setFvCampoDocumento(CampoDTO fvCampoDocumento) {
		this.fvCampoDocumento = fvCampoDocumento;
	}

	public CampoDTO getFvCampoTpDocumento() {
		return fvCampoTpDocumento;
	}

	public void setFvCampoTpDocumento(CampoDTO fvCampoTpDocumento) {
		this.fvCampoTpDocumento = fvCampoTpDocumento;
	}

	public FormularioDTO getFvFormulario() {
		return fvFormulario;
	}

	public void setFvFormulario(FormularioDTO fvFormulario) {
		this.fvFormulario = fvFormulario;
	}

	public String getCentralRiesgo() {
		return centralRiesgo;
	}

	public void setCentralRiesgo(String centralRiesgo) {
		this.centralRiesgo = centralRiesgo;
	}

	public String getConsultaBuc() {
		return consultaBuc;
	}

	public void setConsultaBuc(String consultaBuc) {
		this.consultaBuc = consultaBuc;
	}

	public String getConsultaCentralRiesgo() {
		return consultaCentralRiesgo;
	}

	public void setConsultaCentralRiesgo(String consultaCentralRiesgo) {
		this.consultaCentralRiesgo = consultaCentralRiesgo;
	}

	public String getConsultaListaControl() {
		return consultaListaControl;
	}

	public void setConsultaListaControl(String consultaListaControl) {
		this.consultaListaControl = consultaListaControl;
	}

	public String getConsultaPeopleNet() {
		return consultaPeopleNet;
	}

	public void setConsultaPeopleNet(String consultaPeopleNet) {
		this.consultaPeopleNet = consultaPeopleNet;
	}
	
	


}