package co.com.coomeva.formulario.validador.dao.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.TipoReporte;

/**
* TipoReporte Dao <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
public interface TipoReporteDao  {
    
    /**
     * Obtiene todos los registros activos de tipoReporte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws Exception 
     */
    public List<TipoReporte> findAllActive() throws BusinessException;

    /**
     * Obtiene todos los registros de tipoReporte por su id <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoReporte
     * @return
     * @throws Exception 
     */
    public List<TipoReporte> findByTipoReporte(TipoReporte tipoReporte) throws BusinessException;   

    /**
     * Obtiene todos los registros de tipoReporte por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoReporte
     * @return
     * @throws Exception 
     */
    public List<TipoReporte> findByFiltro(TipoReporte tipoReporte) throws BusinessException; 

    /**
    * Obtiene todos los registros de tipoReporte por nombre <br>
    * Info. Creación: <br>
    * fecha 24 abr 2021 <br>
    * @author GTC
    * @param tipoReporte
    * @return
    * @throws Exception 
    */
    public List<TipoReporte> findByName(TipoReporte tipoReporte) throws BusinessException;  
}
