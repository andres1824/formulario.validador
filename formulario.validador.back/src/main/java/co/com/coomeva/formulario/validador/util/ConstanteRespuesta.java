package co.com.coomeva.formulario.validador.util;

public abstract class ConstanteRespuesta {
	
	private ConstanteRespuesta() {
		
	}
	
	public static final String RESPUESTA_EXITOSA = "0";
	public static final String RESPUESTA_NOEXITOSA = "-1";
	public static final String MSG_RESPUESTA_EXITOSA = "Se ejecuto satisfactoriamente";
	public static final String MSG_RESPUESTA_NOEXITOSA = "Peticion no exitosa";
	public static final String MSG_RESPUESTA_TOKEN_INVALIDO = "Ocurrio un error Token Incorrecto";
	public static final String MSG_RESPUESTA_CABECERA_OBLIGATORIA = "El objeto cabecera es obligatoria";
	public static final String MSG_RESPUESTA_PRODUCTO_OBLIGATORIA = "El objeto producto es obligatorio";
	public static final String MSG_RESPUESTA_COTIZAR_OBLIGATORIA = "El objeto cotizacion es obligatorio";

}
