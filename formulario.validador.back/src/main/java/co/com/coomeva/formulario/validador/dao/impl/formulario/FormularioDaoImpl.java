package co.com.coomeva.formulario.validador.dao.impl.formulario;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import co.com.coomeva.formulario.validador.dao.formulario.FormularioDao;
import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioAgrCampo;

/**
* Formulario Dao Impl <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
@Repository
public class FormularioDaoImpl implements FormularioDao {
    
    @PersistenceContext
    private EntityManager em;
    public static final String FROM = "select a from Formulario a ";
	
	
    public EntityManager getEntityManager() {
        return em;
    }


    @Override
    public List<Formulario> findAllActive() throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.estado = :active ");

        builder.append("order by a.nombre asc ");
        
        
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("active", (EstadoEnum.ACTIVE.getId()));
        return query.getResultList();
    }
    
    @Override
    public List<Formulario> findByFormulario(Formulario formulario) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.formularioId = :id ");

        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("id", formulario.getFormularioId());
        return query.getResultList();
    }

    @Override
    public List<Formulario> findByFiltro(Formulario formulario) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE 1=1 ");
        
        if(formulario.getNombre() != null && !formulario.getNombre().trim().isEmpty()) {
            builder.append("AND lower(a.nombre) like lower(:nombre) ");
        }


        if(formulario.getDescripcion() != null && !formulario.getDescripcion().trim().isEmpty()) {
            builder.append("AND lower(a.descripcion) like lower(:descripcion) ");
        }




        if(formulario.getBanner() != null && !formulario.getBanner().trim().isEmpty()) {
            builder.append("AND lower(a.banner) like lower(:banner) ");
        }


        if(formulario.getFrecuencia() != null && !formulario.getFrecuencia().trim().isEmpty()) {
            builder.append("AND lower(a.frecuencia) like lower(:frecuencia) ");
        }


        if(formulario.getGeneraAsociados() != null && !formulario.getGeneraAsociados().trim().isEmpty()) {
            builder.append("AND lower(a.generaAsociados) like lower(:generaAsociados) ");
        }


        if(formulario.getGeneraNoAsociados() != null && !formulario.getGeneraNoAsociados().trim().isEmpty()) {
            builder.append("AND lower(a.generaNoAsociados) like lower(:generaNoAsociados) ");
        }


        if(formulario.getGeneraReporteAuto() != null && !formulario.getGeneraReporteAuto().trim().isEmpty()) {
            builder.append("AND lower(a.generaReporteAuto) like lower(:generaReporteAuto) ");
        }


        if(formulario.getHorario() != null && !formulario.getHorario().trim().isEmpty()) {
            builder.append("AND lower(a.horario) like lower(:horario) ");
        }


        if(formulario.getIndCssExterno() != null && !formulario.getIndCssExterno().trim().isEmpty()) {
            builder.append("AND lower(a.indCssExterno) like lower(:indCssExterno) ");
        }


        if(formulario.getUrlCssExterno() != null && !formulario.getUrlCssExterno().trim().isEmpty()) {
            builder.append("AND lower(a.urlCssExterno) like lower(:urlCssExterno) ");
        }


        if(formulario.getRutaReporte() != null && !formulario.getRutaReporte().trim().isEmpty()) {
            builder.append("AND lower(a.rutaReporte) like lower(:rutaReporte) ");
        }



        if(formulario.getFvColor() != null && formulario.getFvColor().getColorId() != null) {
            builder.append("AND a.fvColor.colorId = :fvColor ");
        }


        if(formulario.getFvTipografia() != null && formulario.getFvTipografia().getTipografiaId() != null) {
            builder.append("AND a.fvTipografia.tipografiaId = :fvTipografia ");
        }


        if(formulario.getFvTipoReporte() != null && formulario.getFvTipoReporte().getTipoReporteId() != null) {
            builder.append("AND a.fvTipoReporte.tipoReporteId = :fvTipoReporte ");
        }
        
        if(formulario.getFvEmpresa() != null && formulario.getFvEmpresa().getEmpresaId() != null) {
            builder.append("AND a.fvEmpresa.empresaId = :empresaId ");
        }
        
        if(formulario.getFormularioId() != null) {
            builder.append("AND a.formularioId = :formularioId ");
        }



        if(formulario.getEstado() != null && !formulario.getEstado().trim().isEmpty()) {
            builder.append("AND a.estado = :estado ");
        }

        builder.append("order by a.nombre asc ");
        Query query = this.getEntityManager().createQuery(builder.toString());

        if(formulario.getNombre() != null && !formulario.getNombre().trim().isEmpty()) {
            query.setParameter("nombre", "%" + formulario.getNombre() + "%");
        }


        if(formulario.getDescripcion() != null && !formulario.getDescripcion().trim().isEmpty()) {
            query.setParameter("descripcion", "%" + formulario.getDescripcion() + "%");
        }


        if(formulario.getFormularioId() != null) {
        	query.setParameter("formularioId", formulario.getFormularioId());
        }

        if(formulario.getBanner() != null && !formulario.getBanner().trim().isEmpty()) {
            query.setParameter("banner", "%" + formulario.getBanner() + "%");
        }


        if(formulario.getFrecuencia() != null && !formulario.getFrecuencia().trim().isEmpty()) {
            query.setParameter("frecuencia", "%" + formulario.getFrecuencia() + "%");
        }


        if(formulario.getGeneraAsociados() != null && !formulario.getGeneraAsociados().trim().isEmpty()) {
            query.setParameter("generaAsociados", "%" + formulario.getGeneraAsociados() + "%");
        }


        if(formulario.getGeneraNoAsociados() != null && !formulario.getGeneraNoAsociados().trim().isEmpty()) {
            query.setParameter("generaNoAsociados", "%" + formulario.getGeneraNoAsociados() + "%");
        }


        if(formulario.getGeneraReporteAuto() != null && !formulario.getGeneraReporteAuto().trim().isEmpty()) {
            query.setParameter("generaReporteAuto", "%" + formulario.getGeneraReporteAuto() + "%");
        }


        if(formulario.getHorario() != null && !formulario.getHorario().trim().isEmpty()) {
            query.setParameter("horario", "%" + formulario.getHorario() + "%");
        }


        if(formulario.getIndCssExterno() != null && !formulario.getIndCssExterno().trim().isEmpty()) {
            query.setParameter("indCssExterno", "%" + formulario.getIndCssExterno() + "%");
        }


        if(formulario.getUrlCssExterno() != null && !formulario.getUrlCssExterno().trim().isEmpty()) {
            query.setParameter("urlCssExterno", "%" + formulario.getUrlCssExterno() + "%");
        }


        if(formulario.getRutaReporte() != null && !formulario.getRutaReporte().trim().isEmpty()) {
            query.setParameter("rutaReporte", "%" + formulario.getRutaReporte() + "%");
        }



        if(formulario.getFvColor() != null && formulario.getFvColor().getColorId() != null) {
            query.setParameter("fvColor", formulario.getFvColor().getColorId());
        }


        if(formulario.getFvTipografia() != null && formulario.getFvTipografia().getTipografiaId() != null) {
            query.setParameter("fvTipografia", formulario.getFvTipografia().getTipografiaId());
        }


        if(formulario.getFvTipoReporte() != null && formulario.getFvTipoReporte().getTipoReporteId() != null) {
            query.setParameter("fvTipoReporte", formulario.getFvTipoReporte().getTipoReporteId());
        }



        if(formulario.getEstado() != null && !formulario.getEstado().trim().isEmpty()) {
            query.setParameter("estado", formulario.getEstado());
        }
        
        if(formulario.getFvEmpresa() != null && formulario.getFvEmpresa().getEmpresaId() != null) {
            query.setParameter("empresaId", formulario.getFvEmpresa().getEmpresaId());
        }

        return query.getResultList();
    }

    @Override
    public List<Formulario> findByName(Formulario formulario) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE lower(trim(a.nombre)) = lower(trim(:nombre)) ");
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("nombre", formulario.getNombre().trim());
        return query.getResultList();
    }


	@Override
	public List<Formulario> findByFormularioAgrupacionCampo(FormularioAgrCampo formularioId) throws BusinessException {
		StringBuilder builder = new StringBuilder();
        builder.append("SELECT a.fvFormulario FROM FormularioAgrCampo a ");
        builder.append("WHERE a.fvFormulario.formularioId = :formularioId ");
        
        if(formularioId.getFvAgrupacionCampo().getFvAgrupacion() != null) {
        	builder.append("AND a.fvAgrupacionCampo.fvAgrupacion.agrupacionId = :agrupacionCampoId ");
        } else {
        	builder.append("AND a.fvAgrupacionCampo.agrupacionCampoId = :agrupacionCampoId ");
        }
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("formularioId", formularioId.getFvFormulario().getFormularioId());
        
        if(formularioId.getFvAgrupacionCampo().getFvAgrupacion() != null) {
            query.setParameter("agrupacionCampoId", formularioId.getFvAgrupacionCampo().getFvAgrupacion().getAgrupacionId());
        } else {
            query.setParameter("agrupacionCampoId", formularioId.getFvAgrupacionCampo().getAgrupacionCampoId());
        }
        
        return query.getResultList();
	}
}