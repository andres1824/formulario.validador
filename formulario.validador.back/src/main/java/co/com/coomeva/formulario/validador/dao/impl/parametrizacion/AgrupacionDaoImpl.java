package co.com.coomeva.formulario.validador.dao.impl.parametrizacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import co.com.coomeva.formulario.validador.dao.parametrizacion.AgrupacionDao;
import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Agrupacion;
import co.com.coomeva.formulario.validador.model.AgrupacionCampo;

/**
* Agrupacion Dao Impl <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
@Repository
public class AgrupacionDaoImpl implements AgrupacionDao {
    
    @PersistenceContext
    private EntityManager em;
    public static final String FROM = "select a from Agrupacion a ";
	
	
    public EntityManager getEntityManager() {
        return em;
    }


    @Override
    public List<Agrupacion> findAllActive() throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.estado = :active ");

        builder.append("order by a.nombre asc ");
        
        
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("active", (EstadoEnum.ACTIVE.getId()));
        return query.getResultList();
    }
    
    @Override
    public List<Agrupacion> findByAgrupacion(Agrupacion agrupacion) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.agrupacionId = :id ");

        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("id", agrupacion.getAgrupacionId());
        return query.getResultList();
    }

    @Override
    public List<Agrupacion> findByFiltro(Agrupacion agrupacion) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE 1=1 ");
        
        if(agrupacion.getNombre() != null && !agrupacion.getNombre().trim().isEmpty()) {
            builder.append("AND lower(a.nombre) like lower(:nombre) ");
        }


        if(agrupacion.getDescripcion() != null && !agrupacion.getDescripcion().trim().isEmpty()) {
            builder.append("AND lower(a.descripcion) like lower(:descripcion) ");
        }




        if(agrupacion.getEstado() != null && !agrupacion.getEstado().trim().isEmpty()) {
            builder.append("AND a.estado = :estado ");
        }

        builder.append("order by a.nombre asc ");
        Query query = this.getEntityManager().createQuery(builder.toString());

        if(agrupacion.getNombre() != null && !agrupacion.getNombre().trim().isEmpty()) {
            query.setParameter("nombre", "%" + agrupacion.getNombre() + "%");
        }


        if(agrupacion.getDescripcion() != null && !agrupacion.getDescripcion().trim().isEmpty()) {
            query.setParameter("descripcion", "%" + agrupacion.getDescripcion() + "%");
        }




        if(agrupacion.getEstado() != null && !agrupacion.getEstado().trim().isEmpty()) {
            query.setParameter("estado", agrupacion.getEstado());
        }

        return query.getResultList();
    }

    @Override
    public List<Agrupacion> findByName(Agrupacion agrupacion) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE lower(trim(a.nombre)) = lower(trim(:nombre)) ");
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("nombre", agrupacion.getNombre().trim());
        return query.getResultList();
    }


	@Override
	public List<Agrupacion> findByAgrupacionCampo(AgrupacionCampo agrupacion) throws BusinessException {
		StringBuilder builder = new StringBuilder();
        builder.append("SELECT ac.fvAgrupacion FROM AgrupacionCampo ac ");
        builder.append("WHERE ac.fvAgrupacion.agrupacionId = :agrupacionId ");
        
        if(agrupacion.getFvCampo() != null && agrupacion.getFvCampo().getCampoId() != null) {
        	builder.append("AND ac.fvCampo.campoId = :campoId ");
        }
        
        
        Query query = this.getEntityManager().createQuery(builder.toString());
        
        if(agrupacion.getFvCampo() != null && agrupacion.getFvCampo().getCampoId() != null) {
        	query.setParameter("campoId", agrupacion.getFvCampo().getCampoId());
        }
        
        query.setParameter("agrupacionId", agrupacion.getFvAgrupacion().getAgrupacionId());
        return query.getResultList();
	}
}