package co.com.coomeva.formulario.validador.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the FV_TIPO_CAMPO database table.
 * 
 */
public class TipoCampoDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long tipoCampoId;

	private String logica;

	private String nombre;
	private String estado;
	private Date fechaCreacion;
	private Date fechaModificacion;
	private String usuarioCreacion;
	private String usuarioModificacion;
	
	private List<CampoDTO> fvCampos;

	public TipoCampoDTO() {
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public Long getTipoCampoId() {
		return this.tipoCampoId;
	}

	public void setTipoCampoId(Long tipoCampoId) {
		this.tipoCampoId = tipoCampoId;
	}

	public String getLogica() {
		return this.logica;
	}

	public void setLogica(String logica) {
		this.logica = logica;
	}

	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@JsonIgnore
	public List<CampoDTO> getFvCampos() {
		return this.fvCampos;
	}

	public void setFvCampos(List<CampoDTO> fvCampos) {
		this.fvCampos = fvCampos;
	}

	public CampoDTO addFvCampo(CampoDTO fvCampo) {
		getFvCampos().add(fvCampo);
		fvCampo.setFvTipoCampo(this);

		return fvCampo;
	}

	public CampoDTO removeFvCampo(CampoDTO fvCampo) {
		getFvCampos().remove(fvCampo);
		fvCampo.setFvTipoCampo(null);

		return fvCampo;
	}

}