package co.com.coomeva.formulario.validador.controller.parametrizacion;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.formulario.validador.dto.CausalDescarteDTO;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.model.CausalDescarte;
import co.com.coomeva.formulario.validador.service.parametrizacion.CausalDescarteService;
import co.com.coomeva.formulario.validador.util.AppUtil;
import co.com.coomeva.formulario.validador.util.ManagePropertiesLogic;
import co.com.coomeva.formulario.validador.util.ResponseServiceUtil;


/**
* CausalDescarte Controller <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/

@RestController
@RequestMapping(value = "/api/causalDescarte", produces = "application/json; charset=UTF-8")
@CrossOrigin(origins = "http://localhost:4200", methods = { RequestMethod.GET, RequestMethod.POST })
public class CausalDescarteController {

    @Autowired
    CausalDescarteService causalDescarteService;
    
    private static final Logger log = LoggerFactory.getLogger(CausalDescarteController.class);
    ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
    public static final String PROPERTIES = "causalDescarte.properties";

    /**
     * Crea causalDescarte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param causalDescarte
     * @return
     */
    @PostMapping(value = "/create")
    public ResponseService createCausalDescarte(@RequestBody CausalDescarteDTO causalDescarteDTO) {
            ResponseService responseService = new ResponseService();
        try {
            CausalDescarte causalDescarte = new CausalDescarte();
            causalDescarte = (CausalDescarte) AppUtil.convertOneToAnother(causalDescarteDTO, causalDescarte);
            CausalDescarte causalDescarteName = new CausalDescarte();
            causalDescarteName.setNombre(causalDescarte.getNombre());
            List<CausalDescarte> listCausalDescarteName = causalDescarteService.findByName(causalDescarteName);
            
            if(listCausalDescarteName != null && !listCausalDescarteName.isEmpty()) {
                ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                return responseService;
            }
            
            causalDescarte.setFechaCreacion(new Date());
            causalDescarteService.create(causalDescarte);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroCreado"));
        } catch (Exception e) {
            log.error("Error en el servicio /causalDescarte " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }

    /**
     * Modifica causalDescarte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param causalDescarte
     * @return
     */
    @PostMapping(value = "/update")
    public ResponseService updateCausalDescarte(@RequestBody CausalDescarteDTO causalDescarteDTO) {
       ResponseService responseService = new ResponseService();
       try {
            CausalDescarte causalDescarte = new CausalDescarte();
            causalDescarte = (CausalDescarte) AppUtil.convertOneToAnother(causalDescarteDTO, causalDescarte);
            CausalDescarte causalDescarteName = new CausalDescarte();
            causalDescarteName.setNombre(causalDescarte.getNombre());
            List<CausalDescarte> listCausalDescarteName = causalDescarteService.findByName(causalDescarteName);
            
            if(listCausalDescarteName != null && !listCausalDescarteName.isEmpty()) {
                boolean existName = false;
                for (CausalDescarte causalDescarte1 : listCausalDescarteName) {
                    if (!causalDescarte1.getCausalDescarteId().equals(causalDescarte.getCausalDescarteId())) {
                        existName = true;
                    }
                }

                if (existName) {
                    ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                    return responseService;
                }                
            }
            
            causalDescarte.setFechaModificacion(new Date());
            causalDescarteService.store(causalDescarte);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEditado"));
        } catch (Exception e) {
            log.error("Error en el servicio /causalDescarte/update " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Elimina causalDescarte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param causalDescarte
     * @return
     */
    @PostMapping(value = "/delete")
    public ResponseService deleteCausalDescarte(@RequestBody CausalDescarteDTO causalDescarteDTO) {
      ResponseService responseService = new ResponseService();
      try {
            CausalDescarte causalDescarte = new CausalDescarte();
            causalDescarte = (CausalDescarte) AppUtil.convertOneToAnother(causalDescarteDTO, causalDescarte);
            causalDescarteService.remove(causalDescarte);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEliminado"));
        } catch (Exception e) {
            log.error("Error en el servicio /causalDescarte/delete " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Consulta causalDescarte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     */
    @GetMapping(value = "/get/all")
    public ResponseService list() {
        ResponseService responseService = new ResponseService();
        try {     
            List<CausalDescarte> causalDescarteList = causalDescarteService.findAllActive();
            ResponseServiceUtil.buildSuccessResponse(responseService, causalDescarteList);
        } catch (Exception e) {
            log.error("Error en el servicio /causalDescarte/get/all " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Consulta causalDescarte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     */
    @PostMapping(value = "/find")
    public ResponseService find(@RequestBody CausalDescarteDTO causalDescarteDTO) {
       ResponseService responseService = new ResponseService();
       try {
            CausalDescarte causalDescarte = new CausalDescarte();
            causalDescarte = (CausalDescarte) AppUtil.convertOneToAnother(causalDescarteDTO, causalDescarte);
            List<CausalDescarte> causalDescarteList = causalDescarteService.findByFiltro(causalDescarte);
            ResponseServiceUtil.buildSuccessResponse(responseService, causalDescarteList);
        } catch (Exception e) {
            log.error("Error en el servicio /causalDescarte/find " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
}
