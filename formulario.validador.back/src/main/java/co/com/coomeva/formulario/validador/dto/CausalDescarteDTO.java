package co.com.coomeva.formulario.validador.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the FV_AGRUPACION database table.
 * 
 */
public class CausalDescarteDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long causalDescarteId;

	private String estado;

	private Date fechaCreacion;

	private Date fechaModificacion;

	private String nombre;

	private String usuarioCreacion;

	private String usuarioModificacion;


	public CausalDescarteDTO() {
	}

	public Long getCausalDescarteId() {
		return causalDescarteId;
	}

	public void setCausalDescarteId(Long causalDescarteId) {
		this.causalDescarteId = causalDescarteId;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

}