package co.com.coomeva.formulario.validador.util;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import co.com.coomeva.formulario.validador.exception.BusinessException;

@Service
public class Mail implements MainService {
	
	@Value("${mail.server}")
    private String mailServer;
	
	@Value("${mail.port}")
    private String mailPort;
	
	@Value("${mail.user}")
    private String mailUser;
	
	@Value("${mail.password}")
    private String mailPws;
	
	@Value("${smtp.emailFrom}")
    private String mailFrom;
	
	private static final Logger log = LoggerFactory.getLogger(Mail.class);
	public static final String ERROR_ENVIAR = "notificationSendEmail, Error al enviar correo ";
	private static final Properties mailServerProperties = System.getProperties();
	

	/**
	 * Método que envia notificación por el Mail. Carga la informacion del servidor
	 * 
	 * @param toEmail  correo a enviar
	 * @param vipBody  cuerpo del correo
	 * @param isUpdate valida si actualiza o no
	 * @return boolean
	 * @throws MessagingException Excepción para el mensaje
	 */
	public boolean sendEmail(String toEmail, String mailSubject, String mailBody)
			throws BusinessException, MessagingException {
		Session getMailSession;
		MimeMessage generateMailMessage;

		// Paso 1
		mailServerProperties.put("mail.smtp.port", mailPort);
		mailServerProperties.put("mail.smtp.auth", "true");
		mailServerProperties.put("mail.smtp.starttls.enable", "false");

		// Paso 2
		getMailSession = Session.getDefaultInstance(mailServerProperties, null);
		generateMailMessage = new MimeMessage(getMailSession);
		InternetAddress from = new InternetAddress(mailFrom);
		generateMailMessage.setFrom(from);
		generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
		generateMailMessage.setSubject(mailSubject);
		generateMailMessage.setContent(mailBody, "text/html");

		// Paso 3
		Transport transport = getMailSession.getTransport("smtp");
		try {
			transport.connect(mailServer, mailUser, mailPws);
			transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
		} catch (Exception e) {
			log.error(ERROR_ENVIAR + e.getMessage(), e);
			return false;
		} finally {
			transport.close();
		}
		return true;
	}

}
