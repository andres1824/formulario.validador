package co.com.coomeva.formulario.validador.dao.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Tipografia;

/**
* Tipografia Dao <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
public interface TipografiaDao  {
    
    /**
     * Obtiene todos los registros activos de tipografia <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws Exception 
     */
    public List<Tipografia> findAllActive() throws BusinessException;

    /**
     * Obtiene todos los registros de tipografia por su id <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipografia
     * @return
     * @throws Exception 
     */
    public List<Tipografia> findByTipografia(Tipografia tipografia) throws BusinessException;   

    /**
     * Obtiene todos los registros de tipografia por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipografia
     * @return
     * @throws Exception 
     */
    public List<Tipografia> findByFiltro(Tipografia tipografia) throws BusinessException; 

    /**
    * Obtiene todos los registros de tipografia por nombre <br>
    * Info. Creación: <br>
    * fecha 24 abr 2021 <br>
    * @author GTC
    * @param tipografia
    * @return
    * @throws Exception 
    */
    public List<Tipografia> findByName(Tipografia tipografia) throws BusinessException;  
}
