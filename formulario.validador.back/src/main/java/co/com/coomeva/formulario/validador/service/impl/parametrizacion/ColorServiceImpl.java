package co.com.coomeva.formulario.validador.service.impl.parametrizacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.dao.parametrizacion.ColorDao;
import co.com.coomeva.formulario.validador.dao.parametrizacion.ColorRep;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Color;
import co.com.coomeva.formulario.validador.service.parametrizacion.ColorService;

/**
* Color Service Impl <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Service("colorService")
@Transactional
public class ColorServiceImpl implements ColorService {

    @Autowired
    private ColorDao colorDao;

    @Autowired
    private ColorRep colorRep;

    @Override
    public Color create(Color color) throws BusinessException {
        return colorRep.save(color);
    }

    @Override
    public Color store(Color color) throws BusinessException {
       return colorRep.save(color);
    }

    @Override
    public void remove(Color color) throws BusinessException {
        colorRep.delete(color);
    }

    @Override
    public List<Color> findAll() throws BusinessException {
        return colorRep.findAll();
    }

    @Override
    public List<Color> findAllActive() throws BusinessException {
        return colorDao.findAllActive();
    }
    @Override
    public List<Color> findByColor(Color color) throws BusinessException {
        return colorDao.findByColor(color);
    }

    @Override
    public List<Color> findByFiltro(Color color) throws BusinessException {
        return colorDao.findByFiltro(color);
    }

    @Override
    public List<Color> findByName(Color color) throws BusinessException {
        return colorDao.findByName(color);
    }
}