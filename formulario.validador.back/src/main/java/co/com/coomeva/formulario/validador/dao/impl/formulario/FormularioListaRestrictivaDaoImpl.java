package co.com.coomeva.formulario.validador.dao.impl.formulario;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import co.com.coomeva.formulario.validador.dao.formulario.FormularioEmpleadoDao;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioListaRestrictivaDao;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioDao;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioValorDao;
import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.dto.FormularioAgrCampoValueDTO;
import co.com.coomeva.formulario.validador.dto.InspektorResponseDTO;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioAgrCampo;
import co.com.coomeva.formulario.validador.model.FormularioEmpleado;
import co.com.coomeva.formulario.validador.model.FormularioListaRestrictiva;
import co.com.coomeva.formulario.validador.model.FormularioValor;
import co.com.coomeva.formulario.validador.model.InspektorDTO;
import co.com.coomeva.formulario.validador.util.ManagePropertiesLogic;
import co.com.coomeva.www.Empleado.generator.v1.ConsultaDatosEmpleadorRequest;
import co.com.coomeva.www.Empleado.generator.v1.ConsultaDatosEmpleadorResponse;
import co.com.coomeva.www.Empleado.generator.v1.binding.WSEmpleadoEmpleadoHttpService;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
* Formulario Dao Impl <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
@Repository
public class FormularioListaRestrictivaDaoImpl implements FormularioListaRestrictivaDao {
	
	public static String mensajeListaRestrictiva=null;
    
    @PersistenceContext
    private EntityManager em;
    public static final String FROM = "select a from FormularioListaRestrictiva a ";
    public static final String PROPERTIES = "general.properties";
    static ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
    
    public EntityManager getEntityManager() {
        return em;
    }


    @Override
    public List<FormularioListaRestrictiva> findAllActive() throws BusinessException {
        StringBuilder builder = new StringBuilder();
        Query query = this.getEntityManager().createQuery(builder.toString());
        return query.getResultList();
    }
    
    @Override
    public List<FormularioListaRestrictiva> findByFormularioListaRestrictiva(FormularioListaRestrictiva formulario) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.formularioListaRestrictivaId = :id ");

        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("id", formulario.getFormularioListaRestrictivaId());
        return query.getResultList();
    }

    @Override
    public List<FormularioListaRestrictiva> findByFiltro(FormularioListaRestrictiva formulario) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE 1=1 ");
        
        
        if(formulario.getFvFormulario() != null && formulario.getFvFormulario().getFormularioId() != null) {
            builder.append("AND a.fvFormulario.formularioId = :formularioId ");
        }
        

        
        if(formulario.getFormularioListaRestrictivaId() != null) {
            builder.append("AND a.formularioListaRestrictivaId = :formularioListaRestrictivaId ");
        }

        builder.append("order by a.numeroIdentificacion asc ");
        Query query = this.getEntityManager().createQuery(builder.toString());


        if(formulario.getFormularioListaRestrictivaId() != null) {
        	query.setParameter("formularioListaRestrictivaId", formulario.getFormularioListaRestrictivaId());
        }

        return query.getResultList();
    }


    public InspektorResponseDTO saveListasRestrictivas(FormularioAgrCampoValueDTO formularioAgrCampoValueDTO) throws BusinessException, IOException{
    	
    	String passInspector = managePropertiesLogic.getProperties(PROPERTIES, "passInspektor");
    	String appInspector = managePropertiesLogic.getProperties(PROPERTIES, "codigoAppInspector");
    	String negocioInspector = managePropertiesLogic.getProperties(PROPERTIES, "idNegocioInspector");
    	
    	InspektorResponseDTO inspektor = new InspektorResponseDTO();
    	Map<String, Object> params = new LinkedHashMap<>();
		params.put("numeroIdentificacion", formularioAgrCampoValueDTO.getFvAgrupacionCampo().getFvCampo().getValue());
		params.put("nombreCompleto", null);
		params.put("idNegocio", negocioInspector);
		params.put("contraseniaServicio", passInspector);
		params.put("codigoAplicacion", appInspector);

		ObjectMapper obj = new ObjectMapper();
		String jsonStr = obj.writeValueAsString(params);
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody requestBody = RequestBody.create(mediaType, jsonStr);

		OkHttpClient okHttpClient = new OkHttpClient().newBuilder().connectTimeout(60, TimeUnit.SECONDS)
				.readTimeout(60, TimeUnit.SECONDS).build();

		String urlInspektor= managePropertiesLogic.getProperties(PROPERTIES, "urlInspektor");
		Request requestHttp = new Request.Builder()
				.url(urlInspektor)
				.method("POST", requestBody).addHeader("Content-Type", "application/json").build();

		Response response = okHttpClient.newCall(requestHttp).execute();

		String responseInspektor = response.body().string();

		JSONObject json = new JSONObject(responseInspektor);

		Gson gson = new Gson();
		String messageList = "";
		String textoAccion = json.getString("Accion");
		if(textoAccion.equals("NO PERMITIR CONTINUAR CON EL PROCESO QUE ESTA REALIZANDO")
				|| textoAccion.equals("ENVIAR A FLUJO DE AUTORIZACION")) {
			messageList="El usuario se encuentra en la lista restrictiva";
			
		}
		JSONArray array = json.getJSONArray("Listas");
		ArrayList<InspektorDTO> listdata = new ArrayList<InspektorDTO>();
		if (!array.isEmpty()) {
			for (int i = 0; i < array.length(); i++) {
				listdata.add(gson.fromJson(array.getJSONObject(0).toString(), InspektorDTO.class));

			}
		}
		
		if(!listdata.isEmpty()) {
			inspektor.setInspektorList(listdata);
		}
		
		inspektor.setMessage(messageList);
		
		
		return inspektor;
		
		
    }
    
 public static void main(String[] args) {
	String mensaje ="Hola mundo\n";
	mensaje +="Mundial";
	
	System.out.print(mensaje);
}

}