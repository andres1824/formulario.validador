package co.com.coomeva.formulario.validador.model;

import java.util.ArrayList;

public class InspektorDTO {
	
	private Long Prioridad;
	private String Nombre;
	private Integer No;
	private String TipoDoc;
	private Long IdLista;
	private String NombreLista;
	private String NumDoc;
	
	public Long getPrioridad() {
		return Prioridad;
	}
	public void setPrioridad(Long prioridad) {
		Prioridad = prioridad;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public Integer getNo() {
		return No;
	}
	public void setNo(Integer no) {
		No = no;
	}
	public String getTipoDoc() {
		return TipoDoc;
	}
	public void setTipoDoc(String tipoDoc) {
		TipoDoc = tipoDoc;
	}
	public Long getIdLista() {
		return IdLista;
	}
	public void setIdLista(Long idLista) {
		IdLista = idLista;
	}
	public String getNombreLista() {
		return NombreLista;
	}
	public void setNombreLista(String nombreLista) {
		NombreLista = nombreLista;
	}
	public String getNumDoc() {
		return NumDoc;
	}
	public void setNumDoc(String numDoc) {
		NumDoc = numDoc;
	}
	
	
	
	//{"Prioridad":-99,"Nombre":"VILLASMIL BALLESTEROS JOSE SAUL","No":1,"TipoDoc":"Cédula","IdLista":1002,"NombreLista":"Lista Propia","NumDoc":"1237440505"}

}
