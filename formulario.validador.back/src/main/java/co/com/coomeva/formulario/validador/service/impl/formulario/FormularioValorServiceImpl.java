package co.com.coomeva.formulario.validador.service.impl.formulario;

import java.net.MalformedURLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.dao.formulario.FormularioAgrCampoRep;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioDao;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioRep;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioValorDao;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioValorRep;
import co.com.coomeva.formulario.validador.dto.FormularioAgrCampoValueDTO;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioAgrCampo;
import co.com.coomeva.formulario.validador.model.FormularioValor;
import co.com.coomeva.formulario.validador.service.formulario.FormularioService;
import co.com.coomeva.formulario.validador.service.formulario.FormularioValorService;

/**
* Formulario Service Impl <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
@Service("formularioValorService")
@Transactional
public class FormularioValorServiceImpl implements FormularioValorService {

    @Autowired
    private FormularioValorDao formularioDao;

    @Autowired
    private FormularioValorRep formularioRep;

    @Autowired
    private FormularioAgrCampoRep formularioAgrCampoRep;
    
    @Override
    public FormularioValor create(FormularioValor formulario) throws BusinessException {
        return formularioRep.save(formulario);
    }

    @Override
    public FormularioValor store(FormularioValor formulario) throws BusinessException {
       return formularioRep.save(formulario);
    }

    @Override
    public void remove(FormularioValor formulario) throws BusinessException {
        formularioRep.delete(formulario);
    }

    @Override
    public List<FormularioValor> findAll() throws BusinessException {
        return formularioRep.findAll();
    }

    @Override
    public List<FormularioValor> findAllActive() throws BusinessException {
        return formularioDao.findAllActive();
    }
    @Override
    public List<FormularioValor> findByFormulario(FormularioValor formulario) throws BusinessException {
        return formularioDao.findByFormularioValor(formulario);
    }

    @Override
    public List<FormularioValor> findByFiltro(FormularioValor formulario) throws BusinessException {
        return formularioDao.findByFiltro(formulario);
    }
    
	@Override
	public boolean validarAsociado(FormularioAgrCampoValueDTO formularioAgrCampoValueDTO) throws MalformedURLException {
		return formularioDao.validarAsociado(formularioAgrCampoValueDTO);
		
	}

    
}