package co.com.coomeva.formulario.validador.service.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Respuesta;

/**
* Respuesta Service <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
public interface RespuestaService {
    

    /**
     * Crea respuesta <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param respuesta
     * @return 
     * @throws BusinessException 
     */
    public Respuesta create(Respuesta respuesta) throws BusinessException;
    /**
     * Modifica respuesta <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param respuesta
     * @return 
     * @throws BusinessException 
     */
    public Respuesta store(Respuesta respuesta) throws BusinessException;
    /**
     * Elimina respuesta<br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param respuesta
     * @throws BusinessException 
     */
    public void remove(Respuesta respuesta) throws BusinessException;
    /**
     * Obtiene todos los registros de respuesta <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<Respuesta> findAll() throws BusinessException;
    
    /**
     * Obtiene todos los registros activos de respuesta <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<Respuesta> findAllActive() throws BusinessException;
    /**
     * Obtiene todos los registros de respuesta paginados <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param respuesta
     * @return
     * @throws BusinessException 
     */
    public List<Respuesta> findByRespuesta(Respuesta respuesta) throws BusinessException;   

    /**
     * Obtiene todos los registros de respuesta por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param respuesta
     * @return
     * @throws BusinessException 
     */
    public List<Respuesta> findByFiltro(Respuesta respuesta) throws BusinessException;
    /**
     * Obtiene todos los registros de respuesta por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param respuesta
     * @return
     * @throws Exception 
     */
    public List<Respuesta> findByValor(Respuesta respuesta) throws BusinessException;



    
}