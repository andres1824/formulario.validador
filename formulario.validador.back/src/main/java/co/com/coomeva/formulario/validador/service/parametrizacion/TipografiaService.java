package co.com.coomeva.formulario.validador.service.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Tipografia;

/**
* Tipografia Service <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
public interface TipografiaService {
    

    /**
     * Crea tipografia <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipografia
     * @return 
     * @throws BusinessException 
     */
    public Tipografia create(Tipografia tipografia) throws BusinessException;
    /**
     * Modifica tipografia <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipografia
     * @return 
     * @throws BusinessException 
     */
    public Tipografia store(Tipografia tipografia) throws BusinessException;
    /**
     * Elimina tipografia<br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipografia
     * @throws BusinessException 
     */
    public void remove(Tipografia tipografia) throws BusinessException;
    /**
     * Obtiene todos los registros de tipografia <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<Tipografia> findAll() throws BusinessException;
    
    /**
     * Obtiene todos los registros activos de tipografia <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<Tipografia> findAllActive() throws BusinessException;
    /**
     * Obtiene todos los registros de tipografia paginados <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipografia
     * @return
     * @throws BusinessException 
     */
    public List<Tipografia> findByTipografia(Tipografia tipografia) throws BusinessException;   

    /**
     * Obtiene todos los registros de tipografia por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipografia
     * @return
     * @throws BusinessException 
     */
    public List<Tipografia> findByFiltro(Tipografia tipografia) throws BusinessException; 


    /**
    * Obtiene todos los registros de tipografia por nombre <br>
    * Info. Creación: <br>
    * fecha 24 abr 2021 <br>
    * @author GTC
    * @param tipografia
    * @return
    * @throws BusinessException 
    */
    public List<Tipografia> findByName(Tipografia tipografia) throws BusinessException;  

    
}