package co.com.coomeva.formulario.validador.dao.parametrizacion;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.model.Color;
import co.com.coomeva.formulario.validador.model.RespuestaCampo;

/**
* Color Dao <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Repository
@Transactional
public interface RespuestaCampoRep extends JpaRepository<RespuestaCampo, Long>{

	@Query("SELECT p FROM RespuestaCampo p WHERE p.fvCampo.campoId = (:campoId)")
    public List<RespuestaCampo> find(@Param("campoId") Long campoId);
	
	@Query("SELECT p FROM RespuestaCampo p WHERE p.fvCampo.campoId = (:campoId) and p.fvRespuesta.respuestaId = (:respuestaId)")
    public List<RespuestaCampo> find(@Param("campoId") Long campoId, @Param("respuestaId") Long respuestaId);
}