package co.com.coomeva.formulario.validador.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the FV_EMPRESA database table.
 * 
 */
public class EmpresaDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long empresaId;

	private String estado;

	private Date fechaCreacion;

	private Date fechaModificacion;

	private String nombre;

	private String usuarioCreacion;

	private String usuarioModificacion;

	private List<FormularioDTO> fvFormularios;

	public EmpresaDTO() {
	}

	public Long getEmpresaId() {
		return this.empresaId;
	}

	public void setEmpresaId(Long empresaId) {
		this.empresaId = empresaId;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}
	
	@JsonIgnore
	public List<FormularioDTO> getFvFormularios() {
		return this.fvFormularios;
	}

	public void setFvFormularios(List<FormularioDTO> fvFormularios) {
		this.fvFormularios = fvFormularios;
	}

	public FormularioDTO addFvFormulario(FormularioDTO fvFormulario) {
		getFvFormularios().add(fvFormulario);
		fvFormulario.setFvEmpresa(this);

		return fvFormulario;
	}

	public FormularioDTO removeFvFormulario(FormularioDTO fvFormulario) {
		getFvFormularios().remove(fvFormulario);
		fvFormulario.setFvEmpresa(null);

		return fvFormulario;
	}

}