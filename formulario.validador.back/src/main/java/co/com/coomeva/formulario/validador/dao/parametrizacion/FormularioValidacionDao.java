package co.com.coomeva.formulario.validador.dao.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Campo;
import co.com.coomeva.formulario.validador.model.FormularioValidacion;

/**
* FormularioValidacion Dao <br>
* Info. Creación: <br>
* fecha 25 abr 2021 <br>
* @author GTC
**/
public interface FormularioValidacionDao  {
    
    /**
     * Obtiene todos los registros activos de formularioValidacion <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @return
     * @throws Exception 
     */
    public List<FormularioValidacion> findAllActive() throws BusinessException;

    /**
     * Obtiene todos los registros de formularioValidacion por su id <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @return
     * @throws Exception 
     */
    public List<FormularioValidacion> findByFormularioValidacion(FormularioValidacion formularioValidacion) throws BusinessException;   

    /**
     * Obtiene todos los registros de campo por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @return
     * @throws Exception 
     */
    public List<FormularioValidacion> findByFiltro(FormularioValidacion formularioValidacion) throws BusinessException; 


    public List<FormularioValidacion> findByIdFormulario(FormularioValidacion formularioValidacion) throws BusinessException; 
    
    public List<FormularioValidacion> findById(FormularioValidacion formularioValidacion) throws BusinessException;
}
