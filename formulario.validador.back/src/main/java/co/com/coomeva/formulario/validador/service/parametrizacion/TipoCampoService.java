package co.com.coomeva.formulario.validador.service.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.TipoCampo;

/**
* TipoCampo Service <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
public interface TipoCampoService {
    

    /**
     * Crea tipoCampo <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoCampo
     * @return 
     * @throws BusinessException 
     */
    public TipoCampo create(TipoCampo tipoCampo) throws BusinessException;
    /**
     * Modifica tipoCampo <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoCampo
     * @return 
     * @throws BusinessException 
     */
    public TipoCampo store(TipoCampo tipoCampo) throws BusinessException;
    /**
     * Elimina tipoCampo<br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoCampo
     * @throws BusinessException 
     */
    public void remove(TipoCampo tipoCampo) throws BusinessException;
    /**
     * Obtiene todos los registros de tipoCampo <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<TipoCampo> findAll() throws BusinessException;
    
    /**
     * Obtiene todos los registros activos de tipoCampo <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<TipoCampo> findAllActive() throws BusinessException;
    /**
     * Obtiene todos los registros de tipoCampo paginados <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoCampo
     * @return
     * @throws BusinessException 
     */
    public List<TipoCampo> findByTipoCampo(TipoCampo tipoCampo) throws BusinessException;   

    /**
     * Obtiene todos los registros de tipoCampo por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoCampo
     * @return
     * @throws BusinessException 
     */
    public List<TipoCampo> findByFiltro(TipoCampo tipoCampo) throws BusinessException; 


    /**
    * Obtiene todos los registros de tipoCampo por nombre <br>
    * Info. Creación: <br>
    * fecha 24 abr 2021 <br>
    * @author GTC
    * @param tipoCampo
    * @return
    * @throws BusinessException 
    */
    public List<TipoCampo> findByName(TipoCampo tipoCampo) throws BusinessException;  

    
}