package co.com.coomeva.formulario.validador.controller.parametrizacion;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.formulario.validador.dto.ParametroDTO;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.model.Parametro;
import co.com.coomeva.formulario.validador.service.parametrizacion.ParametroService;
import co.com.coomeva.formulario.validador.util.AppUtil;
import co.com.coomeva.formulario.validador.util.ManagePropertiesLogic;
import co.com.coomeva.formulario.validador.util.ResponseServiceUtil;


/**
* Parametro Controller <br>
* Info. Creación: <br>
* fecha 23 abr 2021 <br>
* @author GTC
**/

@RestController
@RequestMapping(value = "/api/parametro", produces = "application/json; charset=UTF-8")
@CrossOrigin(origins = "http://localhost:4200", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS })
public class ParametroController {

    @Autowired
    ParametroService parametroService;
    
    private static final Logger log = LoggerFactory.getLogger(ParametroController.class);
    ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
    public static final String PROPERTIES = "parametro.properties";

    /**
     * Crea parametro <br>
     * Info. Creación: <br>
     * fecha 23 abr 2021 <br>
     * @author GTC
     * @param parametro
     * @return
     */
    @PostMapping(value = "/create")
    public ResponseService createParametro(@RequestBody ParametroDTO parametroDTO) {
            ResponseService responseService = new ResponseService();
        try {
        	Parametro parametro = new Parametro();
        	parametro = (Parametro) AppUtil.convertOneToAnother(parametroDTO, parametro);
            Parametro parametroName = new Parametro();
            parametroName.setNombre(parametro.getNombre());
            List<Parametro> listParametroName = parametroService.findByName(parametroName);
            
            if(listParametroName != null && !listParametroName.isEmpty()) {
                ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                return responseService;
            }
            
            parametro.setFechaCreacion(new Date());
            parametroService.create(parametro);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroCreado"));
        } catch (Exception e) {
            log.error("Error en el servicio /parametro " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }

    /**
     * Modifica parametro <br>
     * Info. Creación: <br>
     * fecha 23 abr 2021 <br>
     * @author GTC
     * @param parametro
     * @return
     */
    @PostMapping(value = "/update")
    public ResponseService updateParametro(@RequestBody ParametroDTO parametroDTO) {
        ResponseService responseService = new ResponseService();
    try {
    		Parametro parametro = new Parametro();
    		parametro = (Parametro) AppUtil.convertOneToAnother(parametroDTO, parametro);
            Parametro parametroName = new Parametro();
            parametroName.setNombre(parametro.getNombre());
            List<Parametro> listParametroName = parametroService.findByName(parametroName);
            
            if(listParametroName != null && !listParametroName.isEmpty()) {
                boolean existName = false;
                for (Parametro parametro1 : listParametroName) {
                    if (!parametro1.getParametroId().equals(parametro.getParametroId())) {
                        existName = true;
                    }
                }

                if (existName) {
                    ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                    return responseService;
                }                
            }
            
            parametro.setFechaModificacion(new Date());
            parametroService.store(parametro);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEditado"));
        } catch (Exception e) {
            log.error("Error en el servicio /parametro/update " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Elimina parametro <br>
     * Info. Creación: <br>
     * fecha 23 abr 2021 <br>
     * @author GTC
     * @param parametro
     * @return
     */
    @PostMapping(value = "/delete")
    public ResponseService deleteParametro(@RequestBody ParametroDTO parametroDTO) {
        ResponseService responseService = new ResponseService();
    try {
	    	Parametro parametro = new Parametro();
	    	parametro = (Parametro) AppUtil.convertOneToAnother(parametroDTO, parametro);
            parametroService.remove(parametro);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEliminado"));
        } catch (Exception e) {
            log.error("Error en el servicio /parametro/delete " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Consulta parametro <br>
     * Info. Creación: <br>
     * fecha 23 abr 2021 <br>
     * @author GTC
     * @return
     */
    @GetMapping(value = "/get/all")
    public ResponseService list() {
        ResponseService responseService = new ResponseService();
        try {     
            List<Parametro> parametroList = parametroService.findAllActive();
            ResponseServiceUtil.buildSuccessResponse(responseService, parametroList);
        } catch (Exception e) {
            log.error("Error en el servicio /parametro/get/all " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

	/**
	 * Consulta parametro <br>
	 * Info. Creación: <br>
	 * fecha 23 abr 2021 <br>
	 * 
	 * @author GTC
	 * @return
	 */
	@PostMapping(value = "/find")
	public ResponseService find(@RequestBody ParametroDTO parametroDTO) {
		ResponseService responseService = new ResponseService();
		try {
			Parametro parametro = new Parametro();
			parametro = (Parametro) AppUtil.convertOneToAnother(parametroDTO, parametro);
			List<Parametro> parametroList = parametroService.findByFiltro(parametro);
			ResponseServiceUtil.buildSuccessResponse(responseService, parametroList);
		} catch (Exception e) {
			log.error("Error en el servicio /parametro/find " + e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);
		}
		return responseService;
	}
}
