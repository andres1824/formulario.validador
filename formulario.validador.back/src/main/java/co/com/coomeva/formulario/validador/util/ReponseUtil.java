package co.com.coomeva.formulario.validador.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import co.com.coomeva.formulario.validador.dto.ResponseDto;

public class ReponseUtil {
	
	public static ResponseEntity<?> responseFail(String error) {
		Map<String, String> responseBody = new HashMap<>();
		responseBody.put("responseCode", ConstanteRespuesta.RESPUESTA_NOEXITOSA);
		responseBody.put("responseDesc", ConstanteRespuesta.MSG_RESPUESTA_NOEXITOSA);
		responseBody.put("responseError", error);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseBody);
	}
	
	public static ResponseEntity<?> responseSuccessError(String error) {
		ResponseDto resp = new ResponseDto();
		resp.setResponseCode(ConstanteRespuesta.RESPUESTA_NOEXITOSA);
		resp.setResponseDesc(error);
		return ResponseEntity.ok(resp);
	}
}
