package co.com.coomeva.formulario.validador.dto;

import java.io.Serializable;


/**
 * The persistent class for the FV_FORMULARIO_AGR_CAMPO database table.
 * 
 */
public class FormularioAgrCampoValueDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long formularioAgrCampoId;
	
	private Long agrupacionId;

	private AgrupacionCampoValueDTO fvAgrupacionCampo;

	private FormularioDTO fvFormulario;

	public FormularioAgrCampoValueDTO() {
	}

	public Long getAgrupacionId() {
		return agrupacionId;
	}

	public void setAgrupacionId(Long agrupacionId) {
		this.agrupacionId = agrupacionId;
	}

	public Long getFormularioAgrCampoId() {
		return this.formularioAgrCampoId;
	}

	public void setFormularioAgrCampoId(Long formularioAgrCampoId) {
		this.formularioAgrCampoId = formularioAgrCampoId;
	}

	public AgrupacionCampoValueDTO getFvAgrupacionCampo() {
		return this.fvAgrupacionCampo;
	}

	public void setFvAgrupacionCampo(AgrupacionCampoValueDTO fvAgrupacionCampo) {
		this.fvAgrupacionCampo = fvAgrupacionCampo;
	}

	public FormularioDTO getFvFormulario() {
		return this.fvFormulario;
	}

	public void setFvFormulario(FormularioDTO fvFormulario) {
		this.fvFormulario = fvFormulario;
	}

}