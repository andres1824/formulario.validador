package co.com.coomeva.formulario.validador.service.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.TipoReporte;

/**
* TipoReporte Service <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
public interface TipoReporteService {
    

    /**
     * Crea tipoReporte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoReporte
     * @return 
     * @throws BusinessException 
     */
    public TipoReporte create(TipoReporte tipoReporte) throws BusinessException;
    /**
     * Modifica tipoReporte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoReporte
     * @return 
     * @throws BusinessException 
     */
    public TipoReporte store(TipoReporte tipoReporte) throws BusinessException;
    /**
     * Elimina tipoReporte<br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoReporte
     * @throws BusinessException 
     */
    public void remove(TipoReporte tipoReporte) throws BusinessException;
    /**
     * Obtiene todos los registros de tipoReporte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<TipoReporte> findAll() throws BusinessException;
    
    /**
     * Obtiene todos los registros activos de tipoReporte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<TipoReporte> findAllActive() throws BusinessException;
    /**
     * Obtiene todos los registros de tipoReporte paginados <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoReporte
     * @return
     * @throws BusinessException 
     */
    public List<TipoReporte> findByTipoReporte(TipoReporte tipoReporte) throws BusinessException;   

    /**
     * Obtiene todos los registros de tipoReporte por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoReporte
     * @return
     * @throws BusinessException 
     */
    public List<TipoReporte> findByFiltro(TipoReporte tipoReporte) throws BusinessException; 


    /**
    * Obtiene todos los registros de tipoReporte por nombre <br>
    * Info. Creación: <br>
    * fecha 24 abr 2021 <br>
    * @author GTC
    * @param tipoReporte
    * @return
    * @throws BusinessException 
    */
    public List<TipoReporte> findByName(TipoReporte tipoReporte) throws BusinessException;  

    
}