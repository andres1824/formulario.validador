package co.com.coomeva.formulario.validador.util;

import javax.mail.MessagingException;

import co.com.coomeva.formulario.validador.exception.BusinessException;

public interface MainService {
	public boolean sendEmail(String toEmail, String mailSubject, String mailBody)
			throws BusinessException, MessagingException;
}
