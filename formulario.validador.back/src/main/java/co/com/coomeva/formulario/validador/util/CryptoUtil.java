package co.com.coomeva.formulario.validador.util;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class CryptoUtil {
	
	public static final String CRYPTO_KEY = "Q2FqYUhlcnJhbWllbnRhcw==";
	
	public static String encrypt(String value, String privateKey) throws UnsupportedEncodingException, InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		IvParameterSpec iv = new IvParameterSpec(privateKey.getBytes(StandardCharsets.UTF_8));
		SecretKeySpec skeySpec = new SecretKeySpec(privateKey.getBytes(StandardCharsets.UTF_8), "AES");
		final String transform = "AES/CBC/PKCS5PADDING";
		Cipher cipher = Cipher.getInstance(transform);
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

		byte[] encrypted = cipher.doFinal(value.getBytes());
		return Base64.getEncoder().encodeToString(encrypted);

	}
	
	public static String decrypt(String encrypted, String privateKey) throws UnsupportedEncodingException, InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		IvParameterSpec iv = new IvParameterSpec(privateKey.getBytes(StandardCharsets.UTF_8));
		SecretKeySpec skeySpec = new SecretKeySpec(privateKey.getBytes(StandardCharsets.UTF_8), "AES");
		final String transform = "AES/CBC/PKCS5PADDING";
		Cipher cipher = Cipher.getInstance(transform);
		cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
		byte[] original = cipher.doFinal(Base64.getDecoder().decode(encrypted));

		return new String(original);

	}
	
	public static String getSecretKey() {
		ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
		return managePropertiesLogic.getProperties("general.properties", "CRYPTO_SECRET_KEY");
	}

}
