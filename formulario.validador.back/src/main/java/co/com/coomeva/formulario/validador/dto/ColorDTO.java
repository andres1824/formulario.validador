package co.com.coomeva.formulario.validador.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the FV_COLOR database table.
 * 
 */
public class ColorDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long colorId;

	private String codigoHtml;

	private String nombre;

	private List<FormularioDTO> fvFormularios;
	
	private String estado;
	private Date fechaCreacion;
	private Date fechaModificacion;
	private String usuarioCreacion;
	private String usuarioModificacion;

	public ColorDTO() {
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public Long getColorId() {
		return this.colorId;
	}

	public void setColorId(Long colorId) {
		this.colorId = colorId;
	}

	public String getCodigoHtml() {
		return this.codigoHtml;
	}

	public void setCodigoHtml(String codigoHtml) {
		this.codigoHtml = codigoHtml;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@JsonIgnore
	public List<FormularioDTO> getFvFormularios() {
		return this.fvFormularios;
	}

	public void setFvFormularios(List<FormularioDTO> fvFormularios) {
		this.fvFormularios = fvFormularios;
	}

	public FormularioDTO addFvFormulario(FormularioDTO fvFormulario) {
		getFvFormularios().add(fvFormulario);
		fvFormulario.setFvColor(this);

		return fvFormulario;
	}

	public FormularioDTO removeFvFormulario(FormularioDTO fvFormulario) {
		getFvFormularios().remove(fvFormulario);
		fvFormulario.setFvColor(null);

		return fvFormulario;
	}

}