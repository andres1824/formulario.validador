package co.com.coomeva.formulario.validador.controller.parametrizacion;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.formulario.validador.dto.CampoDTO;
import co.com.coomeva.formulario.validador.dto.EmpresaDTO;
import co.com.coomeva.formulario.validador.dto.FormularioDTO;
import co.com.coomeva.formulario.validador.dto.FormularioValidadorDTO;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.RespuestaCampoDTO;
import co.com.coomeva.formulario.validador.model.Campo;
import co.com.coomeva.formulario.validador.model.Empresa;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioValidacion;
import co.com.coomeva.formulario.validador.model.RespuestaCampo;
import co.com.coomeva.formulario.validador.service.parametrizacion.CampoService;
import co.com.coomeva.formulario.validador.service.parametrizacion.FormularioValidacionService;
import co.com.coomeva.formulario.validador.util.AppUtil;
import co.com.coomeva.formulario.validador.util.ManagePropertiesLogic;
import co.com.coomeva.formulario.validador.util.ResponseServiceUtil;


/**
* Campo Controller <br>
* Info. Creación: <br>
* fecha 25 abr 2021 <br>
* @author GTC
**/

@RestController
@RequestMapping(value = "/api/formularioValicion", produces = "application/json; charset=UTF-8")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class FormularioValidacionController {

    @Autowired
    FormularioValidacionService formularioValidacionService;
    
    private static final Logger log = LoggerFactory.getLogger(FormularioValidacionController.class);
    ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
    public static final String PROPERTIES = "formularioValidacion.properties";


    
    @PostMapping(value = "/create")
    public ResponseService createFormularioValidacion(@RequestBody FormularioValidadorDTO formularioValidadorDTO) {
            ResponseService responseService = new ResponseService();
        try {
            FormularioValidacion formularioValidacion = new FormularioValidacion();
            formularioValidacion = (FormularioValidacion) AppUtil.convertOneToAnother(formularioValidadorDTO, formularioValidacion);
            FormularioValidacion formularioValidacionIdFormulario= new FormularioValidacion();
            formularioValidacionIdFormulario.setFvFormulario(formularioValidacion.getFvFormulario());
            List<FormularioValidacion> listFormularioValidacion = formularioValidacionService.findByIdFormulario(formularioValidacionIdFormulario);
            
            if(listFormularioValidacion != null && !listFormularioValidacion.isEmpty()) {
                ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgFormularioExiste"));
                return responseService;
            }
            formularioValidacion.setFechaCreacion(new Date());
            
            formularioValidacionService.create(formularioValidacion);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroCreado"));
        } catch (Exception e) {
            log.error("Error en el servicio /FormularioValidacion " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }
    
    @PostMapping(value = "/update")
    public ResponseService updateFormularioValidacion(@RequestBody FormularioValidadorDTO formularioValidadorDTO) {
       ResponseService responseService = new ResponseService();
       try {
    	   	boolean theSame=false;
            FormularioValidacion formulario= new FormularioValidacion();
            formulario = (FormularioValidacion) AppUtil.convertOneToAnother(formularioValidadorDTO, formulario);
            
            FormularioValidacion formularioValidacionIdFormulario= new FormularioValidacion();
            formularioValidacionIdFormulario.setFormularioValidacionId(formularioValidadorDTO.getFormularioValidacionId());
            formularioValidacionIdFormulario.setFvFormulario(formulario.getFvFormulario());
            
            List<FormularioValidacion> listFormularioId = formularioValidacionService.findById(formularioValidacionIdFormulario);
            List<FormularioValidacion> listFormularioValidacion = formularioValidacionService.findByIdFormulario(formularioValidacionIdFormulario);
          
            if(listFormularioId != null && !listFormularioId.isEmpty()) {
            	
            	if(listFormularioId.get(0).getFvFormulario().getFormularioId()==formularioValidadorDTO.getFvFormulario().getFormularioId()) {
            		theSame=true;
            	}
            }
    
            if(listFormularioValidacion != null && !listFormularioValidacion.isEmpty() && !theSame) {
            	
                ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgFormularioExiste"));
                return responseService;
            }
            
           
            
            formulario.setFechaModificacion(new Date());
            formulario = formularioValidacionService.store(formulario);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEditado"), formulario);
        } catch (Exception e) {
            log.error("Error en el servicio /formularioValicion/update " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    
    /**
     * Consulta campo <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @return
     */
    @GetMapping(value = "/get/all")
    public ResponseService list() {
        ResponseService responseService = new ResponseService();
        try {     
            List<FormularioValidacion> campoList = formularioValidacionService.findAllActive();
            ResponseServiceUtil.buildSuccessResponse(responseService, campoList);
        } catch (Exception e) {
            log.error("Error en el servicio /formularioValicion/get/all " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Consulta empresa <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     */
    @PostMapping(value = "/find")
    public ResponseService find(@RequestBody FormularioValidadorDTO formularioValidadorDTO) {
       ResponseService responseService = new ResponseService();
       try {
            FormularioValidacion formularioValidacion = new FormularioValidacion();
            formularioValidacion = (FormularioValidacion) AppUtil.convertOneToAnother(formularioValidadorDTO, formularioValidacion);
            List<FormularioValidacion> formularioValidacionList = formularioValidacionService.findByFiltro(formularioValidacion);
            ResponseServiceUtil.buildSuccessResponse(responseService, formularioValidacionList);
        } catch (Exception e) {
            log.error("Error en el servicio /formularioValicion/find " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

 
}
