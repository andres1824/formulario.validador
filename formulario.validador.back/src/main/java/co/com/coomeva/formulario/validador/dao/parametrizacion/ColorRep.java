package co.com.coomeva.formulario.validador.dao.parametrizacion;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.model.Color;

/**
* Color Dao <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Repository
@Transactional
public interface ColorRep extends JpaRepository<Color, Long>{


}