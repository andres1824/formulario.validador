package co.com.coomeva.formulario.validador.dao.impl.parametrizacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import co.com.coomeva.formulario.validador.dao.parametrizacion.CausalDescarteDao;
import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.CausalDescarte;

/**
* CausalDescarte Dao Impl <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Repository
public class CausalDescarteDaoImpl implements CausalDescarteDao {
    
    @PersistenceContext
    private EntityManager em;
    public static final String FROM = "select a from CausalDescarte a ";
	
	
    public EntityManager getEntityManager() {
        return em;
    }

    @Override
    public List<CausalDescarte> findAllActive() throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.estado = :active ");

        builder.append("order by a.nombre asc ");
        
        
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("active", (EstadoEnum.ACTIVE.getId()));
        return query.getResultList();
    }
    
    @Override
    public List<CausalDescarte> findByCausalDescarte(CausalDescarte causalDescarte) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.causalDescarteId = :id ");

        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("id", causalDescarte.getCausalDescarteId());
        return query.getResultList();
    }

    @Override
    public List<CausalDescarte> findByFiltro(CausalDescarte causalDescarte) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE 1=1 ");
        
        if(causalDescarte.getNombre() != null && !causalDescarte.getNombre().trim().isEmpty()) {
            builder.append("AND lower(a.nombre) like lower(:nombre) ");
        }




        if(causalDescarte.getEstado() != null && !causalDescarte.getEstado().trim().isEmpty()) {
            builder.append("AND a.estado = :estado ");
        }

        builder.append("order by a.nombre asc ");
        Query query = this.getEntityManager().createQuery(builder.toString());

        if(causalDescarte.getNombre() != null && !causalDescarte.getNombre().trim().isEmpty()) {
            query.setParameter("nombre", "%" + causalDescarte.getNombre() + "%");
        }




        if(causalDescarte.getEstado() != null && !causalDescarte.getEstado().trim().isEmpty()) {
            query.setParameter("estado", causalDescarte.getEstado());
        }

        return query.getResultList();
    }

    @Override
    public List<CausalDescarte> findByName(CausalDescarte causalDescarte) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE lower(trim(a.nombre)) = lower(trim(:nombre)) ");
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("nombre", causalDescarte.getNombre().trim());
        return query.getResultList();
    }
}