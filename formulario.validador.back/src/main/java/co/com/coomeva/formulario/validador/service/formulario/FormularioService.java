package co.com.coomeva.formulario.validador.service.formulario;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioAgrCampo;

/**
* Formulario Service <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
public interface FormularioService {
    

    /**
     * Crea formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return 
     * @throws BusinessException 
     */
    public Formulario create(Formulario formulario) throws BusinessException;
    /**
     * Modifica formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return 
     * @throws BusinessException 
     */
    public Formulario store(Formulario formulario) throws BusinessException;
    /**
     * Elimina formulario<br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @throws BusinessException 
     */
    public void remove(Formulario formulario) throws BusinessException;
    /**
     * Obtiene todos los registros de formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<Formulario> findAll() throws BusinessException;
    
    /**
     * Obtiene todos los registros activos de formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<Formulario> findAllActive() throws BusinessException;
    /**
     * Obtiene todos los registros de formulario paginados <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     * @throws BusinessException 
     */
    public List<Formulario> findByFormulario(Formulario formulario) throws BusinessException;   

    /**
     * Obtiene todos los registros de formulario por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     * @throws BusinessException 
     */
    public List<Formulario> findByFiltro(Formulario formulario) throws BusinessException; 


    /**
    * Obtiene todos los registros de formulario por nombre <br>
    * Info. Creación: <br>
    * fecha 1 may 2021 <br>
    * @author GTC
    * @param formulario
    * @return
    * @throws BusinessException 
    */
    public List<Formulario> findByName(Formulario formulario) throws BusinessException;  
    
    /**
     * Obtiene todos los registros de formularioAgrupacionCampo por formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     * @throws BusinessException 
     */
     public List<FormularioAgrCampo> findByFormulario(Long formularioId) throws BusinessException;  
     
     /**
      * Crea formularioAgrCampo <br>
      * Info. Creación: <br>
      * fecha 1 may 2021 <br>
      * @author GTC
      * @param formulario
      * @return 
      * @throws BusinessException 
      */
     public FormularioAgrCampo createAgrupacionCampo(FormularioAgrCampo formulario) throws BusinessException;
     /**
      * Elimina formulario<br>
      * Info. Creación: <br>
      * fecha 1 may 2021 <br>
      * @author GTC
      * @param formulario
      * @throws BusinessException 
      */
     public void removeAgrupacionCampo(FormularioAgrCampo formulario) throws BusinessException;
     
     /**
      * Obtiene todos los registros de formularioAgrupacionCampo por formulario <br>
      * Info. Creación: <br>
      * fecha 1 may 2021 <br>
      * @author GTC
      * @param formulario
      * @return
      * @throws BusinessException 
      */
      public List<Formulario> findByFormularioAgrupacionCampo(FormularioAgrCampo formularioId) throws BusinessException;  
      

    
}