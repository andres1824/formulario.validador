package co.com.coomeva.formulario.validador.dao.parametrizacion;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.model.Agrupacion;

/**
* Agrupacion Dao <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
@Repository
@Transactional
public interface AgrupacionRep extends JpaRepository<Agrupacion, Long>{


}