package co.com.coomeva.formulario.validador.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the FV_TIPO_DATO database table.
 * 
 */
@Entity
@Table(name="FV_TIPO_DATO")
@NamedQuery(name="TipoDato.findAll", query="SELECT t FROM TipoDato t")
public class TipoDato implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @Basic(optional = false)
    @GeneratedValue(generator = "fv_tipo_dato_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "fv_tipo_dato_seq", sequenceName = "fv_tipo_dato_seq",allocationSize=1)    
	@Column(name="TIPO_DATO_ID")
	private Long tipoDatoId;

	private String logica;

	private String nombre;
	
	private String estado;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_MODIFICACION")
	private Date fechaModificacion;


	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;

	@Column(name="USUARIO_MODIFICACION")
	private String usuarioModificacion;

	//bi-directional many-to-one association to Campo
	@OneToMany(mappedBy="fvTipoDato")
	private List<Campo> fvCampos;

	public TipoDato() {
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public Long getTipoDatoId() {
		return this.tipoDatoId;
	}

	public void setTipoDatoId(Long tipoDatoId) {
		this.tipoDatoId = tipoDatoId;
	}

	public String getLogica() {
		return this.logica;
	}

	public void setLogica(String logica) {
		this.logica = logica;
	}

	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@JsonIgnore
	public List<Campo> getFvCampos() {
		return this.fvCampos;
	}

	public void setFvCampos(List<Campo> fvCampos) {
		this.fvCampos = fvCampos;
	}

	public Campo addFvCampo(Campo fvCampo) {
		getFvCampos().add(fvCampo);
		fvCampo.setFvTipoDato(this);

		return fvCampo;
	}

	public Campo removeFvCampo(Campo fvCampo) {
		getFvCampos().remove(fvCampo);
		fvCampo.setFvTipoDato(null);

		return fvCampo;
	}

}