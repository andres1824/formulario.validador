package co.com.coomeva.formulario.validador.dao.impl.formulario;

import java.net.URL;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import co.com.coomeva.formulario.validador.dao.formulario.FormularioEmpleadoDao;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioDao;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioValorDao;
import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.dto.FormularioAgrCampoValueDTO;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioAgrCampo;
import co.com.coomeva.formulario.validador.model.FormularioEmpleado;
import co.com.coomeva.formulario.validador.model.FormularioValor;
import co.com.coomeva.www.Empleado.generator.v1.ConsultaDatosEmpleadorRequest;
import co.com.coomeva.www.Empleado.generator.v1.ConsultaDatosEmpleadorResponse;
import co.com.coomeva.www.Empleado.generator.v1.binding.WSEmpleadoEmpleadoHttpService;

/**
* Formulario Dao Impl <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
@Repository
public class FormularioEmpleadoDaoImpl implements FormularioEmpleadoDao {
    
    @PersistenceContext
    private EntityManager em;
    public static final String FROM = "select a from FormularioEmpleado a ";
	
	
    public EntityManager getEntityManager() {
        return em;
    }


    @Override
    public List<FormularioEmpleado> findAllActive() throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.estado = :active ");
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("active", (EstadoEnum.ACTIVE.getId()));
        return query.getResultList();
    }
    
    @Override
    public List<FormularioEmpleado> findByFormularioAsociado(FormularioEmpleado formulario) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.formularioEmpleadoId = :id ");

        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("id", formulario.getFormularioEmpleadoId());
        return query.getResultList();
    }

    @Override
    public List<FormularioEmpleado> findByFiltro(FormularioEmpleado formulario) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE 1=1 ");
        
        
        if(formulario.getFvFormulario() != null && formulario.getFvFormulario().getFormularioId() != null) {
            builder.append("AND a.fvFormulario.formularioId = :formularioId ");
        }
        

        
        if(formulario.getFormularioEmpleadoId() != null) {
            builder.append("AND a.formularioEmpleadoId = :formularioEmpleadoId ");
        }

        builder.append("order by a.numeroIdentificacion asc ");
        Query query = this.getEntityManager().createQuery(builder.toString());


        if(formulario.getFormularioEmpleadoId() != null) {
        	query.setParameter("formularioEmpleadoId", formulario.getFormularioEmpleadoId());
        }

        return query.getResultList();
    }


	@Override
	public boolean validarAsociado(FormularioAgrCampoValueDTO formularioAgrCampoValueDTO) {

		if (formularioAgrCampoValueDTO.getFvAgrupacionCampo().getFvCampo().getCampoId()==1L || 
				formularioAgrCampoValueDTO.getFvAgrupacionCampo().getFvCampo().getCampoId()==7L) {
			
			  WSEmpleadoEmpleadoHttpService wsEmpleadoEmpleadoHttpService=new 
		        		WSEmpleadoEmpleadoHttpService();
		        ConsultaDatosEmpleadorRequest empleadorRequest=new ConsultaDatosEmpleadorRequest();
		        empleadorRequest.setAplicativoOrigen("1");
		        empleadorRequest.setCedulaEmpleado(formularioAgrCampoValueDTO.getFvAgrupacionCampo().getFvCampo().getValue());
		        
		        ConsultaDatosEmpleadorResponse consultaDatosEmpleadorResponse=wsEmpleadoEmpleadoHttpService.getWSEmpleadoEmpleadoHttpPort().consultaDatosEmpleador(empleadorRequest);
		        
				if(consultaDatosEmpleadorResponse.getEmpleado()==null) {
					return false;
				}
				else {
					return true;
				}
			
			
			
		}
		return false;
      
		
	}



}