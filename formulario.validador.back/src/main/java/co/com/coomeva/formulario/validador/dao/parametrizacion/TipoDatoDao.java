package co.com.coomeva.formulario.validador.dao.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.TipoDato;

/**
* TipoDato Dao <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
public interface TipoDatoDao  {
    
    /**
     * Obtiene todos los registros activos de tipoDato <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws Exception 
     */
    public List<TipoDato> findAllActive() throws BusinessException;

    /**
     * Obtiene todos los registros de tipoDato por su id <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoDato
     * @return
     * @throws Exception 
     */
    public List<TipoDato> findByTipoDato(TipoDato tipoDato) throws BusinessException;   

    /**
     * Obtiene todos los registros de tipoDato por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoDato
     * @return
     * @throws Exception 
     */
    public List<TipoDato> findByFiltro(TipoDato tipoDato) throws BusinessException; 

    /**
    * Obtiene todos los registros de tipoDato por nombre <br>
    * Info. Creación: <br>
    * fecha 24 abr 2021 <br>
    * @author GTC
    * @param tipoDato
    * @return
    * @throws Exception 
    */
    public List<TipoDato> findByName(TipoDato tipoDato) throws BusinessException;  
}
