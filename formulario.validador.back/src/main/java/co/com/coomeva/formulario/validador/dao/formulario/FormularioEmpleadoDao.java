package co.com.coomeva.formulario.validador.dao.formulario;

import java.util.List;

import co.com.coomeva.formulario.validador.dto.FormularioAgrCampoValueDTO;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioAgrCampo;
import co.com.coomeva.formulario.validador.model.FormularioEmpleado;
import co.com.coomeva.formulario.validador.model.FormularioValor;

/**
* Formulario Dao <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
public interface FormularioEmpleadoDao  {
    
    /**
     * Obtiene todos los registros activos de formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     * @throws Exception 
     */
    public List<FormularioEmpleado> findAllActive() throws BusinessException;

    /**
     * Obtiene todos los registros de formulario por su id <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     * @throws Exception 
     */
    public List<FormularioEmpleado> findByFormularioAsociado(FormularioEmpleado formularioAsociado) throws BusinessException;   

    /**
     * Obtiene todos los registros de formulario por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     * @throws Exception 
     */
    public List<FormularioEmpleado> findByFiltro(FormularioEmpleado formularioAsociado) throws BusinessException; 
    public boolean validarAsociado(FormularioAgrCampoValueDTO formularioAgrCampoValueDTO);

     
}
