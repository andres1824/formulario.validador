package co.com.coomeva.formulario.validador.service.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Parametro;

/**
* Parametro Service <br>
* Info. Creación: <br>
* fecha 23 abr 2021 <br>
* @author GTC
**/
public interface ParametroService {
    

    /**
     * Crea parametro <br>
     * Info. Creación: <br>
     * fecha 23 abr 2021 <br>
     * @author GTC
     * @param parametro
     * @return 
     * @throws BusinessException 
     */
    public Parametro create(Parametro parametro) throws BusinessException;
    /**
     * Modifica parametro <br>
     * Info. Creación: <br>
     * fecha 23 abr 2021 <br>
     * @author GTC
     * @param parametro
     * @return 
     * @throws BusinessException 
     */
    public Parametro store(Parametro parametro) throws BusinessException;
    /**
     * Elimina parametro<br>
     * Info. Creación: <br>
     * fecha 23 abr 2021 <br>
     * @author GTC
     * @param parametro
     * @throws BusinessException 
     */
    public void remove(Parametro parametro) throws BusinessException;
    /**
     * Obtiene todos los registros de parametro <br>
     * Info. Creación: <br>
     * fecha 23 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<Parametro> findAll() throws BusinessException;
    
    /**
     * Obtiene todos los registros activos de parametro <br>
     * Info. Creación: <br>
     * fecha 23 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<Parametro> findAllActive() throws BusinessException;
    /**
     * Obtiene todos los registros de parametro paginados <br>
     * Info. Creación: <br>
     * fecha 23 abr 2021 <br>
     * @author GTC
     * @param parametro
     * @return
     * @throws BusinessException 
     */
    public List<Parametro> findByParametro(Parametro parametro) throws BusinessException;   

    /**
     * Obtiene todos los registros de parametro por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 23 abr 2021 <br>
     * @author GTC
     * @param parametro
     * @return
     * @throws BusinessException 
     */
    public List<Parametro> findByFiltro(Parametro parametro) throws BusinessException; 


    /**
    * Obtiene todos los registros de parametro por nombre <br>
    * Info. Creación: <br>
    * fecha 23 abr 2021 <br>
    * @author GTC
    * @param parametro
    * @return
    * @throws BusinessException 
    */
    public List<Parametro> findByName(Parametro parametro) throws BusinessException;  

    
}