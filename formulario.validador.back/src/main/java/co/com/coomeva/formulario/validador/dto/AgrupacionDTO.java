package co.com.coomeva.formulario.validador.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the FV_AGRUPACION database table.
 * 
 */
public class AgrupacionDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long agrupacionId;

	private String estado;
	
	private String descripcion;

	private Date fechaCreacion;

	private Date fechaModificacion;

	private String nombre;

	private String usuarioCreacion;

	private String usuarioModificacion;

	private List<AgrupacionCampoDTO> fvAgrupacionCampos;

	public AgrupacionDTO() {
	}

	public Long getAgrupacionId() {
		return this.agrupacionId;
	}

	public void setAgrupacionId(Long agrupacionId) {
		this.agrupacionId = agrupacionId;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}
	
	@JsonIgnore
	public List<AgrupacionCampoDTO> getFvAgrupacionCampos() {
		return this.fvAgrupacionCampos;
	}

	public void setFvAgrupacionCampos(List<AgrupacionCampoDTO> fvAgrupacionCampos) {
		this.fvAgrupacionCampos = fvAgrupacionCampos;
	}

	public AgrupacionCampoDTO addFvAgrupacionCampo(AgrupacionCampoDTO fvAgrupacionCampo) {
		getFvAgrupacionCampos().add(fvAgrupacionCampo);
		fvAgrupacionCampo.setFvAgrupacion(this);

		return fvAgrupacionCampo;
	}

	public AgrupacionCampoDTO removeFvAgrupacionCampo(AgrupacionCampoDTO fvAgrupacionCampo) {
		getFvAgrupacionCampos().remove(fvAgrupacionCampo);
		fvAgrupacionCampo.setFvAgrupacion(null);

		return fvAgrupacionCampo;
	}

}