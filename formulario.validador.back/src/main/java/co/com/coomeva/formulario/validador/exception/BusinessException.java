/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.coomeva.formulario.validador.exception;

/**
 *
 * @author aflr0108
 */
public class BusinessException extends Exception {

	private static final long serialVersionUID = 1797656298366312786L;

	public BusinessException(Throwable cause) {
		super(cause);
	}

	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}

	public BusinessException(String message) {
		super(message);
	}

	public BusinessException() {
	}
}
