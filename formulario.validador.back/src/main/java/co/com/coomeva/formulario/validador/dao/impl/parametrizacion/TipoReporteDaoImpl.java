package co.com.coomeva.formulario.validador.dao.impl.parametrizacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import co.com.coomeva.formulario.validador.dao.parametrizacion.TipoReporteDao;
import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.TipoReporte;

/**
* TipoReporte Dao Impl <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Repository
public class TipoReporteDaoImpl implements TipoReporteDao {
    
    @PersistenceContext
    private EntityManager em;
    public static final String FROM = "select a from TipoReporte a ";
	
	
    public EntityManager getEntityManager() {
        return em;
    }


    @Override
    public List<TipoReporte> findAllActive() throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.estado = :active ");

        builder.append("order by a.nombre asc ");
        
        
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("active", (EstadoEnum.ACTIVE.getId()));
        return query.getResultList();
    }
    
    @Override
    public List<TipoReporte> findByTipoReporte(TipoReporte tipoReporte) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.tipoReporteId = :id ");

        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("id", tipoReporte.getTipoReporteId());
        return query.getResultList();
    }

    @Override
    public List<TipoReporte> findByFiltro(TipoReporte tipoReporte) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE 1=1 ");
        
        if(tipoReporte.getNombre() != null && !tipoReporte.getNombre().trim().isEmpty()) {
            builder.append("AND lower(a.nombre) like lower(:nombre) ");
        }




        if(tipoReporte.getEstado() != null && !tipoReporte.getEstado().trim().isEmpty()) {
            builder.append("AND a.estado = :estado ");
        }

        builder.append("order by a.nombre asc ");
        Query query = this.getEntityManager().createQuery(builder.toString());

        if(tipoReporte.getNombre() != null && !tipoReporte.getNombre().trim().isEmpty()) {
            query.setParameter("nombre", "%" + tipoReporte.getNombre() + "%");
        }




        if(tipoReporte.getEstado() != null && !tipoReporte.getEstado().trim().isEmpty()) {
            query.setParameter("estado", tipoReporte.getEstado());
        }

        return query.getResultList();
    }

    @Override
    public List<TipoReporte> findByName(TipoReporte tipoReporte) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE lower(trim(a.nombre)) = lower(trim(:nombre)) ");
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("nombre", tipoReporte.getNombre().trim());
        return query.getResultList();
    }
}