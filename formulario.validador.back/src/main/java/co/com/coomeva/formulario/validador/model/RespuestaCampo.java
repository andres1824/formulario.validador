package co.com.coomeva.formulario.validador.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the FV_RESPUESTA_CAMPO database table.
 * 
 */
@Entity
@Table(name="FV_RESPUESTA_CAMPO")
@NamedQuery(name="RespuestaCampo.findAll", query="SELECT r FROM RespuestaCampo r")
public class RespuestaCampo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @Basic(optional = false)
    @GeneratedValue(generator = "fv_respuesta_campo_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "fv_respuesta_campo_seq", sequenceName = "fv_respuesta_campo_seq",allocationSize=1)    
	@Column(name="RESPUESTA_CAMPO_ID")
	private Long respuestaCampoId;

	@Column(name="RESPUESTA_CORRECTA")
	private String respuestaCorrecta;

	//bi-directional many-to-one association to Campo
	@ManyToOne
	@JoinColumn(name="CAMPO_ID")
	private Campo fvCampo;
	
	@ManyToOne
	@JoinColumn(name="CAMPO_DEPENDIENTE_ID")
	private Campo fvCampoDependiente;

	//bi-directional many-to-one association to Respuesta
	@ManyToOne
	@JoinColumn(name="RESPUESTA_ID")
	private Respuesta fvRespuesta;

	public RespuestaCampo() {
	}

	public Long getRespuestaCampoId() {
		return this.respuestaCampoId;
	}

	public void setRespuestaCampoId(Long respuestaCampoId) {
		this.respuestaCampoId = respuestaCampoId;
	}

	public String getRespuestaCorrecta() {
		return this.respuestaCorrecta;
	}

	public void setRespuestaCorrecta(String respuestaCorrecta) {
		this.respuestaCorrecta = respuestaCorrecta;
	}

	public Campo getFvCampo() {
		return this.fvCampo;
	}

	public void setFvCampo(Campo fvCampo) {
		this.fvCampo = fvCampo;
	}

	public Respuesta getFvRespuesta() {
		return this.fvRespuesta;
	}

	public void setFvRespuesta(Respuesta fvRespuesta) {
		this.fvRespuesta = fvRespuesta;
	}

	public Campo getFvCampoDependiente() {
		return fvCampoDependiente;
	}

	public void setFvCampoDependiente(Campo fvCampoDependiente) {
		this.fvCampoDependiente = fvCampoDependiente;
	}

}