package co.com.coomeva.formulario.validador.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the FV_FORMULARIO database table.
 * 
 */
@Entity
@Table(name="FV_FORMULARIO_LISTA_RESTR")
@NamedQuery(name="FormularioListaRestrictiva.findAll", query="SELECT f FROM FormularioListaRestrictiva f")
public class FormularioListaRestrictiva implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @Basic(optional = false)
    @GeneratedValue(generator = "FV_FORMULARIO_LISTA_RESTR_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "FV_FORMULARIO_LISTA_RESTR_SEQ", sequenceName = "FV_FORMULARIO_LISTA_RESTR_SEQ",allocationSize=1)    
	@Column(name="FORMULARIO_LISTA_RESTR_ID")
	private Long formularioListaRestrictivaId;
	
	//bi-directional many-to-one association to TipoReporte
	@ManyToOne
	@JoinColumn(name="FORMULARIO_ID")
	private Formulario fvFormulario;

	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	
	@Column(name="NO_SECUENCIA")
	private String noSecuencia;
	
	@Column(name="PRIORIDAD")
	private String prioridad;
	
	@Column(name="NOMBRE_LISTA")
	private String nombreLista;
	
	@Column(name="TIPO_IDENTIFICACION")
	private String tipoIdentificacion;
	
	@Column(name="NUMERO_IDENTIFICACION")
	private String numeroIdentificacion;



	public Long getFormularioListaRestrictivaId() {
		return formularioListaRestrictivaId;
	}
	public void setFormularioListaRestrictivaId(Long formularioListaRestrictivaId) {
		this.formularioListaRestrictivaId = formularioListaRestrictivaId;
	}
	public String getNoSecuencia() {
		return noSecuencia;
	}
	public void setNoSecuencia(String noSecuencia) {
		this.noSecuencia = noSecuencia;
	}
	public String getPrioridad() {
		return prioridad;
	}
	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}
	public String getNombreLista() {
		return nombreLista;
	}
	public void setNombreLista(String nombreLista) {
		this.nombreLista = nombreLista;
	}
	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}
	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}
	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}

	public Formulario getFvFormulario() {
		return fvFormulario;
	}
	public void setFvFormulario(Formulario fvFormulario) {
		this.fvFormulario = fvFormulario;
	}
	
	

	

}