package co.com.coomeva.formulario.validador.controller.formulario;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.dto.FormularioAgrCampoDTO;
import co.com.coomeva.formulario.validador.dto.FormularioDTO;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.Status;
import co.com.coomeva.formulario.validador.model.Agrupacion;
import co.com.coomeva.formulario.validador.model.AgrupacionCampo;
import co.com.coomeva.formulario.validador.model.Empresa;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioAgrCampo;
import co.com.coomeva.formulario.validador.service.formulario.FormularioService;
import co.com.coomeva.formulario.validador.service.parametrizacion.AgrupacionService;
import co.com.coomeva.formulario.validador.util.AppUtil;
import co.com.coomeva.formulario.validador.util.FileService;
import co.com.coomeva.formulario.validador.util.ManagePropertiesLogic;
import co.com.coomeva.formulario.validador.util.ResponseServiceUtil;


/**
* Formulario Controller <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/

@RestController
@RequestMapping(value = "/api/formulario", produces = "application/json; charset=UTF-8")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class FormularioController {

    @Autowired
    FormularioService formularioService;
    
    @Autowired
    AgrupacionService agrupacionService;
    
    @Autowired
	FileService fileService;
    
    private static final Logger log = LoggerFactory.getLogger(FormularioController.class);
    ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
    public static final String PROPERTIES = "formulario.properties";
    private final Long maxSizeFile = Long.valueOf(managePropertiesLogic.getProperties("general.properties", "maxSizeFile"));

    /**
     * Crea formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     */
    @PostMapping(value = "/create")
    public ResponseService createFormulario(@RequestBody FormularioDTO formularioDTO) {
            ResponseService responseService = new ResponseService();
        try {
            Formulario formulario = new Formulario();
            formulario = (Formulario) AppUtil.convertOneToAnother(formularioDTO, formulario);
            Formulario formularioName = new Formulario();
            formularioName.setNombre(formulario.getNombre());
            List<Formulario> listFormularioName = formularioService.findByName(formularioName);
            
            if(listFormularioName != null && !listFormularioName.isEmpty()) {
                ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                return responseService;
            }
            
            formulario.setFechaCreacion(new Date());
            formulario = formularioService.create(formulario);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroCreado"), formulario);
        } catch (Exception e) {
            log.error("Error en el servicio /formulario " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }

    /**
     * Modifica formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     */
    @PostMapping(value = "/update")
    public ResponseService updateFormulario(@RequestBody FormularioDTO formularioDTO) {
       ResponseService responseService = new ResponseService();
       try {
            Formulario formulario = new Formulario();
            formulario = (Formulario) AppUtil.convertOneToAnother(formularioDTO, formulario);
            Formulario formularioName = new Formulario();
            formularioName.setNombre(formulario.getNombre());
            List<Formulario> listFormularioName = formularioService.findByName(formularioName);
            
            if(listFormularioName != null && !listFormularioName.isEmpty()) {
                boolean existName = false;
                for (Formulario formulario1 : listFormularioName) {
                    if (!formulario1.getFormularioId().equals(formulario.getFormularioId())) {
                        existName = true;
                    }
                }

                if (existName) {
                    ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                    return responseService;
                }                
            }
            
            formulario.setFechaModificacion(new Date());
            formulario = formularioService.store(formulario);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEditado"), formulario);
        } catch (Exception e) {
            log.error("Error en el servicio /formulario/update " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Elimina formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     */
    @PostMapping(value = "/delete")
    public ResponseService deleteFormulario(@RequestBody FormularioDTO formularioDTO) {
      ResponseService responseService = new ResponseService();
      try {
            Formulario formulario = new Formulario();
            formulario = (Formulario) AppUtil.convertOneToAnother(formularioDTO, formulario);
            formularioService.remove(formulario);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEliminado"));
        } catch (Exception e) {
            log.error("Error en el servicio /formulario/delete " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Consulta formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     */
    @GetMapping(value = "/get/all")
    public ResponseService list() {
        ResponseService responseService = new ResponseService();
        try {     
            List<Formulario> formularioList = formularioService.findAllActive();
            ResponseServiceUtil.buildSuccessResponse(responseService, formularioList);
        } catch (Exception e) {
            log.error("Error en el servicio /formulario/get/all " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Consulta formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     */
    @GetMapping(value = "/get/all/{empresaId}")
    public ResponseService list(@PathVariable Long empresaId) {
        ResponseService responseService = new ResponseService();
        try {     
        	Empresa empresa = new Empresa();
        	Formulario formulario = new Formulario();
        	formulario.setFvEmpresa(empresa);
        	formulario.setEstado(EstadoEnum.ACTIVE.getId());
            List<Formulario> formularioList = formularioService.findByFiltro(formulario);
            ResponseServiceUtil.buildSuccessResponse(responseService, formularioList);
        } catch (Exception e) {
            log.error("Error en el servicio /formulario/get/all " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Consulta formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     */
    @PostMapping(value = "/find")
    public ResponseService find(@RequestBody FormularioDTO formularioDTO) {
       ResponseService responseService = new ResponseService();
       try {
            Formulario formulario = new Formulario();
            formulario = (Formulario) AppUtil.convertOneToAnother(formularioDTO, formulario);
            List<Formulario> formularioList = formularioService.findByFiltro(formulario);
            ResponseServiceUtil.buildSuccessResponse(responseService, formularioList);
        } catch (Exception e) {
            log.error("Error en el servicio /formulario/find " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
	 * Carga archivos <br>
	 * Info. Creación: <br>
	 * fecha 22-dic-2019 <br>
	 * 
	 * @author GTC
	 * @param documentoId
	 * @return
	 */
	@PostMapping(value = "/upload/files", headers = "content-type=multipart/form-data")
	public ResponseService updloadFile(@RequestParam("file") MultipartFile file,
			@RequestParam("formularioId") Long formularioId) {
		ResponseService responseService = new ResponseService();
		try {
			String path = managePropertiesLogic.getProperties("general.properties", "pathFile");
			
			fileService.validateSize(responseService, file, maxSizeFile);
			if(responseService.getStatus() != null && responseService.getStatus().equals(Status.FAILURE)) {
				return responseService;
			}
			
			String name = fileService.normalizeString(file.getOriginalFilename());
			fileService.storeFile(file, path, formularioId + name);
			
			Formulario form = new Formulario();
			form.setFormularioId(formularioId);
			List<Formulario> formularioList = formularioService.findByFormulario(form);
			
			if(formularioList != null && !formularioList.isEmpty()) {
				Formulario formulario = formularioList.get(0);
				formulario.setBanner(path + "/" +  formularioId + name);
				formulario.setNombreAdjunto(name);
				formularioService.store(formulario);
			}
			
			
			
			ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties("oficina.properties", "msgRegistroCreado"));
		} catch (Exception e) {
			log.error("Error en el servicio /upload/files " + e.getMessage(), e);
			ResponseServiceUtil.buildFailResponse(responseService, e);

		}
		return responseService;
	}

	/**
	 * Descarga archivos <br>
	 * Info. Creación: <br>
	 * fecha 27-abr-2021 <br>
	 * 
	 * @author GTC
	 * @param formularioId
	 * @return
	 */
	@GetMapping(value = "/download/files/{formularioId}")
	public ResponseEntity<InputStreamResource> downloadFile(@PathVariable Long formularioId) {
		try {
			
			Formulario form = new Formulario();
			form.setFormularioId(formularioId);
			List<Formulario> formularioList = formularioService.findByFormulario(form);
			
			if(formularioList != null && !formularioList.isEmpty()) {
				Formulario formulario = formularioList.get(0);
				File file = new File(formulario.getBanner());
				InputStream stream = new FileInputStream(file);
				InputStreamResource isr = new InputStreamResource(stream);
				HttpHeaders respHeaders = new HttpHeaders();
				respHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
				respHeaders.setContentDispositionFormData("attachment", formulario.getNombreAdjunto());
				return new ResponseEntity<>(isr, respHeaders, HttpStatus.OK);
			}
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
			

		} catch (Exception e) {
			log.error("Error en el servicio /download/file " + e.getMessage(), e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

		}
	}
	
	/**
     * Consulta formularioAgrupacionCampo <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     */
    @GetMapping(value = "/agrupacion/campo/get/all/{formularioId}")
    public ResponseService listAgrupacionCampo(@PathVariable Long formularioId) {
        ResponseService responseService = new ResponseService();
        try {     
            List<FormularioAgrCampo> formularioList = formularioService.findByFormulario(formularioId);
            ResponseServiceUtil.buildSuccessResponse(responseService, formularioList);
        } catch (Exception e) {
            log.error("Error en el servicio /formulario/agrupacion/campo/get/all " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Crea formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     */
    @PostMapping(value = "/createAgrupacionCampo")
    public ResponseService createAgrupacionCampo(@RequestBody FormularioAgrCampoDTO formularioDTO) {
            ResponseService responseService = new ResponseService();
        try {
            FormularioAgrCampo formulario = new FormularioAgrCampo();
            formulario = (FormularioAgrCampo) AppUtil.convertOneToAnother(formularioDTO, formulario);
            FormularioAgrCampo formularioName = new FormularioAgrCampo();
            formularioName.setFvFormulario(formulario.getFvFormulario());
            
            if(formularioDTO.getAgrupacionId() != null) {
            	Agrupacion agrupacion = new Agrupacion();
            	agrupacion.setAgrupacionId(formularioDTO.getAgrupacionId());
            	formulario.getFvAgrupacionCampo().setFvAgrupacion(agrupacion);
            }
            
            formularioName.setFvAgrupacionCampo(formulario.getFvAgrupacionCampo());
            List<Formulario> listFormularioName = formularioService.findByFormularioAgrupacionCampo(formularioName);
            
            if(listFormularioName != null && !listFormularioName.isEmpty()) {
                ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgFormularioAgrCampoExiste"));
                return responseService;
            }
            

            
            if(formularioDTO.getAgrupacionId() != null) {
            	List<AgrupacionCampo> listAgrCampo = agrupacionService.findAllAgrupacionCampo(formularioDTO.getAgrupacionId());
            	
            	if(listAgrCampo == null || listAgrCampo.size() <= 0) {
            		ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgAgrupacionSinCampo"));
                    return responseService;
            	}
            	
            	for (AgrupacionCampo agrupacionCampo : listAgrCampo) {
            		FormularioAgrCampo fm = new FormularioAgrCampo();
            		fm.setFvFormulario(formulario.getFvFormulario());
            		fm.setFvAgrupacionCampo(agrupacionCampo);
            		fm = formularioService.createAgrupacionCampo(fm);
				}
            } else {
            	formulario = formularioService.createAgrupacionCampo(formulario);
            }

            
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroCreado"), formulario);
        } catch (Exception e) {
            log.error("Error en el servicio /formulario " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }
    
    /**
     * Crea formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     */
    @PostMapping(value = "/deleteAgrupacionCampo")
    public ResponseService deleteAgrupacionCampo(@RequestBody FormularioAgrCampoDTO formularioDTO) {
            ResponseService responseService = new ResponseService();
        try {
        	FormularioAgrCampo formulario = new FormularioAgrCampo();
            formulario = (FormularioAgrCampo) AppUtil.convertOneToAnother(formularioDTO, formulario);
            formularioService.removeAgrupacionCampo(formulario);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEliminado"), formulario);
        } catch (Exception e) {
            log.error("Error en el servicio /formulario " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }
}
