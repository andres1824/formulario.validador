package co.com.coomeva.formulario.validador.service.impl.parametrizacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.dao.parametrizacion.CausalDescarteDao;
import co.com.coomeva.formulario.validador.dao.parametrizacion.CausalDescarteRep;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.CausalDescarte;
import co.com.coomeva.formulario.validador.service.parametrizacion.CausalDescarteService;

/**
* CausalDescarte Service Impl <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Service("causalDescarteService")
@Transactional
public class CausalDescarteServiceImpl implements CausalDescarteService {

    @Autowired
    private CausalDescarteDao causalDescarteDao;

    @Autowired
    private CausalDescarteRep causalDescarteRep;

    @Override
    public CausalDescarte create(CausalDescarte causalDescarte) throws BusinessException {
        return causalDescarteRep.save(causalDescarte);
    }

    @Override
    public CausalDescarte store(CausalDescarte causalDescarte) throws BusinessException {
       return causalDescarteRep.save(causalDescarte);
    }

    @Override
    public void remove(CausalDescarte causalDescarte) throws BusinessException {
        causalDescarteRep.delete(causalDescarte);
    }

    @Override
    public List<CausalDescarte> findAll() throws BusinessException {
        return causalDescarteRep.findAll();
    }

    @Override
    public List<CausalDescarte> findAllActive() throws BusinessException {
        return causalDescarteDao.findAllActive();
    }
    @Override
    public List<CausalDescarte> findByCausalDescarte(CausalDescarte causalDescarte) throws BusinessException {
        return causalDescarteDao.findByCausalDescarte(causalDescarte);
    }

    @Override
    public List<CausalDescarte> findByFiltro(CausalDescarte causalDescarte) throws BusinessException {
        return causalDescarteDao.findByFiltro(causalDescarte);
    }

    @Override
    public List<CausalDescarte> findByName(CausalDescarte causalDescarte) throws BusinessException {
        return causalDescarteDao.findByName(causalDescarte);
    }
}