package co.com.coomeva.formulario.validador.service.impl.parametrizacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.dao.parametrizacion.TipoReporteDao;
import co.com.coomeva.formulario.validador.dao.parametrizacion.TipoReporteRep;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.TipoReporte;
import co.com.coomeva.formulario.validador.service.parametrizacion.TipoReporteService;

/**
* TipoReporte Service Impl <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Service("tipoReporteService")
@Transactional
public class TipoReporteServiceImpl implements TipoReporteService {

    @Autowired
    private TipoReporteDao tipoReporteDao;

    @Autowired
    private TipoReporteRep tipoReporteRep;

    @Override
    public TipoReporte create(TipoReporte tipoReporte) throws BusinessException {
        return tipoReporteRep.save(tipoReporte);
    }

    @Override
    public TipoReporte store(TipoReporte tipoReporte) throws BusinessException {
       return tipoReporteRep.save(tipoReporte);
    }

    @Override
    public void remove(TipoReporte tipoReporte) throws BusinessException {
        tipoReporteRep.delete(tipoReporte);
    }

    @Override
    public List<TipoReporte> findAll() throws BusinessException {
        return tipoReporteRep.findAll();
    }

    @Override
    public List<TipoReporte> findAllActive() throws BusinessException {
        return tipoReporteDao.findAllActive();
    }
    @Override
    public List<TipoReporte> findByTipoReporte(TipoReporte tipoReporte) throws BusinessException {
        return tipoReporteDao.findByTipoReporte(tipoReporte);
    }

    @Override
    public List<TipoReporte> findByFiltro(TipoReporte tipoReporte) throws BusinessException {
        return tipoReporteDao.findByFiltro(tipoReporte);
    }

    @Override
    public List<TipoReporte> findByName(TipoReporte tipoReporte) throws BusinessException {
        return tipoReporteDao.findByName(tipoReporte);
    }
}