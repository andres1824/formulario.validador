package co.com.coomeva.formulario.validador.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the FV_FORMULARIO_VALIDACION database table.
 * 
 */
@Entity
@Table(name="FV_FORMULARIO_VALIDACION")
@NamedQuery(name="FormularioValidacion.findAll", query="SELECT f FROM FormularioValidacion f")
public class FormularioValidacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @Basic(optional = false)
    @GeneratedValue(generator = "fv_formulario_validacion_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "fv_formulario_validacion_seq", sequenceName = "fv_formulario_validacion_seq",allocationSize=1)  
	@Column(name="FORMULARIO_VALIDACION_ID")
	private Long formularioValidacionId;

	//bi-directional many-to-one association to AgrupacionCampo
	@ManyToOne
	@JoinColumn(name="CAMPO_DOCUMENTO_ID")
	private Campo fvCampoDocumento;
	
	//bi-directional many-to-one association to AgrupacionCampo
	@ManyToOne
	@JoinColumn(name="CAMPO_TP_DOCUMENTO_ID")
	private Campo fvCampoTpDocumento;

	//bi-directional many-to-one association to Formulario
	@ManyToOne
	@JoinColumn(name="FORMULARIO_ID")
	private Formulario fvFormulario;
	
	@Column(name="CENTRAL_RIESGO")
	private String centralRiesgo;
	
	@Column(name="CONSULTA_BUC")
	private String consultaBuc;
	
	@Column(name="CONSULTA_CENTRALES_RIESGO")
	private String consultaCentralRiesgo;
	
	@Column(name="CONSULTA_LISTA_CONTROL")
	private String consultaListaControl;

	@Column(name="CONSULTA_PEOPLE_NET")
	private String consultaPeopleNet;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_MODIFICACION")
	private Date fechaModificacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	
	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;

	@Column(name="USUARIO_MODIFICACION")
	private String usuarioModificacion;

	
	
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public Long getFormularioValidacionId() {
		return formularioValidacionId;
	}

	public void setFormularioValidacionId(Long formularioValidacionId) {
		this.formularioValidacionId = formularioValidacionId;
	}

	public Campo getFvCampoDocumento() {
		return fvCampoDocumento;
	}

	public void setFvCampoDocumento(Campo fvCampoDocumento) {
		this.fvCampoDocumento = fvCampoDocumento;
	}

	public Campo getFvCampoTpDocumento() {
		return fvCampoTpDocumento;
	}

	public void setFvCampoTpDocumento(Campo fvCampoTpDocumento) {
		this.fvCampoTpDocumento = fvCampoTpDocumento;
	}

	public Formulario getFvFormulario() {
		return fvFormulario;
	}

	public void setFvFormulario(Formulario fvFormulario) {
		this.fvFormulario = fvFormulario;
	}

	public String getCentralRiesgo() {
		return centralRiesgo;
	}

	public void setCentralRiesgo(String centralRiesgo) {
		this.centralRiesgo = centralRiesgo;
	}

	public String getConsultaBuc() {
		return consultaBuc;
	}

	public void setConsultaBuc(String consultaBuc) {
		this.consultaBuc = consultaBuc;
	}

	public String getConsultaCentralRiesgo() {
		return consultaCentralRiesgo;
	}

	public void setConsultaCentralRiesgo(String consultaCentralRiesgo) {
		this.consultaCentralRiesgo = consultaCentralRiesgo;
	}

	public String getConsultaListaControl() {
		return consultaListaControl;
	}

	public void setConsultaListaControl(String consultaListaControl) {
		this.consultaListaControl = consultaListaControl;
	}

	public String getConsultaPeopleNet() {
		return consultaPeopleNet;
	}

	public void setConsultaPeopleNet(String consultaPeopleNet) {
		this.consultaPeopleNet = consultaPeopleNet;
	}
	
	



}