package co.com.coomeva.formulario.validador.dto;

import java.util.List;

import co.com.coomeva.formulario.validador.model.InspektorDTO;

public class InspektorResponseDTO {
	private List<InspektorDTO> inspektorList;
	private String message;
	public List<InspektorDTO> getInspektorList() {
		return inspektorList;
	}
	public void setInspektorList(List<InspektorDTO> inspektorList) {
		this.inspektorList = inspektorList;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
