package co.com.coomeva.formulario.validador.dao.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Color;

/**
* Color Dao <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
public interface ColorDao  {
    
    /**
     * Obtiene todos los registros activos de color <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws Exception 
     */
    public List<Color> findAllActive() throws BusinessException;

    /**
     * Obtiene todos los registros de color por su id <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param color
     * @return
     * @throws Exception 
     */
    public List<Color> findByColor(Color color) throws BusinessException;   

    /**
     * Obtiene todos los registros de color por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param color
     * @return
     * @throws Exception 
     */
    public List<Color> findByFiltro(Color color) throws BusinessException; 

    /**
    * Obtiene todos los registros de color por nombre <br>
    * Info. Creación: <br>
    * fecha 24 abr 2021 <br>
    * @author GTC
    * @param color
    * @return
    * @throws Exception 
    */
    public List<Color> findByName(Color color) throws BusinessException;  
}
