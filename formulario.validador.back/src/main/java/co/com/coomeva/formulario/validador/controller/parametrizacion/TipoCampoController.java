package co.com.coomeva.formulario.validador.controller.parametrizacion;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.TipoCampoDTO;
import co.com.coomeva.formulario.validador.model.TipoCampo;
import co.com.coomeva.formulario.validador.service.parametrizacion.TipoCampoService;
import co.com.coomeva.formulario.validador.util.AppUtil;
import co.com.coomeva.formulario.validador.util.ManagePropertiesLogic;
import co.com.coomeva.formulario.validador.util.ResponseServiceUtil;


/**
* TipoCampo Controller <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/

@RestController
@RequestMapping(value = "/api/tipoCampo", produces = "application/json; charset=UTF-8")
@CrossOrigin(origins = "http://localhost:4200", methods = { RequestMethod.GET, RequestMethod.POST })
public class TipoCampoController {

    @Autowired
    TipoCampoService tipoCampoService;
    
    private static final Logger log = LoggerFactory.getLogger(TipoCampoController.class);
    ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
    public static final String PROPERTIES = "tipoCampo.properties";

    /**
     * Crea tipoCampo <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoCampo
     * @return
     */
    @PostMapping(value = "/create")
    public ResponseService createTipoCampo(@RequestBody TipoCampoDTO tipoCampoDTO) {
            ResponseService responseService = new ResponseService();
        try {
            TipoCampo tipoCampo = new TipoCampo();
            tipoCampo = (TipoCampo) AppUtil.convertOneToAnother(tipoCampoDTO, tipoCampo);
            TipoCampo tipoCampoName = new TipoCampo();
            tipoCampoName.setNombre(tipoCampo.getNombre());
            List<TipoCampo> listTipoCampoName = tipoCampoService.findByName(tipoCampoName);
            
            if(listTipoCampoName != null && !listTipoCampoName.isEmpty()) {
                ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                return responseService;
            }
            
            tipoCampo.setFechaCreacion(new Date());
            tipoCampoService.create(tipoCampo);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroCreado"));
        } catch (Exception e) {
            log.error("Error en el servicio /tipoCampo " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }

    /**
     * Modifica tipoCampo <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoCampo
     * @return
     */
    @PostMapping(value = "/update")
    public ResponseService updateTipoCampo(@RequestBody TipoCampoDTO tipoCampoDTO) {
       ResponseService responseService = new ResponseService();
       try {
            TipoCampo tipoCampo = new TipoCampo();
            tipoCampo = (TipoCampo) AppUtil.convertOneToAnother(tipoCampoDTO, tipoCampo);
            TipoCampo tipoCampoName = new TipoCampo();
            tipoCampoName.setNombre(tipoCampo.getNombre());
            List<TipoCampo> listTipoCampoName = tipoCampoService.findByName(tipoCampoName);
            
            if(listTipoCampoName != null && !listTipoCampoName.isEmpty()) {
                boolean existName = false;
                for (TipoCampo tipoCampo1 : listTipoCampoName) {
                    if (!tipoCampo1.getTipoCampoId().equals(tipoCampo.getTipoCampoId())) {
                        existName = true;
                    }
                }

                if (existName) {
                    ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                    return responseService;
                }                
            }
            
            tipoCampo.setFechaModificacion(new Date());
            tipoCampoService.store(tipoCampo);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEditado"));
        } catch (Exception e) {
            log.error("Error en el servicio /tipoCampo/update " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Elimina tipoCampo <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoCampo
     * @return
     */
    @PostMapping(value = "/delete")
    public ResponseService deleteTipoCampo(@RequestBody TipoCampoDTO tipoCampoDTO) {
      ResponseService responseService = new ResponseService();
      try {
            TipoCampo tipoCampo = new TipoCampo();
            tipoCampo = (TipoCampo) AppUtil.convertOneToAnother(tipoCampoDTO, tipoCampo);
            tipoCampoService.remove(tipoCampo);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEliminado"));
        } catch (Exception e) {
            log.error("Error en el servicio /tipoCampo/delete " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Consulta tipoCampo <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     */
    @GetMapping(value = "/get/all")
    public ResponseService list() {
        ResponseService responseService = new ResponseService();
        try {     
            List<TipoCampo> tipoCampoList = tipoCampoService.findAllActive();
            ResponseServiceUtil.buildSuccessResponse(responseService, tipoCampoList);
        } catch (Exception e) {
            log.error("Error en el servicio /tipoCampo/get/all " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Consulta tipoCampo <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     */
    @PostMapping(value = "/find")
    public ResponseService find(@RequestBody TipoCampoDTO tipoCampoDTO) {
       ResponseService responseService = new ResponseService();
       try {
            TipoCampo tipoCampo = new TipoCampo();
            tipoCampo = (TipoCampo) AppUtil.convertOneToAnother(tipoCampoDTO, tipoCampo);
            List<TipoCampo> tipoCampoList = tipoCampoService.findByFiltro(tipoCampo);
            ResponseServiceUtil.buildSuccessResponse(responseService, tipoCampoList);
        } catch (Exception e) {
            log.error("Error en el servicio /tipoCampo/find " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
}
