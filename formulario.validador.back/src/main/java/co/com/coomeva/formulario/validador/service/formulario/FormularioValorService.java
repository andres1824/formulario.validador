package co.com.coomeva.formulario.validador.service.formulario;

import java.net.MalformedURLException;
import java.util.List;

import co.com.coomeva.formulario.validador.dto.FormularioAgrCampoValueDTO;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioAgrCampo;
import co.com.coomeva.formulario.validador.model.FormularioValor;

/**
* Formulario Service <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
public interface FormularioValorService {
    

    /**
     * Crea formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return 
     * @throws BusinessException 
     */
    public FormularioValor create(FormularioValor formulario) throws BusinessException;
    /**
     * Modifica formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return 
     * @throws BusinessException 
     */
    public FormularioValor store(FormularioValor formulario) throws BusinessException;
    /**
     * Elimina formulario<br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @throws BusinessException 
     */
    public void remove(FormularioValor formulario) throws BusinessException;
    /**
     * Obtiene todos los registros de formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<FormularioValor> findAll() throws BusinessException;
    
    /**
     * Obtiene todos los registros activos de formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<FormularioValor> findAllActive() throws BusinessException;
    /**
     * Obtiene todos los registros de formulario paginados <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     * @throws BusinessException 
     */
    public List<FormularioValor> findByFormulario(FormularioValor formulario) throws BusinessException;   

    /**
     * Obtiene todos los registros de formulario por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     * @throws BusinessException 
     */
    public List<FormularioValor> findByFiltro(FormularioValor formulario) throws BusinessException; 

    public boolean validarAsociado(FormularioAgrCampoValueDTO formularioAgrCampoValueDTO) throws MalformedURLException;

    

    
}