package co.com.coomeva.formulario.validador.dao.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Campo;

/**
* Campo Dao <br>
* Info. Creación: <br>
* fecha 25 abr 2021 <br>
* @author GTC
**/
public interface CampoDao  {
    
    /**
     * Obtiene todos los registros activos de campo <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @return
     * @throws Exception 
     */
    public List<Campo> findAllActive() throws BusinessException;
    
    /**
     * Obtiene todos los registros activos de campo <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<Campo> findAllDependiente(Long campoId) throws BusinessException;

    /**
     * Obtiene todos los registros de campo por su id <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @return
     * @throws Exception 
     */
    public List<Campo> findByCampo(Campo campo) throws BusinessException;   

    /**
     * Obtiene todos los registros de campo por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 25 abr 2021 <br>
     * @author GTC
     * @param campo
     * @return
     * @throws Exception 
     */
    public List<Campo> findByFiltro(Campo campo) throws BusinessException; 

    /**
    * Obtiene todos los registros de campo por nombre <br>
    * Info. Creación: <br>
    * fecha 25 abr 2021 <br>
    * @author GTC
    * @param campo
    * @return
    * @throws Exception 
    */
    public List<Campo> findByName(Campo campo) throws BusinessException;  
}
