package co.com.coomeva.formulario.validador.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.Normalizer;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import co.com.coomeva.formulario.validador.dto.ResponseService;

@Component
public class FileService {
  
	public void storeFile(MultipartFile file, String dir, String fileName) throws IOException {
		ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
		String pathRoot = managePropertiesLogic.getProperties("general.properties", "pathRoot");
		
		Path filePath = Paths.get(dir + pathRoot  + fileName );
 
		Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
	}
	
	public String normalizeString(String cadena) {
		String limpio = null;
		if (cadena != null) {
			limpio = Normalizer
			.normalize(cadena, Normalizer.Form.NFD)
			.replaceAll("[^\\p{ASCII}]", "");
		}
		return limpio;
	}
	
	/**
	 * 
	 * @param responseService
	 * @param file
	 * @param maxSize
	 * @return
	 */
	public void validateSize(ResponseService responseService, MultipartFile file, 
			long maxSize) {
		ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
		String msg = managePropertiesLogic.getProperties("general.properties", "msgMaxSize");
		if(file == null) {
			return;
		}
		
		if(file.getSize() > maxSize) {
			String max = maxSize/1000 + "KB";
			String msgFile = String.format(msg, file.getOriginalFilename(), max);
			ResponseServiceUtil.buildFailResponse(responseService, msgFile);
		}
	}
}