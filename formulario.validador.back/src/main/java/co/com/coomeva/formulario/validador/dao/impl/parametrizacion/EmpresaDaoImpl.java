package co.com.coomeva.formulario.validador.dao.impl.parametrizacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import co.com.coomeva.formulario.validador.dao.parametrizacion.EmpresaDao;
import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Empresa;

/**
* Empresa Dao Impl <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Repository
public class EmpresaDaoImpl implements EmpresaDao {
    
    @PersistenceContext
    private EntityManager em;
    public static final String FROM = "select a from Empresa a ";
	
	
    public EntityManager getEntityManager() {
        return em;
    }


    @Override
    public List<Empresa> findAllActive() throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.estado = :active ");

        builder.append("order by a.nombre asc ");
        
        
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("active", (EstadoEnum.ACTIVE.getId()));
        return query.getResultList();
    }
    
    @Override
    public List<Empresa> findByEmpresa(Empresa empresa) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.empresaId = :id ");

        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("id", empresa.getEmpresaId());
        return query.getResultList();
    }

    @Override
    public List<Empresa> findByFiltro(Empresa empresa) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE 1=1 ");
        
        if(empresa.getNombre() != null && !empresa.getNombre().trim().isEmpty()) {
            builder.append("AND lower(a.nombre) like lower(:nombre) ");
        }




        if(empresa.getEstado() != null && !empresa.getEstado().trim().isEmpty()) {
            builder.append("AND a.estado = :estado ");
        }

        builder.append("order by a.nombre asc ");
        Query query = this.getEntityManager().createQuery(builder.toString());

        if(empresa.getNombre() != null && !empresa.getNombre().trim().isEmpty()) {
            query.setParameter("nombre", "%" + empresa.getNombre() + "%");
        }




        if(empresa.getEstado() != null && !empresa.getEstado().trim().isEmpty()) {
            query.setParameter("estado", empresa.getEstado());
        }

        return query.getResultList();
    }

    @Override
    public List<Empresa> findByName(Empresa empresa) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE lower(trim(a.nombre)) = lower(trim(:nombre)) ");
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("nombre", empresa.getNombre().trim());
        return query.getResultList();
    }
}