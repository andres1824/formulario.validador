package co.com.coomeva.formulario.validador.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import co.com.coomeva.formulario.validador.model.Respuesta;


/**
 * The persistent class for the FV_CAMPO database table.
 * 
 */
public class CampoValueDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long campoId;
	
	private Long campoDependienteId;

	private String estado;

	private Date fechaCreacion;

	private Date fechaModificacion;

	private BigDecimal longitud;

	private String nombre;

	private String obligatorio;

	private String usuarioCreacion;

	private String usuarioModificacion;

	private String value;

	public Long getCampoId() {
		return campoId;
	}

	public void setCampoId(Long campoId) {
		this.campoId = campoId;
	}

	public Long getCampoDependienteId() {
		return campoDependienteId;
	}

	public void setCampoDependienteId(Long campoDependienteId) {
		this.campoDependienteId = campoDependienteId;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public BigDecimal getLongitud() {
		return longitud;
	}

	public void setLongitud(BigDecimal longitud) {
		this.longitud = longitud;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getObligatorio() {
		return obligatorio;
	}

	public void setObligatorio(String obligatorio) {
		this.obligatorio = obligatorio;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	

}