package co.com.coomeva.formulario.validador.dto;

public class AppConstant {
	public static final String ACTIVO = "S";
	public static final String INACTIVO = "N";
	public static final String ERROR = "Error";
	public static final String ERROR_SERVICIO = "Error al consultar el servicio, comuniquese con el administrador";
	public static final String ERROR_SERVICIO_ADDRESS = "Error al enviar token al correo ";
	public static final String ERROR_SERVICIO_COOMEVA = "Error al consultar servicio de MiCoomeva";
	public static final String PARAM_NOT_VIP = "PARAM_NOT_VIP";
	public static final String PARAM_NOT_ACT_EMAIL_ASU = "PARAM_NOT_ACT_EMAIL_ASU";
	public static final String PARAM_ACT_EMAIL_MI_DATO_WEB = "PARAM_ACT_EMAIL_MI_DATO_WEB";
	public static final String PARAM_VIP = "PARAM_VIP";
	public static final String MIS_DATOS_WEB = "Mi Dato Web";
	public static final String DOCUMENTO_WS_DATOSASOCIADOS = "DOCUMENTO_WS_DATOSASOCIADOS";
}
