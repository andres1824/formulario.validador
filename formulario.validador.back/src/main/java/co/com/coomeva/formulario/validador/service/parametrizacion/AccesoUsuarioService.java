package co.com.coomeva.formulario.validador.service.parametrizacion;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.AccesoUsuario;

/**
* Empresa Service <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
public interface AccesoUsuarioService {
    

    public AccesoUsuario create(AccesoUsuario accesoUsuario) throws BusinessException;
	
    public AccesoUsuario store(AccesoUsuario accesoUsuario) throws BusinessException;

    public void remove(AccesoUsuario accesoUsuario) throws BusinessException;

    
}