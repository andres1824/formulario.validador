package co.com.coomeva.formulario.validador.dao.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Empresa;

/**
* Empresa Dao <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
public interface EmpresaDao  {
    
    /**
     * Obtiene todos los registros activos de empresa <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws Exception 
     */
    public List<Empresa> findAllActive() throws BusinessException;

    /**
     * Obtiene todos los registros de empresa por su id <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param empresa
     * @return
     * @throws Exception 
     */
    public List<Empresa> findByEmpresa(Empresa empresa) throws BusinessException;   

    /**
     * Obtiene todos los registros de empresa por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param empresa
     * @return
     * @throws Exception 
     */
    public List<Empresa> findByFiltro(Empresa empresa) throws BusinessException; 

    /**
    * Obtiene todos los registros de empresa por nombre <br>
    * Info. Creación: <br>
    * fecha 24 abr 2021 <br>
    * @author GTC
    * @param empresa
    * @return
    * @throws Exception 
    */
    public List<Empresa> findByName(Empresa empresa) throws BusinessException;  
}
