package co.com.coomeva.formulario.validador.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the FV_CAMPO database table.
 * 
 */
@Entity
@Table(name="FV_CAMPO")
@NamedQuery(name="Campo.findAll", query="SELECT c FROM Campo c")
public class Campo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @Basic(optional = false)
    @GeneratedValue(generator = "fv_campo_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "fv_campo_seq", sequenceName = "fv_campo_seq",allocationSize=1)    
	@Column(name="CAMPO_ID")
	private Long campoId;

	private String estado;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_MODIFICACION")
	private Date fechaModificacion;

	private BigDecimal longitud;

	private String nombre;

	private String obligatorio;

	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;

	@Column(name="USUARIO_MODIFICACION")
	private String usuarioModificacion;

	//bi-directional many-to-one association to AgrupacionCampo
	@OneToMany(mappedBy="fvCampo")
	private List<AgrupacionCampo> fvAgrupacionCampos;

	//bi-directional many-to-one association to TipoCampo
	@ManyToOne
	@JoinColumn(name="TIPO_CAMPO_ID")
	private TipoCampo fvTipoCampo;

	//bi-directional many-to-one association to TipoDato
	@ManyToOne
	@JoinColumn(name="TIPO_DATO_ID")
	private TipoDato fvTipoDato;

	//bi-directional many-to-one association to RespuestaCampo
	@OneToMany(mappedBy="fvCampo")
	private List<RespuestaCampo> fvRespuestaCampos;

	public Campo() {
	}

	public Long getCampoId() {
		return this.campoId;
	}

	public void setCampoId(Long campoId) {
		this.campoId = campoId;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public BigDecimal getLongitud() {
		return this.longitud;
	}

	public void setLongitud(BigDecimal longitud) {
		this.longitud = longitud;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getObligatorio() {
		return this.obligatorio;
	}

	public void setObligatorio(String obligatorio) {
		this.obligatorio = obligatorio;
	}

	public String getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}
	
	@JsonIgnore
	public List<AgrupacionCampo> getFvAgrupacionCampos() {
		return this.fvAgrupacionCampos;
	}

	public void setFvAgrupacionCampos(List<AgrupacionCampo> fvAgrupacionCampos) {
		this.fvAgrupacionCampos = fvAgrupacionCampos;
	}

	public AgrupacionCampo addFvAgrupacionCampo(AgrupacionCampo fvAgrupacionCampo) {
		getFvAgrupacionCampos().add(fvAgrupacionCampo);
		fvAgrupacionCampo.setFvCampo(this);

		return fvAgrupacionCampo;
	}

	public AgrupacionCampo removeFvAgrupacionCampo(AgrupacionCampo fvAgrupacionCampo) {
		getFvAgrupacionCampos().remove(fvAgrupacionCampo);
		fvAgrupacionCampo.setFvCampo(null);

		return fvAgrupacionCampo;
	}
	public TipoCampo getFvTipoCampo() {
		return this.fvTipoCampo;
	}

	public void setFvTipoCampo(TipoCampo fvTipoCampo) {
		this.fvTipoCampo = fvTipoCampo;
	}

	public TipoDato getFvTipoDato() {
		return this.fvTipoDato;
	}

	public void setFvTipoDato(TipoDato fvTipoDato) {
		this.fvTipoDato = fvTipoDato;
	}
	
	@JsonIgnore
	public List<RespuestaCampo> getFvRespuestaCampos() {
		return this.fvRespuestaCampos;
	}

	public void setFvRespuestaCampos(List<RespuestaCampo> fvRespuestaCampos) {
		this.fvRespuestaCampos = fvRespuestaCampos;
	}

	public RespuestaCampo addFvRespuestaCampo(RespuestaCampo fvRespuestaCampo) {
		getFvRespuestaCampos().add(fvRespuestaCampo);
		fvRespuestaCampo.setFvCampo(this);

		return fvRespuestaCampo;
	}

	public RespuestaCampo removeFvRespuestaCampo(RespuestaCampo fvRespuestaCampo) {
		getFvRespuestaCampos().remove(fvRespuestaCampo);
		fvRespuestaCampo.setFvCampo(null);

		return fvRespuestaCampo;
	}

}