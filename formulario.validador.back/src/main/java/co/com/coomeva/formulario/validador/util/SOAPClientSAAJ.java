package co.com.coomeva.formulario.validador.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;

import javax.xml.soap.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SOAPClientSAAJ {

	private static final Logger log = LoggerFactory.getLogger(SOAPClientSAAJ.class);

	private SOAPClientSAAJ() {

	}

	/**
	 * Método que consume el servicio SOAP y su acción
	 * @param envelope -- Esquema del request en xml
	 * @param urlEndPoint -- Url del EndPoint
	 * @return response
	 */
	public static SOAPMessage consumeSOAP(String envelope, String urlEndPoint) {
		SOAPMessage response = null;
		try {
			SOAPConnectionFactory sfc = SOAPConnectionFactory.newInstance();
			SOAPConnection connection = sfc.createConnection();
			InputStream is = new ByteArrayInputStream(envelope.getBytes());
			SOAPMessage request = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL)
					.createMessage(new MimeHeaders(), is);
			request.removeAllAttachments();
			MimeHeaders headers = request.getMimeHeaders();
			headers.addHeader("SOAPAction", "http://schemas.xmlsoap.org/soap/http");
			URL endpoint = new URL(urlEndPoint);
			response = connection.call(request, endpoint);
			response.getSOAPBody();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return response;
	}

}