package co.com.coomeva.formulario.validador.service.impl.parametrizacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.dao.parametrizacion.ParametroDao;
import co.com.coomeva.formulario.validador.dao.parametrizacion.ParametroRep;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Parametro;
import co.com.coomeva.formulario.validador.service.parametrizacion.ParametroService;

/**
* Parametro Service Impl <br>
* Info. Creación: <br>
* fecha 23 abr 2021 <br>
* @author GTC
**/
@Service("parametroService")
@Transactional
public class ParametroServiceImpl implements ParametroService {

    @Autowired
    private ParametroDao parametroDao;
    
    @Autowired
    private ParametroRep parametroRep;

    @Override
    public Parametro create(Parametro parametro) throws BusinessException {
        return parametroRep.save(parametro);
    }

    @Override
    public Parametro store(Parametro parametro) throws BusinessException {
       return parametroRep.save(parametro);
    }

    @Override
    public void remove(Parametro parametro) throws BusinessException {
    	parametroRep.delete(parametro);
    }

    @Override
    public List<Parametro> findAll() throws BusinessException {
        return parametroRep.findAll();
    }

    @Override
    public List<Parametro> findAllActive() throws BusinessException {
        return parametroDao.findAllActive();
    }
    @Override
    public List<Parametro> findByParametro(Parametro parametro) throws BusinessException {
        return parametroDao.findByParametro(parametro);
    }

    @Override
    public List<Parametro> findByFiltro(Parametro parametro) throws BusinessException {
        return parametroDao.findByFiltro(parametro);
    }

    @Override
    public List<Parametro> findByName(Parametro parametro) throws BusinessException {
        return parametroDao.findByName(parametro);
    }
}