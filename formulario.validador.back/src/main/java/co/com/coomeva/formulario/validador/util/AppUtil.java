package co.com.coomeva.formulario.validador.util;

import java.io.IOException;
import java.time.chrono.ChronoLocalDate;
import java.time.chrono.ChronoPeriod;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AppUtil {
	
	
	private AppUtil() {
		
	}
	/**
	 * sumar meses a una fecha
	 * @param d1
	 * @param month
	 * @return
	 * @throws Exception
	 */
	public static Date addMonths(Date d1, int month) {
		Calendar c = Calendar.getInstance();
		c.setTime(d1);
		c.add(Calendar.MONTH, month);
		return c.getTime();
	}
	
	/**
	 * obtiene el primer dia del mes
	 * @param d1
	 * @return
	 */
	public static Date firstDayMonth(Date d1) {
		Calendar c = Calendar.getInstance(); 
		c.setTime(d1);

		//A la fecha actual le pongo el día 1
		c.set(Calendar.DAY_OF_MONTH,1);
		return c.getTime();
	}
	
	/**
	 * Obtiene los meses que existen de dos fechas
	 * @param d1
	 * @param d2
	 * @return
	 * @throws Exception
	 */
	public static Integer getMonthByDate(Date d1, Date d2) {
		Calendar inicio = new GregorianCalendar();
		Calendar fin = new GregorianCalendar();
		inicio.setTime(d1);
		fin.setTime(d2);
		int difA = fin.get(Calendar.YEAR) - inicio.get(Calendar.YEAR);
		return (difA * 12 + fin.get(Calendar.MONTH) - inicio.get(Calendar.MONTH));
	}
	
	/**
	 * Obtiene los meses que existen de dos fechas
	 * @param d1
	 * @param d2
	 * @return
	 * @throws Exception
	 */
	public static Long getMonthByDateStr(String d1, String d2, String format) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        ChronoLocalDate from = ChronoLocalDate.from(formatter.parse(d1));
        ChronoLocalDate to = ChronoLocalDate.from(formatter.parse(d2));
        ChronoPeriod period = ChronoPeriod.between(from, to);
        return ((period.get(java.time.temporal.ChronoUnit.YEARS))*12) + (period.get(java.time.temporal.ChronoUnit.MONTHS));
	}
	
	public static Object convertOneToAnother(Object one, Object finalObject) throws IOException {
		ObjectMapper mapper1 = new ObjectMapper();
		mapper1.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper1.disable(MapperFeature.USE_ANNOTATIONS);
		String jsonString = mapper1.writeValueAsString(one);
		return mapper1.readValue(jsonString, finalObject.getClass());
	}
	
	/**
	 * Obtiene diferencia de dias entre dos fechas
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static int getDiasByDate(Date d1, Date d2) {
		return ((int) ((d2.getTime()-d1.getTime())/86400000));
	}
}
