package co.com.coomeva.formulario.validador.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import co.com.coomeva.formulario.validador.model.Respuesta;


/**
 * The persistent class for the FV_CAMPO database table.
 * 
 */
public class CampoDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long campoId;
	
	private Long campoDependienteId;

	private String estado;

	private Date fechaCreacion;

	private Date fechaModificacion;

	private BigDecimal longitud;

	private String nombre;

	private String obligatorio;

	private String usuarioCreacion;

	private String usuarioModificacion;

	//bi-directional many-to-one association to AgrupacionCampo
	private List<AgrupacionCampoDTO> fvAgrupacionCampos;

	private TipoCampoDTO fvTipoCampo;

	private TipoDatoDTO fvTipoDato;
	
	private Respuesta fvRespuesta;
	
	private String respuestaCorrecta;

	private List<RespuestaCampoDTO> fvRespuestaCampos;

	public CampoDTO() {
	}

	public Long getCampoDependienteId() {
		return campoDependienteId;
	}

	public void setCampoDependienteId(Long campoDependienteId) {
		this.campoDependienteId = campoDependienteId;
	}

	public String getRespuestaCorrecta() {
		return respuestaCorrecta;
	}

	public void setRespuestaCorrecta(String respuestaCorrecta) {
		this.respuestaCorrecta = respuestaCorrecta;
	}

	public Long getCampoId() {
		return this.campoId;
	}

	public void setCampoId(Long campoId) {
		this.campoId = campoId;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Respuesta getFvRespuesta() {
		return fvRespuesta;
	}

	public void setFvRespuesta(Respuesta fvRespuesta) {
		this.fvRespuesta = fvRespuesta;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public BigDecimal getLongitud() {
		return this.longitud;
	}

	public void setLongitud(BigDecimal longitud) {
		this.longitud = longitud;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getObligatorio() {
		return this.obligatorio;
	}

	public void setObligatorio(String obligatorio) {
		this.obligatorio = obligatorio;
	}

	public String getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}
	
	@JsonIgnore
	public List<AgrupacionCampoDTO> getFvAgrupacionCampos() {
		return this.fvAgrupacionCampos;
	}

	public void setFvAgrupacionCampos(List<AgrupacionCampoDTO> fvAgrupacionCampos) {
		this.fvAgrupacionCampos = fvAgrupacionCampos;
	}

	public AgrupacionCampoDTO addFvAgrupacionCampo(AgrupacionCampoDTO fvAgrupacionCampo) {
		getFvAgrupacionCampos().add(fvAgrupacionCampo);
		fvAgrupacionCampo.setFvCampo(this);

		return fvAgrupacionCampo;
	}

	public AgrupacionCampoDTO removeFvAgrupacionCampo(AgrupacionCampoDTO fvAgrupacionCampo) {
		getFvAgrupacionCampos().remove(fvAgrupacionCampo);
		fvAgrupacionCampo.setFvCampo(null);

		return fvAgrupacionCampo;
	}

	public TipoCampoDTO getFvTipoCampo() {
		return this.fvTipoCampo;
	}

	public void setFvTipoCampo(TipoCampoDTO fvTipoCampo) {
		this.fvTipoCampo = fvTipoCampo;
	}

	public TipoDatoDTO getFvTipoDato() {
		return this.fvTipoDato;
	}

	public void setFvTipoDato(TipoDatoDTO fvTipoDato) {
		this.fvTipoDato = fvTipoDato;
	}

	@JsonIgnore
	public List<RespuestaCampoDTO> getFvRespuestaCampos() {
		return this.fvRespuestaCampos;
	}

	public void setFvRespuestaCampos(List<RespuestaCampoDTO> fvRespuestaCampos) {
		this.fvRespuestaCampos = fvRespuestaCampos;
	}

	public RespuestaCampoDTO addFvRespuestaCampo(RespuestaCampoDTO fvRespuestaCampo) {
		getFvRespuestaCampos().add(fvRespuestaCampo);
		fvRespuestaCampo.setFvCampo(this);

		return fvRespuestaCampo;
	}

	public RespuestaCampoDTO removeFvRespuestaCampo(RespuestaCampoDTO fvRespuestaCampo) {
		getFvRespuestaCampos().remove(fvRespuestaCampo);
		fvRespuestaCampo.setFvCampo(null);

		return fvRespuestaCampo;
	}

}