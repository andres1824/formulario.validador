package co.com.coomeva.formulario.validador.dao.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.CausalDescarte;

/**
* CausalDescarte Dao <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
public interface CausalDescarteDao  {
    
    /**
     * Obtiene todos los registros activos de causalDescarte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws Exception 
     */
    public List<CausalDescarte> findAllActive() throws BusinessException;

    /**
     * Obtiene todos los registros de causalDescarte por su id <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param causalDescarte
     * @return
     * @throws Exception 
     */
    public List<CausalDescarte> findByCausalDescarte(CausalDescarte causalDescarte) throws BusinessException;   

    /**
     * Obtiene todos los registros de causalDescarte por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param causalDescarte
     * @return
     * @throws Exception 
     */
    public List<CausalDescarte> findByFiltro(CausalDescarte causalDescarte) throws BusinessException; 

    /**
    * Obtiene todos los registros de causalDescarte por nombre <br>
    * Info. Creación: <br>
    * fecha 24 abr 2021 <br>
    * @author GTC
    * @param causalDescarte
    * @return
    * @throws Exception 
    */
    public List<CausalDescarte> findByName(CausalDescarte causalDescarte) throws BusinessException;  
}
