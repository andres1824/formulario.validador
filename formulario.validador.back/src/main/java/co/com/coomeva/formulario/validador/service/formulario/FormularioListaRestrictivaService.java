package co.com.coomeva.formulario.validador.service.formulario;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.com.coomeva.formulario.validador.dto.FormularioAgrCampoValueDTO;
import co.com.coomeva.formulario.validador.dto.InspektorResponseDTO;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioAgrCampo;
import co.com.coomeva.formulario.validador.model.FormularioEmpleado;
import co.com.coomeva.formulario.validador.model.FormularioListaRestrictiva;
import co.com.coomeva.formulario.validador.model.FormularioValor;
import co.com.coomeva.formulario.validador.model.InspektorDTO;

/**
* Formulario Service <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
public interface FormularioListaRestrictivaService {
    

    /**
     * Crea formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return 
     * @throws BusinessException 
     */
    public FormularioListaRestrictiva create(FormularioListaRestrictiva formulario) throws BusinessException;
    /**
     * Modifica formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return 
     * @throws BusinessException 
     */
    public FormularioListaRestrictiva store(FormularioListaRestrictiva formulario) throws BusinessException;
    /**
     * Elimina formulario<br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @throws BusinessException 
     */
    public void remove(FormularioListaRestrictiva formulario) throws BusinessException;
    /**
     * Obtiene todos los registros de formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<FormularioListaRestrictiva> findAll() throws BusinessException;
    
    /**
     * Obtiene todos los registros activos de formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<FormularioListaRestrictiva> findAllActive() throws BusinessException;
    /**
     * Obtiene todos los registros de formulario paginados <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     * @throws BusinessException 
     */
    public List<FormularioListaRestrictiva> findByFormularioListaRestrictiva(FormularioListaRestrictiva formulario) throws BusinessException;   

    /**
     * Obtiene todos los registros de formulario por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     * @throws BusinessException 
     */
    public List<FormularioListaRestrictiva> findByFiltro(FormularioListaRestrictiva formulario) throws BusinessException; 


    
    public InspektorResponseDTO saveListasRestrictivas(FormularioAgrCampoValueDTO formularioAgrCampoValueDTO) throws BusinessException, IOException;
}