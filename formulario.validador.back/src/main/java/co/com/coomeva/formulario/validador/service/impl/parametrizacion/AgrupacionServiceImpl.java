package co.com.coomeva.formulario.validador.service.impl.parametrizacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.dao.parametrizacion.AgrupacionCampoRep;
import co.com.coomeva.formulario.validador.dao.parametrizacion.AgrupacionDao;
import co.com.coomeva.formulario.validador.dao.parametrizacion.AgrupacionRep;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Agrupacion;
import co.com.coomeva.formulario.validador.model.AgrupacionCampo;
import co.com.coomeva.formulario.validador.service.parametrizacion.AgrupacionService;

/**
* Agrupacion Service Impl <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
@Service("agrupacionService")
@Transactional
public class AgrupacionServiceImpl implements AgrupacionService {

    @Autowired
    private AgrupacionDao agrupacionDao;

    @Autowired
    private AgrupacionRep agrupacionRep;
    
    @Autowired
    private AgrupacionCampoRep agrupacionCampoRep;

    @Override
    public Agrupacion create(Agrupacion agrupacion) throws BusinessException {
        return agrupacionRep.save(agrupacion);
    }

    @Override
    public Agrupacion store(Agrupacion agrupacion) throws BusinessException {
       return agrupacionRep.save(agrupacion);
    }

    @Override
    public void remove(Agrupacion agrupacion) throws BusinessException {
        agrupacionRep.delete(agrupacion);
    }

    @Override
    public List<Agrupacion> findAll() throws BusinessException {
        return agrupacionRep.findAll();
    }

    @Override
    public List<Agrupacion> findAllActive() throws BusinessException {
        return agrupacionDao.findAllActive();
    }
    @Override
    public List<Agrupacion> findByAgrupacion(Agrupacion agrupacion) throws BusinessException {
        return agrupacionDao.findByAgrupacion(agrupacion);
    }

    @Override
    public List<Agrupacion> findByFiltro(Agrupacion agrupacion) throws BusinessException {
        return agrupacionDao.findByFiltro(agrupacion);
    }

    @Override
    public List<Agrupacion> findByName(Agrupacion agrupacion) throws BusinessException {
        return agrupacionDao.findByName(agrupacion);
    }

	@Override
	public List<Agrupacion> findByAgrupacionCampo(AgrupacionCampo agrupacion) throws BusinessException {
		return agrupacionDao.findByAgrupacionCampo(agrupacion);
	}
	
	@Override
    public AgrupacionCampo createCampo(AgrupacionCampo agrupacion) throws BusinessException {
        return agrupacionCampoRep.save(agrupacion);
    }

    @Override
    public void removeCampo(AgrupacionCampo agrupacion) throws BusinessException {
    	agrupacionCampoRep.delete(agrupacion);
    }

	@Override
	public List<AgrupacionCampo> findAllAgrupacionCampo(Long agrupacionId) throws BusinessException {
		return agrupacionCampoRep.find(agrupacionId);
	}
}