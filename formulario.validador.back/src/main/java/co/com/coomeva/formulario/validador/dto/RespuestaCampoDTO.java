package co.com.coomeva.formulario.validador.dto;

import java.io.Serializable;


/**
 * The persistent class for the FV_RESPUESTA_CAMPO database table.
 * 
 */
public class RespuestaCampoDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long respuestaCampoId;

	private String respuestaCorrecta;

	private CampoDTO fvCampo;
	
	private CampoDTO fvCampoDependiente;

	private RespuestaDTO fvRespuesta;

	public RespuestaCampoDTO() {
	}

	public CampoDTO getFvCampoDependiente() {
		return fvCampoDependiente;
	}

	public void setFvCampoDependiente(CampoDTO fvCampoDependiente) {
		this.fvCampoDependiente = fvCampoDependiente;
	}

	public Long getRespuestaCampoId() {
		return this.respuestaCampoId;
	}

	public void setRespuestaCampoId(Long respuestaCampoId) {
		this.respuestaCampoId = respuestaCampoId;
	}

	public String getRespuestaCorrecta() {
		return this.respuestaCorrecta;
	}

	public void setRespuestaCorrecta(String respuestaCorrecta) {
		this.respuestaCorrecta = respuestaCorrecta;
	}

	public CampoDTO getFvCampo() {
		return this.fvCampo;
	}

	public void setFvCampo(CampoDTO fvCampo) {
		this.fvCampo = fvCampo;
	}

	public RespuestaDTO getFvRespuesta() {
		return this.fvRespuesta;
	}

	public void setFvRespuesta(RespuestaDTO fvRespuesta) {
		this.fvRespuesta = fvRespuesta;
	}

}