package co.com.coomeva.formulario.validador.util;

public abstract class ConstanteServicio {
	
	private ConstanteServicio() {
		
	}
	
	public static final String SALESFORCE_CONSULTA_PRODUCTO = "SALESFORCE-PACONSULTAPRODUCTO";
	public static final String OPERACION_CONSULTA = "C";
	public static final String SALESFORCE_COTIZAR_PRODUCTO = "SALESFORCE-PACOTIZAR";
	public static final String OPERACION_INSERCION = "I";

}
