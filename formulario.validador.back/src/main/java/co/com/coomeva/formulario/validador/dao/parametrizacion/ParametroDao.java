package co.com.coomeva.formulario.validador.dao.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Parametro;

/**
* Parametro Dao <br>
* Info. Creación: <br>
* fecha 23 abr 2021 <br>
* @author GTC
**/
public interface ParametroDao  {
    
    /**
     * Obtiene todos los registros activos de parametro <br>
     * Info. Creación: <br>
     * fecha 23 abr 2021 <br>
     * @author GTC
     * @return
     * @throws Exception 
     */
    public List<Parametro> findAllActive() throws BusinessException;

    /**
     * Obtiene todos los registros de parametro por su id <br>
     * Info. Creación: <br>
     * fecha 23 abr 2021 <br>
     * @author GTC
     * @param parametro
     * @return
     * @throws Exception 
     */
    public List<Parametro> findByParametro(Parametro parametro) throws BusinessException;   

    /**
     * Obtiene todos los registros de parametro por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 23 abr 2021 <br>
     * @author GTC
     * @param parametro
     * @return
     * @throws Exception 
     */
    public List<Parametro> findByFiltro(Parametro parametro) throws BusinessException; 

    /**
    * Obtiene todos los registros de parametro por nombre <br>
    * Info. Creación: <br>
    * fecha 23 abr 2021 <br>
    * @author GTC
    * @param parametro
    * @return
    * @throws Exception 
    */
    public List<Parametro> findByName(Parametro parametro) throws BusinessException;  
}
