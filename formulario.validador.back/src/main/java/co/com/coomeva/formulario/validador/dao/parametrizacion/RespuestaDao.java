package co.com.coomeva.formulario.validador.dao.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Respuesta;

/**
* Respuesta Dao <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
public interface RespuestaDao  {
    
    /**
     * Obtiene todos los registros activos de respuesta <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws Exception 
     */
    public List<Respuesta> findAllActive() throws BusinessException;

    /**
     * Obtiene todos los registros de respuesta por su id <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param respuesta
     * @return
     * @throws Exception 
     */
    public List<Respuesta> findByRespuesta(Respuesta respuesta) throws BusinessException;   

    /**
     * Obtiene todos los registros de respuesta por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param respuesta
     * @return
     * @throws Exception 
     */
    public List<Respuesta> findByFiltro(Respuesta respuesta) throws BusinessException; 
    /**
     * Obtiene todos los registros de respuesta por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param respuesta
     * @return
     * @throws Exception 
     */
    public List<Respuesta> findByValor(Respuesta respuesta) throws BusinessException;

}
