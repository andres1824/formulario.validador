package co.com.coomeva.formulario.validador.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the FV_RESPUESTA database table.
 * 
 */
@Entity
@Table(name="FV_RESPUESTA")
@NamedQuery(name="Respuesta.findAll", query="SELECT r FROM Respuesta r")
public class Respuesta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @Basic(optional = false)
    @GeneratedValue(generator = "fv_respuesta_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "fv_respuesta_seq", sequenceName = "fv_respuesta_seq",allocationSize=1)    
	@Column(name="RESPUESTA_ID")
	private Long respuestaId;

	private String valor;
	
	private String estado;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_MODIFICACION")
	private Date fechaModificacion;


	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;

	@Column(name="USUARIO_MODIFICACION")
	private String usuarioModificacion;

	//bi-directional many-to-one association to RespuestaCampo
	@OneToMany(mappedBy="fvRespuesta")
	private List<RespuestaCampo> fvRespuestaCampos;

	public Respuesta() {
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public Long getRespuestaId() {
		return this.respuestaId;
	}

	public void setRespuestaId(Long respuestaId) {
		this.respuestaId = respuestaId;
	}

	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	@JsonIgnore
	public List<RespuestaCampo> getFvRespuestaCampos() {
		return this.fvRespuestaCampos;
	}

	public void setFvRespuestaCampos(List<RespuestaCampo> fvRespuestaCampos) {
		this.fvRespuestaCampos = fvRespuestaCampos;
	}

	public RespuestaCampo addFvRespuestaCampo(RespuestaCampo fvRespuestaCampo) {
		getFvRespuestaCampos().add(fvRespuestaCampo);
		fvRespuestaCampo.setFvRespuesta(this);

		return fvRespuestaCampo;
	}

	public RespuestaCampo removeFvRespuestaCampo(RespuestaCampo fvRespuestaCampo) {
		getFvRespuestaCampos().remove(fvRespuestaCampo);
		fvRespuestaCampo.setFvRespuesta(null);

		return fvRespuestaCampo;
	}

}