package co.com.coomeva.formulario.validador.service.impl.formulario;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.dao.formulario.FormularioAgrCampoRep;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioEmpleadoDao;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioEmpleadoRep;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioDao;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioRep;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioValorDao;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioValorRep;
import co.com.coomeva.formulario.validador.dto.FormularioAgrCampoValueDTO;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioAgrCampo;
import co.com.coomeva.formulario.validador.model.FormularioEmpleado;
import co.com.coomeva.formulario.validador.model.FormularioValor;
import co.com.coomeva.formulario.validador.service.formulario.FormularioEmpleadoService;
import co.com.coomeva.formulario.validador.service.formulario.FormularioService;
import co.com.coomeva.formulario.validador.service.formulario.FormularioValorService;

/**
* Formulario Service Impl <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
@Service("formularioEmpleadoService")
@Transactional
public class FormularioEmpleadoServiceImpl implements FormularioEmpleadoService {

    @Autowired
    private FormularioEmpleadoDao formularioDao;

    @Autowired
    private FormularioEmpleadoRep formularioRep;


    
    @Override
    public FormularioEmpleado create(FormularioEmpleado formulario) throws BusinessException {
        return formularioRep.save(formulario);
    }

    @Override
    public FormularioEmpleado store(FormularioEmpleado formulario) throws BusinessException {
       return formularioRep.save(formulario);
    }

    @Override
    public void remove(FormularioEmpleado formulario) throws BusinessException {
        formularioRep.delete(formulario);
    }

    @Override
    public List<FormularioEmpleado> findAll() throws BusinessException {
        return formularioRep.findAll();
    }

    @Override
    public List<FormularioEmpleado> findAllActive() throws BusinessException {
        return formularioDao.findAllActive();
    }
    @Override
    public List<FormularioEmpleado> findByFormularioAsociado(FormularioEmpleado formulario) throws BusinessException {
        return formularioDao.findByFormularioAsociado(formulario);
    }

    @Override
    public List<FormularioEmpleado> findByFiltro(FormularioEmpleado formulario) throws BusinessException {
        return formularioDao.findByFiltro(formulario);
    }

	@Override
	public boolean validarAsociado(FormularioAgrCampoValueDTO formularioAgrCampoValueDTO) {
		return formularioDao.validarAsociado(formularioAgrCampoValueDTO);
		
	}

    
}