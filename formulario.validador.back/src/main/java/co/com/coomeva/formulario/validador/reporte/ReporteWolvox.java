package co.com.coomeva.formulario.validador.reporte;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReporteWolvox {
	
	public ArrayList<Object> exportExcel(List<Object> lst, String name) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		ArrayList<Object> respuesta = new ArrayList<Object>();
		String base64 = "";
		short rowIndex = 0;
		short colIndex = 0;
		
		XSSFSheet spreadsheet = workbook.createSheet("Wolvox");
		XSSFRow row = spreadsheet.createRow(rowIndex);
		CellStyle cellStyle = row.getSheet().getWorkbook().createCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.LEFT);

		XSSFCell cell = (XSSFCell) row.createCell(colIndex);
		cell.setCellValue("Segmento");
		cell.setCellStyle(cellStyle);
		
		cell = (XSSFCell) row.createCell(colIndex++);
		cell.setCellValue("Cedula");
		cell.setCellStyle(cellStyle);
		
		cell = (XSSFCell) row.createCell(colIndex++);
		cell.setCellValue("Nombre del asociado");
		cell.setCellStyle(cellStyle);
		
		cell = (XSSFCell) row.createCell(colIndex++);
		cell.setCellValue("Celular 1");
		cell.setCellStyle(cellStyle);
		
		cell = (XSSFCell) row.createCell(colIndex++);
		cell.setCellValue("Celular 2");
		cell.setCellStyle(cellStyle);
		
		cell = (XSSFCell) row.createCell(colIndex++);
		cell.setCellValue("Celular 3");
		cell.setCellStyle(cellStyle);
		
		cell = (XSSFCell) row.createCell(colIndex++);
		cell.setCellValue("Regional");
		cell.setCellStyle(cellStyle);
		
		cell = (XSSFCell) row.createCell(colIndex++);
		cell.setCellValue("Ciudad");
		cell.setCellStyle(cellStyle);

		cell = (XSSFCell) row.createCell(colIndex++);
		cell.setCellValue("Oferta");
		cell.setCellStyle(cellStyle);
		
		

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		/*try {

			workbook.write(baos);
			workbook.close();
			base64 = Base64.encodeBase64String(baos.toByteArray());

		} catch (IOException e) {

			e.printStackTrace();
		}*/

		respuesta.add(base64);

		respuesta.add(name + ".xlsx");
		return respuesta;

	}
}
