package co.com.coomeva.formulario.validador.controller.parametrizacion;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.TipografiaDTO;
import co.com.coomeva.formulario.validador.model.Tipografia;
import co.com.coomeva.formulario.validador.service.parametrizacion.TipografiaService;
import co.com.coomeva.formulario.validador.util.AppUtil;
import co.com.coomeva.formulario.validador.util.ManagePropertiesLogic;
import co.com.coomeva.formulario.validador.util.ResponseServiceUtil;


/**
* Tipografia Controller <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/

@RestController
@RequestMapping(value = "/api/tipografia", produces = "application/json; charset=UTF-8")
@CrossOrigin(origins = "http://localhost:4200", methods = { RequestMethod.GET, RequestMethod.POST })
public class TipografiaController {

    @Autowired
    TipografiaService tipografiaService;
    
    private static final Logger log = LoggerFactory.getLogger(TipografiaController.class);
    ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
    public static final String PROPERTIES = "tipografia.properties";

    /**
     * Crea tipografia <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipografia
     * @return
     */
    @PostMapping(value = "/create")
    public ResponseService createTipografia(@RequestBody TipografiaDTO tipografiaDTO) {
            ResponseService responseService = new ResponseService();
        try {
            Tipografia tipografia = new Tipografia();
            tipografia = (Tipografia) AppUtil.convertOneToAnother(tipografiaDTO, tipografia);
            Tipografia tipografiaName = new Tipografia();
            tipografiaName.setNombre(tipografia.getNombre());
            List<Tipografia> listTipografiaName = tipografiaService.findByName(tipografiaName);
            
            if(listTipografiaName != null && !listTipografiaName.isEmpty()) {
                ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                return responseService;
            }
            
            tipografia.setFechaCreacion(new Date());
            tipografiaService.create(tipografia);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroCreado"));
        } catch (Exception e) {
            log.error("Error en el servicio /tipografia " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }

    /**
     * Modifica tipografia <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipografia
     * @return
     */
    @PostMapping(value = "/update")
    public ResponseService updateTipografia(@RequestBody TipografiaDTO tipografiaDTO) {
       ResponseService responseService = new ResponseService();
       try {
            Tipografia tipografia = new Tipografia();
            tipografia = (Tipografia) AppUtil.convertOneToAnother(tipografiaDTO, tipografia);
            Tipografia tipografiaName = new Tipografia();
            tipografiaName.setNombre(tipografia.getNombre());
            List<Tipografia> listTipografiaName = tipografiaService.findByName(tipografiaName);
            
            if(listTipografiaName != null && !listTipografiaName.isEmpty()) {
                boolean existName = false;
                for (Tipografia tipografia1 : listTipografiaName) {
                    if (!tipografia1.getTipografiaId().equals(tipografia.getTipografiaId())) {
                        existName = true;
                    }
                }

                if (existName) {
                    ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                    return responseService;
                }                
            }
            
            tipografia.setFechaModificacion(new Date());
            tipografiaService.store(tipografia);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEditado"));
        } catch (Exception e) {
            log.error("Error en el servicio /tipografia/update " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Elimina tipografia <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipografia
     * @return
     */
    @PostMapping(value = "/delete")
    public ResponseService deleteTipografia(@RequestBody TipografiaDTO tipografiaDTO) {
      ResponseService responseService = new ResponseService();
      try {
            Tipografia tipografia = new Tipografia();
            tipografia = (Tipografia) AppUtil.convertOneToAnother(tipografiaDTO, tipografia);
            tipografiaService.remove(tipografia);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEliminado"));
        } catch (Exception e) {
            log.error("Error en el servicio /tipografia/delete " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Consulta tipografia <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     */
    @GetMapping(value = "/get/all")
    public ResponseService list() {
        ResponseService responseService = new ResponseService();
        try {     
            List<Tipografia> tipografiaList = tipografiaService.findAllActive();
            ResponseServiceUtil.buildSuccessResponse(responseService, tipografiaList);
        } catch (Exception e) {
            log.error("Error en el servicio /tipografia/get/all " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Consulta tipografia <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     */
    @PostMapping(value = "/find")
    public ResponseService find(@RequestBody TipografiaDTO tipografiaDTO) {
       ResponseService responseService = new ResponseService();
       try {
            Tipografia tipografia = new Tipografia();
            tipografia = (Tipografia) AppUtil.convertOneToAnother(tipografiaDTO, tipografia);
            List<Tipografia> tipografiaList = tipografiaService.findByFiltro(tipografia);
            ResponseServiceUtil.buildSuccessResponse(responseService, tipografiaList);
        } catch (Exception e) {
            log.error("Error en el servicio /tipografia/find " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
}
