package co.com.coomeva.formulario.validador.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the FV_FORMULARIO database table.
 * 
 */
public class FormularioDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long formularioId;

	private String banner;
	
	private String nombreArchivoReporte;

	private String codigo;

	private String descripcion;
	
	private String nombreAdjunto;

	private String estado;

	private Date fechaCreacion;

	private Date fechaModificacion;

	private Date fechaVigencia;

	private String frecuencia;

	private String generaAsociados;

	private String generaNoAsociados;

	private String generaReporteAuto;

	private String horario;

	private String indCssExterno;

	private String nombre;

	private String rutaReporte;

	private String urlCssExterno;

	private String usuarioCreacion;

	private String usuarioModificacion;

	private ColorDTO fvColor;

	private EmpresaDTO fvEmpresa;

	private TipografiaDTO fvTipografia;

	private TipoReporteDTO fvTipoReporte;

	private List<FormularioAgrCampoDTO> fvFormularioAgrCampos;

	public FormularioDTO() {
	}

	public Long getFormularioId() {
		return this.formularioId;
	}

	public void setFormularioId(Long formularioId) {
		this.formularioId = formularioId;
	}

	public String getNombreAdjunto() {
		return nombreAdjunto;
	}

	public void setNombreAdjunto(String nombreAdjunto) {
		this.nombreAdjunto = nombreAdjunto;
	}

	public String getBanner() {
		return this.banner;
	}

	public void setBanner(String banner) {
		this.banner = banner;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Date getFechaVigencia() {
		return this.fechaVigencia;
	}

	public void setFechaVigencia(Date fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

	public String getFrecuencia() {
		return this.frecuencia;
	}

	public void setFrecuencia(String frecuencia) {
		this.frecuencia = frecuencia;
	}

	public String getGeneraAsociados() {
		return this.generaAsociados;
	}

	public void setGeneraAsociados(String generaAsociados) {
		this.generaAsociados = generaAsociados;
	}

	public String getGeneraNoAsociados() {
		return this.generaNoAsociados;
	}

	public void setGeneraNoAsociados(String generaNoAsociados) {
		this.generaNoAsociados = generaNoAsociados;
	}

	public String getGeneraReporteAuto() {
		return this.generaReporteAuto;
	}

	public void setGeneraReporteAuto(String generaReporteAuto) {
		this.generaReporteAuto = generaReporteAuto;
	}

	public String getHorario() {
		return this.horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public String getIndCssExterno() {
		return this.indCssExterno;
	}

	public void setIndCssExterno(String indCssExterno) {
		this.indCssExterno = indCssExterno;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
	public String getRutaReporte() {
		return this.rutaReporte;
	}

	public void setRutaReporte(String rutaReporte) {
		this.rutaReporte = rutaReporte;
	}

	public String getUrlCssExterno() {
		return this.urlCssExterno;
	}

	public void setUrlCssExterno(String urlCssExterno) {
		this.urlCssExterno = urlCssExterno;
	}

	public String getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public ColorDTO getFvColor() {
		return this.fvColor;
	}

	public void setFvColor(ColorDTO fvColor) {
		this.fvColor = fvColor;
	}

	public EmpresaDTO getFvEmpresa() {
		return this.fvEmpresa;
	}

	public void setFvEmpresa(EmpresaDTO fvEmpresa) {
		this.fvEmpresa = fvEmpresa;
	}

	public TipografiaDTO getFvTipografia() {
		return this.fvTipografia;
	}

	public void setFvTipografia(TipografiaDTO fvTipografia) {
		this.fvTipografia = fvTipografia;
	}

	public TipoReporteDTO getFvTipoReporte() {
		return this.fvTipoReporte;
	}

	public void setFvTipoReporte(TipoReporteDTO fvTipoReporte) {
		this.fvTipoReporte = fvTipoReporte;
	}
	
	@JsonIgnore
	public List<FormularioAgrCampoDTO> getFvFormularioAgrCampos() {
		return this.fvFormularioAgrCampos;
	}

	public void setFvFormularioAgrCampos(List<FormularioAgrCampoDTO> fvFormularioAgrCampos) {
		this.fvFormularioAgrCampos = fvFormularioAgrCampos;
	}

	public FormularioAgrCampoDTO addFvFormularioAgrCampo(FormularioAgrCampoDTO fvFormularioAgrCampo) {
		getFvFormularioAgrCampos().add(fvFormularioAgrCampo);
		fvFormularioAgrCampo.setFvFormulario(this);

		return fvFormularioAgrCampo;
	}

	public FormularioAgrCampoDTO removeFvFormularioAgrCampo(FormularioAgrCampoDTO fvFormularioAgrCampo) {
		getFvFormularioAgrCampos().remove(fvFormularioAgrCampo);
		fvFormularioAgrCampo.setFvFormulario(null);

		return fvFormularioAgrCampo;
	}

	public String getNombreArchivoReporte() {
		return nombreArchivoReporte;
	}

	public void setNombreArchivoReporte(String nombreArchivoReporte) {
		this.nombreArchivoReporte = nombreArchivoReporte;
	}
	
	

}