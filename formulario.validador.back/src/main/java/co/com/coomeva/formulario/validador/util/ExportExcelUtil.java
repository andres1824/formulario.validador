package co.com.coomeva.formulario.validador.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class ExportExcelUtil {
	public static Double infinityOrDouble(Double n) {
		if (n == null) {
			return new Double(0);
		}
		if (n.isNaN()) {
			return new Double(0);
		}
		if (n.isInfinite()) {
			return new Double(0);
		}
		return n;
	}

	public static Double fixedvalues(Double ob) {
		String rt = "0";
		if (!ob.toString().equals("0.0") && !ob.isNaN() && !ob.isInfinite()) {
			NumberFormat formatter = new DecimalFormat("#0.00");
			rt = formatter.format(ob);
		}
		rt = rt.contains(",") ? rt.replace(",", ".") : rt;

		rt = rt.equals("?") ? rt.replace("?", "0") : rt;
		return Double.parseDouble(rt);
	}

	public static String ConvertToPrice(Double number) {
		String s = "0";
		if (!number.toString().equals("0.0") && !number.isNaN() && !number.isInfinite()) {

			DecimalFormat df = new DecimalFormat("#,###.00");

			s = df.format(number);

			s = s.replace(",00", "");
		}
		return s;
	}
}
