package co.com.coomeva.formulario.validador.service.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.TipoDato;

/**
* TipoDato Service <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
public interface TipoDatoService {
    

    /**
     * Crea tipoDato <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoDato
     * @return 
     * @throws BusinessException 
     */
    public TipoDato create(TipoDato tipoDato) throws BusinessException;
    /**
     * Modifica tipoDato <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoDato
     * @return 
     * @throws BusinessException 
     */
    public TipoDato store(TipoDato tipoDato) throws BusinessException;
    /**
     * Elimina tipoDato<br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoDato
     * @throws BusinessException 
     */
    public void remove(TipoDato tipoDato) throws BusinessException;
    /**
     * Obtiene todos los registros de tipoDato <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<TipoDato> findAll() throws BusinessException;
    
    /**
     * Obtiene todos los registros activos de tipoDato <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<TipoDato> findAllActive() throws BusinessException;
    /**
     * Obtiene todos los registros de tipoDato paginados <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoDato
     * @return
     * @throws BusinessException 
     */
    public List<TipoDato> findByTipoDato(TipoDato tipoDato) throws BusinessException;   

    /**
     * Obtiene todos los registros de tipoDato por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoDato
     * @return
     * @throws BusinessException 
     */
    public List<TipoDato> findByFiltro(TipoDato tipoDato) throws BusinessException; 


    /**
    * Obtiene todos los registros de tipoDato por nombre <br>
    * Info. Creación: <br>
    * fecha 24 abr 2021 <br>
    * @author GTC
    * @param tipoDato
    * @return
    * @throws BusinessException 
    */
    public List<TipoDato> findByName(TipoDato tipoDato) throws BusinessException;  

    
}