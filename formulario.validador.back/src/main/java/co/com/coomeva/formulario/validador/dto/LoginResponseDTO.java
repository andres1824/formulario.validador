package co.com.coomeva.formulario.validador.dto;

import java.util.List;

public class LoginResponseDTO {
	private String token;
	private UserInfoDTO userInfo;
	
	
	public UserInfoDTO getUserInfo() {
		return userInfo;
	}
	public void setUserInfo(UserInfoDTO userInfo) {
		this.userInfo = userInfo;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	
}
