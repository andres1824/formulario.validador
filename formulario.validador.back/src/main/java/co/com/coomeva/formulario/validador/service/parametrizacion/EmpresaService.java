package co.com.coomeva.formulario.validador.service.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Empresa;

/**
* Empresa Service <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
public interface EmpresaService {
    

    /**
     * Crea empresa <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param empresa
     * @return 
     * @throws BusinessException 
     */
    public Empresa create(Empresa empresa) throws BusinessException;
    /**
     * Modifica empresa <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param empresa
     * @return 
     * @throws BusinessException 
     */
    public Empresa store(Empresa empresa) throws BusinessException;
    /**
     * Elimina empresa<br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param empresa
     * @throws BusinessException 
     */
    public void remove(Empresa empresa) throws BusinessException;
    /**
     * Obtiene todos los registros de empresa <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<Empresa> findAll() throws BusinessException;
    
    /**
     * Obtiene todos los registros activos de empresa <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<Empresa> findAllActive() throws BusinessException;
    /**
     * Obtiene todos los registros de empresa paginados <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param empresa
     * @return
     * @throws BusinessException 
     */
    public List<Empresa> findByEmpresa(Empresa empresa) throws BusinessException;   

    /**
     * Obtiene todos los registros de empresa por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param empresa
     * @return
     * @throws BusinessException 
     */
    public List<Empresa> findByFiltro(Empresa empresa) throws BusinessException; 


    /**
    * Obtiene todos los registros de empresa por nombre <br>
    * Info. Creación: <br>
    * fecha 24 abr 2021 <br>
    * @author GTC
    * @param empresa
    * @return
    * @throws BusinessException 
    */
    public List<Empresa> findByName(Empresa empresa) throws BusinessException;  

    
}