package co.com.coomeva.formulario.validador.service.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Agrupacion;
import co.com.coomeva.formulario.validador.model.AgrupacionCampo;

/**
* Agrupacion Service <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
public interface AgrupacionService {
    

    /**
     * Crea agrupacion <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param agrupacion
     * @return 
     * @throws BusinessException 
     */
    public Agrupacion create(Agrupacion agrupacion) throws BusinessException;
    /**
     * Modifica agrupacion <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param agrupacion
     * @return 
     * @throws BusinessException 
     */
    public Agrupacion store(Agrupacion agrupacion) throws BusinessException;
    /**
     * Elimina agrupacion<br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param agrupacion
     * @throws BusinessException 
     */
    public void remove(Agrupacion agrupacion) throws BusinessException;
    /**
     * Obtiene todos los registros de agrupacion <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<Agrupacion> findAll() throws BusinessException;
    
    /**
     * Obtiene todos los registros activos de agrupacion <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<Agrupacion> findAllActive() throws BusinessException;
    
    /**
     * Obtiene todos los registros de agrupacion paginados <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param agrupacion
     * @return
     * @throws BusinessException 
     */
    public List<Agrupacion> findByAgrupacion(Agrupacion agrupacion) throws BusinessException;   

    /**
     * Obtiene todos los registros de agrupacion por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param agrupacion
     * @return
     * @throws BusinessException 
     */
    public List<Agrupacion> findByFiltro(Agrupacion agrupacion) throws BusinessException; 


    /**
    * Obtiene todos los registros de agrupacion por nombre <br>
    * Info. Creación: <br>
    * fecha 1 may 2021 <br>
    * @author GTC
    * @param agrupacion
    * @return
    * @throws BusinessException 
    */
    public List<Agrupacion> findByName(Agrupacion agrupacion) throws BusinessException; 
    
    /**
     * Obtiene todos los registros de agrupacion campo <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param agrupacion
     * @return
     * @throws BusinessException 
     */
    public List<Agrupacion> findByAgrupacionCampo(AgrupacionCampo agrupacion) throws BusinessException;
    
    /**
     * Crea agrupacion campo <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param agrupacion
     * @return 
     * @throws BusinessException 
     */
    public AgrupacionCampo createCampo(AgrupacionCampo agrupacion) throws BusinessException;
    /**
     * Elimina agrupacion campo<br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param agrupacion
     * @throws BusinessException 
     */
    public void removeCampo(AgrupacionCampo agrupacion) throws BusinessException;
    /**
     * Obtiene todos los registros de agrupacion campo <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param agrupacionId
     * @return
     * @throws BusinessException 
     */
    public List<AgrupacionCampo> findAllAgrupacionCampo(Long agrupacionId) throws BusinessException;
    
    

    
}