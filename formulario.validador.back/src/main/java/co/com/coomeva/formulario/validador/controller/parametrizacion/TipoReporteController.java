package co.com.coomeva.formulario.validador.controller.parametrizacion;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.TipoReporteDTO;
import co.com.coomeva.formulario.validador.model.TipoReporte;
import co.com.coomeva.formulario.validador.service.parametrizacion.TipoReporteService;
import co.com.coomeva.formulario.validador.util.AppUtil;
import co.com.coomeva.formulario.validador.util.ManagePropertiesLogic;
import co.com.coomeva.formulario.validador.util.ResponseServiceUtil;


/**
* TipoReporte Controller <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/

@RestController
@RequestMapping(value = "/api/tipoReporte", produces = "application/json; charset=UTF-8")
@CrossOrigin(origins = "http://localhost:4200", methods = { RequestMethod.GET, RequestMethod.POST })
public class TipoReporteController {

    @Autowired
    TipoReporteService tipoReporteService;
    
    private static final Logger log = LoggerFactory.getLogger(TipoReporteController.class);
    ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
    public static final String PROPERTIES = "tipoReporte.properties";

    /**
     * Crea tipoReporte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoReporte
     * @return
     */
    @PostMapping(value = "/create")
    public ResponseService createTipoReporte(@RequestBody TipoReporteDTO tipoReporteDTO) {
            ResponseService responseService = new ResponseService();
        try {
            TipoReporte tipoReporte = new TipoReporte();
            tipoReporte = (TipoReporte) AppUtil.convertOneToAnother(tipoReporteDTO, tipoReporte);
            TipoReporte tipoReporteName = new TipoReporte();
            tipoReporteName.setNombre(tipoReporte.getNombre());
            List<TipoReporte> listTipoReporteName = tipoReporteService.findByName(tipoReporteName);
            
            if(listTipoReporteName != null && !listTipoReporteName.isEmpty()) {
                ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                return responseService;
            }
            
            tipoReporte.setFechaCreacion(new Date());
            tipoReporteService.create(tipoReporte);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroCreado"));
        } catch (Exception e) {
            log.error("Error en el servicio /tipoReporte " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }

    /**
     * Modifica tipoReporte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoReporte
     * @return
     */
    @PostMapping(value = "/update")
    public ResponseService updateTipoReporte(@RequestBody TipoReporteDTO tipoReporteDTO) {
       ResponseService responseService = new ResponseService();
       try {
            TipoReporte tipoReporte = new TipoReporte();
            tipoReporte = (TipoReporte) AppUtil.convertOneToAnother(tipoReporteDTO, tipoReporte);
            TipoReporte tipoReporteName = new TipoReporte();
            tipoReporteName.setNombre(tipoReporte.getNombre());
            List<TipoReporte> listTipoReporteName = tipoReporteService.findByName(tipoReporteName);
            
            if(listTipoReporteName != null && !listTipoReporteName.isEmpty()) {
                boolean existName = false;
                for (TipoReporte tipoReporte1 : listTipoReporteName) {
                    if (!tipoReporte1.getTipoReporteId().equals(tipoReporte.getTipoReporteId())) {
                        existName = true;
                    }
                }

                if (existName) {
                    ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                    return responseService;
                }                
            }
            
            tipoReporte.setFechaModificacion(new Date());
            tipoReporteService.store(tipoReporte);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEditado"));
        } catch (Exception e) {
            log.error("Error en el servicio /tipoReporte/update " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Elimina tipoReporte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoReporte
     * @return
     */
    @PostMapping(value = "/delete")
    public ResponseService deleteTipoReporte(@RequestBody TipoReporteDTO tipoReporteDTO) {
      ResponseService responseService = new ResponseService();
      try {
            TipoReporte tipoReporte = new TipoReporte();
            tipoReporte = (TipoReporte) AppUtil.convertOneToAnother(tipoReporteDTO, tipoReporte);
            tipoReporteService.remove(tipoReporte);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEliminado"));
        } catch (Exception e) {
            log.error("Error en el servicio /tipoReporte/delete " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Consulta tipoReporte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     */
    @GetMapping(value = "/get/all")
    public ResponseService list() {
        ResponseService responseService = new ResponseService();
        try {     
            List<TipoReporte> tipoReporteList = tipoReporteService.findAllActive();
            ResponseServiceUtil.buildSuccessResponse(responseService, tipoReporteList);
        } catch (Exception e) {
            log.error("Error en el servicio /tipoReporte/get/all " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Consulta tipoReporte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     */
    @PostMapping(value = "/find")
    public ResponseService find(@RequestBody TipoReporteDTO tipoReporteDTO) {
       ResponseService responseService = new ResponseService();
       try {
            TipoReporte tipoReporte = new TipoReporte();
            tipoReporte = (TipoReporte) AppUtil.convertOneToAnother(tipoReporteDTO, tipoReporte);
            List<TipoReporte> tipoReporteList = tipoReporteService.findByFiltro(tipoReporte);
            ResponseServiceUtil.buildSuccessResponse(responseService, tipoReporteList);
        } catch (Exception e) {
            log.error("Error en el servicio /tipoReporte/find " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
}
