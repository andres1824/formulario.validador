package co.com.coomeva.formulario.validador.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the FV_EMPRESA database table.
 * 
 */
@Entity
@Table(name="FV_EMPRESA")
@NamedQuery(name="Empresa.findAll", query="SELECT e FROM Empresa e")
public class Empresa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @Basic(optional = false)
    @GeneratedValue(generator = "fv_empresa_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "fv_empresa_seq", sequenceName = "fv_empresa_seq",allocationSize=1)    
	@Column(name="EMPRESA_ID")
	private Long empresaId;

	private String estado;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_MODIFICACION")
	private Date fechaModificacion;

	private String nombre;

	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;

	@Column(name="USUARIO_MODIFICACION")
	private String usuarioModificacion;

	//bi-directional many-to-one association to Formulario
	@OneToMany(mappedBy="fvEmpresa")
	private List<Formulario> fvFormularios;

	public Empresa() {
	}

	public Long getEmpresaId() {
		return this.empresaId;
	}

	public void setEmpresaId(Long empresaId) {
		this.empresaId = empresaId;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUsuarioCreacion() {
		return this.usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}
	
	@JsonIgnore
	public List<Formulario> getFvFormularios() {
		return this.fvFormularios;
	}

	public void setFvFormularios(List<Formulario> fvFormularios) {
		this.fvFormularios = fvFormularios;
	}

	public Formulario addFvFormulario(Formulario fvFormulario) {
		getFvFormularios().add(fvFormulario);
		fvFormulario.setFvEmpresa(this);

		return fvFormulario;
	}

	public Formulario removeFvFormulario(Formulario fvFormulario) {
		getFvFormularios().remove(fvFormulario);
		fvFormulario.setFvEmpresa(null);

		return fvFormulario;
	}

}