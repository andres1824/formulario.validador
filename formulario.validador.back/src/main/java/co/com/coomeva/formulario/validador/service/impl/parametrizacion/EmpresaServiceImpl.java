package co.com.coomeva.formulario.validador.service.impl.parametrizacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.dao.parametrizacion.EmpresaDao;
import co.com.coomeva.formulario.validador.dao.parametrizacion.EmpresaRep;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Empresa;
import co.com.coomeva.formulario.validador.service.parametrizacion.EmpresaService;

/**
* Empresa Service Impl <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Service("empresaService")
@Transactional
public class EmpresaServiceImpl implements EmpresaService {

    @Autowired
    private EmpresaDao empresaDao;

    @Autowired
	@Qualifier("empresaRep")
    private EmpresaRep empresaRep;

    @Override
    public Empresa create(Empresa empresa) throws BusinessException {
        return empresaRep.save(empresa);
    }

    @Override
    public Empresa store(Empresa empresa) throws BusinessException {
       return empresaRep.save(empresa);
    }

    @Override
    public void remove(Empresa empresa) throws BusinessException {
        empresaRep.delete(empresa);
    }

    @Override
    public List<Empresa> findAll() throws BusinessException {
        return empresaRep.findAll();
    }

    @Override
    public List<Empresa> findAllActive() throws BusinessException {
        return empresaDao.findAllActive();
    }
    @Override
    public List<Empresa> findByEmpresa(Empresa empresa) throws BusinessException {
        return empresaDao.findByEmpresa(empresa);
    }

    @Override
    public List<Empresa> findByFiltro(Empresa empresa) throws BusinessException {
        return empresaDao.findByFiltro(empresa);
    }

    @Override
    public List<Empresa> findByName(Empresa empresa) throws BusinessException {
        return empresaDao.findByName(empresa);
    }
}