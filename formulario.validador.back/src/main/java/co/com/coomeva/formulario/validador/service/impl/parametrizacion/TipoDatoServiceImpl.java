package co.com.coomeva.formulario.validador.service.impl.parametrizacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.dao.parametrizacion.TipoDatoDao;
import co.com.coomeva.formulario.validador.dao.parametrizacion.TipoDatoRep;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.TipoDato;
import co.com.coomeva.formulario.validador.service.parametrizacion.TipoDatoService;

/**
* TipoDato Service Impl <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Service("tipoDatoService")
@Transactional
public class TipoDatoServiceImpl implements TipoDatoService {

    @Autowired
    private TipoDatoDao tipoDatoDao;

    @Autowired
    private TipoDatoRep tipoDatoRep;

    @Override
    public TipoDato create(TipoDato tipoDato) throws BusinessException {
        return tipoDatoRep.save(tipoDato);
    }

    @Override
    public TipoDato store(TipoDato tipoDato) throws BusinessException {
       return tipoDatoRep.save(tipoDato);
    }

    @Override
    public void remove(TipoDato tipoDato) throws BusinessException {
        tipoDatoRep.delete(tipoDato);
    }

    @Override
    public List<TipoDato> findAll() throws BusinessException {
        return tipoDatoRep.findAll();
    }

    @Override
    public List<TipoDato> findAllActive() throws BusinessException {
        return tipoDatoDao.findAllActive();
    }
    @Override
    public List<TipoDato> findByTipoDato(TipoDato tipoDato) throws BusinessException {
        return tipoDatoDao.findByTipoDato(tipoDato);
    }

    @Override
    public List<TipoDato> findByFiltro(TipoDato tipoDato) throws BusinessException {
        return tipoDatoDao.findByFiltro(tipoDato);
    }

    @Override
    public List<TipoDato> findByName(TipoDato tipoDato) throws BusinessException {
        return tipoDatoDao.findByName(tipoDato);
    }
}