package co.com.coomeva.formulario.validador.dao.impl.parametrizacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import co.com.coomeva.formulario.validador.dao.parametrizacion.TipoDatoDao;
import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.TipoDato;

/**
* TipoDato Dao Impl <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Repository
public class TipoDatoDaoImpl implements TipoDatoDao {
    
    @PersistenceContext
    private EntityManager em;
    public static final String FROM = "select a from TipoDato a ";
	
	
    public EntityManager getEntityManager() {
        return em;
    }


    @Override
    public List<TipoDato> findAllActive() throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.estado = :active ");

        builder.append("order by a.nombre asc ");
        
        
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("active", (EstadoEnum.ACTIVE.getId()));
        return query.getResultList();
    }
    
    @Override
    public List<TipoDato> findByTipoDato(TipoDato tipoDato) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.tipoDatoId = :id ");

        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("id", tipoDato.getTipoDatoId());
        return query.getResultList();
    }

    @Override
    public List<TipoDato> findByFiltro(TipoDato tipoDato) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE 1=1 ");
        
        if(tipoDato.getNombre() != null && !tipoDato.getNombre().trim().isEmpty()) {
            builder.append("AND lower(a.nombre) like lower(:nombre) ");
        }


        if(tipoDato.getLogica() != null && !tipoDato.getLogica().trim().isEmpty()) {
            builder.append("AND lower(a.logica) like lower(:logica) ");
        }




        if(tipoDato.getEstado() != null && !tipoDato.getEstado().trim().isEmpty()) {
            builder.append("AND a.estado = :estado ");
        }

        builder.append("order by a.nombre asc ");
        Query query = this.getEntityManager().createQuery(builder.toString());

        if(tipoDato.getNombre() != null && !tipoDato.getNombre().trim().isEmpty()) {
            query.setParameter("nombre", "%" + tipoDato.getNombre() + "%");
        }


        if(tipoDato.getLogica() != null && !tipoDato.getLogica().trim().isEmpty()) {
            query.setParameter("logica", "%" + tipoDato.getLogica() + "%");
        }




        if(tipoDato.getEstado() != null && !tipoDato.getEstado().trim().isEmpty()) {
            query.setParameter("estado", tipoDato.getEstado());
        }

        return query.getResultList();
    }

    @Override
    public List<TipoDato> findByName(TipoDato tipoDato) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE lower(trim(a.nombre)) = lower(trim(:nombre)) ");
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("nombre", tipoDato.getNombre().trim());
        return query.getResultList();
    }
}