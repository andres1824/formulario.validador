package co.com.coomeva.formulario.validador.controller.parametrizacion;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.TipoDatoDTO;
import co.com.coomeva.formulario.validador.model.TipoDato;
import co.com.coomeva.formulario.validador.service.parametrizacion.TipoDatoService;
import co.com.coomeva.formulario.validador.util.AppUtil;
import co.com.coomeva.formulario.validador.util.ManagePropertiesLogic;
import co.com.coomeva.formulario.validador.util.ResponseServiceUtil;


/**
* TipoDato Controller <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/

@RestController
@RequestMapping(value = "/api/tipoDato", produces = "application/json; charset=UTF-8")
@CrossOrigin(origins = "http://localhost:4200", methods = { RequestMethod.GET, RequestMethod.POST })
public class TipoDatoController {

    @Autowired
    TipoDatoService tipoDatoService;
    
    private static final Logger log = LoggerFactory.getLogger(TipoDatoController.class);
    ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
    public static final String PROPERTIES = "tipoDato.properties";

    /**
     * Crea tipoDato <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoDato
     * @return
     */
    @PostMapping(value = "/create")
    public ResponseService createTipoDato(@RequestBody TipoDatoDTO tipoDatoDTO) {
            ResponseService responseService = new ResponseService();
        try {
            TipoDato tipoDato = new TipoDato();
            tipoDato = (TipoDato) AppUtil.convertOneToAnother(tipoDatoDTO, tipoDato);
            TipoDato tipoDatoName = new TipoDato();
            tipoDatoName.setNombre(tipoDato.getNombre());
            List<TipoDato> listTipoDatoName = tipoDatoService.findByName(tipoDatoName);
            
            if(listTipoDatoName != null && !listTipoDatoName.isEmpty()) {
                ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                return responseService;
            }
            
            tipoDato.setFechaCreacion(new Date());
            tipoDatoService.create(tipoDato);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroCreado"));
        } catch (Exception e) {
            log.error("Error en el servicio /tipoDato " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
            
        }
        return responseService;
    }

    /**
     * Modifica tipoDato <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoDato
     * @return
     */
    @PostMapping(value = "/update")
    public ResponseService updateTipoDato(@RequestBody TipoDatoDTO tipoDatoDTO) {
       ResponseService responseService = new ResponseService();
       try {
            TipoDato tipoDato = new TipoDato();
            tipoDato = (TipoDato) AppUtil.convertOneToAnother(tipoDatoDTO, tipoDato);
            TipoDato tipoDatoName = new TipoDato();
            tipoDatoName.setNombre(tipoDato.getNombre());
            List<TipoDato> listTipoDatoName = tipoDatoService.findByName(tipoDatoName);
            
            if(listTipoDatoName != null && !listTipoDatoName.isEmpty()) {
                boolean existName = false;
                for (TipoDato tipoDato1 : listTipoDatoName) {
                    if (!tipoDato1.getTipoDatoId().equals(tipoDato.getTipoDatoId())) {
                        existName = true;
                    }
                }

                if (existName) {
                    ResponseServiceUtil.buildFailResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgNombreExiste"));
                    return responseService;
                }                
            }
            
            tipoDato.setFechaModificacion(new Date());
            tipoDatoService.store(tipoDato);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEditado"));
        } catch (Exception e) {
            log.error("Error en el servicio /tipoDato/update " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Elimina tipoDato <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param tipoDato
     * @return
     */
    @PostMapping(value = "/delete")
    public ResponseService deleteTipoDato(@RequestBody TipoDatoDTO tipoDatoDTO) {
      ResponseService responseService = new ResponseService();
      try {
            TipoDato tipoDato = new TipoDato();
            tipoDato = (TipoDato) AppUtil.convertOneToAnother(tipoDatoDTO, tipoDato);
            tipoDatoService.remove(tipoDato);
            ResponseServiceUtil.buildSuccessResponse(responseService, managePropertiesLogic.getProperties(PROPERTIES, "msgRegistroEliminado"));
        } catch (Exception e) {
            log.error("Error en el servicio /tipoDato/delete " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
    
    /**
     * Consulta tipoDato <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     */
    @GetMapping(value = "/get/all")
    public ResponseService list() {
        ResponseService responseService = new ResponseService();
        try {     
            List<TipoDato> tipoDatoList = tipoDatoService.findAllActive();
            ResponseServiceUtil.buildSuccessResponse(responseService, tipoDatoList);
        } catch (Exception e) {
            log.error("Error en el servicio /tipoDato/get/all " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }

    /**
     * Consulta tipoDato <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     */
    @PostMapping(value = "/find")
    public ResponseService find(@RequestBody TipoDatoDTO tipoDatoDTO) {
       ResponseService responseService = new ResponseService();
       try {
            TipoDato tipoDato = new TipoDato();
            tipoDato = (TipoDato) AppUtil.convertOneToAnother(tipoDatoDTO, tipoDato);
            List<TipoDato> tipoDatoList = tipoDatoService.findByFiltro(tipoDato);
            ResponseServiceUtil.buildSuccessResponse(responseService, tipoDatoList);
        } catch (Exception e) {
            log.error("Error en el servicio /tipoDato/find " + e.getMessage(), e);
            ResponseServiceUtil.buildFailResponse(responseService, e);
        }
        return responseService;
    }
}
