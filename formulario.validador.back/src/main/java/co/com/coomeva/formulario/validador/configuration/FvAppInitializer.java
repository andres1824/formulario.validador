package co.com.coomeva.formulario.validador.configuration;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * 
 * @author aflr0108
 *
 */
public class FvAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
	
	private String TMP_FOLDER = "/tmp"; 
    private long maxUploadSize = 5 * 1024 * 1024; 
    private int maxUploadThread = (int) maxUploadSize/2; 
	
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{FvAppConfiguration.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
    
    @Override
    public void onStartup(ServletContext container) throws ServletException {
        
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.setConfigLocation(FvAppConfiguration.class.getName());
        
        WebApplicationContext webContext = context;
        
        Dynamic registration = container.addServlet("dispatcher", new DispatcherServlet(webContext));
        registration.setLoadOnStartup(1);
        registration.addMapping("/*");
        long maxUpload = maxUploadSize * 2L;
        MultipartConfigElement multipartConfigElement = new MultipartConfigElement(TMP_FOLDER, 
        		maxUploadSize, maxUpload, maxUploadThread);
               
        registration.setMultipartConfig(multipartConfigElement);
    }

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[1];
	}
}
