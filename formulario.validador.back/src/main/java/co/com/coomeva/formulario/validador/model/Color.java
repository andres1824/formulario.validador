package co.com.coomeva.formulario.validador.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the FV_COLOR database table.
 * 
 */
@Entity
@Table(name="FV_COLOR")
@NamedQuery(name="Color.findAll", query="SELECT c FROM Color c")
public class Color implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @Basic(optional = false)
    @GeneratedValue(generator = "fv_color_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "fv_color_seq", sequenceName = "fv_color_seq",allocationSize=1)    
	@Column(name="COLOR_ID")
	private Long colorId;

	@Column(name="CODIGO_HTML")
	private String codigoHtml;

	private String nombre;
	
	private String estado;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_MODIFICACION")
	private Date fechaModificacion;


	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;

	@Column(name="USUARIO_MODIFICACION")
	private String usuarioModificacion;

	//bi-directional many-to-one association to Formulario
	@OneToMany(mappedBy="fvColor")
	private List<Formulario> fvFormularios;

	public Color() {
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public Long getColorId() {
		return this.colorId;
	}

	public void setColorId(Long colorId) {
		this.colorId = colorId;
	}

	public String getCodigoHtml() {
		return this.codigoHtml;
	}

	public void setCodigoHtml(String codigoHtml) {
		this.codigoHtml = codigoHtml;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@JsonIgnore
	public List<Formulario> getFvFormularios() {
		return this.fvFormularios;
	}

	public void setFvFormularios(List<Formulario> fvFormularios) {
		this.fvFormularios = fvFormularios;
	}

	public Formulario addFvFormulario(Formulario fvFormulario) {
		getFvFormularios().add(fvFormulario);
		fvFormulario.setFvColor(this);

		return fvFormulario;
	}

	public Formulario removeFvFormulario(Formulario fvFormulario) {
		getFvFormularios().remove(fvFormulario);
		fvFormulario.setFvColor(null);

		return fvFormulario;
	}

}