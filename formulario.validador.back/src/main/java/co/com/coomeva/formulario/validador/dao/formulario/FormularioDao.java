package co.com.coomeva.formulario.validador.dao.formulario;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioAgrCampo;

/**
* Formulario Dao <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
public interface FormularioDao  {
    
    /**
     * Obtiene todos los registros activos de formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @return
     * @throws Exception 
     */
    public List<Formulario> findAllActive() throws BusinessException;

    /**
     * Obtiene todos los registros de formulario por su id <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     * @throws Exception 
     */
    public List<Formulario> findByFormulario(Formulario formulario) throws BusinessException;   

    /**
     * Obtiene todos los registros de formulario por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     * @throws Exception 
     */
    public List<Formulario> findByFiltro(Formulario formulario) throws BusinessException; 

    /**
    * Obtiene todos los registros de formulario por nombre <br>
    * Info. Creación: <br>
    * fecha 1 may 2021 <br>
    * @author GTC
    * @param formulario
    * @return
    * @throws Exception 
    */
    public List<Formulario> findByName(Formulario formulario) throws BusinessException;  
    
    /**
     * Obtiene todos los registros de formularioAgrupacionCampo por formulario <br>
     * Info. Creación: <br>
     * fecha 1 may 2021 <br>
     * @author GTC
     * @param formulario
     * @return
     * @throws BusinessException 
     */
     public List<Formulario> findByFormularioAgrupacionCampo(FormularioAgrCampo formularioId) throws BusinessException;  
     
}
