package co.com.coomeva.formulario.validador.dao.impl.parametrizacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import co.com.coomeva.formulario.validador.dao.parametrizacion.CampoDao;
import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Campo;

/**
* Campo Dao Impl <br>
* Info. Creación: <br>
* fecha 25 abr 2021 <br>
* @author GTC
**/
@Repository
public class CampoDaoImpl implements CampoDao {
    
    @PersistenceContext
    private EntityManager em;
    public static final String FROM = "select a from Campo a ";
	
	
    public EntityManager getEntityManager() {
        return em;
    }


    @Override
    public List<Campo> findAllActive() throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.estado = :active ");

        builder.append("order by a.nombre asc ");
        
        
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("active", (EstadoEnum.ACTIVE.getId()));
        return query.getResultList();
    }
    
    @Override
    public List<Campo> findByCampo(Campo campo) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.campoId = :id ");

        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("id", campo.getCampoId());
        return query.getResultList();
    }

    @Override
    public List<Campo> findByFiltro(Campo campo) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE 1=1 ");
        
        if(campo.getNombre() != null && !campo.getNombre().trim().isEmpty()) {
            builder.append("AND lower(a.nombre) like lower(:nombre) ");
        }


        if(campo.getObligatorio() != null && !campo.getObligatorio().trim().isEmpty()) {
            builder.append("AND lower(a.obligatorio) like lower(:obligatorio) ");
        }





        if(campo.getFvTipoCampo() != null && campo.getFvTipoCampo().getTipoCampoId() != null) {
            builder.append("AND a.fvTipoCampo.tipoCampoId = :tipoCampo ");
        }


        if(campo.getFvTipoDato() != null && campo.getFvTipoDato().getTipoDatoId() != null) {
            builder.append("AND a.fvTipoCampo.tipoDatoId = :tipoDato ");
        }



        if(campo.getEstado() != null && !campo.getEstado().trim().isEmpty()) {
            builder.append("AND a.estado = :estado ");
        }

        builder.append("order by a.nombre asc ");
        Query query = this.getEntityManager().createQuery(builder.toString());

        if(campo.getNombre() != null && !campo.getNombre().trim().isEmpty()) {
            query.setParameter("nombre", "%" + campo.getNombre() + "%");
        }

        if(campo.getObligatorio() != null && !campo.getObligatorio().trim().isEmpty()) {
            query.setParameter("obligatorio", "%" + campo.getObligatorio() + "%");
        }





        if(campo.getFvTipoCampo() != null && campo.getFvTipoCampo().getTipoCampoId() != null) {
            query.setParameter("tipoCampo", campo.getFvTipoCampo().getTipoCampoId());
        }


        if(campo.getFvTipoDato() != null && campo.getFvTipoDato().getTipoDatoId() != null) {
            query.setParameter("tipoDato", campo.getFvTipoDato().getTipoDatoId());
        }



        if(campo.getEstado() != null && !campo.getEstado().trim().isEmpty()) {
            query.setParameter("estado", campo.getEstado());
        }

        return query.getResultList();
    }

    @Override
    public List<Campo> findByName(Campo campo) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE lower(trim(a.nombre)) = lower(trim(:nombre)) ");
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("nombre", campo.getNombre().trim());
        return query.getResultList();
    }


	@Override
	public List<Campo> findAllDependiente(Long campoId) throws BusinessException {
		StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.estado = :active ");
        builder.append("AND a.campoId NOT IN (:campoId) ");

        builder.append("order by a.nombre asc ");
        
        
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("active", (EstadoEnum.ACTIVE.getId()));
        query.setParameter("campoId", (campoId));
        return query.getResultList();
	}
}