package co.com.coomeva.formulario.validador.dao.impl.formulario;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.Normalizer;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import co.com.coomeva.formulario.validador.dao.formulario.FormularioDao;
import co.com.coomeva.formulario.validador.dao.formulario.FormularioValorDao;
import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.dto.FormularioAgrCampoValueDTO;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioAgrCampo;
import co.com.coomeva.formulario.validador.model.FormularioValor;
import co.com.coomeva.formulario.validador.util.ManagePropertiesLogic;
import co.com.coomeva.www.Empleado.generator.v1.ConsultaDatosEmpleadorRequest;
import co.com.coomeva.www.Empleado.generator.v1.ConsultaDatosEmpleadorResponse;
import co.com.coomeva.www.Empleado.generator.v1.binding.WSEmpleadoEmpleadoHttpService;

/**
* Formulario Dao Impl <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
@Repository
public class FormularioValorDaoImpl implements FormularioValorDao {

    
    @PersistenceContext
    private EntityManager em;
    public static final String FROM = "select a from FormularioValor a ";
	
	
    public EntityManager getEntityManager() {
        return em;
    }


    @Override
    public List<FormularioValor> findAllActive() throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.estado = :active ");
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("active", (EstadoEnum.ACTIVE.getId()));
        return query.getResultList();
    }
    
    @Override
    public List<FormularioValor> findByFormularioValor(FormularioValor formulario) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.formularioValorId = :id ");

        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("id", formulario.getFormularioValorId());
        return query.getResultList();
    }

    @Override
    public List<FormularioValor> findByFiltro(FormularioValor formulario) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE 1=1 ");
        
        
        if(formulario.getFvCampo() != null && formulario.getFvCampo().getCampoId() != null) {
            builder.append("AND a.fvCampo.campoId = :campoId ");
        }
        
        
        if(formulario.getFvFormulario() != null && formulario.getFvFormulario().getFormularioId() != null) {
            builder.append("AND a.fvFormulario.formularioId = :formularioId ");
        }
        
        
        if(formulario.getFormularioValorId() != null) {
            builder.append("AND a.formularioValorId = :formularioValorId ");
        }

        builder.append("order by a.nombre asc ");
        Query query = this.getEntityManager().createQuery(builder.toString());


        if(formulario.getFormularioValorId() != null) {
        	query.setParameter("formularioValorId", formulario.getFormularioValorId());
        }

 
        if(formulario.getFvFormulario() != null && formulario.getFvFormulario().getFormularioId() != null) {
            query.setParameter("fvFormulario", formulario.getFvFormulario().getFormularioId());
        }


        if(formulario.getFvCampo() != null && formulario.getFvCampo().getCampoId() != null) {
            query.setParameter("fvCampo", formulario.getFvCampo().getCampoId());
        }

        return query.getResultList();
    }
    
	@Override
	public boolean validarAsociado(FormularioAgrCampoValueDTO formularioAgrCampoValueDTO) throws MalformedURLException {
		ManagePropertiesLogic managePropertiesLogic = new ManagePropertiesLogic();
		String maxSizeFile = managePropertiesLogic.getProperties("general.properties", "url");
			
			String nombreCampo=cleanString(formularioAgrCampoValueDTO.getFvAgrupacionCampo().getFvCampo().getNombre());
		if (nombreCampo.equalsIgnoreCase(managePropertiesLogic.getProperties("general.properties", "ndIdentificacion")) || nombreCampo.equalsIgnoreCase(managePropertiesLogic.getProperties("general.properties", "ndDeIdentificacion")) ||
				nombreCampo.equalsIgnoreCase(managePropertiesLogic.getProperties("general.properties", "dcDeIdentificacion")) || nombreCampo.equalsIgnoreCase(managePropertiesLogic.getProperties("general.properties", "dcIdentificacion"))) {
			
			String urlSoap = managePropertiesLogic.getProperties("general.properties", "url");
			
			 URL url=new 
		        		URL(urlSoap);
		        WSEmpleadoEmpleadoHttpService wsEmpleadoEmpleadoHttpService=new 
		        		WSEmpleadoEmpleadoHttpService(url);
		        ConsultaDatosEmpleadorRequest empleadorRequest=new ConsultaDatosEmpleadorRequest();
		        empleadorRequest.setAplicativoOrigen("1");
		        empleadorRequest.setCedulaEmpleado(formularioAgrCampoValueDTO.getFvAgrupacionCampo().getFvCampo().getValue());
		        
		        ConsultaDatosEmpleadorResponse consultaDatosEmpleadorResponse=wsEmpleadoEmpleadoHttpService.getWSEmpleadoEmpleadoHttpPort().consultaDatosEmpleador(empleadorRequest);
		        
				if(consultaDatosEmpleadorResponse.getEmpleado()==null) {
					return false;
				}
				else {
					return true;
				}
			
			
			
		}
		
		return false;
      
		
	}
	
	public static String cleanString(String text) {
	
		text=text.toLowerCase().trim();
		String cleanTextFinal=Normalizer.normalize(text,  Normalizer.Form.NFD);
		cleanTextFinal = cleanTextFinal.replaceAll("[^\\p{ASCII}(N\u0303)(n\u0303)(\u00A1)(\u00BF)(\u00B0)(U\u0308)(u\u0308)]", "");
		return cleanTextFinal;
	}



}