package co.com.coomeva.formulario.validador.service.impl.parametrizacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.dao.parametrizacion.TipoCampoDao;
import co.com.coomeva.formulario.validador.dao.parametrizacion.TipoCampoRep;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.TipoCampo;
import co.com.coomeva.formulario.validador.service.parametrizacion.TipoCampoService;

/**
* TipoCampo Service Impl <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Service("tipoCampoService")
@Transactional
public class TipoCampoServiceImpl implements TipoCampoService {

    @Autowired
    private TipoCampoDao tipoCampoDao;

    @Autowired
    private TipoCampoRep tipoCampoRep;

    @Override
    public TipoCampo create(TipoCampo tipoCampo) throws BusinessException {
        return tipoCampoRep.save(tipoCampo);
    }

    @Override
    public TipoCampo store(TipoCampo tipoCampo) throws BusinessException {
       return tipoCampoRep.save(tipoCampo);
    }

    @Override
    public void remove(TipoCampo tipoCampo) throws BusinessException {
        tipoCampoRep.delete(tipoCampo);
    }

    @Override
    public List<TipoCampo> findAll() throws BusinessException {
        return tipoCampoRep.findAll();
    }

    @Override
    public List<TipoCampo> findAllActive() throws BusinessException {
        return tipoCampoDao.findAllActive();
    }
    @Override
    public List<TipoCampo> findByTipoCampo(TipoCampo tipoCampo) throws BusinessException {
        return tipoCampoDao.findByTipoCampo(tipoCampo);
    }

    @Override
    public List<TipoCampo> findByFiltro(TipoCampo tipoCampo) throws BusinessException {
        return tipoCampoDao.findByFiltro(tipoCampo);
    }

    @Override
    public List<TipoCampo> findByName(TipoCampo tipoCampo) throws BusinessException {
        return tipoCampoDao.findByName(tipoCampo);
    }
}