package co.com.coomeva.formulario.validador.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the FV_FORMULARIO_AGR_CAMPO database table.
 * 
 */
@Entity
@Table(name="FV_FORMULARIO_AGR_CAMPO")
@NamedQuery(name="FormularioAgrCampo.findAll", query="SELECT f FROM FormularioAgrCampo f")
public class FormularioAgrCampo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @Basic(optional = false)
    @GeneratedValue(generator = "fv_formulario_agr_campo_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "fv_formulario_agr_campo_seq", sequenceName = "fv_formulario_agr_campo_seq",allocationSize=1)    
	@Column(name="FORMULARIO_AGR_CAMPO_ID")
	private Long formularioAgrCampoId;

	//bi-directional many-to-one association to AgrupacionCampo
	@ManyToOne
	@JoinColumn(name="AGRUPACION_CAMPO_ID")
	private AgrupacionCampo fvAgrupacionCampo;

	//bi-directional many-to-one association to Formulario
	@ManyToOne
	@JoinColumn(name="FORMULARIO_ID")
	private Formulario fvFormulario;

	public FormularioAgrCampo() {
	}

	public Long getFormularioAgrCampoId() {
		return this.formularioAgrCampoId;
	}

	public void setFormularioAgrCampoId(Long formularioAgrCampoId) {
		this.formularioAgrCampoId = formularioAgrCampoId;
	}

	public AgrupacionCampo getFvAgrupacionCampo() {
		return this.fvAgrupacionCampo;
	}

	public void setFvAgrupacionCampo(AgrupacionCampo fvAgrupacionCampo) {
		this.fvAgrupacionCampo = fvAgrupacionCampo;
	}

	public Formulario getFvFormulario() {
		return this.fvFormulario;
	}

	public void setFvFormulario(Formulario fvFormulario) {
		this.fvFormulario = fvFormulario;
	}

}