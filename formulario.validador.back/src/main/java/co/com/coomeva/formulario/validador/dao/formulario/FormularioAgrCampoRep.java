package co.com.coomeva.formulario.validador.dao.formulario;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.model.Campo;
import co.com.coomeva.formulario.validador.model.Formulario;
import co.com.coomeva.formulario.validador.model.FormularioAgrCampo;

/**
* Formulario Dao <br>
* Info. Creación: <br>
* fecha 1 may 2021 <br>
* @author GTC
**/
@Repository
@Transactional
public interface FormularioAgrCampoRep extends JpaRepository<FormularioAgrCampo, Long>{

	@Query("SELECT p FROM FormularioAgrCampo p WHERE p.fvFormulario.formularioId = :formularioId")
    public List<FormularioAgrCampo> findByFormulario(@Param("formularioId") Long formularioId);
}