package co.com.coomeva.formulario.validador.dao.impl.parametrizacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import co.com.coomeva.formulario.validador.dao.parametrizacion.ParametroDao;
import co.com.coomeva.formulario.validador.dto.EstadoEnum;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Parametro;

/**
* Parametro Dao Impl <br>
* Info. Creación: <br>
* fecha 23 abr 2021 <br>
* @author GTC
**/
@Repository
public class ParametroDaoImpl implements ParametroDao {
    
    @PersistenceContext
    private EntityManager em;
    public static final String FROM = "select a from Parametro a ";
	
	
    public EntityManager getEntityManager() {
        return em;
    }


    @Override
    public List<Parametro> findAllActive() throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.estado = :active ");

        builder.append("order by a.nombre asc ");
        
        
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("active", (EstadoEnum.ACTIVE.getId()));
        return query.getResultList();
    }
    
    @Override
    public List<Parametro> findByParametro(Parametro parametro) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE a.parametroId = :id ");

        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("id", parametro.getParametroId());
        return query.getResultList();
    }

    @Override
    public List<Parametro> findByFiltro(Parametro parametro) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE 1=1 ");
        
        if(parametro.getNombre() != null && !parametro.getNombre().trim().isEmpty()) {
            builder.append("AND lower(a.nombre) like lower(:nombre) ");
        }


        if(parametro.getValor() != null && !parametro.getValor().trim().isEmpty()) {
            builder.append("AND lower(a.valor) like lower(:valor) ");
        }


        if(parametro.getDescripcion() != null && !parametro.getDescripcion().trim().isEmpty()) {
            builder.append("AND lower(a.descripcion) like lower(:descripcion) ");
        }




        if(parametro.getEstado() != null && !parametro.getEstado().trim().isEmpty()) {
            builder.append("AND a.estado = :estado ");
        }

        builder.append("order by a.nombre asc ");
        Query query = this.getEntityManager().createQuery(builder.toString());

        if(parametro.getNombre() != null && !parametro.getNombre().trim().isEmpty()) {
            query.setParameter("nombre", "%" + parametro.getNombre() + "%");
        }


        if(parametro.getValor() != null && !parametro.getValor().trim().isEmpty()) {
            query.setParameter("valor", "%" + parametro.getValor() + "%");
        }


        if(parametro.getDescripcion() != null && !parametro.getDescripcion().trim().isEmpty()) {
            query.setParameter("descripcion", "%" + parametro.getDescripcion() + "%");
        }




        if(parametro.getEstado() != null && !parametro.getEstado().trim().isEmpty()) {
            query.setParameter("estado", parametro.getEstado());
        }

        return query.getResultList();
    }

    @Override
    public List<Parametro> findByName(Parametro parametro) throws BusinessException {
        StringBuilder builder = new StringBuilder();
        builder.append(FROM);
        builder.append("WHERE lower(trim(a.nombre)) = lower(trim(:nombre)) ");
        Query query = this.getEntityManager().createQuery(builder.toString());
        query.setParameter("nombre", parametro.getNombre().trim());
        return query.getResultList();
    }
}