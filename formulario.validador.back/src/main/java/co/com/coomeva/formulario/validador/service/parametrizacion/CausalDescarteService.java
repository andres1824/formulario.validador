package co.com.coomeva.formulario.validador.service.parametrizacion;

import java.util.List;

import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.CausalDescarte;

/**
* CausalDescarte Service <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
public interface CausalDescarteService {
    

    /**
     * Crea causalDescarte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param causalDescarte
     * @return 
     * @throws BusinessException 
     */
    public CausalDescarte create(CausalDescarte causalDescarte) throws BusinessException;
    /**
     * Modifica causalDescarte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param causalDescarte
     * @return 
     * @throws BusinessException 
     */
    public CausalDescarte store(CausalDescarte causalDescarte) throws BusinessException;
    /**
     * Elimina causalDescarte<br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param causalDescarte
     * @throws BusinessException 
     */
    public void remove(CausalDescarte causalDescarte) throws BusinessException;
    /**
     * Obtiene todos los registros de causalDescarte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<CausalDescarte> findAll() throws BusinessException;
    
    /**
     * Obtiene todos los registros activos de causalDescarte <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @return
     * @throws BusinessException 
     */
    public List<CausalDescarte> findAllActive() throws BusinessException;
    /**
     * Obtiene todos los registros de causalDescarte paginados <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param causalDescarte
     * @return
     * @throws BusinessException 
     */
    public List<CausalDescarte> findByCausalDescarte(CausalDescarte causalDescarte) throws BusinessException;   

    /**
     * Obtiene todos los registros de causalDescarte por el filtro del usuario <br>
     * Info. Creación: <br>
     * fecha 24 abr 2021 <br>
     * @author GTC
     * @param causalDescarte
     * @return
     * @throws BusinessException 
     */
    public List<CausalDescarte> findByFiltro(CausalDescarte causalDescarte) throws BusinessException; 


    /**
    * Obtiene todos los registros de causalDescarte por nombre <br>
    * Info. Creación: <br>
    * fecha 24 abr 2021 <br>
    * @author GTC
    * @param causalDescarte
    * @return
    * @throws BusinessException 
    */
    public List<CausalDescarte> findByName(CausalDescarte causalDescarte) throws BusinessException;  

    
}