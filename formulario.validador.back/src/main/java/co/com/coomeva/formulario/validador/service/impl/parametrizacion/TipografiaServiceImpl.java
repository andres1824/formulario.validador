package co.com.coomeva.formulario.validador.service.impl.parametrizacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.coomeva.formulario.validador.dao.parametrizacion.TipografiaDao;
import co.com.coomeva.formulario.validador.dao.parametrizacion.TipografiaRep;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Tipografia;
import co.com.coomeva.formulario.validador.service.parametrizacion.TipografiaService;

/**
* Tipografia Service Impl <br>
* Info. Creación: <br>
* fecha 24 abr 2021 <br>
* @author GTC
**/
@Service("tipografiaService")
@Transactional
public class TipografiaServiceImpl implements TipografiaService {

    @Autowired
    private TipografiaDao tipografiaDao;

    @Autowired
    private TipografiaRep tipografiaRep;

    @Override
    public Tipografia create(Tipografia tipografia) throws BusinessException {
        return tipografiaRep.save(tipografia);
    }

    @Override
    public Tipografia store(Tipografia tipografia) throws BusinessException {
       return tipografiaRep.save(tipografia);
    }

    @Override
    public void remove(Tipografia tipografia) throws BusinessException {
        tipografiaRep.delete(tipografia);
    }

    @Override
    public List<Tipografia> findAll() throws BusinessException {
        return tipografiaRep.findAll();
    }

    @Override
    public List<Tipografia> findAllActive() throws BusinessException {
        return tipografiaDao.findAllActive();
    }
    @Override
    public List<Tipografia> findByTipografia(Tipografia tipografia) throws BusinessException {
        return tipografiaDao.findByTipografia(tipografia);
    }

    @Override
    public List<Tipografia> findByFiltro(Tipografia tipografia) throws BusinessException {
        return tipografiaDao.findByFiltro(tipografia);
    }

    @Override
    public List<Tipografia> findByName(Tipografia tipografia) throws BusinessException {
        return tipografiaDao.findByName(tipografia);
    }
}