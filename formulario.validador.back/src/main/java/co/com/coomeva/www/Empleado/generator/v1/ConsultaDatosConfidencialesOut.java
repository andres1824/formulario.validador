package co.com.coomeva.www.Empleado.generator.v1;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConsultaDatosConfidencialesResponse" type="{http://www.coomeva.com.co/Empleado}ConsultaDatosConfidencialesResponse"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaDatosConfidencialesResponse"
})
@XmlRootElement(name = "ConsultaDatosConfidencialesOut")
public class ConsultaDatosConfidencialesOut {

    @XmlElement(name = "ConsultaDatosConfidencialesResponse", required = true, nillable = true)
    protected ConsultaDatosConfidencialesResponse consultaDatosConfidencialesResponse;

    /**
     * Obtiene el valor de la propiedad consultaDatosConfidencialesResponse.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaDatosConfidencialesResponse }
     *     
     */
    public ConsultaDatosConfidencialesResponse getConsultaDatosConfidencialesResponse() {
        return consultaDatosConfidencialesResponse;
    }

    /**
     * Define el valor de la propiedad consultaDatosConfidencialesResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaDatosConfidencialesResponse }
     *     
     */
    public void setConsultaDatosConfidencialesResponse(ConsultaDatosConfidencialesResponse value) {
        this.consultaDatosConfidencialesResponse = value;
    }

}
