package co.com.coomeva.www.Empleado.generator.v1;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConsultaDatosEmpleadorResponse" type="{http://www.coomeva.com.co/Empleado}ConsultaDatosEmpleadorResponse"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaDatosEmpleadorResponse"
})
@XmlRootElement(name = "ConsultaDatosEmpleadorOut")
public class ConsultaDatosEmpleadorOut {

    @XmlElement(name = "ConsultaDatosEmpleadorResponse", required = true, nillable = true)
    protected ConsultaDatosEmpleadorResponse consultaDatosEmpleadorResponse;

    /**
     * Obtiene el valor de la propiedad consultaDatosEmpleadorResponse.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaDatosEmpleadorResponse }
     *     
     */
    public ConsultaDatosEmpleadorResponse getConsultaDatosEmpleadorResponse() {
        return consultaDatosEmpleadorResponse;
    }

    /**
     * Define el valor de la propiedad consultaDatosEmpleadorResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaDatosEmpleadorResponse }
     *     
     */
    public void setConsultaDatosEmpleadorResponse(ConsultaDatosEmpleadorResponse value) {
        this.consultaDatosEmpleadorResponse = value;
    }

}
