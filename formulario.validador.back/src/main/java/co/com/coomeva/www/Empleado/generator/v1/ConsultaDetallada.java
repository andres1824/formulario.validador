package co.com.coomeva.www.Empleado.generator.v1;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConsultaDetalladaRequest" type="{http://www.coomeva.com.co/Empleado}ConsultaDetalladaRequest"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaDetalladaRequest"
})
@XmlRootElement(name = "ConsultaDetallada")
public class ConsultaDetallada {

    @XmlElement(name = "ConsultaDetalladaRequest", required = true, nillable = true)
    protected ConsultaDetalladaRequest consultaDetalladaRequest;

    /**
     * Obtiene el valor de la propiedad consultaDetalladaRequest.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaDetalladaRequest }
     *     
     */
    public ConsultaDetalladaRequest getConsultaDetalladaRequest() {
        return consultaDetalladaRequest;
    }

    /**
     * Define el valor de la propiedad consultaDetalladaRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaDetalladaRequest }
     *     
     */
    public void setConsultaDetalladaRequest(ConsultaDetalladaRequest value) {
        this.consultaDetalladaRequest = value;
    }

}
