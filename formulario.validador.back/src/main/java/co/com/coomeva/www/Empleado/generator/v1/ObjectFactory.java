package co.com.coomeva.www.Empleado.generator.v1;


import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.com.coomeva.empleado package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.com.coomeva.empleado
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConsultaDatosBasicos }
     * 
     */
    public ConsultaDatosBasicos createConsultaDatosBasicos() {
        return new ConsultaDatosBasicos();
    }

    /**
     * Create an instance of {@link ConsultaDatosBasicosRequest }
     * 
     */
    public ConsultaDatosBasicosRequest createConsultaDatosBasicosRequest() {
        return new ConsultaDatosBasicosRequest();
    }

    /**
     * Create an instance of {@link ConsultaDatosBasicosOut }
     * 
     */
    public ConsultaDatosBasicosOut createConsultaDatosBasicosOut() {
        return new ConsultaDatosBasicosOut();
    }

    /**
     * Create an instance of {@link ConsultaDatosBasicosResponse }
     * 
     */
    public ConsultaDatosBasicosResponse createConsultaDatosBasicosResponse() {
        return new ConsultaDatosBasicosResponse();
    }

    /**
     * Create an instance of {@link ConsultaDatosEmpleador }
     * 
     */
    public ConsultaDatosEmpleador createConsultaDatosEmpleador() {
        return new ConsultaDatosEmpleador();
    }

    /**
     * Create an instance of {@link ConsultaDatosEmpleadorRequest }
     * 
     */
    public ConsultaDatosEmpleadorRequest createConsultaDatosEmpleadorRequest() {
        return new ConsultaDatosEmpleadorRequest();
    }

    /**
     * Create an instance of {@link ConsultaDatosEmpleadorOut }
     * 
     */
    public ConsultaDatosEmpleadorOut createConsultaDatosEmpleadorOut() {
        return new ConsultaDatosEmpleadorOut();
    }

    /**
     * Create an instance of {@link ConsultaDatosEmpleadorResponse }
     * 
     */
    public ConsultaDatosEmpleadorResponse createConsultaDatosEmpleadorResponse() {
        return new ConsultaDatosEmpleadorResponse();
    }

    /**
     * Create an instance of {@link ConsultaDatosConfidenciales }
     * 
     */
    public ConsultaDatosConfidenciales createConsultaDatosConfidenciales() {
        return new ConsultaDatosConfidenciales();
    }

    /**
     * Create an instance of {@link ConsultaDatosConfidencialesRequest }
     * 
     */
    public ConsultaDatosConfidencialesRequest createConsultaDatosConfidencialesRequest() {
        return new ConsultaDatosConfidencialesRequest();
    }

    /**
     * Create an instance of {@link ConsultaDatosConfidencialesOut }
     * 
     */
    public ConsultaDatosConfidencialesOut createConsultaDatosConfidencialesOut() {
        return new ConsultaDatosConfidencialesOut();
    }

    /**
     * Create an instance of {@link ConsultaDatosConfidencialesResponse }
     * 
     */
    public ConsultaDatosConfidencialesResponse createConsultaDatosConfidencialesResponse() {
        return new ConsultaDatosConfidencialesResponse();
    }

    /**
     * Create an instance of {@link ConsultaDetallada }
     * 
     */
    public ConsultaDetallada createConsultaDetallada() {
        return new ConsultaDetallada();
    }

    /**
     * Create an instance of {@link ConsultaDetalladaRequest }
     * 
     */
    public ConsultaDetalladaRequest createConsultaDetalladaRequest() {
        return new ConsultaDetalladaRequest();
    }

    /**
     * Create an instance of {@link ConsultaDetalladaOut }
     * 
     */
    public ConsultaDetalladaOut createConsultaDetalladaOut() {
        return new ConsultaDetalladaOut();
    }

    /**
     * Create an instance of {@link ConsultaDetalladaResponse }
     * 
     */
    public ConsultaDetalladaResponse createConsultaDetalladaResponse() {
        return new ConsultaDetalladaResponse();
    }

    /**
     * Create an instance of {@link EmpleadoDatosBasicos }
     * 
     */
    public EmpleadoDatosBasicos createEmpleadoDatosBasicos() {
        return new EmpleadoDatosBasicos();
    }

    /**
     * Create an instance of {@link DatosBasicos }
     * 
     */
    public DatosBasicos createDatosBasicos() {
        return new DatosBasicos();
    }

    /**
     * Create an instance of {@link EmpleadoDatosEmpleador }
     * 
     */
    public EmpleadoDatosEmpleador createEmpleadoDatosEmpleador() {
        return new EmpleadoDatosEmpleador();
    }

    /**
     * Create an instance of {@link DatosEmpleador }
     * 
     */
    public DatosEmpleador createDatosEmpleador() {
        return new DatosEmpleador();
    }

    /**
     * Create an instance of {@link EmpleadoDatosConfidenciales }
     * 
     */
    public EmpleadoDatosConfidenciales createEmpleadoDatosConfidenciales() {
        return new EmpleadoDatosConfidenciales();
    }

    /**
     * Create an instance of {@link DatosConfidenciales }
     * 
     */
    public DatosConfidenciales createDatosConfidenciales() {
        return new DatosConfidenciales();
    }

    /**
     * Create an instance of {@link EmpleadoDetalles }
     * 
     */
    public EmpleadoDetalles createEmpleadoDetalles() {
        return new EmpleadoDetalles();
    }

    /**
     * Create an instance of {@link Detalles }
     * 
     */
    public Detalles createDetalles() {
        return new Detalles();
    }

    /**
     * Create an instance of {@link Error }
     * 
     */
    public Error createError() {
        return new Error();
    }

}
