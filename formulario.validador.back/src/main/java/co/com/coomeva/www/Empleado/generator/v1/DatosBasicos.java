package co.com.coomeva.www.Empleado.generator.v1;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para DatosBasicos complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DatosBasicos"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idHr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ordinalPeriodo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="tipoIdentificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cedulaEmpleado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="primerNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="segundoNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="primerApellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="segundoApellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="codCiudadNacimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ciudadNacimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sexo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="estadoCivil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="numeroHijos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codigoCargo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nombreCargo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codCiudadResidencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ciudadResidencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="eMail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="extension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="profesion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codEmpresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="empresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="abreviaturaEmpresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nitEmpresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="razonSocial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codNegocio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="negocio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codSeccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="seccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codRegional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="regional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codLugarTrabajo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codCiudadLugarTrabajo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CiudadLugarTrabajo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="estadoEmpleado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="tipoContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codNivelEducacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NivelEducacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="metodoPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codTipoEmpleado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="tipoEmpleado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="procedimientoRetefuente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="empleadoAusente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="antiguedadEmpleado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nuevoAntiguo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="usuarioRed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="personasCargoMayores" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="personasCargoMenores" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DatosBasicos", propOrder = {
    "idHr",
    "ordinalPeriodo",
    "tipoIdentificacion",
    "cedulaEmpleado",
    "primerNombre",
    "segundoNombre",
    "primerApellido",
    "segundoApellido",
    "fechaNacimiento",
    "codCiudadNacimiento",
    "ciudadNacimiento",
    "sexo",
    "estadoCivil",
    "numeroHijos",
    "codigoCargo",
    "nombreCargo",
    "codCiudadResidencia",
    "ciudadResidencia",
    "eMail",
    "extension",
    "profesion",
    "codEmpresa",
    "empresa",
    "abreviaturaEmpresa",
    "nitEmpresa",
    "razonSocial",
    "codNegocio",
    "negocio",
    "codSeccion",
    "seccion",
    "codRegional",
    "regional",
    "codLugarTrabajo",
    "codCiudadLugarTrabajo",
    "ciudadLugarTrabajo",
    "estadoEmpleado",
    "codContrato",
    "tipoContrato",
    "codNivelEducacion",
    "nivelEducacion",
    "metodoPago",
    "codTipoEmpleado",
    "tipoEmpleado",
    "procedimientoRetefuente",
    "empleadoAusente",
    "antiguedadEmpleado",
    "nuevoAntiguo",
    "usuarioRed",
    "personasCargoMayores",
    "personasCargoMenores"
})
public class DatosBasicos {

    protected String idHr;
    protected Integer ordinalPeriodo;
    protected String tipoIdentificacion;
    protected String cedulaEmpleado;
    protected String primerNombre;
    protected String segundoNombre;
    protected String primerApellido;
    protected String segundoApellido;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaNacimiento;
    protected String codCiudadNacimiento;
    protected String ciudadNacimiento;
    protected String sexo;
    protected String estadoCivil;
    protected String numeroHijos;
    protected String codigoCargo;
    protected String nombreCargo;
    protected String codCiudadResidencia;
    protected String ciudadResidencia;
    protected String eMail;
    protected String extension;
    protected String profesion;
    protected String codEmpresa;
    protected String empresa;
    protected String abreviaturaEmpresa;
    protected String nitEmpresa;
    protected String razonSocial;
    protected String codNegocio;
    protected String negocio;
    protected String codSeccion;
    protected String seccion;
    protected String codRegional;
    protected String regional;
    protected String codLugarTrabajo;
    protected String codCiudadLugarTrabajo;
    @XmlElement(name = "CiudadLugarTrabajo")
    protected String ciudadLugarTrabajo;
    protected String estadoEmpleado;
    protected String codContrato;
    protected String tipoContrato;
    protected String codNivelEducacion;
    @XmlElement(name = "NivelEducacion")
    protected String nivelEducacion;
    protected String metodoPago;
    protected String codTipoEmpleado;
    protected String tipoEmpleado;
    protected String procedimientoRetefuente;
    protected String empleadoAusente;
    protected String antiguedadEmpleado;
    protected String nuevoAntiguo;
    protected String usuarioRed;
    protected String personasCargoMayores;
    protected String personasCargoMenores;

    /**
     * Obtiene el valor de la propiedad idHr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdHr() {
        return idHr;
    }

    /**
     * Define el valor de la propiedad idHr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdHr(String value) {
        this.idHr = value;
    }

    /**
     * Obtiene el valor de la propiedad ordinalPeriodo.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrdinalPeriodo() {
        return ordinalPeriodo;
    }

    /**
     * Define el valor de la propiedad ordinalPeriodo.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrdinalPeriodo(Integer value) {
        this.ordinalPeriodo = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Define el valor de la propiedad tipoIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIdentificacion(String value) {
        this.tipoIdentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad cedulaEmpleado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCedulaEmpleado() {
        return cedulaEmpleado;
    }

    /**
     * Define el valor de la propiedad cedulaEmpleado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCedulaEmpleado(String value) {
        this.cedulaEmpleado = value;
    }

    /**
     * Obtiene el valor de la propiedad primerNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimerNombre() {
        return primerNombre;
    }

    /**
     * Define el valor de la propiedad primerNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimerNombre(String value) {
        this.primerNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad segundoNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegundoNombre() {
        return segundoNombre;
    }

    /**
     * Define el valor de la propiedad segundoNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegundoNombre(String value) {
        this.segundoNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad primerApellido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimerApellido() {
        return primerApellido;
    }

    /**
     * Define el valor de la propiedad primerApellido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimerApellido(String value) {
        this.primerApellido = value;
    }

    /**
     * Obtiene el valor de la propiedad segundoApellido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegundoApellido() {
        return segundoApellido;
    }

    /**
     * Define el valor de la propiedad segundoApellido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegundoApellido(String value) {
        this.segundoApellido = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaNacimiento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * Define el valor de la propiedad fechaNacimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaNacimiento(XMLGregorianCalendar value) {
        this.fechaNacimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad codCiudadNacimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCiudadNacimiento() {
        return codCiudadNacimiento;
    }

    /**
     * Define el valor de la propiedad codCiudadNacimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCiudadNacimiento(String value) {
        this.codCiudadNacimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad ciudadNacimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudadNacimiento() {
        return ciudadNacimiento;
    }

    /**
     * Define el valor de la propiedad ciudadNacimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudadNacimiento(String value) {
        this.ciudadNacimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad sexo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * Define el valor de la propiedad sexo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSexo(String value) {
        this.sexo = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoCivil.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoCivil() {
        return estadoCivil;
    }

    /**
     * Define el valor de la propiedad estadoCivil.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoCivil(String value) {
        this.estadoCivil = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroHijos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroHijos() {
        return numeroHijos;
    }

    /**
     * Define el valor de la propiedad numeroHijos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroHijos(String value) {
        this.numeroHijos = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoCargo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCargo() {
        return codigoCargo;
    }

    /**
     * Define el valor de la propiedad codigoCargo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCargo(String value) {
        this.codigoCargo = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreCargo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCargo() {
        return nombreCargo;
    }

    /**
     * Define el valor de la propiedad nombreCargo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCargo(String value) {
        this.nombreCargo = value;
    }

    /**
     * Obtiene el valor de la propiedad codCiudadResidencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCiudadResidencia() {
        return codCiudadResidencia;
    }

    /**
     * Define el valor de la propiedad codCiudadResidencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCiudadResidencia(String value) {
        this.codCiudadResidencia = value;
    }

    /**
     * Obtiene el valor de la propiedad ciudadResidencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudadResidencia() {
        return ciudadResidencia;
    }

    /**
     * Define el valor de la propiedad ciudadResidencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudadResidencia(String value) {
        this.ciudadResidencia = value;
    }

    /**
     * Obtiene el valor de la propiedad eMail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMail() {
        return eMail;
    }

    /**
     * Define el valor de la propiedad eMail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMail(String value) {
        this.eMail = value;
    }

    /**
     * Obtiene el valor de la propiedad extension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtension() {
        return extension;
    }

    /**
     * Define el valor de la propiedad extension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtension(String value) {
        this.extension = value;
    }

    /**
     * Obtiene el valor de la propiedad profesion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfesion() {
        return profesion;
    }

    /**
     * Define el valor de la propiedad profesion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfesion(String value) {
        this.profesion = value;
    }

    /**
     * Obtiene el valor de la propiedad codEmpresa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEmpresa() {
        return codEmpresa;
    }

    /**
     * Define el valor de la propiedad codEmpresa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEmpresa(String value) {
        this.codEmpresa = value;
    }

    /**
     * Obtiene el valor de la propiedad empresa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     * Define el valor de la propiedad empresa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpresa(String value) {
        this.empresa = value;
    }

    /**
     * Obtiene el valor de la propiedad abreviaturaEmpresa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAbreviaturaEmpresa() {
        return abreviaturaEmpresa;
    }

    /**
     * Define el valor de la propiedad abreviaturaEmpresa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAbreviaturaEmpresa(String value) {
        this.abreviaturaEmpresa = value;
    }

    /**
     * Obtiene el valor de la propiedad nitEmpresa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNitEmpresa() {
        return nitEmpresa;
    }

    /**
     * Define el valor de la propiedad nitEmpresa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNitEmpresa(String value) {
        this.nitEmpresa = value;
    }

    /**
     * Obtiene el valor de la propiedad razonSocial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     * Define el valor de la propiedad razonSocial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRazonSocial(String value) {
        this.razonSocial = value;
    }

    /**
     * Obtiene el valor de la propiedad codNegocio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodNegocio() {
        return codNegocio;
    }

    /**
     * Define el valor de la propiedad codNegocio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodNegocio(String value) {
        this.codNegocio = value;
    }

    /**
     * Obtiene el valor de la propiedad negocio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNegocio() {
        return negocio;
    }

    /**
     * Define el valor de la propiedad negocio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNegocio(String value) {
        this.negocio = value;
    }

    /**
     * Obtiene el valor de la propiedad codSeccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodSeccion() {
        return codSeccion;
    }

    /**
     * Define el valor de la propiedad codSeccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodSeccion(String value) {
        this.codSeccion = value;
    }

    /**
     * Obtiene el valor de la propiedad seccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeccion() {
        return seccion;
    }

    /**
     * Define el valor de la propiedad seccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeccion(String value) {
        this.seccion = value;
    }

    /**
     * Obtiene el valor de la propiedad codRegional.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodRegional() {
        return codRegional;
    }

    /**
     * Define el valor de la propiedad codRegional.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodRegional(String value) {
        this.codRegional = value;
    }

    /**
     * Obtiene el valor de la propiedad regional.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegional() {
        return regional;
    }

    /**
     * Define el valor de la propiedad regional.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegional(String value) {
        this.regional = value;
    }

    /**
     * Obtiene el valor de la propiedad codLugarTrabajo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodLugarTrabajo() {
        return codLugarTrabajo;
    }

    /**
     * Define el valor de la propiedad codLugarTrabajo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodLugarTrabajo(String value) {
        this.codLugarTrabajo = value;
    }

    /**
     * Obtiene el valor de la propiedad codCiudadLugarTrabajo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCiudadLugarTrabajo() {
        return codCiudadLugarTrabajo;
    }

    /**
     * Define el valor de la propiedad codCiudadLugarTrabajo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCiudadLugarTrabajo(String value) {
        this.codCiudadLugarTrabajo = value;
    }

    /**
     * Obtiene el valor de la propiedad ciudadLugarTrabajo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudadLugarTrabajo() {
        return ciudadLugarTrabajo;
    }

    /**
     * Define el valor de la propiedad ciudadLugarTrabajo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudadLugarTrabajo(String value) {
        this.ciudadLugarTrabajo = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoEmpleado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoEmpleado() {
        return estadoEmpleado;
    }

    /**
     * Define el valor de la propiedad estadoEmpleado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoEmpleado(String value) {
        this.estadoEmpleado = value;
    }

    /**
     * Obtiene el valor de la propiedad codContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodContrato() {
        return codContrato;
    }

    /**
     * Define el valor de la propiedad codContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodContrato(String value) {
        this.codContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoContrato() {
        return tipoContrato;
    }

    /**
     * Define el valor de la propiedad tipoContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoContrato(String value) {
        this.tipoContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad codNivelEducacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodNivelEducacion() {
        return codNivelEducacion;
    }

    /**
     * Define el valor de la propiedad codNivelEducacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodNivelEducacion(String value) {
        this.codNivelEducacion = value;
    }

    /**
     * Obtiene el valor de la propiedad nivelEducacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNivelEducacion() {
        return nivelEducacion;
    }

    /**
     * Define el valor de la propiedad nivelEducacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNivelEducacion(String value) {
        this.nivelEducacion = value;
    }

    /**
     * Obtiene el valor de la propiedad metodoPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMetodoPago() {
        return metodoPago;
    }

    /**
     * Define el valor de la propiedad metodoPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMetodoPago(String value) {
        this.metodoPago = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoEmpleado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoEmpleado() {
        return codTipoEmpleado;
    }

    /**
     * Define el valor de la propiedad codTipoEmpleado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoEmpleado(String value) {
        this.codTipoEmpleado = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoEmpleado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoEmpleado() {
        return tipoEmpleado;
    }

    /**
     * Define el valor de la propiedad tipoEmpleado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoEmpleado(String value) {
        this.tipoEmpleado = value;
    }

    /**
     * Obtiene el valor de la propiedad procedimientoRetefuente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcedimientoRetefuente() {
        return procedimientoRetefuente;
    }

    /**
     * Define el valor de la propiedad procedimientoRetefuente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcedimientoRetefuente(String value) {
        this.procedimientoRetefuente = value;
    }

    /**
     * Obtiene el valor de la propiedad empleadoAusente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpleadoAusente() {
        return empleadoAusente;
    }

    /**
     * Define el valor de la propiedad empleadoAusente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpleadoAusente(String value) {
        this.empleadoAusente = value;
    }

    /**
     * Obtiene el valor de la propiedad antiguedadEmpleado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAntiguedadEmpleado() {
        return antiguedadEmpleado;
    }

    /**
     * Define el valor de la propiedad antiguedadEmpleado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAntiguedadEmpleado(String value) {
        this.antiguedadEmpleado = value;
    }

    /**
     * Obtiene el valor de la propiedad nuevoAntiguo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNuevoAntiguo() {
        return nuevoAntiguo;
    }

    /**
     * Define el valor de la propiedad nuevoAntiguo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNuevoAntiguo(String value) {
        this.nuevoAntiguo = value;
    }

    /**
     * Obtiene el valor de la propiedad usuarioRed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioRed() {
        return usuarioRed;
    }

    /**
     * Define el valor de la propiedad usuarioRed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioRed(String value) {
        this.usuarioRed = value;
    }

    /**
     * Obtiene el valor de la propiedad personasCargoMayores.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonasCargoMayores() {
        return personasCargoMayores;
    }

    /**
     * Define el valor de la propiedad personasCargoMayores.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonasCargoMayores(String value) {
        this.personasCargoMayores = value;
    }

    /**
     * Obtiene el valor de la propiedad personasCargoMenores.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonasCargoMenores() {
        return personasCargoMenores;
    }

    /**
     * Define el valor de la propiedad personasCargoMenores.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonasCargoMenores(String value) {
        this.personasCargoMenores = value;
    }

}
