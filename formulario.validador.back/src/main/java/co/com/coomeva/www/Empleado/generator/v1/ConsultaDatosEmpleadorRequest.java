package co.com.coomeva.www.Empleado.generator.v1;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ConsultaDatosEmpleadorRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ConsultaDatosEmpleadorRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="aplicativoOrigen" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="cedulaEmpleado" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultaDatosEmpleadorRequest", propOrder = {
    "aplicativoOrigen",
    "cedulaEmpleado"
})
public class ConsultaDatosEmpleadorRequest {

    @XmlElement(required = true)
    protected String aplicativoOrigen;
    @XmlElement(required = true)
    protected String cedulaEmpleado;

    /**
     * Obtiene el valor de la propiedad aplicativoOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAplicativoOrigen() {
        return aplicativoOrigen;
    }

    /**
     * Define el valor de la propiedad aplicativoOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAplicativoOrigen(String value) {
        this.aplicativoOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad cedulaEmpleado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCedulaEmpleado() {
        return cedulaEmpleado;
    }

    /**
     * Define el valor de la propiedad cedulaEmpleado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCedulaEmpleado(String value) {
        this.cedulaEmpleado = value;
    }

}
