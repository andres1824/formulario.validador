package co.com.coomeva.www.Empleado.generator.v1;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConsultaDatosBasicosRequest" type="{http://www.coomeva.com.co/Empleado}ConsultaDatosBasicosRequest"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaDatosBasicosRequest"
})
@XmlRootElement(name = "ConsultaDatosBasicos")
public class ConsultaDatosBasicos {

    @XmlElement(name = "ConsultaDatosBasicosRequest", required = true, nillable = true)
    protected ConsultaDatosBasicosRequest consultaDatosBasicosRequest;

    /**
     * Obtiene el valor de la propiedad consultaDatosBasicosRequest.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaDatosBasicosRequest }
     *     
     */
    public ConsultaDatosBasicosRequest getConsultaDatosBasicosRequest() {
        return consultaDatosBasicosRequest;
    }

    /**
     * Define el valor de la propiedad consultaDatosBasicosRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaDatosBasicosRequest }
     *     
     */
    public void setConsultaDatosBasicosRequest(ConsultaDatosBasicosRequest value) {
        this.consultaDatosBasicosRequest = value;
    }

}
