package co.com.coomeva.www.Empleado.generator.v1;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConsultaDatosBasicosResponse" type="{http://www.coomeva.com.co/Empleado}ConsultaDatosBasicosResponse"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaDatosBasicosResponse"
})
@XmlRootElement(name = "ConsultaDatosBasicosOut")
public class ConsultaDatosBasicosOut {

    @XmlElement(name = "ConsultaDatosBasicosResponse", required = true, nillable = true)
    protected ConsultaDatosBasicosResponse consultaDatosBasicosResponse;

    /**
     * Obtiene el valor de la propiedad consultaDatosBasicosResponse.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaDatosBasicosResponse }
     *     
     */
    public ConsultaDatosBasicosResponse getConsultaDatosBasicosResponse() {
        return consultaDatosBasicosResponse;
    }

    /**
     * Define el valor de la propiedad consultaDatosBasicosResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaDatosBasicosResponse }
     *     
     */
    public void setConsultaDatosBasicosResponse(ConsultaDatosBasicosResponse value) {
        this.consultaDatosBasicosResponse = value;
    }

}
