package co.com.coomeva.www.Empleado.generator.v1;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ConsultaDatosEmpleadorResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ConsultaDatosEmpleadorResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Empleado" type="{http://www.coomeva.com.co/Empleado}EmpleadoDatosEmpleador"/&gt;
 *         &lt;element name="Error" type="{http://www.coomeva.com.co/Empleado}Error"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultaDatosEmpleadorResponse", propOrder = {
    "empleado",
    "error"
})
public class ConsultaDatosEmpleadorResponse {

    @XmlElement(name = "Empleado", required = true)
    protected EmpleadoDatosEmpleador empleado;
    @XmlElement(name = "Error", required = true)
    protected Error error;

    /**
     * Obtiene el valor de la propiedad empleado.
     * 
     * @return
     *     possible object is
     *     {@link EmpleadoDatosEmpleador }
     *     
     */
    public EmpleadoDatosEmpleador getEmpleado() {
        return empleado;
    }

    /**
     * Define el valor de la propiedad empleado.
     * 
     * @param value
     *     allowed object is
     *     {@link EmpleadoDatosEmpleador }
     *     
     */
    public void setEmpleado(EmpleadoDatosEmpleador value) {
        this.empleado = value;
    }

    /**
     * Obtiene el valor de la propiedad error.
     * 
     * @return
     *     possible object is
     *     {@link Error }
     *     
     */
    public Error getError() {
        return error;
    }

    /**
     * Define el valor de la propiedad error.
     * 
     * @param value
     *     allowed object is
     *     {@link Error }
     *     
     */
    public void setError(Error value) {
        this.error = value;
    }

}
