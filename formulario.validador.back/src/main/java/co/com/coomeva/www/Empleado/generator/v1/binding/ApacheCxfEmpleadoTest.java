package co.com.coomeva.www.Empleado.generator.v1.binding;

import java.net.MalformedURLException;
import java.net.URL;

import co.com.coomeva.www.Empleado.generator.v1.ConsultaDatosEmpleadorRequest;
import co.com.coomeva.www.Empleado.generator.v1.ConsultaDatosEmpleadorResponse;

public class ApacheCxfEmpleadoTest {
	
	public static void main(String[] args) throws MalformedURLException {
        URL url=new 
        		URL("http://peoplenet.intracoomeva.com.co/EmpleadoWeb/sca/WS-Empleado/WEB-INF/wsdl/Empleado_WS-Empleado.wsdl");
        WSEmpleadoEmpleadoHttpService wsEmpleadoEmpleadoHttpService=new 
        		WSEmpleadoEmpleadoHttpService(url);
        ConsultaDatosEmpleadorRequest empleadorRequest=new ConsultaDatosEmpleadorRequest();
        empleadorRequest.setAplicativoOrigen("1");
        empleadorRequest.setCedulaEmpleado("1144075568");
        
        ConsultaDatosEmpleadorResponse consultaDatosEmpleadorResponse=wsEmpleadoEmpleadoHttpService.getWSEmpleadoEmpleadoHttpPort().consultaDatosEmpleador(empleadorRequest);
        
		System.out.print(consultaDatosEmpleadorResponse);
	}

}
