package co.com.coomeva.www.Empleado.generator.v1;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConsultaDatosEmpleadorRequest" type="{http://www.coomeva.com.co/Empleado}ConsultaDatosEmpleadorRequest"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaDatosEmpleadorRequest"
})
@XmlRootElement(name = "ConsultaDatosEmpleador")
public class ConsultaDatosEmpleador {

    @XmlElement(name = "ConsultaDatosEmpleadorRequest", required = true, nillable = true)
    protected ConsultaDatosEmpleadorRequest consultaDatosEmpleadorRequest;

    /**
     * Obtiene el valor de la propiedad consultaDatosEmpleadorRequest.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaDatosEmpleadorRequest }
     *     
     */
    public ConsultaDatosEmpleadorRequest getConsultaDatosEmpleadorRequest() {
        return consultaDatosEmpleadorRequest;
    }

    /**
     * Define el valor de la propiedad consultaDatosEmpleadorRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaDatosEmpleadorRequest }
     *     
     */
    public void setConsultaDatosEmpleadorRequest(ConsultaDatosEmpleadorRequest value) {
        this.consultaDatosEmpleadorRequest = value;
    }

}
