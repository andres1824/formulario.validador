package co.com.coomeva.www.Empleado.generator.v1;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para DatosEmpleador complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DatosEmpleador"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codCentroCosto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="centrosCosto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fechaIngresoEmpresa" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="fechaInicioContrato" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="fechaFinContrato" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="cedulaJefe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cedulaJefeCC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="categoriaViajerc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="salarioMLV" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cedulaEnteAprobador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cedulaSuplenteAprobador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cedulaSecretariaAprobador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cedulaTurismo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vivienda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codigoAgencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nombreAgencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codEmpresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="empresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DatosEmpleador", propOrder = {
    "codCentroCosto",
    "centrosCosto",
    "fechaIngresoEmpresa",
    "fechaInicioContrato",
    "fechaFinContrato",
    "cedulaJefe",
    "cedulaJefeCC",
    "categoriaViajerc",
    "salarioMLV",
    "cedulaEnteAprobador",
    "cedulaSuplenteAprobador",
    "cedulaSecretariaAprobador",
    "cedulaTurismo",
    "vivienda",
    "codigoAgencia",
    "nombreAgencia",
    "codEmpresa",
    "empresa"
})
public class DatosEmpleador {

    protected String codCentroCosto;
    protected String centrosCosto;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaIngresoEmpresa;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaInicioContrato;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaFinContrato;
    protected String cedulaJefe;
    protected String cedulaJefeCC;
    protected String categoriaViajerc;
    protected String salarioMLV;
    protected String cedulaEnteAprobador;
    protected String cedulaSuplenteAprobador;
    protected String cedulaSecretariaAprobador;
    protected String cedulaTurismo;
    protected String vivienda;
    protected String codigoAgencia;
    protected String nombreAgencia;
    protected String codEmpresa;
    protected String empresa;

    /**
     * Obtiene el valor de la propiedad codCentroCosto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCentroCosto() {
        return codCentroCosto;
    }

    /**
     * Define el valor de la propiedad codCentroCosto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCentroCosto(String value) {
        this.codCentroCosto = value;
    }

    /**
     * Obtiene el valor de la propiedad centrosCosto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCentrosCosto() {
        return centrosCosto;
    }

    /**
     * Define el valor de la propiedad centrosCosto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCentrosCosto(String value) {
        this.centrosCosto = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaIngresoEmpresa.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaIngresoEmpresa() {
        return fechaIngresoEmpresa;
    }

    /**
     * Define el valor de la propiedad fechaIngresoEmpresa.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaIngresoEmpresa(XMLGregorianCalendar value) {
        this.fechaIngresoEmpresa = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaInicioContrato.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaInicioContrato() {
        return fechaInicioContrato;
    }

    /**
     * Define el valor de la propiedad fechaInicioContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaInicioContrato(XMLGregorianCalendar value) {
        this.fechaInicioContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaFinContrato.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaFinContrato() {
        return fechaFinContrato;
    }

    /**
     * Define el valor de la propiedad fechaFinContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaFinContrato(XMLGregorianCalendar value) {
        this.fechaFinContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad cedulaJefe.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCedulaJefe() {
        return cedulaJefe;
    }

    /**
     * Define el valor de la propiedad cedulaJefe.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCedulaJefe(String value) {
        this.cedulaJefe = value;
    }

    /**
     * Obtiene el valor de la propiedad cedulaJefeCC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCedulaJefeCC() {
        return cedulaJefeCC;
    }

    /**
     * Define el valor de la propiedad cedulaJefeCC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCedulaJefeCC(String value) {
        this.cedulaJefeCC = value;
    }

    /**
     * Obtiene el valor de la propiedad categoriaViajerc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoriaViajerc() {
        return categoriaViajerc;
    }

    /**
     * Define el valor de la propiedad categoriaViajerc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoriaViajerc(String value) {
        this.categoriaViajerc = value;
    }

    /**
     * Obtiene el valor de la propiedad salarioMLV.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalarioMLV() {
        return salarioMLV;
    }

    /**
     * Define el valor de la propiedad salarioMLV.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalarioMLV(String value) {
        this.salarioMLV = value;
    }

    /**
     * Obtiene el valor de la propiedad cedulaEnteAprobador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCedulaEnteAprobador() {
        return cedulaEnteAprobador;
    }

    /**
     * Define el valor de la propiedad cedulaEnteAprobador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCedulaEnteAprobador(String value) {
        this.cedulaEnteAprobador = value;
    }

    /**
     * Obtiene el valor de la propiedad cedulaSuplenteAprobador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCedulaSuplenteAprobador() {
        return cedulaSuplenteAprobador;
    }

    /**
     * Define el valor de la propiedad cedulaSuplenteAprobador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCedulaSuplenteAprobador(String value) {
        this.cedulaSuplenteAprobador = value;
    }

    /**
     * Obtiene el valor de la propiedad cedulaSecretariaAprobador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCedulaSecretariaAprobador() {
        return cedulaSecretariaAprobador;
    }

    /**
     * Define el valor de la propiedad cedulaSecretariaAprobador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCedulaSecretariaAprobador(String value) {
        this.cedulaSecretariaAprobador = value;
    }

    /**
     * Obtiene el valor de la propiedad cedulaTurismo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCedulaTurismo() {
        return cedulaTurismo;
    }

    /**
     * Define el valor de la propiedad cedulaTurismo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCedulaTurismo(String value) {
        this.cedulaTurismo = value;
    }

    /**
     * Obtiene el valor de la propiedad vivienda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVivienda() {
        return vivienda;
    }

    /**
     * Define el valor de la propiedad vivienda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVivienda(String value) {
        this.vivienda = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoAgencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAgencia() {
        return codigoAgencia;
    }

    /**
     * Define el valor de la propiedad codigoAgencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAgencia(String value) {
        this.codigoAgencia = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreAgencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreAgencia() {
        return nombreAgencia;
    }

    /**
     * Define el valor de la propiedad nombreAgencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreAgencia(String value) {
        this.nombreAgencia = value;
    }

    /**
     * Obtiene el valor de la propiedad codEmpresa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEmpresa() {
        return codEmpresa;
    }

    /**
     * Define el valor de la propiedad codEmpresa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEmpresa(String value) {
        this.codEmpresa = value;
    }

    /**
     * Obtiene el valor de la propiedad empresa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     * Define el valor de la propiedad empresa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpresa(String value) {
        this.empresa = value;
    }

}
