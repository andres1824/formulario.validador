package co.com.coomeva.www.Empleado.generator.v1;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConsultaDatosConfidencialesRequest" type="{http://www.coomeva.com.co/Empleado}ConsultaDatosConfidencialesRequest"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaDatosConfidencialesRequest"
})
@XmlRootElement(name = "ConsultaDatosConfidenciales")
public class ConsultaDatosConfidenciales {

    @XmlElement(name = "ConsultaDatosConfidencialesRequest", required = true, nillable = true)
    protected ConsultaDatosConfidencialesRequest consultaDatosConfidencialesRequest;

    /**
     * Obtiene el valor de la propiedad consultaDatosConfidencialesRequest.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaDatosConfidencialesRequest }
     *     
     */
    public ConsultaDatosConfidencialesRequest getConsultaDatosConfidencialesRequest() {
        return consultaDatosConfidencialesRequest;
    }

    /**
     * Define el valor de la propiedad consultaDatosConfidencialesRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaDatosConfidencialesRequest }
     *     
     */
    public void setConsultaDatosConfidencialesRequest(ConsultaDatosConfidencialesRequest value) {
        this.consultaDatosConfidencialesRequest = value;
    }

}
