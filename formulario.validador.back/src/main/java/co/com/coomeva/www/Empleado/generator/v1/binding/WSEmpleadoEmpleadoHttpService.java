package co.com.coomeva.www.Empleado.generator.v1.binding;



import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;

import co.com.coomeva.www.Empleado.generator.v1.Empleado;

import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.1.6
 * 2021-06-27T11:55:06.616-05:00
 * Generated source version: 3.1.6
 * 
 */
@WebServiceClient(name = "WS-Empleado_EmpleadoHttpService", 
                  wsdlLocation = "file:src/main/java/co/com/coomeva/www/Empleado/generator/v1/binding/Empleado.xml",
                  targetNamespace = "http://www.coomeva.com.co/Empleado/Binding") 
public class WSEmpleadoEmpleadoHttpService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://www.coomeva.com.co/Empleado/Binding", "WS-Empleado_EmpleadoHttpService");
    public final static QName WSEmpleadoEmpleadoHttpPort = new QName("http://www.coomeva.com.co/Empleado/Binding", "WS-Empleado_EmpleadoHttpPort");
    static {
        URL url = null;
        try {
            url = new URL("file:src/main/java/co/com/coomeva/www/Empleado/generator/v1/binding/Empleado.xml");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(WSEmpleadoEmpleadoHttpService.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "file:src/main/java/co/com/coomeva/www/Empleado/generator/v1/binding/Empleado.xml");
        }
        WSDL_LOCATION = url;
    }

    public WSEmpleadoEmpleadoHttpService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public WSEmpleadoEmpleadoHttpService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public WSEmpleadoEmpleadoHttpService() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    public WSEmpleadoEmpleadoHttpService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public WSEmpleadoEmpleadoHttpService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public WSEmpleadoEmpleadoHttpService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }    
    
    /**
     *
     * @return
     *     returns Empleado
     */
    @WebEndpoint(name = "WS-Empleado_EmpleadoHttpPort")
    public Empleado getWSEmpleadoEmpleadoHttpPort() {
        return super.getPort(WSEmpleadoEmpleadoHttpPort, Empleado.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns Empleado
     */
    @WebEndpoint(name = "WS-Empleado_EmpleadoHttpPort")
    public Empleado getWSEmpleadoEmpleadoHttpPort(WebServiceFeature... features) {
        return super.getPort(WSEmpleadoEmpleadoHttpPort, Empleado.class, features);
    }

}
