package co.com.coomeva.formulario.validador.controller;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import co.com.coomeva.formulario.validador.controller.parametrizacion.TipografiaController;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.Status;
import co.com.coomeva.formulario.validador.dto.TipografiaDTO;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Tipografia;
import co.com.coomeva.formulario.validador.service.parametrizacion.TipografiaService;


public class TipografiaControllerTest {
	@InjectMocks
	private TipografiaController controller;
	@Mock
	private TipografiaService TipografiaService;
	
	@Before
    public void setUp() throws BusinessException {
		MockitoAnnotations.initMocks(this);
		Mockito.when(TipografiaService.findByName(new Tipografia())).thenReturn(null);
		Mockito.when(TipografiaService.create(new Tipografia())).thenReturn(null);
    }
	
	@Test
	public void create() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.createTipografia(new TipografiaDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
	
	@Test
	public void update() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.updateTipografia(new TipografiaDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
}
