package co.com.coomeva.formulario.validador.controller;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import co.com.coomeva.formulario.validador.controller.parametrizacion.CausalDescarteController;
import co.com.coomeva.formulario.validador.dto.CausalDescarteDTO;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.Status;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.CausalDescarte;
import co.com.coomeva.formulario.validador.service.parametrizacion.CausalDescarteService;


public class CausalDescarteControllerTest {
	
	@InjectMocks
	private CausalDescarteController controller;
	@Mock
	private CausalDescarteService causalDescarteService;
	
	@Before
    public void setUp() throws BusinessException {
		MockitoAnnotations.initMocks(this);
		Mockito.when(causalDescarteService.findByName(new CausalDescarte())).thenReturn(null);
		Mockito.when(causalDescarteService.create(new CausalDescarte())).thenReturn(null);
    }
	
	@Test
	public void create() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.createCausalDescarte(new CausalDescarteDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
	
	@Test
	public void update() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.updateCausalDescarte(new CausalDescarteDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
}
