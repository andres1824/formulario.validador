package co.com.coomeva.formulario.validador.controller;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import co.com.coomeva.formulario.validador.controller.parametrizacion.TipoCampoController;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.Status;
import co.com.coomeva.formulario.validador.dto.TipoCampoDTO;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.TipoCampo;
import co.com.coomeva.formulario.validador.service.parametrizacion.TipoCampoService;


public class TipoCampoControllerTest {
	@InjectMocks
	private TipoCampoController controller;
	@Mock
	private TipoCampoService TipoCampoService;
	
	@Before
    public void setUp() throws BusinessException {
		MockitoAnnotations.initMocks(this);
		Mockito.when(TipoCampoService.findByName(new TipoCampo())).thenReturn(null);
		Mockito.when(TipoCampoService.create(new TipoCampo())).thenReturn(null);
    }
	
	@Test
	public void create() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.createTipoCampo(new TipoCampoDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
	
	@Test
	public void update() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.updateTipoCampo(new TipoCampoDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
}
