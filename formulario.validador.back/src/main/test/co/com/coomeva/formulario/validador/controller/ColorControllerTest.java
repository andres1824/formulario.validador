package co.com.coomeva.formulario.validador.controller;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import co.com.coomeva.formulario.validador.controller.parametrizacion.ColorController;
import co.com.coomeva.formulario.validador.dto.ColorDTO;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.Status;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Color;
import co.com.coomeva.formulario.validador.service.parametrizacion.ColorService;


public class ColorControllerTest {
	@InjectMocks
	private ColorController controller;
	@Mock
	private ColorService colorService;
	
	@Before
    public void setUp() throws BusinessException {
		MockitoAnnotations.initMocks(this);		
		Mockito.when(colorService.findByName(new Color())).thenReturn(null);
		Mockito.when(colorService.create(new Color())).thenReturn(null);
    }
	
	@Test
	public void create() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.createColor(new ColorDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
	
	@Test
	public void update() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.updateColor(new ColorDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
}
