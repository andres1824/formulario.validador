package co.com.coomeva.formulario.validador.controller;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import co.com.coomeva.formulario.validador.controller.parametrizacion.ParametroController;
import co.com.coomeva.formulario.validador.dto.ParametroDTO;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.Status;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Parametro;
import co.com.coomeva.formulario.validador.service.parametrizacion.ParametroService;


public class ParametroControllerTest {
	
	@InjectMocks
	private ParametroController controller;
	@Mock
	private ParametroService ParametroService;
	
	@Before
    public void setUp() throws BusinessException {
		MockitoAnnotations.initMocks(this);
		Mockito.when(ParametroService.findByName(new Parametro())).thenReturn(null);
		Mockito.when(ParametroService.store(new Parametro())).thenReturn(null);
    }
	
	@Test
	public void create() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.createParametro(new ParametroDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
	
	@Test
	public void update() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.updateParametro(new ParametroDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
}
