package co.com.coomeva.formulario.validador.controller;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import co.com.coomeva.formulario.validador.controller.parametrizacion.EmpresaController;
import co.com.coomeva.formulario.validador.dto.EmpresaDTO;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.Status;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Empresa;
import co.com.coomeva.formulario.validador.service.parametrizacion.EmpresaService;


public class EmpresaControllerTest {
	@InjectMocks
	private EmpresaController controller;
	@Mock
	private EmpresaService EmpresaService;
	
	@Before
    public void setUp() throws BusinessException {
		MockitoAnnotations.initMocks(this);
		Mockito.when(EmpresaService.findByName(new Empresa())).thenReturn(null);
		Mockito.when(EmpresaService.create(new Empresa())).thenReturn(null);
    }
	
	@Test
	public void createEmpresa() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.createEmpresa(new EmpresaDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
	
	@Test
	public void updateEmpresa() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.updateEmpresa(new EmpresaDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
}
