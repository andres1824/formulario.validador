package co.com.coomeva.formulario.validador.controller;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import co.com.coomeva.formulario.validador.controller.parametrizacion.RespuestaController;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.RespuestaDTO;
import co.com.coomeva.formulario.validador.dto.Status;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Respuesta;
import co.com.coomeva.formulario.validador.service.parametrizacion.RespuestaService;


public class RespuestaControllerTest {
	@InjectMocks
	private RespuestaController controller;
	@Mock
	private RespuestaService RespuestaService;
	
	@Before
    public void setUp() throws BusinessException {
		MockitoAnnotations.initMocks(this);
		Mockito.when(RespuestaService.findByValor(new Respuesta())).thenReturn(null);
		Mockito.when(RespuestaService.create(new Respuesta())).thenReturn(null);
    }
	
	@Test
	public void create() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.createRespuesta(new RespuestaDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
	
	@Test
	public void update() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.updateRespuesta(new RespuestaDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
}
