package co.com.coomeva.formulario.validador.controller;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import co.com.coomeva.formulario.validador.controller.parametrizacion.CampoController;
import co.com.coomeva.formulario.validador.dto.CampoDTO;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.Status;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.Campo;
import co.com.coomeva.formulario.validador.service.parametrizacion.CampoService;


public class CampoControllerTest {
	
	@Mock
	private CampoService campoService;
	
	@InjectMocks
	private CampoController controller;
	
	
	@Before
    public void setUp() throws BusinessException {
		
		MockitoAnnotations.initMocks(this);
		Mockito.when(campoService.findByName(new Campo())).thenReturn(null);
		Mockito.when(campoService.create(new Campo())).thenReturn(null);
    }
	
	@Test
	public void create() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.createCampo(new CampoDTO());	
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
	
	@Test
	public void update() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.updateCampo(new CampoDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
}
