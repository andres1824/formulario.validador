package co.com.coomeva.formulario.validador.controller;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import co.com.coomeva.formulario.validador.controller.parametrizacion.TipoReporteController;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.Status;
import co.com.coomeva.formulario.validador.dto.TipoReporteDTO;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.TipoReporte;
import co.com.coomeva.formulario.validador.service.parametrizacion.TipoReporteService;


public class TipoReporteControllerTest {
	@InjectMocks
	private TipoReporteController controller;
	@Mock
	private TipoReporteService TipoReporteService;
	
	@Before
    public void setUp() throws BusinessException {
		MockitoAnnotations.initMocks(this);
		Mockito.when(TipoReporteService.findByName(new TipoReporte())).thenReturn(null);
		Mockito.when(TipoReporteService.create(new TipoReporte())).thenReturn(null);
    }
	
	@Test
	public void createTipoReporte() throws IOException, InterruptedException, BusinessException {

		ResponseService httpResponse = controller.createTipoReporte(new TipoReporteDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
	
	@Test
	public void updateTipoReporte() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.updateTipoReporte(new TipoReporteDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
}
