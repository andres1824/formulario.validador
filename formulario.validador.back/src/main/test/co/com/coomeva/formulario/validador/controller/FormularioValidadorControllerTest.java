package co.com.coomeva.formulario.validador.controller;

import java.io.IOException;

import org.apache.commons.codec.EncoderException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import co.com.coomeva.formulario.validador.controller.parametrizacion.FormularioValidacionController;
import co.com.coomeva.formulario.validador.controller.parametrizacion.TipoReporteController;
import co.com.coomeva.formulario.validador.dto.TipoReporteDTO;
import co.com.coomeva.formulario.validador.dto.FormularioValidadorDTO;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.Status;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.FormularioValidacion;
import co.com.coomeva.formulario.validador.model.TipoReporte;
import co.com.coomeva.formulario.validador.service.parametrizacion.FormularioValidacionService;
import co.com.coomeva.formulario.validador.service.parametrizacion.TipoReporteService;


public class FormularioValidadorControllerTest {
	@InjectMocks
	private FormularioValidacionController controller;
	@Mock
	private FormularioValidacionService formularioValidacionService;
	
	@Before
    public void setUp() throws BusinessException {
		MockitoAnnotations.initMocks(this);
		Mockito.when(formularioValidacionService.findById(new FormularioValidacion())).thenReturn(null);
		Mockito.when(formularioValidacionService.findByIdFormulario(new FormularioValidacion())).thenReturn(null);
		Mockito.when(formularioValidacionService.create(new FormularioValidacion())).thenReturn(null);
    }
	
	@Test
	public void create() throws EncoderException, IOException, InterruptedException, BusinessException {

		ResponseService httpResponse = controller.createFormularioValidacion(new FormularioValidadorDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
	
	@Test
	public void update() throws EncoderException, IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.updateFormularioValidacion(new FormularioValidadorDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
}
