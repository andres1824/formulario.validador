package co.com.coomeva.formulario.validador.controller;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import co.com.coomeva.formulario.validador.controller.parametrizacion.TipoDatoController;
import co.com.coomeva.formulario.validador.dto.ResponseService;
import co.com.coomeva.formulario.validador.dto.Status;
import co.com.coomeva.formulario.validador.dto.TipoDatoDTO;
import co.com.coomeva.formulario.validador.exception.BusinessException;
import co.com.coomeva.formulario.validador.model.TipoDato;
import co.com.coomeva.formulario.validador.service.parametrizacion.TipoDatoService;


public class TipoDatoControllerTest {
	@InjectMocks
	private TipoDatoController controller;
	@Mock
	private TipoDatoService TipoDatoService;
	
	@Before
    public void setUp() throws BusinessException {
		MockitoAnnotations.initMocks(this);
		Mockito.when(TipoDatoService.findByName(new TipoDato())).thenReturn(null);
		Mockito.when(TipoDatoService.create(new TipoDato())).thenReturn(null);
    }
	
	@Test
	public void create() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.createTipoDato(new TipoDatoDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
	
	@Test
	public void update() throws IOException, InterruptedException, BusinessException {
		ResponseService httpResponse = controller.updateTipoDato(new TipoDatoDTO());
		
        Assert.assertEquals(httpResponse.getStatus(), Status.OK);
	}
}
