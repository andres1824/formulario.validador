import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../services/http.service.module';
import { Modals } from '../shared/modals';
import { StorageManager } from '../services/storageManager';
import { ApplicationUtil } from '../shared/applicationUtil';
import { DomSanitizer } from '@angular/platform-browser';
import { routesNames } from '../shared/routes.name';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {MatSort} from '@angular/material/sort';
@Component({
    selector: 'app-empresa',
    templateUrl: './empresa.component.html'
})
export class EmpresaComponent implements OnInit {
    
    empresa: any = {
    nombre:'',
    estado:'',
        usuarioCreacion: '',
        usuarioModificacion: ''
    };
    estado: boolean = true;
    isEdit: boolean = false;
    isDetailVisible: boolean = false;
    isDetailVisibleCol: boolean = false;

    formGeneral = new FormGroup({
    nombre:new FormControl('', [Validators.required]),
    estado:new FormControl('', [Validators.required]),
      });

    public empresaList: any;
    public empresaListCopy: any;
    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
    @ViewChild(MatSort, {static: false}) sort: MatSort;
    @ViewChild('data', {static: true}) datatable: ElementRef;
    public tableHTML;
    public displayedColumns: string[] = ['empresaId', 
    'nombre',
        'estado',
    'acciones'];

    public displayedColumnsDetail: string[] = ['empresaId', 
    'nombre',
    'estado',
    'usuarioCreacion',
    'usuarioModificacion', 'fechaCreacion', 'fechaModificacion'];
    constructor(private http: HttpService, private router: Router, private modals: Modals,
    private storage: StorageManager, private appCons: ApplicationUtil, private route: ActivatedRoute,
    public sanitizer: DomSanitizer) {

    }

    ngOnInit() {
        this.getAll(0);
        this.isEdit = false;
    }

    view(element) {
        let data = [];
        data[0] = element;
        this.empresaListCopy = this.empresaList;
        this.empresaList = new MatTableDataSource<any>(data);
        this.isDetailVisibleCol = true;
        this.isDetailVisible = true;
        setTimeout(() => {
            this.tableHTML = this.datatable.nativeElement.innerHTML;
        }, 200);
    }

    closeModal() {
        this.empresaList = this.empresaListCopy;
        this.isDetailVisibleCol = false;
        this.isDetailVisible = false;
    }

    getAll(type) {
        this.clearValidations();
        if (type == 1) {
            this.empresa.estado = this.estado == true ? 'Activo': 'Inactivo';
        }
        let URL = this.appCons.URL_SERVICE + 'empresa/find';
        this.http.postOauth(URL, this.empresa).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                  }

                if (response.data != null && response.data.length > 0) {
                    this.empresaList = new MatTableDataSource<any>(response.data);
                    this.empresaList.paginator = this.paginator;
                    this.empresaList.sort = this.sort;
                } else {
                    if (type == 1) {
                        this.modals.showMessageWarn('No se encontraron registros para el filtro ingresado');
                    }
                    this.empresaList = new MatTableDataSource<any>([]);
                    this.empresaList.paginator = null;
                    this.empresaList.sort = null;
                }
                this.clearValidations();
                
            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    submit() {
        if (this.validar()) {
            this.empresa.estado = this.estado == true ? 'Activo': 'Inactivo';
            let user = this.storage.get('user');
            let uri = 'empresa/create';

            if (!this.isEdit) {
                this.empresa.usuarioCreacion = user.userId;
            } else {
                uri = 'empresa/update';
                this.empresa.usuarioModificacion = user.userId;
            }

            let URL = this.appCons.URL_SERVICE + uri;
            this.http.postOauth(URL, this.empresa).
                then((response) => {
                    if (response.status == 'FAILURE') {
                        this.modals.error(response.message);
                        return;
                    }
                    this.clean();
                    this.modals.info(response.message);
                })
        }
    }

    edit(item) {	
        window.scrollTo(0,0);
        this.isEdit = true;
        this.empresa = this.appCons.deepCopy(item);

        if(this.empresa != null) {
            this.estado = this.empresa.estado == 'Activo' ? true: false;
        }
    }

    applyFilter(filter: string) {
        this.empresaList.filterPredicate = (data, filter) => JSON.stringify(data).toLocaleLowerCase().includes(filter);
        this.empresaList.filter = (filter).trim().toLocaleLowerCase();
    }

    validar() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).updateValueAndValidity();
        });
        let error = '<div style="text-align: left;">';
        let isValid = true;

        if(!this.formGeneral.valid) {
            isValid = false;
            error += "Por favor ingrese los campos marcados en rojo <br>";
        }

        if(!isValid) {
            this.modals.error(error);
        }

        return isValid;
    }

    clearValidations() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).clearValidators();
            this.formGeneral.get(key).updateValueAndValidity();
        });
        this.formGeneral.get('nombre').setValidators([Validators.required]);
        this.formGeneral.get('estado').setValidators([Validators.required]);
    }

    clean() {
        this.empresa = {
        nombre:'',
        estado:'',
        };
        this.empresaList = new MatTableDataSource<any>([]);
        this.empresaList.paginator = this.paginator;
        this.empresaList.sort = null;
        this.isEdit = false;
        this.estado = true;
        this.getAll(0);
    }

}