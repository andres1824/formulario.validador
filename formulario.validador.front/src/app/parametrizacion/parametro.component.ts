import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../services/http.service.module';
import { Modals } from '../shared/modals';
import { StorageManager } from '../services/storageManager';
import { ApplicationUtil } from '../shared/applicationUtil';
import { DomSanitizer } from '@angular/platform-browser';
import { routesNames } from '../shared/routes.name';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
@Component({
    selector: 'app-parametro',
    templateUrl: './parametro.component.html'
})
export class ParametroComponent implements OnInit {

    parametro: any = {
        nombre: '',
        valor: '',
        descripcion: '',
        estado: '',
        usuarioCreacion: '',
        usuarioModificacion: ''
    };
    estado: boolean = true;
    isEdit: boolean = false;
    isDetailVisible: boolean = false;
    isDetailVisibleCol: boolean = false;

    formGeneral = new FormGroup({
        nombre: new FormControl('', [Validators.required]),
        valor: new FormControl('', [Validators.required]),
        descripcion: new FormControl(''),
        estado: new FormControl('', [Validators.required]),
    });

    public parametroList: any;
    public parametroListCopy: any;
    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: false }) sort: MatSort;
    @ViewChild('data', { static: true }) datatable: ElementRef;
    public tableHTML;
    public displayedColumns: string[] = ['parametroId',
        'nombre',
        'valor',
        'descripcion',
        'estado',
        'acciones'];

    public displayedColumnsDetail: string[] = ['parametroId',
        'nombre',
        'valor',
        'descripcion',
        'estado',
        'usuarioCreacion',
        'usuarioModificacion', 'fechaCreacion', 'fechaModificacion'];
    constructor(private http: HttpService, private router: Router, private modals: Modals,
        private storage: StorageManager, private appCons: ApplicationUtil, private route: ActivatedRoute,
        public sanitizer: DomSanitizer) {

    }

    ngOnInit() {
        this.getAll(0);
        this.isEdit = false;
    }

    view(element) {
        let data = [];
        data[0] = element;
        this.parametroListCopy = this.parametroList;
        this.parametroList = new MatTableDataSource<any>(data);
        this.isDetailVisibleCol = true;
        this.isDetailVisible = true;
        setTimeout(() => {
            this.tableHTML = this.datatable.nativeElement.innerHTML;
        }, 200);
    }

    closeModal() {
        this.parametroList = this.parametroListCopy;
        this.isDetailVisibleCol = false;
        this.isDetailVisible = false;
    }

    getAll(type) {
        this.clearValidations();
        if (type == 1) {
            this.parametro.estado = this.estado == true ? 'Activo' : 'Inactivo';
        }
        let URL = this.appCons.URL_SERVICE + 'parametro/find';
        this.http.postOauth(URL, this.parametro).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }

                if (response.data != null && response.data.length > 0) {
                    this.parametroList = new MatTableDataSource<any>(response.data);
                    this.parametroList.paginator = this.paginator;
                    this.parametroList.sort = this.sort;
                } else {
                    if (type == 1) {
                        this.modals.showMessageWarn('No se encontraron registros para el filtro ingresado');
                    }
                    this.parametroList = new MatTableDataSource<any>([]);
                    this.parametroList.paginator = null;
                    this.parametroList.sort = null;
                }
                this.clearValidations();

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    submit() {
        if (this.validar()) {
            this.parametro.estado = this.estado == true ? 'Activo' : 'Inactivo';
            let user = this.storage.get('user');
            let uri = 'parametro/create';

            if (!this.isEdit) {
                this.parametro.usuarioCreacion = user.userId;
            } else {
                uri = 'parametro/update';
                this.parametro.usuarioModificacion = user.userId;
            }

            let URL = this.appCons.URL_SERVICE + uri;
            this.http.postOauth(URL, this.parametro).
                then((response) => {
                    if (response.status == 'FAILURE') {
                        this.modals.error(response.message);
                        return;
                    }
                    this.clean();
                    this.modals.info(response.message);
                })
        }
    }

    edit(item) {
        window.scrollTo(0,0);
        this.isEdit = true;
        this.parametro = this.appCons.deepCopy(item);

        if (this.parametro != null) {
            this.estado = this.parametro.estado == 'Activo' ? true : false;
        }
    }

    applyFilter(filter: string) {
        this.parametroList.filterPredicate = (data, filter) => JSON.stringify(data).toLocaleLowerCase().includes(filter);
        this.parametroList.filter = (filter).trim().toLocaleLowerCase();
    }

    validar() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).updateValueAndValidity();
        });
        let error = '<div style="text-align: left;">';
        let isValid = true;

        if (!this.formGeneral.valid) {
            isValid = false;
            error += "Por favor ingrese los campos marcados en rojo <br>";
        }

        if (!isValid) {
            this.modals.error(error);
        }

        return isValid;
    }

    clearValidations() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).clearValidators();
            this.formGeneral.get(key).updateValueAndValidity();
        });
        this.formGeneral.get('nombre').setValidators([Validators.required]);
        this.formGeneral.get('valor').setValidators([Validators.required]);
        this.formGeneral.get('descripcion').setValidators([]);
        this.formGeneral.get('estado').setValidators([Validators.required]);
    }

    clean() {
        this.parametro = {
            nombre: '',
            valor: '',
            descripcion: '',
            estado: '',
        };
        this.parametroList = new MatTableDataSource<any>([]);
        this.parametroList.paginator = this.paginator;
        this.parametroList.sort = null;
        this.isEdit = false;
        this.estado = true;
        this.getAll(0);
    }

}