import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../services/http.service.module';
import { Modals } from '../shared/modals';
import { StorageManager } from '../services/storageManager';
import { ApplicationUtil } from '../shared/applicationUtil';
import { DomSanitizer } from '@angular/platform-browser';
import { routesNames } from '../shared/routes.name';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {MatSort} from '@angular/material/sort';


@Component({
    selector: 'app-formulario-validador',
    templateUrl: './formulario.validador.component.html'
})
export class FormularioValidador implements OnInit {

    formularioValidacion: any = {
    fvFormulario:{formularioId:''},
    fvCampoDocumento:{campoId:''},
    fvCampoTpDocumento:{campoId:''},
    consultaListaControl:'',
    consultaCentralRiesgo:'',
    centralRiesgo:'',
    consultaBuc:'',
    consultaPeopleNet:'',
    usuarioCreacion: '',
    usuarioModificacion: ''
    };

    fvAgrupacionCampo: any={
    fvCampo:''
    }

   
    consultaListaControl: boolean = true;
    consultaCentralRiesgo: boolean = true;
    consultaCentralRiesgoNA: boolean = true;
    consultaBUC: boolean = true;
    consultaPeopleNet: boolean = true;

    isEdit: boolean = false;
    isDetailVisible: boolean = false;
    isDetailVisibleCol: boolean = false;

    formGeneral = new FormGroup({
    fvFormulario:new FormControl('', [Validators.required]),
    fvCampoDocumento:new FormControl('', [Validators.required]),
    fvCampoTpDocumento:new FormControl('', [Validators.required]),
    consultaListaControl:new FormControl('', [Validators.required]),
    consultaCentralRiesgo:new FormControl('', [Validators.required]),
    consultaCentralRiesgoNA:new FormControl('', [Validators.required]),
    consultaBUC:new FormControl('', [Validators.required]),
    consultaPeopleNet:new FormControl('', [Validators.required]),
      });

    public formularioValidacionList: any;
    public formularioList: any;
    public formularioValidacionListCopy: any;
    public campoDocumentoList: any;
    public campoTipoDocumentoList: any;
    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
    @ViewChild(MatSort, {static: false}) sort: MatSort;
    @ViewChild('data', {static: true}) datatable: ElementRef;
    public tableHTML;
    public displayedColumns: string[] = ['formularioNombre', 
    'campoDocumento',
        'campoTipoDocumento',
        'centralRiesgo',
        'consultaBuc',
    'consultaCentralRiesgo',
    'consultaListaControl',   
    'consultaPeopleNet',

    'acciones'];

    public displayedColumnsDetail: string[] = ['formularioNombre', 
    'campoDocumento',
    'campoTipoDocumento',
    'centralRiesgo',
    'consultaBuc',
    'consultaCentralRiesgo',
    'consultaListaControl',   
    'consultaPeopleNet',
    'usuarioCreacion',
    'usuarioModificacion',
    'fechaCreacion',   
    'fechaModificacion'
];

    constructor(private http: HttpService, private router: Router, private modals: Modals,
    private storage: StorageManager, private appCons: ApplicationUtil, private route: ActivatedRoute,
    public sanitizer: DomSanitizer) {

    }

    ngOnInit() {
        this.getCampoDocumento();
        this.getCampoTipoDocumento();
        this.getAll(0);
        this.isEdit = false;
  
    }
    putItemValue($event, item) {
        item = $event;
    }

    view(element) {
        let data = [];
        data[0] = element;
        this.formularioValidacionListCopy = this.formularioValidacionList;
        this.formularioValidacionList = new MatTableDataSource<any>(data);
        this.isDetailVisibleCol = true;
        this.isDetailVisible = true;
        setTimeout(() => {
            this.tableHTML = this.datatable.nativeElement.innerHTML;
        }, 200);
    }

    closeModal() {
        this.formularioValidacionList = this.formularioValidacionListCopy;
        this.isDetailVisibleCol = false;
        this.isDetailVisible = false;
    }

    
    getCampoDocumento() {
        let URL = this.appCons.URL_SERVICE + 'campo/get/tipoCampo/logica/DOCUMENTO';

        this.http.getOauth(URL).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }

                if (response.data != null && response.data.length > 0) {
                    this.campoDocumentoList = (response.data);
                }

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    getCampoTipoDocumento() {
        let URL = this.appCons.URL_SERVICE + 'campo/get/tipoCampo/logica/TIPO_DOCUMENTO';

        this.http.getOauth(URL).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }

                if (response.data != null && response.data.length > 0) {
                    this.campoTipoDocumentoList = (response.data);
                }

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }


    getAll(type) {
        
        this.clearValidations();
        let URL = this.appCons.URL_SERVICE + 'formularioValicion/find';

        this.http.postOauth(URL,this.formularioValidacion).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }
                console.log(response.data)
                if (response.data != null && response.data.length > 0) {
                    this.formularioValidacionList = new MatTableDataSource<any>(response.data);
                    this.formularioValidacionList.paginator = this.paginator;
                    this.formularioValidacionList.sort = this.sort;
                } else {
                    if (type == 1) {
                        this.modals.showMessageWarn('No se encontraron registros para el filtro ingresado');
                    }
                    this.formularioValidacionList = new MatTableDataSource<any>([]);
                    this.formularioValidacionList.paginator = null;
                    this.formularioValidacionList.sort = null;
                }
                this.clearValidations();
            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })

        let getFormulario = this.appCons.URL_SERVICE + 'formulario/get/all';

        this.http.getOauth(getFormulario).then((response)=>{

            if (response.status == 'FAILURE') {
                this.modals.error(response.message);
                return;
              }
              else{
                this.formularioList=response.data;
              }

        })
        

    }

    submit() {

        if (this.validar()) {
            let user = this.storage.get('user');
            this.formularioValidacion.consultaListaControl = this.consultaListaControl == true ? 'Si' : 'No';
            this.formularioValidacion.centralRiesgo = this.consultaCentralRiesgoNA == true ? 'Si' : 'No';
            this.formularioValidacion.consultaCentralRiesgo = this.consultaCentralRiesgo == true ? 'Si' : 'No';
            this.formularioValidacion.consultaBuc = this.consultaBUC == true ? 'Si' : 'No';
            this.formularioValidacion.consultaPeopleNet = this.consultaPeopleNet == true ? 'Si' : 'No';
            let uri = 'formularioValicion/create';

            if (!this.isEdit) {
                this.formularioValidacion.usuarioCreacion = user.userId;
            } else {
                uri = 'formularioValicion/update';
                this.formularioValidacion.usuarioModificacion = user.userId;
            }

            let URL = this.appCons.URL_SERVICE + uri;
            this.http.postOauth(URL, this.formularioValidacion).
                then((response) => {
                    if (response.status == 'FAILURE') {
                        this.modals.error(response.message);
                        return;
                    }
                    this.clean();
                    this.modals.info(response.message);
                })
        }
    }

    edit(item) {	
        window.scrollTo(0,0);
        this.isEdit = true;
        this.formularioValidacion = this.appCons.deepCopy(item);

        if(this.formularioValidacion != null) {

            this.consultaCentralRiesgoNA = this.formularioValidacion.centralRiesgo == 'Si' ? true: false;
            this.consultaBUC = this.formularioValidacion.consultaBuc == 'Si' ? true: false;
            this.consultaCentralRiesgo = this.formularioValidacion.consultaCentralRiesgo == 'Si' ? true: false;
            this.consultaListaControl = this.formularioValidacion.consultaListaControl == 'Si' ? true: false;
            this.consultaPeopleNet = this.formularioValidacion.consultaPeopleNet == 'Si' ? true: false;
        }
    }

    applyFilter(filter: string) {
        this.formularioValidacionList.filterPredicate = (data, filter) => JSON.stringify(data).toLocaleLowerCase().includes(filter);
        this.formularioValidacionList.filter = (filter).trim().toLocaleLowerCase();
    }

    validar() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).updateValueAndValidity();
        });
        let error = '<div style="text-align: left;">';
        let isValid = true;

        if(!this.formGeneral.valid) {
            isValid = false;
            error += "Por favor ingrese los campos marcados en rojo <br>";
        }

        if(!isValid) {
            this.modals.error(error);
        }

        return isValid;
    }

    clearValidations() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).clearValidators();
            this.formGeneral.get(key).updateValueAndValidity();
        });

        this.formGeneral.get('fvFormulario').setValidators([Validators.required]);
        this.formGeneral.get('fvCampoDocumento').setValidators([Validators.required]);
        this.formGeneral.get('fvCampoTpDocumento').setValidators([Validators.required]);
        this.formGeneral.get('consultaListaControl').setValidators([Validators.required]);
        this.formGeneral.get('consultaCentralRiesgo').setValidators([Validators.required]);
        this.formGeneral.get('consultaCentralRiesgoNA').setValidators([Validators.required]);
        this.formGeneral.get('consultaBUC').setValidators([Validators.required]);
        this.formGeneral.get('consultaPeopleNet').setValidators([Validators.required]);
    }

    clean() {

            this.formularioValidacion = {
                fvFormulario:{formularioId:''},
                fvCampoDocumento:{campoId:''},
                fvCampoTpDocumento:{campoId:''},
                consultaListaControl:'',
                consultaCentralRiesgo:'',
                centralRiesgo:'',
                consultaBuc:'',
                consultaPeopleNet:'',
                usuarioCreacion: '',
                usuarioModificacion: ''
                };
        this.formularioValidacionList = new MatTableDataSource<any>([]);
        this.formularioValidacionList.paginator = this.paginator;
        this.formularioValidacionList.sort = null;
        this.isEdit = false;
        this.consultaListaControl = true;


        this.consultaCentralRiesgo = true;
        this.consultaCentralRiesgoNA = true;
        this.consultaBUC = true;
        this.consultaPeopleNet = true;
        this.consultaListaControl = true;
        this.getAll(0);
    }

}