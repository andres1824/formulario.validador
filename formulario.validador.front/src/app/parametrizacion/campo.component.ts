import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../services/http.service.module';
import { Modals } from '../shared/modals';
import { StorageManager } from '../services/storageManager';
import { ApplicationUtil } from '../shared/applicationUtil';
import { DomSanitizer } from '@angular/platform-browser';
import { routesNames } from '../shared/routes.name';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
@Component({
    selector: 'app-campo',
    templateUrl: './campo.component.html'
})
export class CampoComponent implements OnInit {

    campo: any = {
        nombre: '',
        dependiente: 'NO',
        obligatorio: '',
        longitud: '',
        fvTipoCampo: { tipoCampoId: '' },
        fvTipoDato: { tipoDatoId: '' },
        fvRespuesta: { respuestaId: '' },
        estado: '',
        usuarioCreacion: '',
        usuarioModificacion: ''
    };
    respuesta: any = {
        respuestaId: '',
        campoId: '',
        respuestaCorrecta: '',
        campoDependienteId: '',
    }
    estado: boolean = true;
    isEdit: boolean = false;
    isDetailVisible: boolean = false;
    isDetailVisibleCol: boolean = false;
    isRespuestaVisible: boolean = false;
    tipoCampoList: any[] = []
    tipoDatoList: any[] = []
    campoDependienteList: any[] = []
    respuestaDependienteList: any[] = []

    formGeneral = new FormGroup({
        nombre: new FormControl('', [Validators.required]),
        obligatorio: new FormControl('', [Validators.required]),
        longitud: new FormControl('', [Validators.required]),
        tipoCampo: new FormControl('', [Validators.required]),
        estado: new FormControl('', [Validators.required]),
    });

    public respuestaCampoList: any;
    public campoList: any;
    public campoListCopy: any;
    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: false }) sort: MatSort;
    @ViewChild('data', { static: true }) datatable: ElementRef;
    @ViewChild(MatPaginator, { static: true }) paginatorRespuestaCampo: MatPaginator;
    @ViewChild(MatSort, { static: true }) sortRespuestaCampo: MatSort;
    public tableHTML;
    public displayedColumns: string[] = ['campoId',
        'nombre',
        'dependiente',
        'obligatorio',
        'longitud',
        'tipoCampo',
        'estado',
        'acciones'];
    campoDependiente = 'display:none'

    public displayedColumnsDetail: string[] = ['campoId',
        'nombre',
        'dependiente',
        'obligatorio',
        'longitud',
        'tipoCampo',
        'estado',
        'usuarioCreacion',
        'usuarioModificacion', 'fechaCreacion', 'fechaModificacion'];

    public displayedColumnsRespuesta: string[] = ['campoId',
        'campo',
        'respuesta', 'respuestaCorrecta', 'campoDependiente', 'acciones'];
    public disabledCampoDependiente: any = true;
    constructor(private http: HttpService, private router: Router, private modals: Modals,
        private storage: StorageManager, private appCons: ApplicationUtil, private route: ActivatedRoute,
        public sanitizer: DomSanitizer) {

    }

    ngOnInit() {
        this.getAll(0);
        this.getTipoCampoAll();
        this.isEdit = false;
    }

    view(element) {
        let data = [];
        data[0] = element;
        this.campoListCopy = this.campoList;
        this.campoList = new MatTableDataSource<any>(data);
        this.isDetailVisibleCol = true;
        this.isDetailVisible = true;
        setTimeout(() => {
            this.tableHTML = this.datatable.nativeElement.innerHTML;
        }, 200);
    }

    closeModal() {
        this.campoList = this.campoListCopy;
        this.isDetailVisibleCol = false;
        this.isDetailVisible = false;
    }

    getData(element) {
        if (element != null) {
            return element.nombre;
        }

        return '';
    }

    getAll(type) {
        this.clearValidations();
        if (type == 1) {
            this.campo.estado = this.estado == true ? 'Activo' : 'Inactivo';
        }
        let URL = this.appCons.URL_SERVICE + 'campo/find';
        this.http.postOauth(URL, this.campo).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }

                if (response.data != null && response.data.length > 0) {
                    this.campoList = new MatTableDataSource<any>(response.data);
                    this.campoList.paginator = this.paginator;

                    this.campoList.sort = this.sort;

                    this.campoList.sortingDataAccessor = (client, property) => {
                        switch (property) {
                            case 'tipoDato': return client.nombre;
                            case 'tipoCampo': return client.nombre;
                            default: return client[property];
                        }
                    };
                } else {
                    if (type == 1) {
                        this.modals.showMessageWarn('No se encontraron registros para el filtro ingresado');
                    }
                    this.campoList = new MatTableDataSource<any>([]);
                    this.campoList.paginator = null;
                    this.campoList.sort = null;
                }
                this.clearValidations();

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    getTipoCampoAll() {
        let URL = this.appCons.URL_SERVICE + 'tipoCampo/get/all';
        this.http.getOauth(URL).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }
                this.tipoCampoList = [];

                if (response.data != null && response.data.length > 0) {
                    this.tipoCampoList = (response.data);
                }

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    getTipoDatoAll() {
        let URL = this.appCons.URL_SERVICE + 'tipoDato/get/all';
        this.http.getOauth(URL).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }
                this.tipoDatoList = [];

                if (response.data != null && response.data.length > 0) {
                    this.tipoDatoList = (response.data);
                }

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    getCampoDependienteChange($event) {
        if(this.respuesta.respuestaCorrecta == 'SI') {
            this.disabledCampoDependiente = false;
        } else {
            this.disabledCampoDependiente = true;
        }
    }

    getCampoDependienteAll(campoId) {
        let URL = this.appCons.URL_SERVICE + 'campo/get/dependiente/' + campoId;
        this.http.getOauth(URL).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }
                this.campoDependienteList = [];

                if (response.data != null && response.data.length > 0) {
                    this.campoDependienteList = (response.data);
                }

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    getRespuestaDependienteAll() {
        let URL = this.appCons.URL_SERVICE + 'respuesta/get/all';
        this.http.getOauth(URL).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }
                this.respuestaDependienteList = [];

                if (response.data != null && response.data.length > 0) {
                    this.respuestaDependienteList = (response.data);
                }

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    submit() {
        if (this.validar()) {
            this.campo.estado = this.estado == true ? 'Activo' : 'Inactivo';
            let user = this.storage.get('user');
            let uri = 'campo/create';

            if (!this.isEdit) {
                this.campo.usuarioCreacion = user.userId;
            } else {
                uri = 'campo/update';
                this.campo.usuarioModificacion = user.userId;
            }

            let URL = this.appCons.URL_SERVICE + uri;
            this.http.postOauth(URL, this.campo).
                then((response) => {
                    if (response.status == 'FAILURE') {
                        this.modals.error(response.message);
                        return;
                    }
                    this.clean();
                    this.modals.info(response.message);
                })
        }
    }

    edit(item) {
        window.scrollTo(0,0);
        this.isEdit = true;
        this.campo = this.appCons.deepCopy(item);

        if (this.campo != null) {
            this.estado = this.campo.estado == 'Activo' ? true : false;
        }
    }

    addPreguntas(item) {
        this.respuesta.campoId = item.campoId;
        this.getRespuestaDependienteAll();
        this.getRespuestaCampoAll();
        this.getCampoDependienteAll(item.campoId);
        this.isRespuestaVisible = true;
    }

    closeModalRespuestas() {
        this.isRespuestaVisible = false;
    }

    submitRespuesta() {
        let isValid = true;
        let error = '<div style="text-align: left;">';

        if (this.respuesta.respuestaId == '') {
            isValid = false;
            error += "Debe seleccionar el campo respuesta <br>";
        }

        if (this.respuesta.respuestaCorrecta == '') {
            isValid = false;
            error += "Debe seleccionar el campo dependencia <br>";
        }

        if (!isValid) {
            this.modals.error(error);
        }

        if (isValid) {
            let uri = 'campo/createRespuesta';
            let URL = this.appCons.URL_SERVICE + uri;
            let data = {
                fvRespuesta: this.respuesta, campoId: this.respuesta.campoId,
                respuestaCorrecta: this.respuesta.respuestaCorrecta, campoDependienteId: this.respuesta.campoDependienteId
            }
            this.http.postOauth(URL, data).
                then((response) => {
                    if (response.status == 'FAILURE') {
                        this.modals.error(response.message);
                        return;
                    }
                    this.getRespuestaCampoAll();
                    this.modals.info(response.message);
                })
        }
    }

    cleanRespuesta() {
        this.respuesta.respuestaId = '';
        this.respuesta.respuestaCorrecta = '';
        this.respuesta.campoDependienteId = '';
    }

    getRespuestaCampoAll() {
        let URL = this.appCons.URL_SERVICE + 'campo/respuestaCampo/get/all';
        let data = { fvRespuesta: this.respuesta, campoId: this.respuesta.campoId }
        this.http.postOauth(URL, data).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }

                if (response.data != null && response.data.length > 0) {
                    this.respuestaCampoList = new MatTableDataSource<any>(response.data);
                    this.respuestaCampoList.paginator = this.paginatorRespuestaCampo;

                    this.respuestaCampoList.sortingDataAccessor = (client, property) => {
                        switch (property) {
                            case 'respuesta': return client.fvRespuesta.valor;
                            case 'campoDependiente': return client.fvCampoDependiente == null ? '':client.fvCampoDependiente.nombre;
                            default: return client[property];
                        }
                    };

                    this.respuestaCampoList.sort = this.sortRespuestaCampo;

                    
                } else {
                    this.respuestaCampoList = new MatTableDataSource<any>([]);
                    this.respuestaCampoList.paginator = null;
                    this.respuestaCampoList.sort = null;
                }
            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    removeRespuestaCampo(element) {
        this.modals.confirmDelete((result) => {
            if (result.isConfirmed) {
                let uri = 'campo/deleteRespuesta';
                let URL = this.appCons.URL_SERVICE + uri;
                this.http.postOauth(URL, element).
                    then((response) => {
                        if (response.status == 'FAILURE') {
                            this.modals.error(response.message);
                            return;
                        }
                        this.getRespuestaCampoAll();
                        this.modals.info(response.message);
                    })
            }
        });


    }

    getNombreCampoDependiente(campo) {
        if (campo != null) {
            return campo.nombre;
        }

        return "";
    }

    applyFilter(filter: string) {
        this.campoList.filterPredicate = (data, filter) => JSON.stringify(data).toLocaleLowerCase().includes(filter);
        this.campoList.filter = (filter).trim().toLocaleLowerCase();
    }

    validar() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).updateValueAndValidity();
        });
        let error = '<div style="text-align: left;">';
        let isValid = true;

        if (!this.formGeneral.valid) {
            isValid = false;
            error += "Por favor ingrese los campos marcados en rojo <br>";
        }

        if (!this.appCons.isNumber(this.campo.longitud)) {
            isValid = false;
            error += "El campo longitud debe ser num\u00E9rico <br>";
        }

        if (!isValid) {
            this.modals.error(error);
        }

        return isValid;
    }

    clearValidations() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).clearValidators();
            this.formGeneral.get(key).updateValueAndValidity();
        });
        this.formGeneral.get('nombre').setValidators([Validators.required]);
        this.formGeneral.get('obligatorio').setValidators([Validators.required]);
        this.formGeneral.get('longitud').setValidators([Validators.required]);
        this.formGeneral.get('tipoCampo').setValidators([Validators.required]);
        this.formGeneral.get('estado').setValidators([Validators.required]);
    }

    clean() {
        this.campo = {
            nombre: '',
            dependiente: 'NO',
            obligatorio: '',
            longitud: '',
            tipoCampo: '',
            tipoDato: '',
            estado: '',
            fvTipoCampo: { tipoCampoId: '' },
            fvTipoDato: { tipoDatoId: '' },
            fvRespuesta: { respuestaId: '' }
        };
        this.campoList = new MatTableDataSource<any>([]);
        this.campoList.paginator = this.paginator;
        this.campoList.sort = null;
        this.isEdit = false;
        this.estado = true;
        this.getAll(0);
    }

    getVisibleRespuesta(tipoCampo) {
        
        if(tipoCampo.logica == 'COMBO' || tipoCampo.logica == 'COMBO_MULTIPLE' || tipoCampo.logica == 'TIPO_DOCUMENTO') {
            return true;
        }
        
        return false;
    }

}