import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../services/http.service.module';
import { Modals } from '../shared/modals';
import { StorageManager } from '../services/storageManager';
import { ApplicationUtil } from '../shared/applicationUtil';
import { DomSanitizer } from '@angular/platform-browser';
import { routesNames } from '../shared/routes.name';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {MatSort} from '@angular/material/sort';
@Component({
    selector: 'app-tipoCampo',
    templateUrl: './tipoCampo.component.html'
})
export class TipoCampoComponent implements OnInit {
    
    tipoCampo: any = {
    nombre:'',
    logica:'',
    estado:'',
        usuarioCreacion: '',
        usuarioModificacion: ''
    };
    estado: boolean = true;
    isEdit: boolean = false;
    isDetailVisible: boolean = false;
    isDetailVisibleCol: boolean = false;

    formGeneral = new FormGroup({
    nombre:new FormControl('', [Validators.required]),
    logica:new FormControl('', [Validators.required]),
    estado:new FormControl('', [Validators.required]),
      });

    public tipoCampoList: any;
    public tipoCampoListCopy: any;
    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
    @ViewChild(MatSort, {static: false}) sort: MatSort;
    @ViewChild('data', {static: true}) datatable: ElementRef;
    public tableHTML;
    public displayedColumns: string[] = ['tipoCampoId', 
    'nombre',
        'logica',
        'estado',
    'acciones'];

    public displayedColumnsDetail: string[] = ['tipoCampoId', 
    'nombre',
    'logica',
    'estado',
    'usuarioCreacion',
    'usuarioModificacion', 'fechaCreacion', 'fechaModificacion'];
    constructor(private http: HttpService, private router: Router, private modals: Modals,
    private storage: StorageManager, private appCons: ApplicationUtil, private route: ActivatedRoute,
    public sanitizer: DomSanitizer) {

    }

    ngOnInit() {
        this.getAll(0);
        this.isEdit = false;
    }

    view(element) {
        let data = [];
        data[0] = element;
        this.tipoCampoListCopy = this.tipoCampoList;
        this.tipoCampoList = new MatTableDataSource<any>(data);
        this.isDetailVisibleCol = true;
        this.isDetailVisible = true;
        setTimeout(() => {
            this.tableHTML = this.datatable.nativeElement.innerHTML;
        }, 200);
    }

    tipoCampoChange($event) {
        if(this.tipoCampo.nombre == '') {
            this.tipoCampo.nombre = this.tipoCampo.logica;
        }
    }

    closeModal() {
        this.tipoCampoList = this.tipoCampoListCopy;
        this.isDetailVisibleCol = false;
        this.isDetailVisible = false;
    }

    getAll(type) {
        this.clearValidations();
        if (type == 1) {
            this.tipoCampo.estado = this.estado == true ? 'Activo': 'Inactivo';
        }
        let URL = this.appCons.URL_SERVICE + 'tipoCampo/find';
        this.http.postOauth(URL, this.tipoCampo).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                  }

                if (response.data != null && response.data.length > 0) {
                    this.tipoCampoList = new MatTableDataSource<any>(response.data);
                    this.tipoCampoList.paginator = this.paginator;
                    this.tipoCampoList.sort = this.sort;
                } else {
                    if (type == 1) {
                        this.modals.showMessageWarn('No se encontraron registros para el filtro ingresado');
                    }
                    this.tipoCampoList = new MatTableDataSource<any>([]);
                    this.tipoCampoList.paginator = null;
                    this.tipoCampoList.sort = null;
                }
                this.clearValidations();
                
            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    submit() {
        if (this.validar()) {
            this.tipoCampo.estado = this.estado == true ? 'Activo': 'Inactivo';
            let user = this.storage.get('user');
            let uri = 'tipoCampo/create';

            if (!this.isEdit) {
                this.tipoCampo.usuarioCreacion = user.userId;
            } else {
                uri = 'tipoCampo/update';
                this.tipoCampo.usuarioModificacion = user.userId;
            }

            let URL = this.appCons.URL_SERVICE + uri;
            this.http.postOauth(URL, this.tipoCampo).
                then((response) => {
                    if (response.status == 'FAILURE') {
                        this.modals.error(response.message);
                        return;
                    }
                    this.clean();
                    this.modals.info(response.message);
                })
        }
    }

    edit(item) {	
        window.scrollTo(0,0);
        this.isEdit = true;
        this.tipoCampo = this.appCons.deepCopy(item);

        if(this.tipoCampo != null) {
            this.estado = this.tipoCampo.estado == 'Activo' ? true: false;
        }
    }

    applyFilter(filter: string) {
        this.tipoCampoList.filterPredicate = (data, filter) => {let arr = Object.entries(data);
            let value = '';
            for(let i = 0;i < arr.length; i ++) {
                value += arr[i][1] + ','; 
            }
            return value.toLocaleLowerCase().includes(filter); }
        ;
        this.tipoCampoList.filter = (filter).trim().toLocaleLowerCase();
    }

    validar() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).updateValueAndValidity();
        });
        let error = '<div style="text-align: left;">';
        let isValid = true;

        if(!this.formGeneral.valid) {
            isValid = false;
            error += "Por favor ingrese los campos marcados en rojo <br>";
        }

        if(!isValid) {
            this.modals.error(error);
        }

        return isValid;
    }

    clearValidations() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).clearValidators();
            this.formGeneral.get(key).updateValueAndValidity();
        });
        this.formGeneral.get('nombre').setValidators([Validators.required]);
        this.formGeneral.get('logica').setValidators([Validators.required]);
        this.formGeneral.get('estado').setValidators([Validators.required]);
    }

    clean() {
        this.tipoCampo = {
        nombre:'',
        logica:'',
        estado:'',
        };
        this.tipoCampoList = new MatTableDataSource<any>([]);
        this.tipoCampoList.paginator = this.paginator;
        this.tipoCampoList.sort = null;
        this.isEdit = false;
        this.estado = true;
        this.getAll(0);
    }

}