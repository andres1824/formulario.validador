import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../services/http.service.module';
import { Modals } from '../shared/modals';
import { StorageManager } from '../services/storageManager';
import { ApplicationUtil } from '../shared/applicationUtil';
import { DomSanitizer } from '@angular/platform-browser';
import { routesNames } from '../shared/routes.name';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
@Component({
    selector: 'app-color',
    templateUrl: './color.component.html'
})
export class ColorComponent implements OnInit {

    color: any = {
        nombre: '',
        estado: '',
        codigoHtml: '',
        usuarioCreacion: '',
        usuarioModificacion: ''
    };
    estado: boolean = true;
    isEdit: boolean = false;
    isDetailVisible: boolean = false;
    isDetailVisibleCol: boolean = false;

    formGeneral = new FormGroup({
        nombre: new FormControl('', [Validators.required]),
        estado: new FormControl('', [Validators.required]),
        codigoHtml: new FormControl('', [Validators.required]),
    });

    public colorList: any;
    public colorListCopy: any;
    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: false }) sort: MatSort;
    @ViewChild('data', { static: true }) datatable: ElementRef;
    public tableHTML;
    public displayedColumns: string[] = ['colorId',
        'nombre',
        'codigoHtml',
        'estado',
        'acciones'];

    public displayedColumnsDetail: string[] = ['colorId',
        'nombre',
        'codigoHtml',
        'estado',
        'usuarioCreacion',
        'usuarioModificacion', 'fechaCreacion', 'fechaModificacion'];
    constructor(private http: HttpService, private router: Router, private modals: Modals,
        private storage: StorageManager, private appCons: ApplicationUtil, private route: ActivatedRoute,
        public sanitizer: DomSanitizer) {

    }

    ngOnInit() {
        this.getAll(0);
        this.isEdit = false;
    }

    view(element) {
        let data = [];
        data[0] = element;
        this.colorListCopy = this.colorList;
        this.colorList = new MatTableDataSource<any>(data);
        this.isDetailVisibleCol = true;
        this.isDetailVisible = true;
        setTimeout(() => {
            this.tableHTML = this.datatable.nativeElement.innerHTML;
        }, 200);
    }

    closeModal() {
        this.colorList = this.colorListCopy;
        this.isDetailVisibleCol = false;
        this.isDetailVisible = false;
    }

    getAll(type) {
        this.clearValidations();
        if (type == 1) {
            this.color.estado = this.estado == true ? 'Activo' : 'Inactivo';
        }
        let URL = this.appCons.URL_SERVICE + 'color/find';
        this.http.postOauth(URL, this.color).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }

                if (response.data != null && response.data.length > 0) {
                    this.colorList = new MatTableDataSource<any>(response.data);
                    this.colorList.paginator = this.paginator;
                    this.colorList.sort = this.sort;
                } else {
                    if (type == 1) {
                        this.modals.showMessageWarn('No se encontraron registros para el filtro ingresado');
                    }
                    this.colorList = new MatTableDataSource<any>([]);
                    this.colorList.paginator = null;
                    this.colorList.sort = null;
                }
                this.clearValidations();

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    submit() {
        if (this.validar()) {
            this.color.estado = this.estado == true ? 'Activo' : 'Inactivo';
            let user = this.storage.get('user');
            let uri = 'color/create';

            if (!this.isEdit) {
                this.color.usuarioCreacion = user.userId;
            } else {
                uri = 'color/update';
                this.color.usuarioModificacion = user.userId;
            }

            let URL = this.appCons.URL_SERVICE + uri;
            this.http.postOauth(URL, this.color).
                then((response) => {
                    if (response.status == 'FAILURE') {
                        this.modals.error(response.message);
                        return;
                    }
                    this.clean();
                    this.modals.info(response.message);
                })
        }
    }

    edit(item) {
        window.scrollTo(0,0);
        this.isEdit = true;
        this.color = this.appCons.deepCopy(item);

        if (this.color != null) {
            this.estado = this.color.estado == 'Activo' ? true : false;
        }
    }

    applyFilter(filter: string) {
        this.colorList.filterPredicate = (data, filter) => JSON.stringify(data).toLocaleLowerCase().includes(filter);
        this.colorList.filter = (filter).trim().toLocaleLowerCase();
    }

    validar() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).updateValueAndValidity();
        });
        let error = '<div style="text-align: left;">';
        let isValid = true;

        if (!this.formGeneral.valid) {
            isValid = false;
            error += "Por favor ingrese los campos marcados en rojo <br>";
        }

        if (!isValid) {
            this.modals.error(error);
        }

        return isValid;
    }

    clearValidations() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).clearValidators();
            this.formGeneral.get(key).updateValueAndValidity();
        });
        this.formGeneral.get('nombre').setValidators([Validators.required]);
        this.formGeneral.get('estado').setValidators([Validators.required]);
        this.formGeneral.get('codigoHtml').setValidators([Validators.required]);
    }

    clean() {
        this.color = {
            nombre: '',
            estado: '',
            codigoHtml: '',
        };
        this.colorList = new MatTableDataSource<any>([]);
        this.colorList.paginator = this.paginator;
        this.colorList.sort = null;
        this.isEdit = false;
        this.estado = true;
        this.getAll(0);
    }

}