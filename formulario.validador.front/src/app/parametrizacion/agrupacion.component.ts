import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../services/http.service.module';
import { Modals } from '../shared/modals';
import { StorageManager } from '../services/storageManager';
import { ApplicationUtil } from '../shared/applicationUtil';
import { DomSanitizer } from '@angular/platform-browser';
import { routesNames } from '../shared/routes.name';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
@Component({
    selector: 'app-agrupacion',
    templateUrl: './agrupacion.component.html'
})
export class AgrupacionComponent implements OnInit {

    agrupacion: any = {
        nombre: '',
        descripcion: '',
        estado: '',
        usuarioCreacion: '',
        usuarioModificacion: ''
    };

    agrupacionCampo: any = {
        fvAgrupacion: {agrupacionId: ''},
        fvCampo:  {campoId: ''}
    };

    estado: boolean = true;
    isEdit: boolean = false;
    isDetailVisible: boolean = false;
    isDetailVisibleCol: boolean = false;
    nombreList: any[] = []
    descripcionList: any[] = []
    campoList: any[] = []

    formGeneral = new FormGroup({
        nombre: new FormControl('', [Validators.required]),
        descripcion: new FormControl(''),
        estado: new FormControl('', [Validators.required]),
    });

    public agrupacionList: any;
    public isCampoVisible: boolean = false;
    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: false }) sort: MatSort;
    @ViewChild('data', { static: true }) datatable: ElementRef;
    @ViewChild('scheduledOrdersPaginator') paginatorAgrupacionCampo: MatPaginator;
    @ViewChild('sortAgrupacionCampo') sortAgrupacionCampo: MatSort;
    public agrupacionCampoList: MatTableDataSource<any>;
    public agrupacionListCopy: any;
    public tableHTML;
    public ordenList;
    public displayedColumns: string[] = ['agrupacionId',
        'nombre',
        'descripcion',
        'estado',
        'acciones'];

    public displayedColumnsDetail: string[] = ['agrupacionId',
        'nombre',
        'descripcion',
        'estado',
        'usuarioCreacion',
        'usuarioModificacion', 'fechaCreacion', 'fechaModificacion'];

    public displayedColumnsCampo: string[] = ['campoId', 'agrupacion',
        'campo', 'orden', 'acciones'];
    constructor(private http: HttpService, private router: Router, private modals: Modals,
        private storage: StorageManager, private appCons: ApplicationUtil, private route: ActivatedRoute,
        public sanitizer: DomSanitizer, private cdr: ChangeDetectorRef) {

    }

    ngOnInit() {
        this.getAll(0);
        this.getOrdenList();
        this.isEdit = false;
    }

    view(element) {
        let data = [];
        data[0] = element;
        this.agrupacionListCopy = this.agrupacionList;
        this.agrupacionList = new MatTableDataSource<any>(data);
        this.isDetailVisibleCol = true;
        this.isDetailVisible = true;
        setTimeout(() => {
            this.tableHTML = this.datatable.nativeElement.innerHTML;
        }, 200);
    }

    closeModal() {
        this.agrupacionList = this.agrupacionListCopy;
        this.isDetailVisibleCol = false;
        this.isDetailVisible = false;
    }

    getOrdenList() {
        this.ordenList = [];
        for(let i = 1;i <= 100;i ++) {
            let p = {orden: i};
            this.ordenList.push(p);
        }
    }

    getAll(type) {
        this.clearValidations();
        if (type == 1) {
            this.agrupacion.estado = this.estado == true ? 'Activo' : 'Inactivo';
        }
        let URL = this.appCons.URL_SERVICE + 'agrupacion/find';
        this.http.postOauth(URL, this.agrupacion).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }

                if (response.data != null && response.data.length > 0) {
                    this.agrupacionList = new MatTableDataSource<any>(response.data);
                    this.agrupacionList.paginator = this.paginator;
                    this.agrupacionList.sort = this.sort;
                } else {
                    if (type == 1) {
                        this.modals.showMessageWarn('No se encontraron registros para el filtro ingresado');
                    }
                    this.agrupacionList = new MatTableDataSource<any>([]);
                    this.agrupacionList.paginator = null;
                    this.agrupacionList.sort = null;
                }
                this.clearValidations();

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    getCampoAll() {
        let URL = this.appCons.URL_SERVICE + 'campo/get/all';
        this.http.getOauth(URL).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }
                this.campoList = [];

                if (response.data != null && response.data.length > 0) {
                    this.campoList = (response.data);
                }

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    addCampo(item) {
        this.isCampoVisible = true;
        this.agrupacionCampo.fvAgrupacion = item;
        this.getCampoAll();
        this.getAgrupacionCampoAll();
    }

    closeModalCampo() {
        this.isCampoVisible = false;
    }

    clearCampo() {
        this.agrupacionCampo.fvCampo.campoId = '';
    }

    submitCampo() {
        let isValid = true;
        let error = '<div style="text-align: left;">';

        if (this.agrupacionCampo.fvCampo.campoId == '') {
            isValid = false;
            error += "Debe seleccionar el campo <br>";
        }

        if (!isValid) {
            this.modals.error(error);
        }

        if (isValid) {
            let uri = 'agrupacion/createCampo';
            let URL = this.appCons.URL_SERVICE + uri;
            this.http.postOauth(URL, this.agrupacionCampo).
                then((response) => {
                    if (response.status == 'FAILURE') {
                        this.modals.error(response.message);
                        return;
                    }
                    this.getAgrupacionCampoAll();
                    this.clearCampo();
                    this.modals.info(response.message);
                })
        }
    }

    getAgrupacionCampoAll() {
        let URL = this.appCons.URL_SERVICE + 'agrupacion/campo/get/all/'+this.agrupacionCampo.fvAgrupacion.agrupacionId;
        this.http.getOauth(URL).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }

                if (response.data != null && response.data.length > 0) {
                    
                    for(let i = 0; i < response.data.length; i ++) {
                        for(let j = 0;j < this.ordenList.length;j ++) {
                            if(response.data[i].orden == this.ordenList[j].orden) {
                                this.ordenList.splice(j, 1);
                            }
                        }
                    }
                    


                    this.agrupacionCampoList = new MatTableDataSource<any>(response.data);
                    setTimeout(() => this.agrupacionCampoList.paginator = this.paginatorAgrupacionCampo);

                    this.agrupacionCampoList.sortingDataAccessor = (client, property) => {
                        switch (property) {
                            case 'campoId': return client.fvCampo.campoId;
                            case 'campo': return client.fvCampo.nombre;
                            case 'agrupacion': return client.fvAgrupacion == null ? '':client.fvAgrupacion.nombre;
                            default: return client[property];
                        }
                    };

                    setTimeout(() => this.agrupacionCampoList.sort = this.sortAgrupacionCampo);

                    this.cdr.detectChanges();
                } else {
                    this.agrupacionCampoList = new MatTableDataSource<any>([]);
                    this.agrupacionCampoList.paginator = null;
                    this.agrupacionCampoList.sort = null;
                }
            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    removeCampo(element) {
        let uri = 'agrupacion/deleteCampo';

        this.modals.confirmDelete((result) => {
            if (result.isConfirmed) {
                let URL = this.appCons.URL_SERVICE + uri;
                this.http.postOauth(URL, element).
                then((response) => {
                    if (response.status == 'FAILURE') {
                        this.modals.error(response.message);
                        return;
                    }
                    this.getAgrupacionCampoAll();
                    this.modals.info(response.message);
                })
            }
          });
    }

    submit() {
        if (this.validar()) {
            this.agrupacion.estado = this.estado == true ? 'Activo' : 'Inactivo';
            let user = this.storage.get('user');
            let uri = 'agrupacion/create';

            if (!this.isEdit) {
                this.agrupacion.usuarioCreacion = user.userId;
            } else {
                uri = 'agrupacion/update';
                this.agrupacion.usuarioModificacion = user.userId;
            }

            let URL = this.appCons.URL_SERVICE + uri;
            this.http.postOauth(URL, this.agrupacion).
                then((response) => {
                    if (response.status == 'FAILURE') {
                        this.modals.error(response.message);
                        return;
                    }
                    this.clean();
                    this.modals.info(response.message);
                })
        }
    }

    edit(item) {
        window.scrollTo(0,0);
        this.isEdit = true;
        this.agrupacion = this.appCons.deepCopy(item);

        if (this.agrupacion != null) {
            this.estado = this.agrupacion.estado == 'Activo' ? true : false;
        }
    }

    applyFilter(filter: string) {
        this.agrupacionList.filterPredicate = (data, filter) => JSON.stringify(data).toLocaleLowerCase().includes(filter);
        this.agrupacionList.filter = (filter).trim().toLocaleLowerCase();
    }

    validar() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).updateValueAndValidity();
        });
        let error = '<div style="text-align: left;">';
        let isValid = true;

        if (!this.formGeneral.valid) {
            isValid = false;
            error += "Por favor ingrese los campos marcados en rojo <br>";
        }

        if (!isValid) {
            this.modals.error(error);
        }

        return isValid;
    }

    clearValidations() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).clearValidators();
            this.formGeneral.get(key).updateValueAndValidity();
        });
        this.formGeneral.get('nombre').setValidators([Validators.required]);
        this.formGeneral.get('descripcion').setValidators([]);
        this.formGeneral.get('estado').setValidators([Validators.required]);
    }

    clean() {
        this.agrupacion = {
            nombre: '',
            descripcion: '',
            estado: '',
        };
        this.agrupacionList = new MatTableDataSource<any>([]);
        this.agrupacionList.paginator = this.paginator;
        this.agrupacionList.sort = null;
        this.isEdit = false;
        this.estado = true;
        this.getAll(0);
    }

}