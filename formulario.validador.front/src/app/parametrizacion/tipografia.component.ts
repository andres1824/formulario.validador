import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../services/http.service.module';
import { Modals } from '../shared/modals';
import { StorageManager } from '../services/storageManager';
import { ApplicationUtil } from '../shared/applicationUtil';
import { DomSanitizer } from '@angular/platform-browser';
import { routesNames } from '../shared/routes.name';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {MatSort} from '@angular/material/sort';
@Component({
    selector: 'app-tipografia',
    templateUrl: './tipografia.component.html'
})
export class TipografiaComponent implements OnInit {
    
    tipografia: any = {
    nombre:'',
    estado:'',
    codigoHtml:'',
        usuarioCreacion: '',
        usuarioModificacion: ''
    };
    estado: boolean = true;
    isEdit: boolean = false;
    isDetailVisible: boolean = false;
    isDetailVisibleCol: boolean = false;

    formGeneral = new FormGroup({
    nombre:new FormControl('', [Validators.required]),
    estado:new FormControl('', [Validators.required]),
    codigoHtml:new FormControl('', [Validators.required]),
      });

    public tipografiaList: any;
    public tipografiaListCopy: any;
    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
    @ViewChild(MatSort, {static: false}) sort: MatSort;
    @ViewChild('data', {static: true}) datatable: ElementRef;
    public tableHTML;
    public displayedColumns: string[] = ['tipografiaId', 
    'nombre',
        'codigoHtml',
        'estado',
    'acciones'];

    public displayedColumnsDetail: string[] = ['tipografiaId', 
    'nombre',
    'codigoHtml',
    'estado',
    'usuarioCreacion',
    'usuarioModificacion', 'fechaCreacion', 'fechaModificacion'];
    constructor(private http: HttpService, private router: Router, private modals: Modals,
    private storage: StorageManager, private appCons: ApplicationUtil, private route: ActivatedRoute,
    public sanitizer: DomSanitizer) {

    }

    ngOnInit() {
        this.getAll(0);
        this.isEdit = false;
    }

    view(element) {
        let data = [];
        data[0] = element;
        this.tipografiaListCopy = this.tipografiaList;
        this.tipografiaList = new MatTableDataSource<any>(data);
        this.isDetailVisibleCol = true;
        this.isDetailVisible = true;
        setTimeout(() => {
            this.tableHTML = this.datatable.nativeElement.innerHTML;
        }, 200);
    }

    closeModal() {
        this.tipografiaList = this.tipografiaListCopy;
        this.isDetailVisibleCol = false;
        this.isDetailVisible = false;
    }

    getAll(type) {
        this.clearValidations();
        if (type == 1) {
            this.tipografia.estado = this.estado == true ? 'Activo': 'Inactivo';
        }
        let URL = this.appCons.URL_SERVICE + 'tipografia/find';
        this.http.postOauth(URL, this.tipografia).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                  }

                if (response.data != null && response.data.length > 0) {
                    this.tipografiaList = new MatTableDataSource<any>(response.data);
                    this.tipografiaList.paginator = this.paginator;
                    this.tipografiaList.sort = this.sort;
                } else {
                    if (type == 1) {
                        this.modals.showMessageWarn('No se encontraron registros para el filtro ingresado');
                    }
                    this.tipografiaList = new MatTableDataSource<any>([]);
                    this.tipografiaList.paginator = null;
                    this.tipografiaList.sort = null;
                }
                this.clearValidations();
                
            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    submit() {
        if (this.validar()) {
            this.tipografia.estado = this.estado == true ? 'Activo': 'Inactivo';
            let user = this.storage.get('user');
            let uri = 'tipografia/create';

            if (!this.isEdit) {
                this.tipografia.usuarioCreacion = user.userId;
            } else {
                uri = 'tipografia/update';
                this.tipografia.usuarioModificacion = user.userId;
            }

            let URL = this.appCons.URL_SERVICE + uri;
            this.http.postOauth(URL, this.tipografia).
                then((response) => {
                    if (response.status == 'FAILURE') {
                        this.modals.error(response.message);
                        return;
                    }
                    this.clean();
                    this.modals.info(response.message);
                })
        }
    }

    edit(item) {	
        window.scrollTo(0,0);
        this.isEdit = true;
        this.tipografia = this.appCons.deepCopy(item);

        if(this.tipografia != null) {
            this.estado = this.tipografia.estado == 'Activo' ? true: false;
        }
    }

    applyFilter(filter: string) {
        this.tipografiaList.filterPredicate = (data, filter) => JSON.stringify(data).toLocaleLowerCase().includes(filter);
        this.tipografiaList.filter = (filter).trim().toLocaleLowerCase();
    }

    validar() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).updateValueAndValidity();
        });
        let error = '<div style="text-align: left;">';
        let isValid = true;

        if(!this.formGeneral.valid) {
            isValid = false;
            error += "Por favor ingrese los campos marcados en rojo <br>";
        }

        if(!isValid) {
            this.modals.error(error);
        }

        return isValid;
    }

    clearValidations() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).clearValidators();
            this.formGeneral.get(key).updateValueAndValidity();
        });
        this.formGeneral.get('nombre').setValidators([Validators.required]);
        this.formGeneral.get('estado').setValidators([Validators.required]);
        this.formGeneral.get('codigoHtml').setValidators([Validators.required]);
    }

    clean() {
        this.tipografia = {
        nombre:'',
        estado:'',
        codigoHtml:'',
        };
        this.tipografiaList = new MatTableDataSource<any>([]);
        this.tipografiaList.paginator = this.paginator;
        this.tipografiaList.sort = null;
        this.isEdit = false;
        this.estado = true;
        this.getAll(0);
    }

}