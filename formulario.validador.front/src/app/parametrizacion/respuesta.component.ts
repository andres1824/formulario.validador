import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../services/http.service.module';
import { Modals } from '../shared/modals';
import { StorageManager } from '../services/storageManager';
import { ApplicationUtil } from '../shared/applicationUtil';
import { DomSanitizer } from '@angular/platform-browser';
import { routesNames } from '../shared/routes.name';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
@Component({
    selector: 'app-respuesta',
    templateUrl: './respuesta.component.html'
})
export class RespuestaComponent implements OnInit {

    respuesta: any = {
        valor: '',
        estado: '',
        usuarioCreacion: '',
        usuarioModificacion: ''
    };
    estado: boolean = true;
    isEdit: boolean = false;
    isDetailVisible: boolean = false;
    isDetailVisibleCol: boolean = false;

    formGeneral = new FormGroup({
        valor: new FormControl('', [Validators.required]),
        estado: new FormControl('', [Validators.required]),
    });

    public respuestaList: any;
    public respuestaListCopy: any;
    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: false }) sort: MatSort;
    @ViewChild('data', { static: true }) datatable: ElementRef;
    public tableHTML;
    public displayedColumns: string[] = ['respuestaId',
        'valor',
        'estado',
        'acciones'];

    public displayedColumnsDetail: string[] = ['respuestaId',
        'valor',
        'estado',
        'usuarioCreacion',
        'usuarioModificacion', 'fechaCreacion', 'fechaModificacion'];
    constructor(private http: HttpService, private router: Router, private modals: Modals,
        private storage: StorageManager, private appCons: ApplicationUtil, private route: ActivatedRoute,
        public sanitizer: DomSanitizer) {

    }

    ngOnInit() {
        this.getAll(0);
        this.isEdit = false;
    }

    view(element) {
        let data = [];
        data[0] = element;
        this.respuestaListCopy = this.respuestaList;
        this.respuestaList = new MatTableDataSource<any>(data);
        this.isDetailVisibleCol = true;
        this.isDetailVisible = true;
        setTimeout(() => {
            this.tableHTML = this.datatable.nativeElement.innerHTML;
        }, 200);
    }

    closeModal() {
        this.respuestaList = this.respuestaListCopy;
        this.isDetailVisibleCol = false;
        this.isDetailVisible = false;
    }

    getAll(type) {
        this.clearValidations();
        if (type == 1) {
            this.respuesta.estado = this.estado == true ? 'Activo' : 'Inactivo';
        }
        let URL = this.appCons.URL_SERVICE + 'respuesta/find';
        this.http.postOauth(URL, this.respuesta).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }

                if (response.data != null && response.data.length > 0) {
                    this.respuestaList = new MatTableDataSource<any>(response.data);
                    this.respuestaList.paginator = this.paginator;
                    this.respuestaList.sort = this.sort;
                } else {
                    if (type == 1) {
                        this.modals.showMessageWarn('No se encontraron registros para el filtro ingresado');
                    }
                    this.respuestaList = new MatTableDataSource<any>([]);
                    this.respuestaList.paginator = null;
                    this.respuestaList.sort = null;
                }
                this.clearValidations();

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    submit() {
        if (this.validar()) {
            this.respuesta.estado = this.estado == true ? 'Activo' : 'Inactivo';
            let user = this.storage.get('user');
            let uri = 'respuesta/create';

            if (!this.isEdit) {
                this.respuesta.usuarioCreacion = user.userId;
            } else {
                uri = 'respuesta/update';
                this.respuesta.usuarioModificacion = user.userId;
            }

            let URL = this.appCons.URL_SERVICE + uri;
            this.http.postOauth(URL, this.respuesta).
                then((response) => {
                    if (response.status == 'FAILURE') {
                        this.modals.error(response.message);
                        return;
                    }
                    this.clean();
                    this.modals.info(response.message);
                })
        }
    }

    edit(item) {
        window.scrollTo(0,0);
        this.isEdit = true;
        this.respuesta = this.appCons.deepCopy(item);

        if (this.respuesta != null) {
            this.estado = this.respuesta.estado == 'Activo' ? true : false;
        }
    }

    applyFilter(filter: string) {
        this.respuestaList.filterPredicate = (data, filter) => JSON.stringify(data).toLocaleLowerCase().includes(filter);
        this.respuestaList.filter = (filter).trim().toLocaleLowerCase();
    }

    validar() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).updateValueAndValidity();
        });
        let error = '<div style="text-align: left;">';
        let isValid = true;

        if (!this.formGeneral.valid) {
            isValid = false;
            error += "Por favor ingrese los campos marcados en rojo <br>";
        }

        if (!isValid) {
            this.modals.error(error);
        }

        return isValid;
    }

    clearValidations() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).clearValidators();
            this.formGeneral.get(key).updateValueAndValidity();
        });
        this.formGeneral.get('valor').setValidators([Validators.required]);
        this.formGeneral.get('estado').setValidators([Validators.required]);
    }

    clean() {
        this.respuesta = {
            valor: '',
            estado: '',
        };
        this.respuestaList = new MatTableDataSource<any>([]);
        this.respuestaList.paginator = this.paginator;
        this.respuestaList.sort = null;
        this.isEdit = false;
        this.estado = true;
        this.getAll(0);
    }

}