export const routesNames = {
    PARAMETRIZACION_OPT: 'parametro',
    EMPRESA_OPT: 'empresa',
    CAUSAL_DESCARTE_OPT: 'causal_descarte',
    RESPUESTA_OPT: 'respuesta',
    TIPO_DATO_OPT: 'tipo_dato',
    TIPO_CAMPO_OPT: 'tipo_campo',
    TIPO_REPORTE_OPT: 'tipo_reporte',
    TIPOGRAFIA_OPT: 'tipografia',
    COLOR_OPT: 'color',
    CAMPO_OPT: 'campo',
    FORMULARIO_VALIDADOR_OPT: 'formulario_validador',
    AGRUPACION_OPT: 'agrupacion',
    FORMULARIO_OPT: 'formulario',
    VIEW_FORMULARIO_OPT: 'view_formulario'
}
