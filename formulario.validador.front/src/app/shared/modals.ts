import { Optional, Injectable, Output, EventEmitter, Component, OnInit, Inject, forwardRef, ComponentFactoryResolver } from '@angular/core'
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';
import swal from 'sweetalert2';

@Injectable()
export class Modals {

    public className: string = 'overlay-none';
    public isLoading: boolean = false;
    public modals = [];

    constructor(
        private toastyService: ToastyService, private toastyConfig: ToastyConfig
    ) {
        // Assign the selected theme name to the `theme` property of the instance of ToastyConfig. 
        // Possible values: default, bootstrap, material
        this.toastyConfig.theme = 'material';
    }

    ngOnInit() {

    }

    /**
   * Muestra los mensajes
   * @param type
   * @param title
   * @param detail
   */
    showMessage(type: string, title: string, detail: string) {
        this.modals.push({ type: type, text: detail, title: title });
        swal.queue(this.modals)
        setTimeout(() => {
            this.modals = []
        }, 500);
    }

    showMessageWarn(detail: string) {
        this.modals.push({ type: 'warning', html: detail });
        swal.queue(this.modals)
        setTimeout(() => {
            this.modals = []
        }, 500);
    }

    showMessageError(detail: string) {
        this.modals.push({ type: 'error', html: detail, title: 'Error' });
        swal.queue(this.modals)
        setTimeout(() => {
            this.modals = []
        }, 500);
    }

    showMessageInfo(detail: string) {
        this.modals.push({ type: 'success', html: detail, title: 'Correcto' });
        swal.queue(this.modals)
        setTimeout(() => {
            this.modals = []
        }, 500);
    }

    loading() {
        setTimeout(() => this.isLoading = true, 0);
    }

    noLoading() {
        setTimeout(() => this.isLoading = false, 0);
    }

    info(msg: string, title?: string) {
        this.showMessageInfo(msg);
    }

    success(msg: string, title?: string) {
        this.showMessageInfo(msg);
    }

    error(msg: string, title?: string) {
        this.showMessageError(msg);
    }

    warning(msg: string, title?: string) {
        this.showMessageWarn(msg);
    }

    confirmDelete(fn) {
        swal.fire({
            title: 'Est\u00E1 seguro de borrar el registro?',
            text: "",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No',
            confirmButtonText: 'Si'
          }).then(fn)
    }

    confirmMsg(fn, message) {
        swal.fire({
            title: message,
            text: "",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'No',
            confirmButtonText: 'Si'
          }).then(fn)
    }

}