import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';


@Injectable()
export class ApplicationUtil {

	//public URL_SERVICE: string = "http://localhost:8086/formulario.validador.back/api/";
	public URL_SERVICE: string = "http://eap7-gsisin.pruebas.intracoomeva.com.co/formulario.validador.back/api/";
	public CRYPTO_KEY = 'Q2FqYUhlcnJhbWllbnRhcw==';


	encrypt(value) {
		var key = CryptoJS.enc.Utf8.parse(this.CRYPTO_KEY);
		var iv = CryptoJS.enc.Utf8.parse(this.CRYPTO_KEY);  
		var encrypted = CryptoJS.AES.encrypt(value, key,
			{
				keySize: 128/8,
				iv: iv,
				mode: CryptoJS.mode.CBC,
				padding: CryptoJS.pad.Pkcs7
			});
			;
		return encrypted.toString();
	}

	decrypt(value) {
		var key = CryptoJS.enc.Utf8.parse(this.CRYPTO_KEY);
		var iv = CryptoJS.enc.Utf8.parse(this.CRYPTO_KEY);
		var decrypted = CryptoJS.AES.decrypt(value, key, {
			keySize: 128/8,
			iv: iv,
			mode: CryptoJS.mode.CBC,
			padding: CryptoJS.pad.Pkcs7
		});

		return (decrypted.toString(CryptoJS.enc.Utf8));
	}

	deepCopy(obj) {
        var copy;

        // Handle the 3 simple types, and null or undefined
        if (null == obj || "object" != typeof obj) return obj;

        // Handle Date
        if (obj instanceof Date) {
            copy = new Date();
            copy.setTime(obj.getTime());
            return copy;
        }

        // Handle Array
        if (obj instanceof Array) {
            copy = [];
            for (var i = 0, len = obj.length; i < len; i++) {
                copy[i] = this.deepCopy(obj[i]);
            }
            return copy;
        }

        // Handle Object
        if (obj instanceof Object) {
            copy = {};
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = this.deepCopy(obj[attr]);
            }
            return copy;
        }

        throw new Error("Unable to copy obj! Its type isn't supported.");
    }

	validarURL(url) {
        var valid = /^(ftp|http|https):\/\/[^ "]+$/.test(url);
        return valid;
    }

	isNumber(value: string | number): boolean {
		return ((value != null) && !isNaN(Number(value.toString())));
	}

	convertDate(milliseconds) {

		if (milliseconds == null || milliseconds == '') {
			return '';
		}

		let date = new Date(milliseconds);
		let yy = date.getFullYear();
		let d = '' + date.getDate();
		let mm = '' + (date.getMonth() + 1);
		let h = date.getHours();
		let m = date.getMinutes();
		let s = date.getSeconds();

		if (date.getDate() < 10) {
			d = '0' + d;
		}
		if ((date.getMonth() + 1) < 10) {
			mm = '0' + mm;
		}

		return (d + '/' + mm + '/' + yy + " " + h + ":" + m + ':' + s);
	}

	getDateFromMiliseconds(milliseconds) {

		if (milliseconds == null || milliseconds == '') {
			return '';
		}

		let date = new Date(milliseconds);
		return date;
	}

	convertDateFormat(milliseconds) {

		if (milliseconds == null || milliseconds == '') {
			return '';
		}

		let date = new Date(milliseconds);
		let yy = date.getFullYear();
		let d = '' + date.getDate();
		let mm = '' + (date.getMonth() + 1);
		let h = date.getHours();
		let m = date.getMinutes();
		let s = date.getSeconds();

		if (date.getDate() < 10) {
			d = '0' + d;
		}
		if ((date.getMonth() + 1) < 10) {
			mm = '0' + mm;
		}

		return (d + '/' + mm + '/' + yy);
	}

	formatCurrencyText(val: string) {
		//Check if arrow keys are pressed - we want to allow navigation around textbox using arrow keys		
		val = val.toString();
		let value = '';
		val = val.replace(/,/g, "");
		value = "";
		val += '';
		let x = val.split('.');
		let x1 = x[0];
		let x2 = x.length > 1 ? '.' + x[1] : '';

		var rgx = /(\d+)(\d{3})/;

		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}

		value = x1 + x2;
		return value;
	}

	optionValidRole(menu, option) {
		let isMenuValid = false;
		for (let i = 0; i < menu.length; i++) {
			let m = menu[i];

			if (m.name == option) {
				isMenuValid = true;
				break;
			}
		}

		return isMenuValid;
	}

	/**
	* Mientras se escribe se valida que el valor ingresado
	* sea un número y corresponda un todos esos valores a longitud establecida en el REGEX
	* @param event
	*/
	onKeyTelefono(event: any) {
		var p = event.target.value + event.key;
		var s = p.match(/^[0-9]{0,15}$/) != null;
		if (!s && event.keyCode !== 8 && event.keyCode !== 69) {
			return false;
		}
	}

	/**
  * Mientras se escribe se valida que el valor ingresado
  * sea un número y corresponda un todos esos valores a longitud establecida en el REGEX
  * @param event
  */
	onKeyLengthCelular(event: any) {
		var p = event.target.value + event.key;
		var s = p.match(/^[0-9]{0,10}$/) != null;
		if (!s && event.keyCode !== 8 && event.keyCode !== 69) {
			return false;
		}
	}
}