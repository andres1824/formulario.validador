import { Injectable } from '@angular/core';
import { MatDateFormats, NativeDateAdapter } from '@angular/material/core';

@Injectable()
export class AppDateAdapter extends NativeDateAdapter {

    
    parse(value: any): Date | null {
        if(value == '') {
            return null;
        }
        if ((typeof value === 'string') && (value.indexOf('/') > -1)) {
            const str = value.split('/');
            const year = Number(str[2]);
            const month = Number(str[1]) - 1;
            const date = Number(str[0]);
            return new Date(year, month, date);
        } else if(value.indexOf('/') < 0){
            const year = Number(value.substring(4));
            const month = Number(value.substring(2, 4)) - 1;
            const date = Number(value.substring(0, 2));
            console.debug(date + ' ' + month + ' ' + year);
            return new Date(year, month, date);
        }
        const timestamp = typeof value === 'number' ? value : Date.parse(value);
        return isNaN(timestamp) ? null : new Date(timestamp);
    }

    format(date: Date, displayFormat: Object): string {
        if (displayFormat === 'input') {
            const day = date.getDate();
            const month = date.getMonth() + 1;
            const year = date.getFullYear();
            return this._to2digit(day) + '/' + this._to2digit(month) + '/' + year;
            // return year  + '/' + this._to2digit(month) + '/' + this._to2digit(day) ;
        } else {    
            return date.toLocaleDateString('es-ES', {
                weekday: "short",
                year: "numeric",
                month: "short",
                day: "numeric"
            });
        }
    }

    private _to2digit(n: number) {
        return ('00' + n).slice(-2);
    }
}

export const APP_DATE_FORMATS =
{
    parse: {
        dateInput: { month: 'short', year: 'numeric', day: 'numeric' }
    },
    display: {
        dateInput: 'input',
        monthYearLabel: { year: 'numeric', month: 'long' },
        dateA11yLabel: { year: 'numeric', month: 'long', day: 'long' },
        monthYearA11yLabel: { year: 'numeric', month: 'long' },
        
    }
}