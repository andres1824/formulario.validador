import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../services/http.service.module';
import { Modals } from '../shared/modals';
import { StorageManager } from '../services/storageManager';
import { ApplicationUtil } from '../shared/applicationUtil';
import { DomSanitizer } from '@angular/platform-browser';
import { routesNames } from '../shared/routes.name';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { FileUploader } from 'ng2-file-upload';
import { ResponseContentType } from '@angular/http';
import * as FileSaver from 'file-saver';
import { FormularioEmmit } from '../formulario/formulario.emmit';

@Component({
    selector: 'selectM-formulario',
    templateUrl: './selectM.formulario.component.html'
})
export class SelectMFormularioComponent implements OnInit {
    @Input() campo: any;
    @Input() required: any;
    @Input() style: any;
    @Output() event = new EventEmitter<string>();
    public respuestaCampoList: any[] = [];


    constructor(private http: HttpService, private router: Router, private modals: Modals,
        private storage: StorageManager, private appCons: ApplicationUtil, private route: ActivatedRoute,
        public sanitizer: DomSanitizer,
        private formularioEmmit: FormularioEmmit) {
    }

    ngOnInit() {
        this.getRespuestaCampoAll();
    }

    sendItem(campo: any) {
        this.event.emit(campo);
    }

    getRespuestaCampoAll() {
        let URL = this.appCons.URL_SERVICE + 'campo/respuestaCampo/get/all';
        let data = { fvRespuesta: null, campoId: this.campo.campoId }
        this.http.postOauth(URL, data).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }

                if (response.data != null && response.data.length > 0) {
                    this.respuestaCampoList = (response.data);
                }
            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    isObligatorio() {
        if(this.campo.obligatorio == 'SI') {
            return "*";
        }
    }
}