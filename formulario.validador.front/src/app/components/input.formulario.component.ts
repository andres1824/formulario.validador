import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../services/http.service.module';
import { Modals } from '../shared/modals';
import { StorageManager } from '../services/storageManager';
import { ApplicationUtil } from '../shared/applicationUtil';
import { DomSanitizer } from '@angular/platform-browser';
import { routesNames } from '../shared/routes.name';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { FileUploader } from 'ng2-file-upload';
import { ResponseContentType } from '@angular/http';
import * as FileSaver from 'file-saver';
import { FormularioEmmit } from '../formulario/formulario.emmit';

@Component({
    selector: 'input-formulario',
    templateUrl: './input.formulario.component.html'
})
export class InputFormularioComponent implements OnInit {
    @Input() campo: any;
    @Input() required: any;
    @Input() style: any;
    @Output() event = new EventEmitter<string>();


    constructor(private http: HttpService, private router: Router, private modals: Modals,
        private storage: StorageManager, private appCons: ApplicationUtil, private route: ActivatedRoute,
        public sanitizer: DomSanitizer,
        private formularioEmmit: FormularioEmmit) {
    }

    ngOnInit() {

    }

    sendItem(campo: any) {
        this.event.emit(campo);
    }

    isObligatorio() {
        if(this.campo.obligatorio == 'SI') {
            return "*";
        }
    }
}