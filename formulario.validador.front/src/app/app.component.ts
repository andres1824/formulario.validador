import { Component, OnInit, Input } from '@angular/core';
import { Modals } from './shared/modals';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { DateAdapter } from '@angular/material/core';

export interface DialogData {
  movie: any;
  overview: string;
  poster_path: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
animations: [
  trigger('fadeOut', [
    transition(':enter', [
      style({ opacity: '1' }),
      animate('300ms ease-in', style({ opacity: '0' }))
    ]),
    transition(':leave', [
      style({ opacity: '0' }),
      animate('300ms ease-in', style({ opacity: '1' }))
    ])
  ]),
  trigger('routerTransition', [
    state('void', style({ position: 'absolute', top: '0', left: '0', right: '0', bottom: '0', zIndex: '3000', width: '100%' })),
    state('*', style({ position: 'absolute', top: '0', left: '0', right: '0', bottom: '0', zIndex: '3000', width: '100%' })),
    transition(':enter', [
      style({ transform: 'translateX(100%)' }),
      animate('.8s ease-in-out', style({ transform: 'translateX(0%)' }))
    ]),
    transition(':leave', [
      style({ transform: 'translateX(0%)' }),
      animate('.8s ease-in-out', style({ transform: 'translateX(-100%)' }))
    ])
  ])
]
})
export class AppComponent implements OnInit {

  constructor(public modals: Modals, private dateAdapter: DateAdapter<any>) {
    //this.dateAdapter.setLocale('fr');
  }

  ngOnInit() {
  }

}