import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, RequestOptionsArgs } from "@angular/http";
import { Router } from '@angular/router';
import { Modals } from '../shared/modals';
import { ApplicationUtil } from '../shared/applicationUtil';
import { StorageManager } from '../services/storageManager';

import "rxjs/add/operator/toPromise";
import { throwError } from 'rxjs';

@Injectable()
export class HttpService {
    public accessToken: string;

    constructor(private http: Http,
        public router: Router,
        private modals: Modals,
        private appConst: ApplicationUtil,
        private storage: StorageManager
    ) {

    }

    postLogin(url: string, parameters?: Object, headers?: Headers) {
        this.modals.loading();
        let options = new RequestOptions({ headers: headers });

        return this.http.post(url, parameters, options)
            .toPromise().then(response => {
                this.modals.noLoading(); return response.json();
            }).catch((err: Response) => {
                this.modals.noLoading();
                if (err.status == 401) {
                    this.router.navigate(['/login']);
                    this.modals.error("Usuario/Clave incorrecta", "Cerrar");
                } else {
                    this.modals.error('Error en el servidor');
                    return throwError(err.text);
                }
            });
    }

    post(url: string, parameters?: Object, headers?: Headers) {
        this.modals.loading();
        if (headers) {
            let options = new RequestOptions({ headers: headers });

            return this.http.post(url, parameters, options)
                .toPromise().then(response => {
                    this.modals.noLoading(); return response.json();
                }).catch((err: Response) => {
                    this.modals.noLoading();
                    this.modals.error('Error en el servidor');
                    return throwError(err.text);
                });
        }

        if (parameters) {
            return this.http.post(url, parameters)
                .toPromise().then(response => {
                    this.modals.noLoading(); return response.json();
                }).catch((err: Response) => {
                    this.modals.noLoading();
                    this.modals.error('Error en el servidor');
                    return throwError(err.text);
                });
        }

        return this.http.post(url, null)
            .toPromise().then(response => {
                this.modals.noLoading(); return response.json();
            }).catch((err: Response) => {
                this.modals.noLoading();
                this.modals.error('Error en el servidor');
                return throwError(err.text);
            });

    }

    postOauth(url: string, parameters?: Object, headers?: Headers) {
        this.modals.loading();
        //let access_token = this.storage.get('access_token');
        //url = url + '?access_token=' + access_token;
        if (headers) {
            let options = new RequestOptions({ headers: headers });

            return this.http.post(url, parameters, options)
                .toPromise().then(response => {
                    this.modals.noLoading(); return response.json();
                }).catch((err: Response) => {
                    this.modals.noLoading();
                    if (err.status == 401) {
                        this.router.navigate(['/login']);
                        this.modals.error("Session invalida", "Cerrar");
                    } else {
                        this.modals.error('Error en el servidor');
                        return throwError(err.text);
                    }
                });
        }

        if (parameters) {
            return this.http.post(url, parameters)
                .toPromise().then(response => {
                    this.modals.noLoading(); return response.json();
                }).catch((err: Response) => {
                    this.modals.noLoading();
                    if (err.status == 401) {
                        this.router.navigate(['/login']);
                        this.modals.error("Session invalida", "Cerrar");
                    } else {
                        this.modals.error('Error en el servidor');
                        return throwError(err.text);
                    }
                });
        }

        return this.http.post(url, null)
            .toPromise().then(response => {
                this.modals.noLoading(); return response.json();
            }).catch((err: Response) => {
                this.modals.noLoading();
                if (err.status == 401) {
                    this.router.navigate(['/login']);
                    this.modals.error("Session invalida", "Cerrar");
                } else {
                    this.modals.error('Error en el servidor');
                    return throwError(err.text);
                }
            });

    }

    getOauth(url: string, parameters?: string, headers?: RequestOptionsArgs) {
        //let access_token = this.storage.get('access_token');
        //url = url + '?access_token=' + access_token;
        this.modals.loading();
        if (headers) {
            if (!parameters) {
                return this.http.get(url, headers)
                    .toPromise().then(response => {
                        this.modals.noLoading(); return response.json();
                    }).catch((err: Response) => {
                        this.modals.noLoading();
                        if (err.status == 401) {
                            this.router.navigate(['/login']);
                            this.modals.error("Session invalida", "Cerrar");
                        } else {
                            this.modals.error('Error en el servidor');
                            return throwError(err.text);
                        }
                    });
            }

            return this.http.get(`${url}&${parameters}`, headers)
                .toPromise().then(response => {
                    this.modals.noLoading(); return response.json();
                }).catch((err: Response) => {
                    if (err.status == 401) {
                        this.router.navigate(['/login']);
                        this.modals.error("Session invalida", "Cerrar");
                    } else {
                        this.modals.error('Error en el servidor');
                        return throwError(err.text);
                    }
                });
        }

        if (!parameters) {
            return this.http.get(url)
                .toPromise().then(response => {
                    this.modals.noLoading(); return response.json();
                }).catch((err: Response) => {
                    if (err.status == 401) {
                        this.router.navigate(['/login']);
                        this.modals.error("Session invalida", "Cerrar");
                    } else {
                        this.modals.error('Error en el servidor');
                        return throwError(err.text);
                    }
                });
        }

        return this.http.get(`${url}&${parameters}`)
            .toPromise().then(response => {
                this.modals.noLoading(); return response.json();
            }).catch((err: Response) => {
                if (err.status == 401) {
                    this.router.navigate(['/login']);
                    this.modals.error("Session invalida", "Cerrar");
                } else {
                    this.modals.error('Error en el servidor');
                    return throwError(err.text);
                }
            });
    }

    get(url: string, parameters?: string, headers?: Headers) {

        this.modals.loading();
        if (headers) {
            if (!parameters) {
                return this.http.get(url, { headers: headers })
                    .toPromise().then(response => {
                        this.modals.noLoading(); return response.json();
                    }).catch((err: Response) => {
                        this.modals.noLoading();
                        this.modals.error('Error en el servidor');
                        return throwError(err.text);
                    });
            }

            return this.http.get(`${url}&${parameters}`, { headers: headers })
                .toPromise().then(response => {
                    this.modals.noLoading(); return response.json();
                }).catch((err: Response) => {
                    this.modals.noLoading();
                    this.modals.error('Error en el servidor');
                    return throwError(err.text);
                });
        }

        if (!parameters) {
            return this.http.get(url)
                .toPromise().then(response => {
                    this.modals.noLoading(); return response.json();
                }).catch((err: Response) => {
                    this.modals.noLoading();
                    this.modals.error('Error en el servidor');
                    return throwError(err.text);
                });
        }

        return this.http.get(`${url}&${parameters}`)
            .toPromise().then(response => {
                this.modals.noLoading(); return response.json();
            }).catch((err: Response) => {
                this.modals.noLoading();
                this.modals.error('Error en el servidor');
                return throwError(err.text);
            });
    }

    getPath(url: string, parameters?: string, headers?: Headers) {
        this.modals.loading();
        if (headers) {
            if (!parameters) {
                return this.http.get(url, { headers: headers })
                    .toPromise().then(response => {
                        this.modals.noLoading(); return response.json();
                    }).catch((err: Response) => {
                        this.modals.noLoading();
                        this.modals.error('Error en el servidor');
                    });
            }

            return this.http.get(`${url}/${parameters}`, { headers: headers })
                .toPromise().then(response => {
                    this.modals.noLoading(); return response.json();
                }).catch((err: Response) => {
                    this.modals.noLoading();
                    this.modals.error('Error en el servidor');
                });
        }

        if (!parameters) {
            return this.http.get(url)
                .toPromise().then(response => {
                    this.modals.noLoading(); return response.json();
                }).catch((err: Response) => {
                    this.modals.noLoading();
                    this.modals.error('Error en el servidor');
                });
        }

        return this.http.get(`${url}/${parameters}`)
            .toPromise().then(response => {
                this.modals.noLoading(); return response.json();
            }).catch((err: Response) => {
                this.modals.noLoading();
                this.modals.error('Error en el servidor');
            });
    }
}