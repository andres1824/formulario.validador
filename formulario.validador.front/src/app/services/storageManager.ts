import {Injectable} from '@angular/core'

@Injectable()
export class StorageManager {

    private tokenKey: string = 'access_token';

    public set(tokenKey: string, content: Object) {
        localStorage.setItem(tokenKey, JSON.stringify(content));
    }

    public get(tokenKey: string) {
        let storedToken = JSON.parse(localStorage.getItem(tokenKey));
        if (!storedToken) throw 'no se encuentra llave de session ' + tokenKey;
        return storedToken;
    }

    public exists(tokenKey: string) {
        let storedToken = JSON.parse(localStorage.getItem(tokenKey));
        if (!storedToken) return false;
        return true;
    }
    
    public setTokenAccess(content: Object) {
        localStorage.setItem(this.tokenKey, JSON.stringify(content));
    }

    public getTokenAccess() {
        let storedToken = JSON.parse(localStorage.getItem(this.tokenKey));
        if (!storedToken) throw 'no se encuentra access token';
        return storedToken;
    }
}