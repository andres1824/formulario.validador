import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../services/http.service.module';
import { Modals } from '../shared/modals';
import { StorageManager } from '../services/storageManager';
import { ApplicationUtil } from '../shared/applicationUtil';
import { Http, Headers, Response, RequestOptions } from "@angular/http";
import 'url-search-params-polyfill';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public usuario: string;
  public password: string;
  public empresa: string;
  public empresaList: any[] = [];

  constructor(private http: HttpService, private router: Router, private modals: Modals,
    private appCons: ApplicationUtil,
    private storage: StorageManager, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getEmpresaAll();
  }

  public login() {

    let isValid = true;
    let error = '<div style="text-align: left;">';
    if (this.usuario == '' || this.usuario == null || this.password == '' || this.password == null) {
      error +=('Por favor ingresar usuario y contrase\u00F1a <br>');
      isValid = false;
    }

    if (this.empresa == '' || this.empresa == null) {
      error +=('Debe seleccionar la empresa <br>');
      isValid = false;
    }

    /*this.storage.set('authorized', "true");
    this.storage.set('user', {});
    this.storage.set('empresa', { empresaId: '1', nombre: 'empresa' });
    this.router.navigate(['/main']);*/
    if (isValid) {
      this.storage.set('authorized', "false");
      let URL = this.appCons.URL_SERVICE + "login/access";
      var data = { 'login': this.usuario, 'password': this.password, companyId: this.empresa, company: this.getNombreEmpresa(this.empresa) };

          this.http.postOauth(URL, data).
            then((response) => {
              if (response.status == 'FAILURE') {
                this.modals.showMessageError(response.message);
                return;
              }
              this.storage.set('authorized', "true");
              this.storage.set('user', (response.data.userInfo));
              this.storage.set('empresa', { empresaId: this.empresa, nombre: this.getNombreEmpresa(this.empresa) });
              this.router.navigate(['/main']);

            }, (fail) => {
              this.modals.showMessageError('Error en el servicio ' + fail);
            })
    } else {
      this.modals.error(error);
    }
  }

  getEmpresaAll() {
    
    let URL = this.appCons.URL_SERVICE + 'empresa/get/all';
    this.http.get(URL).
      then((response) => {
        if (response.status == 'FAILURE') {
          this.modals.error(response.message);
          return;
        }

        this.empresaList = response.data;
      })
  }

  getNombreEmpresa(empresaId) {
    for(let i = 0;i < this.empresaList.length;i ++) {
      if(this.empresaList[i].empresaId == empresaId) {
        return this.empresaList[i].nombre;
      }
    }
    return '';
  }
}
