import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { StorageManager } from '../services/storageManager';
import { DomSanitizer } from '@angular/platform-browser';
import { Modals } from '../shared/modals';
import * as $ from 'jquery';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  animations: [
    trigger('fadeOut', [
      transition(':enter', [
        style({ opacity: '1' }),
        animate('300ms ease-in', style({ opacity: '0' }))
      ]),
      transition(':leave', [
        style({ opacity: '0' }),
        animate('300ms ease-in', style({ opacity: '1' }))
      ])
    ]),
    trigger('routerTransition', [
      state('void', style({ position: 'absolute', top: '0', left: '0', right: '0', bottom: '0', zIndex: '3000', width: '100%' })),
      state('*', style({ position: 'absolute', top: '0', left: '0', right: '0', bottom: '0', zIndex: '3000', width: '100%' })),
      transition(':enter', [
        style({ transform: 'translateX(100%)' }),
        animate('.8s ease-in-out', style({ transform: 'translateX(0%)' }))
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0%)' }),
        animate('.8s ease-in-out', style({ transform: 'translateX(-100%)' }))
      ])
    ])
  ]
})
export class MainComponent implements OnInit {
  public menu: any = 2;
  public roles: any;
  public user: any;
  public empresa: any;
  width: any;
  classSidebar: string;
  title = 'formulario';
  statusSidebar: boolean;
  toggle: boolean = false;
  toggleClass: string;
  inicio: boolean;

  constructor(private router: Router, private route: ActivatedRoute,
    private storage: StorageManager, public sanitizer: DomSanitizer, private modals: Modals) { }

  ngOnInit() {
    if (this.router.url === '/main') {
      this.inicio = true;
    } else {

      this.inicio = false;
    }
    let authorized = this.storage.get('authorized');
    if (authorized == 'false') {
      this.modals.error('Usuario no autorizado');
      this.router.navigate(['/401']);
    }

    this.menu = (this.buildMenu());
    this.user = (this.storage.get('user'));
    this.empresa = (this.storage.get('empresa'));


    var css  = document.getElementById('customCssCompany');

    if(css != null || css != undefined) {
      css.remove();
    }

    /**
   * Método JQUERY para expandir o retraer el menubar
   */
    $(window).resize(() => {
      this.width = $(window).width();
      if (this.width < 768) {
        this.toggleResponsive(true);
      } else {
        this.toggleResponsive(false);
      }
    });

  }

  buildMenu() {
      let menu = [];
      let parametrizacion = {name: 'parametrizacion', description: 'Parametrizaci\u00F3n', 
      module: 'Parametrizaci\u00F3n', icon: 'fa fa-fw fa-wrench', actions: []};
      let formulario = {name: 'formulario', description: 'Formulario', 
      module: 'Formulario', icon: 'fa fa-fw fa-wrench', actions: []};
      let parametro = {name: 'parametro', description: 'Param\u00E9tros', icon: 'fa fa-fw fa-wrench'};
      let empresa = {name: 'empresa', description: 'Empresa', icon: 'fa fa-fw fa-wrench'};
      let causal = {name: 'causal_descarte', description: 'Causal Descarte', icon: 'fa fa-fw fa-wrench'};
      let respuesta = {name: 'respuesta', description: 'Opci\u00F3n Respuesta', icon: 'fa fa-fw fa-wrench'};
      let tipoCampo = {name: 'tipo_campo', description: 'Tipo Campo', icon: 'fa fa-fw fa-wrench'};
      let tipoReporte = {name: 'tipo_reporte', description: 'Tipo Reporte', icon: 'fa fa-fw fa-wrench'};
      let tipografia = {name: 'tipografia', description: 'Tipograf\u00EDa', icon: 'fa fa-fw fa-wrench'};
      let color = {name: 'color', description: 'Color', icon: 'fa fa-fw fa-wrench'};
      let campo = {name: 'campo', description: 'Campo', icon: 'fa fa-fw fa-wrench'};
      let agrupacion = {name: 'agrupacion', description: 'Agrupaci\u00F3n', icon: 'fa fa-fw fa-wrench'};
      let formularioGestion = {name: 'formulario', description: 'Gestionar Formulario', icon: 'fa fa-fw fa-wrench'};
      let formularioValidador = {name: 'formulario_validador', description: 'Validaci\u00F3n Formulario', icon: 'fa fa-fw fa-wrench'};
      parametrizacion.actions.push(parametro);
      parametrizacion.actions.push(empresa);
      parametrizacion.actions.push(causal);
      parametrizacion.actions.push(respuesta);
      parametrizacion.actions.push(tipoCampo);
      parametrizacion.actions.push(tipoReporte);
      parametrizacion.actions.push(tipografia);
      parametrizacion.actions.push(color);
      
      formulario.actions.push(campo);
      formulario.actions.push(agrupacion);
      formulario.actions.push(formularioGestion);
      formulario.actions.push(formularioValidador);
      
      menu.push(parametrizacion);
      menu.push(formulario);
      return menu;
  }


  logout() {
    this.storage.set('authorized', "false");
    this.storage.set('roles', "");
    this.storage.set('sections', "");
    this.storage.set('access_token', "");
    this.storage.set('expires_in', "");
    this.storage.set('refresh_token', "");
    this.storage.set('campanaEmpresa', '');
    this.storage.set('loginSilencioso', "");
    this.router.navigate(['/login']);
  }

  /**
   * Muestra o oculta el sidebar de la aplicación
   */
  toggleSidebar() {
    this.statusSidebar = !this.statusSidebar;
    if (this.statusSidebar) {
      this.classSidebar = 'hideSidebar';
    } else {
      this.classSidebar = 'hideSidebar';
    }
  }

  /**
   * Muestra o oculta el sidebar de la aplicación responsivamente
   */
  toggleResponsive(status: boolean) {
    if (status) {
      this.classSidebar = 'hideSidebar';
    } else {
      this.classSidebar = 'hideSidebar';
    }
  }

  /**
   *Muestra la lista en el menú
   */
  showList(module) {
    console.log(module);
    if (!this.toggle) {
      this.toggle = true;
      this.toggleClass = 'rotateArrow' + module;
      console.log(this.toggle, this.toggleClass);
      $('.list-unstyled' + module).slideDown();
    } else {
      this.toggle = false;
      this.toggleClass = '';
      console.log(this.toggle, this.toggleClass);
      $('.list-unstyled' + module).slideUp();
    }
  }

}
