import { Injectable, EventEmitter } from '@angular/core';
// @Injectable makes to available services in root level.
@Injectable({ providedIn: 'root' })
export class FormularioEmmit {
    dialog = new EventEmitter();
    login = new EventEmitter();
    form = new EventEmitter();
    recidencial = new EventEmitter();
    comercial = new EventEmitter();
    financiera = new EventEmitter();
    reporte = new EventEmitter();
}