import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../services/http.service.module';
import { Modals } from '../shared/modals';
import { StorageManager } from '../services/storageManager';
import { ApplicationUtil } from '../shared/applicationUtil';
import { DomSanitizer } from '@angular/platform-browser';
import { routesNames } from '../shared/routes.name';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { FileUploader } from 'ng2-file-upload';
import { ResponseContentType } from '@angular/http';
import * as FileSaver from 'file-saver';
import * as $ from 'jquery';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
    selector: 'app-view-formulario',
    templateUrl: './view.formulario.component.html',
    styleUrls: ['./view.formulario.component.css'],
    animations: [
        trigger('fadeOut', [
          transition(':enter', [
            style({ opacity: '1' }),
            animate('300ms ease-in', style({ opacity: '0' }))
          ]),
          transition(':leave', [
            style({ opacity: '0' }),
            animate('300ms ease-in', style({ opacity: '1' }))
          ])
        ]),
        trigger('routerTransition', [
          state('void', style({ position: 'absolute', top: '0', left: '0', right: '0', bottom: '0', zIndex: '3000', width: '100%' })),
          state('*', style({ position: 'absolute', top: '0', left: '0', right: '0', bottom: '0', zIndex: '3000', width: '100%' })),
          transition(':enter', [
            style({ transform: 'translateX(100%)' }),
            animate('.8s ease-in-out', style({ transform: 'translateX(0%)' }))
          ]),
          transition(':leave', [
            style({ transform: 'translateX(0%)' }),
            animate('.8s ease-in-out', style({ transform: 'translateX(-100%)' }))
          ])
        ])
      ]
    })
export class ViewFormularioComponent implements OnInit {


    empresa = this.storage.get('empresa');
    formulario: any = {
        formularioId: '',
        nombre: '',
        descripcion: '',
        fechaVigencia: '',
        banner: '',
        frecuencia: '',
        generaAsociados: '',
        generaNoAsociados: '',
        generaReporteAuto: '',
        horario: '',
        indCssExterno: '',
        urlCssExterno: '',
        rutaReporte: '',
        fvColor: { colorId: '' },
        fvTipografia: { tipografiaId: '' },
        fvTipoReporte: { tipoReporteId: '' },
        fvEmpresa: { empresaId: this.empresa.empresaId },
        estado: '',
        usuarioCreacion: '',
        usuarioModificacion: ''
    };

    formularioCopy: any = {
        formularioId: '',
        nombre: '',
        descripcion: '',
        fechaVigencia: '',
        banner: '',
        frecuencia: '',
        generaAsociados: '',
        generaNoAsociados: '',
        generaReporteAuto: '',
        horario: '',
        indCssExterno: '',
        urlCssExterno: '',
        rutaReporte: '',
        fvColor: { colorId: '' },
        fvTipografia: { tipografiaId: '' },
        fvTipoReporte: { tipoReporteId: '' },
        fvEmpresa: { empresaId: this.empresa.empresaId },
        estado: '',
        usuarioCreacion: '',
        usuarioModificacion: ''
    };
    banerVisible: any = 'height:250px; width:900px';
    formularioAgrCamposList: any[] = [];
    formularioAgrCamposListCopy: any[] = [];

    formGeneral = new FormGroup({
        nombre: new FormControl('', [Validators.required]),
        descripcion: new FormControl(''),
        fechaVigencia: new FormControl('', [Validators.required]),
        banner: new FormControl(''),
        frecuencia: new FormControl(''),
        generaAsociados: new FormControl(''),
        generaNoAsociados: new FormControl(''),
        generaReporteAuto: new FormControl(''),
        horario: new FormControl(''),
        indCssExterno: new FormControl('', [Validators.required]),
        urlCssExterno: new FormControl(''),
        rutaReporte: new FormControl(''),
        fvColor: new FormControl(''),
        fvTipografia: new FormControl(''),
        fvTipoReporte: new FormControl(''),
        estado: new FormControl('', [Validators.required]),
    });

    agrupacionFormulario: any = {
        fvAgrupacionCampo: { agrupacionCampoId: '' },
        fvFormulario: { formularioId: '' },
        agrupacionId: ''
    };

    public formularioList: any;
    public tableHTML;
    @ViewChild('fileInput', { static: false }) fileInput: ElementRef;
    uploader: FileUploader = new FileUploader({
        isHTML5: true
    });
    constructor(private http: HttpService, private router: Router, private modals: Modals,
        private storage: StorageManager, private appCons: ApplicationUtil, private route: ActivatedRoute,
        public sanitizer: DomSanitizer) {
        let urlTree = this.router.parseUrl(router.url);
        this.formulario.formularioId = (urlTree.queryParams['id']);
        console.info(this.formulario)
    }

    ngOnInit() {
        this.getFormularioAll();//Consulta formulario por id
        this.getAgrupacionFormularioAll();//Consulta agrupacion por id

        /*var elements = document.querySelectorAll('link[rel=stylesheet][href~="styles.css"]');
        for(var i=0;i<elements.length;i++){
            elements[i].parentNode.removeChild(elements[i]);
        }*/

    }

    putItemValue($event, item) {
        item = $event;
    }

    getBanner() {
        if (this.formulario.banner != '') {
            return this.appCons.URL_SERVICE + 'formulario/download/files/' + this.formulario.formularioId;
        }

        return '';
    }

    getFormularioAll() {
        this.formulario.estado = 'Activo';
        let URL = this.appCons.URL_SERVICE + 'formulario/find';
        this.http.postOauth(URL, this.formulario).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }

                if (response.data != null && response.data.length > 0) {
                    this.formulario = (response.data[0]);
                    this.formularioCopy = this.appCons.deepCopy(response.data[0]);

                    if (this.formulario.banner == '') {
                        this.banerVisible = 'display:none'
                    }

                    if (this.formulario.urlCssExterno !== '') {
                        var head  = document.getElementsByTagName('head')[0];
                        var link  = document.createElement('link');
                        link.rel  = 'stylesheet';
                        link.id = 'customCssCompany'
                        link.type = 'text/css';
                        link.href = this.formulario.urlCssExterno;
                        link.media = 'all';
                        head.appendChild(link);
                    }

                    
                } else {
                    this.modals.showMessageWarn('No se encontraron registros para el formulario ingresado');
                }

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    
    getAgrupacionFormularioAll() {
        let URL = this.appCons.URL_SERVICE + 'formulario/agrupacion/campo/get/all/' + this.formulario.formularioId;
        this.http.getOauth(URL).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }

                if (response.data != null && response.data.length > 0) {
                    this.formularioAgrCamposList = (response.data);
                    this.formularioAgrCamposListCopy = this.appCons.deepCopy(response.data);

                }
            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    submit() {
        let uri = 'formularioValor/create';
        console.info(this.formularioAgrCamposList);
        if(this.validar()) {

            let URL = this.appCons.URL_SERVICE + uri;
            console.log("URL"+URL)
            this.http.postOauth(URL, this.formularioAgrCamposList).
                then((response) => {
                    if (response.status == 'FAILURE') {
                        this.modals.error(response.message);
                        return;
                    }
                    this.clean();
                    this.modals.info(response.message);
                })
        }
        
    }

    validar() {
        let isValid = true;
        let error = '<div style="text-align: left;">';
        for(let i = 0; i < this.formularioAgrCamposList.length;i ++) {
            let formArgCampo = this.formularioAgrCamposList[i];
            let obligatorio = formArgCampo.fvAgrupacionCampo.fvCampo.obligatorio;
            let campo = formArgCampo.fvAgrupacionCampo.fvCampo.nombre;
            let value = formArgCampo.fvAgrupacionCampo.fvCampo.value;

            if((value == '' || value == undefined) && obligatorio == 'SI') {
                isValid = false;
                error += "Debe seleccionar el campo " + campo + " <br>";
            }

        }

        if (!isValid) {
            this.modals.error(error);
        }

        return isValid;
    }

    clean() {
        this.formulario = this.appCons.deepCopy(this.formularioCopy);
        this.formularioAgrCamposList = this.appCons.deepCopy(this.formularioAgrCamposListCopy);
    }
    
    showField(form, type) {
        var typeArr = type.split(",");
        //console.debug(typeArr)
        
        for(let i = 0;i < typeArr.length; i ++) {
            let typeStr = typeArr[i];

            if(form.fvAgrupacionCampo.fvCampo.fvTipoCampo.logica == typeStr) {
                return true;
            }
        }

        

        return false;
    }

    getCustomStyle() {
        let style = '';
        let tipografia = '';
        let color = '';
        if(this.formulario != null) {
            

            if(this.formulario.fvTipografia != null) {
                tipografia = this.formulario.fvTipografia.codigoHtml;
                style += 'font-family:' + tipografia + ';';
            }

            if(this.formulario.fvColor != null) {
                color = this.formulario.fvColor.codigoHtml;
                style += 'color:' + color + ';';
            }
        }
        
        return style;
    }

    return() {
        var css  = document.getElementById('customCssCompany');
        css.remove();
        //window.location.href = window.location.origin + '/#/main/formulario';
        this.router.navigateByUrl('/main/formulario');
        //window.location.reload();
    }

}