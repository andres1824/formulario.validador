import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../services/http.service.module';
import { Modals } from '../shared/modals';
import { StorageManager } from '../services/storageManager';
import { ApplicationUtil } from '../shared/applicationUtil';
import { DomSanitizer } from '@angular/platform-browser';
import { routesNames } from '../shared/routes.name';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { FileUploader } from 'ng2-file-upload';
import { ResponseContentType } from '@angular/http';
import * as FileSaver from 'file-saver';

@Component({
    selector: 'app-formulario',
    templateUrl: './formulario.component.html'
})
export class FormularioComponent implements OnInit {


    empresa = this.storage.get('empresa');
    formulario: any = {
        nombre: '',
        descripcion: '',
        fechaVigencia: '',
        banner: '',
        frecuencia: '',
        generaAsociados: '',
        generaNoAsociados: '',
        generaReporteAuto: '',
        horario: '',
        indCssExterno: '',
        urlCssExterno: '',
        rutaReporte: '',
        fvColor: { colorId: '' },
        fvTipografia: { tipografiaId: '' },
        fvTipoReporte: { tipoReporteId: '' },
        fvEmpresa: { empresaId: this.empresa.empresaId },
        estado: '',
        usuarioCreacion: '',
        nombreArchivoReporte: '',
        usuarioModificacion: ''
    };
    estado: boolean = true;
    isEdit: boolean = false;
    disabledCssExterno: any = true;
    isDetailVisible: boolean = false;
    isDetailVisibleCol: boolean = false;
    nombreList: any[] = []
    descripcionList: any[] = []
    fechaVigenciaList: any[] = []
    bannerList: any[] = []
    frecuenciaList: any[] = []
    generaAsociadosList: any[] = []
    generaNoAsociadosList: any[] = []
    generaReporteAutoList: any[] = []
    horarioList: any[] = []
    indCssExternoList: any[] = []
    urlCssExternoList: any[] = []
    rutaReporteList: any[] = []
    fvColorList: any[] = []
    fvTipografiaList: any[] = []
    fvTipoReporteList: any[] = []
    campoList: any[] = []
    agrupacionList: any[] = []
    formularioAgrupacionCampoList: any;
    public isCampoVisible: boolean = false;
    maxDate: Date = new Date();

    formGeneral = new FormGroup({
        nombre: new FormControl('', [Validators.required]),
        descripcion: new FormControl(''),
        fechaVigencia: new FormControl('', [Validators.required]),
        banner: new FormControl(''),
        frecuencia: new FormControl(''),
        generaAsociados: new FormControl('', [Validators.required]),
        generaNoAsociados: new FormControl('', [Validators.required]),
        generaReporteAuto: new FormControl('', [Validators.required]),
        horario: new FormControl(''),
        indCssExterno: new FormControl('', [Validators.required]),
        urlCssExterno: new FormControl(''),
        rutaReporte: new FormControl(''),
        fvColor: new FormControl(''),
        fvTipografia: new FormControl(''),
        fvTipoReporte: new FormControl(''),
        nombreArchivoReporte: new FormControl(''),
        estado: new FormControl('', [Validators.required]),
    });

    agrupacionFormulario: any = {
        fvAgrupacionCampo: { agrupacionCampoId: '' },
        fvFormulario: { formularioId: '' },
        agrupacionId: ''
    };

    public formularioList: any;
    public formularioListCopy: any;
    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: false }) sort: MatSort;
    @ViewChild('paginatorFormularioAgrupacionCampo') paginatorFormularioAgrupacionCampo: MatPaginator;
    @ViewChild(MatSort, { static: false }) sortFormularioAgrupacionCampo: MatSort;
    @ViewChild('data', { static: true }) datatable: ElementRef;
    public tableHTML;
    public displayedColumns: string[] = ['formularioId',
        'nombre',
        'descripcion',
        'fechaVigencia',
        'banner',
        'frecuencia',
        'generaAsociados',
        'generaNoAsociados',
        'generaReporteAuto',
        'horario',
        'indCssExterno',
        'urlCssExterno',
        'rutaReporte',
        'nombreArchivoReporte',
        'fvColor',
        'fvTipografia',
        'fvTipoReporte',
        'estado',
        'acciones'];

    public displayedColumnsDetail: string[] = ['formularioId',
        'nombre',
        'descripcion',
        'fechaVigencia',
        'banner',
        'frecuencia',
        'generaAsociados',
        'generaNoAsociados',
        'generaReporteAuto',
        'horario',
        'indCssExterno',
        'urlCssExterno',
        'rutaReporte',
        'nombreArchivoReporte',
        'fvColor',
        'fvTipografia',
        'fvTipoReporte',
        'estado',
        'usuarioCreacion',
        'usuarioModificacion', 'fechaCreacion', 'fechaModificacion'];

    public displayedColumnsCampo: string[] = [
        'formularioId',
        'formulario',
        'agrupacion',
        'campo', 'acciones'];
    @ViewChild('fileInput', { static: false }) fileInput: ElementRef;
    uploader: FileUploader = new FileUploader({
        isHTML5: true
    });
    constructor(private http: HttpService, private router: Router, private modals: Modals,
        private storage: StorageManager, private appCons: ApplicationUtil, private route: ActivatedRoute,
        public sanitizer: DomSanitizer) {

    }

    ngOnInit() {
        this.getAll(0);
        this.getFvTipoReporteAll();
        this.getFvTipografiaAll();
        this.getFvColorAll();
        this.isEdit = false;
    }
    
    view(element) {
        let data = [];
        data[0] = element;
        this.formularioListCopy = this.formularioList;
        this.formularioList = new MatTableDataSource<any>(data);
        this.isDetailVisibleCol = true;
        this.isDetailVisible = true;
        setTimeout(() => {
            this.tableHTML = this.datatable.nativeElement.innerHTML;
        }, 200);
    }

    closeModal() {
        this.formularioList = this.formularioListCopy;
        this.isDetailVisibleCol = false;
        this.isDetailVisible = false;
    }

    getNameElement(element) {
        if (element != null) {
            if (element.nombre != undefined && element.nombre != null) {
                return element.nombre;
            }

            if (element.valor != undefined && element.valor != null) {
                return element.valor;
            }
        }
        return '';
    }

    getAll(type) {
        this.clearValidations();
        if (type == 1) {
            this.formulario.estado = this.estado == true ? 'Activo' : 'Inactivo';
        }
        let URL = this.appCons.URL_SERVICE + 'formulario/find';
        this.http.postOauth(URL, this.formulario).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }

                if (response.data != null && response.data.length > 0) {
                    this.formularioList = new MatTableDataSource<any>(response.data);
                    this.formularioList.paginator = this.paginator;
                    this.formularioList.sort = this.sort;
                } else {
                    if (type == 1) {
                        this.modals.showMessageWarn('No se encontraron registros para el filtro ingresado');
                    }
                    this.formularioList = new MatTableDataSource<any>([]);
                    this.formularioList.paginator = null;
                    this.formularioList.sort = null;
                }
                this.clearValidations();

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    getFvColorAll() {
        let URL = this.appCons.URL_SERVICE + 'color/get/all';
        this.http.getOauth(URL).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }
                this.fvColorList = [];

                if (response.data != null && response.data.length > 0) {
                    this.fvColorList = (response.data);
                }

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    getFvTipografiaAll() {
        let URL = this.appCons.URL_SERVICE + 'tipografia/get/all';
        this.http.getOauth(URL).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }
                this.fvTipografiaList = [];

                if (response.data != null && response.data.length > 0) {
                    this.fvTipografiaList = (response.data);
                }

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    getFvTipoReporteAll() {
        let URL = this.appCons.URL_SERVICE + 'tipoReporte/get/all';
        this.http.getOauth(URL).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }
                this.fvTipoReporteList = [];

                if (response.data != null && response.data.length > 0) {
                    this.fvTipoReporteList = (response.data);
                }

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    getFileName() {
        if (this.uploader.queue.length <= 0) {
            return 'Seleccione archivo';
        }

        if (this.uploader.queue.length > 0) {
            let fileItem: File = this.uploader.queue[this.uploader.queue.length - 1]._file;

            if (!this.validateFile(fileItem.name)) {
                this.modals.error('El archivo no tiene una extensi&oacute;n v&aacute;lida' +
                    ', verifique que cumpla con las siguientes extensiones (png, jpg, jpeg)');
                this.uploader.clearQueue();
            }
        }

        if (this.uploader.queue.length > 0) {
            let name = this.uploader.queue[this.uploader.queue.length - 1]._file.name;
            return name;
        }

        return 'Seleccione archivo';
    }


    submit() {
        if (this.validar()) {

            if (this.formulario.fvColor.colorId == '') {
                this.formulario.fvColor = null;
            }

            if (this.formulario.fvTipoReporte.tipoReporteId == '') {
                this.formulario.fvTipoReporte = null;
            }

            if (this.formulario.fvTipografia.tipografiaId == '') {
                this.formulario.fvTipografia = null;
            }

            this.formulario.estado = this.estado == true ? 'Activo' : 'Inactivo';
            let user = this.storage.get('user');
            let uri = 'formulario/create';

            if (!this.isEdit) {
                this.formulario.usuarioCreacion = user.userId;
            } else {
                uri = 'formulario/update';
                this.formulario.usuarioModificacion = user.userId;
            }

            let URL = this.appCons.URL_SERVICE + uri;
            this.http.postOauth(URL, this.formulario).
                then((response) => {
                    if (response.status == 'FAILURE') {
                        this.modals.error(response.message);
                        return;
                    }
                    let id = response.data.formularioId;
                    this.uploadFile(id);
                    this.modals.info(response.message);
                })
        }
    }

    edit(item) {
        window.scrollTo(0,0);
        this.isEdit = true;
        this.formulario = this.appCons.deepCopy(item);

        if(this.formulario.fvTipoReporte == null) {
            this.formulario.fvTipoReporte = {tipoReporteId: ''}
        }

        if(this.formulario.fvTipografia == null) {
            this.formulario.fvTipografia = {tipografiaId: ''}
        }

        if(this.formulario.fvColor == null) {
            this.formulario.fvColor = {colorId: ''}
        }

        
        if(item.fechaVigencia != '') {
            this.formulario.fechaVigencia = this.appCons.getDateFromMiliseconds(item.fechaVigencia);
        }

        if (this.formulario != null) {
            this.estado = this.formulario.estado == 'Activo' ? true : false;
        }
    }

    applyFilter(filter: string) {
        this.formularioList.filterPredicate = (data, filter) => JSON.stringify(data).toLocaleLowerCase().includes(filter);
        this.formularioList.filter = (filter).trim().toLocaleLowerCase();
    }

    validar() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).updateValueAndValidity();
        });
        let error = '<div style="text-align: left;">';
        let isValid = true;

        if (!this.formGeneral.valid) {
            isValid = false;
            error += "Por favor ingrese los campos marcados en rojo <br>";
        }

        let fechaActual = new Date();

        if (this.formulario.fechaVigencia < fechaActual) {
            isValid = false;
            error += "El campo fecha vigencia debe ser mayor o igual a la fecha actual <br>";
        }

        if (this.formulario.indCssExterno != 'NO' && !this.appCons.validarURL(this.formulario.urlCssExterno)) {
            isValid = false;
            error += "El campo url css externo debe ser una URL v&aacute;lida <br>";
        }

        if (this.uploader.queue.length > 0) {
            let fileItem: File = this.uploader.queue[this.uploader.queue.length - 1]._file;

            if (!this.validateFile(fileItem.name)) {
                isValid = false;
                error += 'El archivo no tiene una extensi&oacute;n v&aacute;lida' +
                    ', verifique que cumpla con las siguientes extensiones (png, jpg, jpeg)';
            }
        }


        if (!isValid) {
            this.modals.error(error);
        }

        return isValid;
    }

    downloadAdjunto(formularioId, nombre) {
        let URL = this.appCons.URL_SERVICE + 'formulario/download/files/' + formularioId;
        this.http.getOauth(URL, null,
            {
                responseType: ResponseContentType.Blob
            }).then((res) => {
                var blob = new Blob([res], {
                    type: res.type
                });
                FileSaver.saveAs(blob, nombre);
            })
    }

    uploadFile(formularioId) {
        if (this.uploader.queue.length <= 0) {
            this.clean();
            return;
        }

        let URL = this.appCons.URL_SERVICE + "formulario/upload/files";
        let data = new FormData();
        let fileItem: any = this.uploader.queue[this.uploader.queue.length - 1]._file;
        data.append('file', fileItem);

        data.append('formularioId', formularioId);

        this.http.postOauth(URL, data).
            then((response) => {
                this.uploader.clearQueue();
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }
                this.clean();
            }, (fail) => {
                this.modals.error('Error en el servicio ' + fail);
            });
    }

    validateFile(name: String) {
        var ext = name.substring(name.lastIndexOf('.') + 1);
        if (ext.toLowerCase() == 'png' || ext.toLowerCase() == 'jpg'
            || ext.toLowerCase() == 'jpeg') {
            return true;
        }
        else {
            return false;
        }
    }

    addAgrupacion(element) {
        this.agrupacionFormulario.fvFormulario.formularioId = element.formularioId;
        this.getAgrupacionAll();
        this.isCampoVisible = true;
        this.getFormularioAgrupacionCampoAll();
    }

    closeModalCampo() {
        this.isCampoVisible = false;
    }

    clearCampo() {
        this.agrupacionFormulario.fvAgrupacionCampo.agrupacionCampoId = '';
        this.agrupacionFormulario.agrupacionId = '';
    }

    getAgrupacionAll() {
        let URL = this.appCons.URL_SERVICE + '/agrupacion/get/all/';
        this.http.getOauth(URL).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }
                this.agrupacionList = [];

                if (response.data != null && response.data.length > 0) {
                    this.agrupacionList = (response.data);
                }

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    getAgrupacionCampoAll($event) {
        if (this.agrupacionFormulario.agrupacionId == '') {
            return;
        }
        let URL = this.appCons.URL_SERVICE + '/agrupacion/campo/get/all/' + this.agrupacionFormulario.agrupacionId;
        this.http.getOauth(URL).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }
                this.campoList = [];

                if (response.data != null && response.data.length > 0) {
                    this.campoList = (response.data);
                }

            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    submitCampo() {
        let isValid = true;
        let error = '<div style="text-align: left;">';

        if (this.agrupacionFormulario.agrupacionId == '') {
            isValid = false;
            error += "Debe seleccionar la agrupaci\u00F3n <br>";
        }

        if (this.agrupacionFormulario.fvAgrupacionCampo.agrupacionCampoId != '') {
            this.agrupacionFormulario.agrupacionId = '';
        }

        if (!isValid) {
            this.modals.error(error);
        }

        if (isValid) {
            if (this.agrupacionFormulario.fvAgrupacionCampo.agrupacionCampoId == '') {
                this.modals.confirmMsg((result) => {
                    if (result.isConfirmed) {
                        let uri = 'formulario/createAgrupacionCampo';
                        let URL = this.appCons.URL_SERVICE + uri;
                        this.http.postOauth(URL, this.agrupacionFormulario).
                            then((response) => {
                                if (response.status == 'FAILURE') {
                                    this.modals.error(response.message);
                                    return;
                                }
                                this.getFormularioAgrupacionCampoAll();
                                this.clearCampo();
                                this.modals.info(response.message);
                            })
                    }
                }, 'Desea agregar todos los campos de la agrupaci\u00F3n');
            } else {
                let uri = 'formulario/createAgrupacionCampo';
                let URL = this.appCons.URL_SERVICE + uri;
                this.http.postOauth(URL, this.agrupacionFormulario).
                    then((response) => {
                        if (response.status == 'FAILURE') {
                            this.modals.error(response.message);
                            return;
                        }
                        this.getFormularioAgrupacionCampoAll();
                        this.clearCampo();
                        this.modals.info(response.message);
                    })
            }



        }
    }

    getFormularioAgrupacionCampoAll() {
        let URL = this.appCons.URL_SERVICE + 'formulario/agrupacion/campo/get/all/' + this.agrupacionFormulario.fvFormulario.formularioId;
        this.http.getOauth(URL).
            then((response) => {
                if (response.status == 'FAILURE') {
                    this.modals.error(response.message);
                    return;
                }

                if (response.data != null && response.data.length > 0) {
                    this.formularioAgrupacionCampoList = new MatTableDataSource(response.data);
                    this.formularioAgrupacionCampoList.paginator = this.paginatorFormularioAgrupacionCampo;
                    this.formularioAgrupacionCampoList.sort = this.sortFormularioAgrupacionCampo;

                } else {
                    this.formularioAgrupacionCampoList = new MatTableDataSource<any>([]);
                    this.formularioAgrupacionCampoList.paginator = null;
                    this.formularioAgrupacionCampoList.sort = null;
                }
            }, (fail) => {
                console.debug('fail ')
                console.debug(fail)
            })
    }

    removeCampo(element) {
        let uri = 'formulario/deleteAgrupacionCampo';

        this.modals.confirmDelete((result) => {
            if (result.isConfirmed) {
                let URL = this.appCons.URL_SERVICE + uri;
                this.http.postOauth(URL, element).
                    then((response) => {
                        if (response.status == 'FAILURE') {
                            this.modals.error(response.message);
                            return;
                        }
                        this.getFormularioAgrupacionCampoAll();
                        this.modals.info(response.message);
                    })
            }
        });
    }

    getCssExternoChange($event) {
        if(this.formulario.indCssExterno == 'SI') {
            this.disabledCssExterno = false;
        } else {
            this.disabledCssExterno = true;
            this.formulario.urlCssExterno = '';
        }
    }

    preview(element) {
        //window.location.href = window.location.origin + '/formulario.validador.web/#/view_formulario?id=' + element.formularioId;
        //window.location.reload();
        this.router.navigateByUrl('/view_formulario?id=' + element.formularioId);
    }

    clearValidations() {
        Object.keys(this.formGeneral.controls).forEach(key => {
            this.formGeneral.get(key).clearValidators();
            this.formGeneral.get(key).updateValueAndValidity();
        });
        this.formGeneral.get('nombre').setValidators([Validators.required]);
        this.formGeneral.get('descripcion').setValidators([]);
        this.formGeneral.get('fechaVigencia').setValidators([Validators.required]);
        this.formGeneral.get('banner').setValidators([]);
        this.formGeneral.get('frecuencia').setValidators([]);
        this.formGeneral.get('generaAsociados').setValidators([Validators.required]);
        this.formGeneral.get('generaNoAsociados').setValidators([Validators.required]);
        this.formGeneral.get('generaReporteAuto').setValidators([Validators.required]);
        this.formGeneral.get('horario').setValidators([]);
        this.formGeneral.get('indCssExterno').setValidators([Validators.required]);
        this.formGeneral.get('urlCssExterno').setValidators([]);
        this.formGeneral.get('rutaReporte').setValidators([]);
        this.formGeneral.get('fvColor').setValidators([]);
        this.formGeneral.get('fvTipografia').setValidators([]);
        this.formGeneral.get('fvTipoReporte').setValidators([]);
        this.formGeneral.get('nombreArchivoReporte').setValidators([]);
        this.formGeneral.get('estado').setValidators([Validators.required]);
    }

    clean() {
        this.formulario = {
            nombre: '',
            descripcion: '',
            fechaVigencia: '',
            banner: '',
            frecuencia: '',
            generaAsociados: '',
            generaNoAsociados: '',
            generaReporteAuto: '',
            horario: '',
            indCssExterno: '',
            nombreArchivoReporte: '',
            urlCssExterno: '',
            rutaReporte: '',
            fvColor: { colorId: '' },
            fvTipografia: { tipografiaId: '' },
            fvTipoReporte: { tipoReporteId: '' },
            fvEmpresa: { empresaId: this.empresa.empresaId },
            estado: '',
            usuarioCreacion: '',
            usuarioModificacion: ''
        };
        this.formularioList = new MatTableDataSource<any>([]);
        this.formularioList.paginator = this.paginator;
        this.formularioList.sort = null;
        this.isEdit = false;
        this.estado = true;
        this.getAll(0);
    }

}