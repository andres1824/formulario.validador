import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from './material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Routing } from './routes';
import {HttpService} from './services/http.service.module';
import { Modals } from './shared/modals';
import { AppComponent } from './app.component';
import {StorageManager} from './services/storageManager';
import {ToastyModule} from 'ng2-toasty';
import {ApplicationUtil} from './shared/applicationUtil';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { ThousandsPipe } from './shared/thousandsPipe.pipe';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { NgxLoadingModule } from 'ngx-loading';
import { CurrencyMaskModule } from "ngx-currency-mask";
import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from "ngx-currency-mask/src/currency-mask.config";
import {NgbModule, NgbDateParserFormatter, NgbDatepickerI18n} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateCustomParserFormatter} from './shared/NgbDateCustomParserFormatter';
import {CustomDatepickerI18n, I18n} from './shared/CustomDatepickerI18n';
import {FileUploadModule} from 'ng2-file-upload';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { DialogModule } from 'primeng/dialog';
import { ParametroComponent } from './parametrizacion/parametro.component';
import { EmpresaComponent } from './parametrizacion/empresa.component';
import { CausalDescarteComponent } from './parametrizacion/causalDescarte.component';
import { RespuestaComponent } from './parametrizacion/respuesta.component';
import { TipoCampoComponent } from './parametrizacion/tipoCampo.component';
import { TipoDatoComponent } from './parametrizacion/tipoDato.component';
import { TipoReporteComponent } from './parametrizacion/tipoReporte.component';
import { TipografiaComponent } from './parametrizacion/tipografia.component';
import { ColorComponent } from './parametrizacion/color.component';
import { CampoComponent } from './parametrizacion/campo.component';
import { FormularioValidador } from './parametrizacion/formulario.validador.component';
import { AgrupacionComponent } from './parametrizacion/agrupacion.component';
import { FormularioComponent } from './formulario/formulario.component';
import { ViewFormularioComponent } from './formulario/view.formulario.component';
import { InputFormularioComponent } from './components/input.formulario.component';
import { SelectFormularioComponent } from './components/select.formulario.component';
import { NumberFormularioComponent } from './components/number.formulario.component';
import { DateFormularioComponent } from './components/date.formulario.component';
import { EmailFormularioComponent } from './components/email.formulario.component';
import { PhoneFormularioComponent } from './components/phone.formulario.component';
import { AppDateAdapter, APP_DATE_FORMATS } from './shared/format.datepicker';
import { ColorFormularioComponent } from './components/color.formulario.component';
import { SelectMFormularioComponent } from './components/selectM.formulario.component';
import { TextFormularioComponent } from './components/text.formulario.component';
import { HoraFormularioComponent } from './components/hora.formulario.component';


export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: "left",
  allowNegative: true,
  allowZero: true,
  decimal: ",",
  precision: 0,
  prefix: "$ ",
  suffix: "",
  thousands: "."
};

@NgModule({
  declarations: [
    AppComponent,
    ParametroComponent,
    LoginComponent,
    MainComponent,
    ThousandsPipe,
    EmpresaComponent,
    CausalDescarteComponent,
    RespuestaComponent,
    TipoCampoComponent,
    TipoDatoComponent,
    TipoReporteComponent,
    TipografiaComponent,
    ColorComponent,
    CampoComponent,
    FormularioValidador,
    AgrupacionComponent,
    FormularioComponent,
    ViewFormularioComponent,
    InputFormularioComponent,
    SelectFormularioComponent,
    NumberFormularioComponent,
    DateFormularioComponent,
    EmailFormularioComponent,
    PhoneFormularioComponent,
    ColorFormularioComponent,
    SelectMFormularioComponent,
    TextFormularioComponent,
    HoraFormularioComponent
    ],
  imports: [
    DialogModule,
    Routing,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    MaterialModule,
    BrowserAnimationsModule,
    ToastyModule.forRoot(),
    FlexLayoutModule,
    NgxLoadingModule.forRoot({}),
    CurrencyMaskModule,
    NgbModule,
    FileUploadModule
  ],
  entryComponents: [],
  providers: [HttpService, Modals, ApplicationUtil, AppComponent, I18n,
    StorageManager,
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    {provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig},
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS},
    {provide: MAT_DATE_LOCALE, useValue: 'es-CO'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }