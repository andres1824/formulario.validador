import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { ParametroComponent } from './parametrizacion/parametro.component';
import { EmpresaComponent } from './parametrizacion/empresa.component';
import { CausalDescarteComponent } from './parametrizacion/causalDescarte.component';
import { RespuestaComponent } from './parametrizacion/respuesta.component';
import { TipoCampoComponent } from './parametrizacion/tipoCampo.component';
import { TipoDatoComponent } from './parametrizacion/tipoDato.component';
import { routesNames } from './shared/routes.name';
import { TipoReporteComponent } from './parametrizacion/tipoReporte.component';
import { TipografiaComponent } from './parametrizacion/tipografia.component';
import { ColorComponent } from './parametrizacion/color.component';
import { CampoComponent } from './parametrizacion/campo.component';
import { FormularioValidador } from './parametrizacion/formulario.validador.component';
import { AgrupacionComponent } from './parametrizacion/agrupacion.component';
import { FormularioComponent } from './formulario/formulario.component';
import { ViewFormularioComponent } from './formulario/view.formulario.component';

const routes: Routes = [
    {
        path: '',
        component: LoginComponent
    },
    {
        path: 'login',
        component: LoginComponent
    }, 
    {
        path: routesNames.VIEW_FORMULARIO_OPT,
        component: ViewFormularioComponent
    },
    {
        path: 'main',
        component: MainComponent,
        children: [
            {
                path: routesNames.PARAMETRIZACION_OPT,
                component: ParametroComponent
            },
            {
                path: routesNames.EMPRESA_OPT,
                component: EmpresaComponent
            },
            {
                path: routesNames.CAUSAL_DESCARTE_OPT,
                component: CausalDescarteComponent
            },
            {
                path: routesNames.RESPUESTA_OPT,
                component: RespuestaComponent
            },
            {
                path: routesNames.TIPO_CAMPO_OPT,
                component: TipoCampoComponent
            },
            {
                path: routesNames.FORMULARIO_VALIDADOR_OPT,
                component: FormularioValidador
            },
            {
                path: routesNames.TIPO_DATO_OPT,
                component: TipoDatoComponent
            },
            {
                path: routesNames.TIPO_REPORTE_OPT,
                component: TipoReporteComponent
            },
            {
                path: routesNames.TIPOGRAFIA_OPT,
                component: TipografiaComponent
            },
            {
                path: routesNames.COLOR_OPT,
                component: ColorComponent
            },
            {
                path: routesNames.CAMPO_OPT,
                component: CampoComponent
            },
            {
                path: routesNames.AGRUPACION_OPT,
                component: AgrupacionComponent
            },{
                path: routesNames.FORMULARIO_OPT,
                component: FormularioComponent
            }
        ]
    }, {
        path: '401',
        component: LoginComponent
    }
    
];

export const Routing = RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload', relativeLinkResolution: 'legacy' });